package cn.zchfax.model.borrow;

import cn.zchfax.context.Constant;
import cn.zchfax.domain.Borrow;
import cn.zchfax.tool.interest.EndInterestCalculator;
import cn.zchfax.tool.interest.InterestCalculator;


/**
 * 流转标
 * 
 * @author fuxingxing
 * @date 2012-11-2 下午4:27:51
 * @version
 *
 * 
 *
 */
public class FlowBorrowModel extends BaseBorrowModel {
	
	private static final long serialVersionUID = 7375703874958748525L;

	private Borrow model;

	public FlowBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		this.model.setType(Constant.TYPE_FLOW);
		init();
	}

	@Override
	public double calculateInterest() {
		InterestCalculator ic= interestCalculator();
		double interest=ic.getMoneyPerMonth()*ic.getPeriod()-getModel().getFlowMoney()*getModel().getFlowCount();
		return interest;
	}

	@Override
	public InterestCalculator interestCalculator() {
		Borrow model=getModel();
		double account=model.getFlowMoney()*model.getFlowCount();
		InterestCalculator ic=interestCalculator(account);
		return ic;
	}

	@Override
	public InterestCalculator interestCalculator(double validAccount) {
		Borrow model=getModel();
		InterestCalculator ic=null;
		double apr=model.getApr()/100;
		if(model.getIsday()==1){
			int time_limit_day=model.getTimeLimitDay();
			ic =new EndInterestCalculator(validAccount,apr,time_limit_day);
		}else{
			int period=model.getTimeLimit();
			ic =new EndInterestCalculator(validAccount,apr,period,InterestCalculator.TYPE_MONTH_END);
		}
		ic.each();
		return ic;
	}
	
}

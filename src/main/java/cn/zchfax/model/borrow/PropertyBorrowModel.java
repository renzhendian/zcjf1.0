package cn.zchfax.model.borrow;

import cn.zchfax.context.Constant;
import cn.zchfax.domain.Borrow;
/**
 * 资产标或者净值标
 * 
 * @author fuxingxing
 * @date 2012-9-5 下午4:37:46
 * @version
 *
 * 
 *
 */
public class PropertyBorrowModel extends BaseBorrowModel {
	
	private Borrow model;
	
	private static final long serialVersionUID = -1490035608742973452L;

	public PropertyBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		if( model.getType() ==0){
			this.model.setType(Constant.TYPE_PROPERTY);
		}
		init();
	}
}

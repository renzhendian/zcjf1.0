package cn.zchfax.model.borrow;

import cn.zchfax.domain.Borrow;


/**
 * 秒还标
 * 
 * @author fuxingxing
 * @date 2012-9-5 下午4:35:59
 * @version
 *
 * 
 *
 */
public class SecondBorrowModel extends BaseBorrowModel {
	
	private static final long serialVersionUID = 7375703874958748525L;

	private Borrow model;

	public SecondBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		init();
	}
}

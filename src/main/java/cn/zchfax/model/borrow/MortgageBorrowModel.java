package cn.zchfax.model.borrow;

import cn.zchfax.domain.Borrow;
/**
 * 抵押标或者给力标
 * 
 * @author fuxingxing
 * @date 2012-9-5 下午5:18:32
 * @version
 *
 * 
 *
 */
public class MortgageBorrowModel extends BaseBorrowModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -965497211520156565L;
	private Borrow model;

	public MortgageBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		init();
	}
	

}

package cn.zchfax.model.borrow;

import cn.zchfax.domain.Borrow;
/**
 * 信用标
 * 
 * @author fuxingxing
 * @date 2012-9-5 下午5:18:52
 * @version
 *
 * 
 *
 */
public class CreditBorrowModel extends BaseBorrowModel {

	private static final long serialVersionUID = 6478298326297026207L;

	private Borrow model;
	

	public CreditBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		init();
	}
	
}

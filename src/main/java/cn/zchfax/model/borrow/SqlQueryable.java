package cn.zchfax.model.borrow;

import cn.zchfax.tool.Page;

public interface SqlQueryable {
	
	public String getTypeSql();
	
	public String getStatusSql();
	
	public String getOrderSql();
	
	public String getSearchParamSql();
	
	public String getLimitSql();
	
	public String getPageStr(Page p);
	
	public String getSerachStr();
	
	public String getOrderStr();
	
}

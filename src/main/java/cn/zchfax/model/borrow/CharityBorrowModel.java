package cn.zchfax.model.borrow;

import cn.zchfax.domain.Borrow;

/**
 * 慈善标
 * 
 * @author fuxingxing
 * @date 2012-9-5 下午5:18:32
 * @version
 *
 * 
 *
 */
public class CharityBorrowModel extends BaseBorrowModel{
	private static final long serialVersionUID = -965497211520156565L;
	private Borrow model;

	public CharityBorrowModel(Borrow model) {
		super(model);
		this.model=model;
		init();
	}

}

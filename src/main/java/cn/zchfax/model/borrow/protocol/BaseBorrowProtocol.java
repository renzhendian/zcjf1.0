package cn.zchfax.model.borrow.protocol;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowCollection;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.Linkage;
import cn.zchfax.domain.User;
import cn.zchfax.tool.itext.PdfHelper;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;

public class BaseBorrowProtocol extends BorrowProtocol {
	public BaseBorrowProtocol(User user, long borrow_id, long tender_id,
			int borrowType, int templateType) {
		super(user, borrow_id, tender_id, borrowType, templateType);
	}

	public BaseBorrowProtocol(User user, long borrow_id, long tender_id) {
		super(user, borrow_id, tender_id);
	}

	@Override
	protected void addPdfTable(PdfHelper pdf, Borrow b)
			throws DocumentException {
		
		float cellWidth=100;
		float[] widths = {cellWidth, cellWidth, cellWidth,cellWidth};
		PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
		table.setSpacingBefore(10f);
		table.setTotalWidth(450);
		PdfPCell cell = null;
		//row 1
		cell = new PdfPCell(new Paragraph("项目名称",pdf.getFont()));// 建立一个单元格
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph(b.getName(),pdf.getFont()));// 建立一个单元格
		cell.setColspan(8);
		table.addCell(cell);
		//row 2
		cell = new PdfPCell(new Paragraph("项目编号",pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph(b.getId()+"",pdf.getFont()));
		cell.setColspan(8);
		table.addCell(cell);
		//row 3
		cell = new PdfPCell(new Paragraph("借款本金数",pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph(b.getAccount()+"",pdf.getFont()));
		cell.setColspan(8);
		table.addCell(cell);
		//row 4
		cell = new PdfPCell(new Paragraph("年化利率",pdf.getFont()));
		table.addCell(cell);
		
		cell = new PdfPCell(new Paragraph(b.getApr()+"%",pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph("借款周期",pdf.getFont()));
		table.addCell(cell);
		if(b.getIsday()==1){
			cell = new PdfPCell(new Paragraph(b.getTimeLimitDay()+"天",pdf.getFont()));
			table.addCell(cell);
		}else{
			cell = new PdfPCell(new Paragraph(b.getTimeLimit()+"月",pdf.getFont()));
			table.addCell(cell);
		}
		//row 5
		cell = new PdfPCell(new Paragraph("还款方式",pdf.getFont()));
		table.addCell(cell);
		String style = "";
		if(b.getStyle().equals("0")){
			style = "等额本息还款";
		}else if(b.getStyle().equals("1")){
			style = "每月还息到期还本";
		}else{
			style = "一次性还款";
		}
		cell = new PdfPCell(new Paragraph(style,pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph("担保方",pdf.getFont()));
		table.addCell(cell);
		String warrant ="";
		if(b.getBorrowProperty().getWarrant()!=null){
			 warrant = b.getBorrowProperty().getWarrant().getName();
		}else{
			warrant = "/";
		}
		cell = new PdfPCell(new Paragraph(warrant+"",pdf.getFont()));
		table.addCell(cell);
		//row 6
		cell = new PdfPCell(new Paragraph("借款起始日",pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph(DateUtils.newdateStr6(DateUtils.rollDay(b.getVerifyTime(), 0))+"", pdf.getFont()));
		table.addCell(cell);
		cell = new PdfPCell(new Paragraph("借款结束日",pdf.getFont()));
		table.addCell(cell);
		if(b.getIsday()==1){
			cell = new PdfPCell(new Paragraph(DateUtils.newdateStr6(DateUtils.rollDay(b.getVerifyTime(), b.getTimeLimitDay()))+"", pdf.getFont()));
		}else{
			cell = new PdfPCell(new Paragraph(DateUtils.newdateStr6(DateUtils.rollMon(b.getVerifyTime(), b.getTimeLimit()))+"", pdf.getFont()));
		}
		table.addCell(cell);
		
		pdf.addTable(table);
	}
	@Override
	protected void addTenderPropertyPdfTable(PdfHelper pdf, Borrow b)
			throws DocumentException {
		Linkage linkage = getLinkageService().getLinkageById(NumberUtils.getInt(b.getUsetype()));
		List cellList = new ArrayList();
		List<String>[] args = new List[8];
		String repayType = borrowType(NumberUtils.getInt(b.getStyle()));  //获取还款方式
		//v1.8.0.4_u2 TGPROJECT-299 lx start	
		
		cellList.add("借款详细用途");
		String type = usetype(b.getUsetype());
		cellList.add(type);
		args[0] = cellList;
		cellList.add("借款本金数额");
		cellList.add(b.getAccount()+"元人民币");
		args[1] = cellList;
		cellList.add("借款年化利率");
		cellList.add(b.getApr()+"%/年");
		args[2]=(cellList);
		cellList.add("还款方式");
		cellList.add(repayType);
		cellList.add(repayType);
		args[3] = cellList;
		cellList.add("还款分期月数");
		cellList.add(b.getTimeLimit()+"个月");
		args[4]=cellList;
		cellList.add("还款日");
		cellList.add("自"+DateUtils.newdateStr6(DateUtils.rollDay(b.getVerifyTime(), 0))+"起，每月截止"+DateUtils.getDay(b.getVerifyTime())+"号");
		args[5] = cellList;
		cellList.add("起止日期");
		cellList.add(DateUtils.newdateStr6(DateUtils.rollDay(b.getVerifyTime(), 0))+"(借款起始日至)"+DateUtils.newdateStr6(DateUtils.rollMon(b.getVerifyTime(), b.getTimeLimit()))+"(借款结束日)");
		args[6]=(cellList);
		
		
		//v1.8.0.4_u2 TGPROJECT-299 lx end
		float cellWidth=100;
		float[] widths = {cellWidth, 350};
		PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
		table.setSpacingBefore(10f);// 设置表格上面空白宽度
		//table.setSpacingBefore(10f);
		table.setTotalWidth(450);// 设置表格的宽度
		table.setLockedWidth(false);// 设置表格的宽度固定
		PdfPCell cell = null;
		try {
			for (int i = 0; i < 1; i++) {
				for (String s : args[i]) {
					cell = new PdfPCell(new Paragraph(s, pdf.getFont()));//new PdfPCell(new Paragraph(s, pdf.getFont()));// 建立一个单元格
					table.addCell(cell);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		table.addCell(cell);
		pdf.addTable(table);
	}
	
	
	
	@Override
	protected void addTenderPlainPdfTable(PdfHelper pdf, Borrow b)
			throws DocumentException {
		List<BorrowCollection> collectionList = new ArrayList<BorrowCollection>(); 
		collectionList=getBorrowService().getCollectionList(b.getId()); 
		String repayType = borrowType(NumberUtils.getInt(b.getStyle())); 
		List<String> cellList = null;
		List<String>[] args = new ArrayList[collectionList.size() + 1];
		cellList = new ArrayList<String>();
	
		cellList.add("约定还款日");
		
		cellList.add("还款总额");
		cellList.add("还款方式");
		args[0] = cellList;
		if(getTender_id() == 0){
			for (int i = 1; i < collectionList.size() + 1; i++) {
				
				BorrowCollection bc = collectionList.get(i - 1);
				
				cellList = new ArrayList<String>();
				
				cellList.add(bc.getBorrowTender().getUser().getUsername());
				Date date=bc.getRepayTime();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
				String startTime = sdf.format(date);
				cellList.add(startTime);
				
				cellList.add(bc.getRepayAccount()+"");
				cellList.add(borrowType(b.getType()));
				args[i] = cellList;
			}
		}else{
			for (int i = 1; i < collectionList.size() + 1; i++) {
				BorrowCollection bc = collectionList.get(i - 1);
				
				cellList = new ArrayList<String>();
				if(getTender_id()==bc.getBorrowTender().getId()){
				
				Date date=bc.getRepayTime();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
				String startTime = sdf.format(date);
				cellList.add(startTime);
				
				cellList.add(bc.getRepayAccount()+"");
				cellList.add(repayType);
				}
				args[i] = cellList;
				
			}
		}
		
		
		float cellWidth=150;
		int num=3;
		float[] widths = {cellWidth, cellWidth, cellWidth};
		PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
		table.setSpacingBefore(10f);// 设置表格上面空白宽度
		table.setSpacingBefore(10f);
		table.setTotalWidth(cellWidth * num);// 设置表格的宽度
		table.setLockedWidth(true);// 设置表格的宽度固定
		PdfPCell cell = null;
		for (int i = 0; i < args.length; i++) {
			for (String s : args[i]) {
				cell = new PdfPCell(new Paragraph(s, pdf.getFont()));// 建立一个单元格
				table.addCell(cell);
			}
		}
		pdf.addTable(table);
	}
	/**
	 * 获取标的还款方式
	 * @param type
	 * @return
	 */
	private String borrowType(int type){
		switch (type) {
		case 0:
			return "月等额本息";
		case 1:
			return "无";
		case 2:
			return "一次性还款";
		case 3:
			return "月还息，到期还本";
		case 4:
			return "只还本金";
		default:
			return "方式不确定";
		}
	}
	private String usetype(String type){
		if(type.equals("244")){
			return "短期周转";
		}else if(type.equals("245")){
			return "生产经营";
		}else if(type.equals("246")){
			return "投资创业";
		}else{
			return "其他用途";
		}

	}
	 public static String zhuanhuan(Object ob){
		  String s=new DecimalFormat("#.00").format(ob);
		  s=s.replaceAll("\\.", "");//将字符串中的"."去掉
		  char d[]={ '零', '壹', '贰', '叁', '肆', 
		    '伍', '陆', '柒', '捌', '玖'}; 
		  String unit="仟佰拾兆仟佰拾亿仟佰拾万仟佰拾元角分";
		  int c=unit.length();
		  StringBuffer sb=new StringBuffer(unit);
		  for(int i=s.length()-1;i>=0;i--){
		   sb.insert(c-s.length()+i,d[s.charAt(i)-0x30]);
		  }
		  s=sb.substring(c-s.length(),c+s.length());
		  s=s.replaceAll("零[仟佰拾]", "零").
		  replaceAll("零{2,}", "零"). 
		  replaceAll("零([兆万元Ԫ])", "$1"). 
		  replaceAll("零[角分]", ""); 
		  return s; 
		 }
}

package cn.zchfax.web.action.member;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ModelDriven;

import cn.zchfax.context.ExcelType;
import cn.zchfax.context.Global;
import cn.zchfax.domain.Friend;
import cn.zchfax.domain.InviteUser;
import cn.zchfax.domain.InviteUserRebate;
import cn.zchfax.domain.SendSmsLog;
import cn.zchfax.domain.User;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.OrderFilter.OrderType;
import cn.zchfax.model.SearchFilter.Operator;
import cn.zchfax.sendPhone.PhoneUtil;
import cn.zchfax.service.FriendService;
import cn.zchfax.service.InviteUserRebateService;
import cn.zchfax.service.SendSmsLogService;
import cn.zchfax.service.UserService;
import cn.zchfax.tool.coder.BASE64Encoder;
import cn.zchfax.tool.jxl.ExcelHelper;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;
import cn.zchfax.web.action.BaseAction;

@Namespace("/member/friend")
@ParentPackage("p2p-default") 
public class FriendAction extends BaseAction implements ModelDriven<Friend>{

	private static Logger logger = Logger.getLogger(FriendAction.class);
	@Autowired
	private FriendService friendService;
	@Autowired
	private  UserService userService;
	//v1.8.0.4_u2 TGPROJECT-302  lx start
	@Autowired
	private InviteUserRebateService inviteUserRebateService;
	//v1.8.0.4_u2 TGPROJECT-302  lx end
	@Autowired
	private SendSmsLogService sendSmsLogService;
	private Friend friend=new Friend();


	@Override
	public Friend getModel() {
		return friend;
	}

	@Action(value="myfriend",results={@Result(name="success", type="ftl",location="/member/friend/myfriend.html")}) 
	public String myfriend(){
		User user=getSessionUser();
		int page=paramInt("page");	
		SearchParam param = SearchParam.getInstance().addPage(page);
		param.addParam("inviteUser", new User(user.getUserId()));
		//List<Friend> list=friendService.getFriendsRequest(user.getUserId());//输出好友列表
		PageDataList<InviteUser>	list =userService.getInvitreUserBySearchParam(param);
		setPageAttribute(list, param); 
		
		return "success";
	}

	@Action(value="showmyfriend",results={@Result(name="success",type="ftl",location="/member/friend/myfriend.html")})
	public String showmyfriend() throws Exception{
		String username=request.getParameter("username");
		User user=getSessionUser();
		List list=friendService.selFriend(user.getUserId(), username);
		request.setAttribute("list", list);
		return "success";
	}

	@Action(value="delfriend",results={@Result(name="success", type="ftl",location="/member/friend/myfriend.html")})
	public String delfriend(){//删除好友
		User user=getSessionUser();
		String username=request.getParameter("username");
		friendService.delFriend(user.getUserId(), username);
		return "success";		
	}


	@Action(value="blackfriend",results={@Result(name="success", type="ftl",location="/member/friend/black.html")})
	public String blackfriend(){//添加黑名单
		User user=getSessionUser();
		String username=request.getParameter("username");
		String ip=getRequestIp();
		friendService.addBlackFriend(user.getUserId(), username,ip);
		return black();
	}


	@Action(value="request",results={@Result(name="success", type="ftl",location="/member/friend/request.html")})
	public String request(){//添加向你提出邀请的好友
		String idstr = request.getParameter("userid");
		User user=getSessionUser();
		List<Friend> list=friendService.readdFriend(user.getUserId());//输出邀请我的好友列表
		System.out.println(list.toString());
		request.setAttribute("list", list);
		return "success";
	}

	//v1.8.0.4_u2 TGPROJECT-302  lx start
	@Action(value="inviterebate",results={@Result(name="success", type="ftl",location="/member/friend/inviterebate.html")})
	public String inviterebate(){//添加向你提出邀请的好友
		User user=getSessionUser();
		//用户名
		String username = paramString("username");	
		String dotime1 = paramString("dotime1");
		String dotime2 = paramString("dotime2");
		SearchParam param=SearchParam.getInstance().addPage(paramInt("page"));
		int borrowId=paramInt("borrowId");
		if(!StringUtils.isBlank(username)){
			param.addParam("borrowTender.user.username", Operator.LIKE, username);
		}
		if(borrowId!=0) {
			param.addParam("borrowTender.borrow.id", borrowId);
		}
		if(!StringUtils.isBlank(dotime1)){
			param.addParam("addtime", Operator.GTE, DateUtils.getDate2(dotime1+" 00:00:00"));
		}
		if(!StringUtils.isBlank(dotime2)){
			param.addParam("addtime", Operator.LTE, DateUtils.getDate2(dotime2+" 23:59:59"));
		}

		param.addParam("status", 1);
		param.addParam("inviteUser", user);
		param.addOrder(OrderType.DESC, "addtime");
		PageDataList<InviteUserRebate> list=inviteUserRebateService.getInviteUserRebateListBySearchParam(param);
		param.addParam("dotime1",dotime1);
		param.addParam("dotime2", dotime2);
		setPageAttribute(list, param);
		return "success";


	}
	//v1.8.0.4_u2 TGPROJECT-302  lx end
	@Action("checkPhone")
	public String checkPhone() throws Exception {
		String phone = request.getParameter("phone");
		
		int a =userService.getPhoneOnly(phone);
		boolean result;
		if (a >0 ){
			result = false;
		}else{
			result = true;
		}
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		// 直接输入响应的内容
		if (result) {
			out.println("1");
		} else {
			out.println("");
		}
		out.flush();
		out.close();
		return null;
	}

	@Action(value="readdfriend",results={@Result(name="success", type="ftl",location="/member/friend/myfriend.html")})
	public String readdfriend(){//重新加为好友

		String username = request.getParameter("username");
		User user=getSessionUser();
		friendService.readdFriend(user.getUserId(),username);

		return myfriend();
	}

	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 start
	@Action(value="invite",results={@Result(name="success", type="ftl",location="/member/friend/invite.html")})
	public String invite(){//邀请好友
		User user = this.getSessionUser();
		long user_id = user.getUserId();
		//  v1.8.0.4 TGPROJECT-249 lx  start
		int page=paramInt("page");
		SearchParam param = SearchParam.getInstance().addPage(page);		
		param.addParam("inviteUser", new User(user.getUserId()));
		PageDataList<InviteUser>	list =userService.getInvitreUserBySearchParam(param);
		setPageAttribute(list, param); 
		//  v1.8.0.4 TGPROJECT-249 lx  end
		String userid=user_id+"";
		BASE64Encoder encoder = new BASE64Encoder();
		String s=encoder.encode((user.getUserId()+"").getBytes());
		request.setAttribute("userid", s);
		request.setAttribute("user", user);
		return "success";
	} 
	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 end
	@Action(value="black",results={@Result(name="success", type="ftl",location="/member/friend/black.html")})
	public String black(){
		User user=getSessionUser();
		List<Friend> list=friendService.getBlackList(user.getUserId());//输出黑名单友列表
		request.setAttribute("list", list);
		return "success";	

	} 

	@Action(value="addfriend",results={@Result(name="success", type="ftl",location="/member/friend/myfriend.html")})
	public String addfriend(){
		String id=request.getParameter("friends_id");
		String friend=request.getParameter("friend");
		String content=request.getParameter("content");
		friendService.addfriend(id, friend,content);
		return myfriend();
	} 

	@Action(value="sendFreeMsg")
	public void sendFreeMsg() {
		boolean isSend = false;
		//验证码			
		checkValidImg();
		int phoneType = 1;
		String phone = paramString("phone");
		String content = paramString("content");
		String message = "";
		User user = getSessionUser();
		if (user == null) {
			throw new BussinessException("非法访问");
		}
		if (phone == null || phone.equals("")) {
			message="手机号码不能为空!";
			throw new BussinessException(message);
		} else {
			if (!StringUtils.isMobile(StringUtils.isNull(phone))) {
				message="手机号码格式不对！";
				throw new BussinessException(message);
			}
		}
		SendSmsLog sms = new SendSmsLog();
		int phoneResult = userService.getPhoneOnly(phone);//查询手机号码是否存在
		SendSmsLog smslog = sendSmsLogService.getSendSmsByPhone(phone);
		int count = 3;
		int i = 0;
		if (phoneResult > 0) {
			
			message="该手机号已经注册！";
			
			throw new BussinessException(message);
		}else{
			if( smslog == null ){
				String msg = PhoneUtil.sentPhone(phoneType, phone, content);
				if(msg.equals("短信发送成功")){
					i++;
					sms.setStatus(2);
					isSend = true;
					message="短信发送成功";
				}else{
					sms.setStatus(-1);
					isSend = false;
					message="短信发送失败";
				}
				sms.setPhone(phone);
				sms.setCount(i);
				sms.setContent(content);
				sms.setAddtime(new Date());
				sms.setAddip(this.getRequestIp());
				sendSmsLogService.addSendSms(sms);
				throw new BussinessException(message);
			}else{
				int sentCount = smslog.getCount();
				int unsentCount = count - sentCount;//手机可发送次数
				if(unsentCount <= 0){
					message="该手机号近期已被推荐！";
					isSend = false;
					throw new BussinessException(message);
				}
				String msg = PhoneUtil.sentPhone(phoneType, phone, content);
				if(msg.equals("短信发送成功") && unsentCount>0){
					sentCount++;
					smslog.setStatus(2);
					isSend = true;
					message="短信发送成功";
				}else{
					smslog.setStatus(-1);
					isSend = false;
					message="短信发送失败";
				}
				smslog.setCount(sentCount);
				smslog.setContent(content);
				smslog.setAddtime(new Date());
				smslog.setAddip(this.getRequestIp());
				sendSmsLogService.updateSendSms(smslog);
				throw new BussinessException(message);
			}
		}
		/*Map<String, String> map = new HashMap<String, String>();
		map.put("data", content);
		map.put("message", message);
		map.put("mobile_token", saveToken("mobile_token"));
		map.put("isSend", isSend+"");
		//printJson(JSON.toJSONString(map));*/
	}
}

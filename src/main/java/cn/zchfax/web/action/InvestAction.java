package cn.zchfax.web.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowDetail;
import cn.zchfax.domain.BorrowInterestRate;
import cn.zchfax.domain.BorrowRepayment;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.CreditRank;
import cn.zchfax.domain.Rule;
import cn.zchfax.domain.User;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.OrderFilter.OrderType;
import cn.zchfax.model.SearchFilter.Operator;
import cn.zchfax.model.account.InvestmentModel;
import cn.zchfax.model.account.UserAccountSummary;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.BorrowInterestRateService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.LinkageService;
import cn.zchfax.service.RuleService;
import cn.zchfax.service.UserService;
import cn.zchfax.tool.Page;
import cn.zchfax.tool.interest.EndInterestCalculator;
import cn.zchfax.tool.interest.InterestCalculator;
import cn.zchfax.tool.interest.MonthEqualCalculator;
import cn.zchfax.tool.interest.MonthInterestCalculator;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;

@Namespace("/invest")
@ParentPackage("p2p-default") 
@Result(name="success",type="ftl", location="/invest/index.html") 
public class InvestAction extends BaseAction{
	
	@Autowired 
	private BorrowService borrowService;
	@Autowired
	private UserService userService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private LinkageService linkageService;
	@Autowired
	private RuleService ruleService;
	@Autowired
	private BorrowInterestRateService borrowInterestRateService;
	
	
	/**
	 * 介绍页面
	 */
	@Action(value="introduceInvest",
			results={@Result(name="success", type="ftl",location="/invest/introduceInvest.html")
	})
	public String introduceInvest() throws Exception {
		
		double borrowSum = borrowService.getBorrowSum();//累计完成融资金额
		double repayed = borrowService.getRepayed();//已还金额
		double sumInterest = borrowService.getSumInterest();//为投资人带来预期收益
		double borrowSumInterest = borrowService.getBorrowSumInterest();//已发放收益
		request.setAttribute("borrowSum",borrowSum);
		request.setAttribute("repayed",repayed);	
		request.setAttribute("sumInterest",sumInterest);
		request.setAttribute("borrowSumInterest",borrowSumInterest);
		return "success";
	}
	
	@Action("index")
	public String index() throws Exception {
		//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  start
		Map<String, Object> newmap = new HashMap<String, Object>();
		//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  end
		int status = paramInt("status");
		int page =  paramInt("page")==0?1:paramInt("page");
		int pageNum = paramInt("pageNum")==0?Page.ROWS:paramInt("pageNum");
		//int pageNum = 2;
		SearchParam param = null;
		param = SearchParam.getInstance().addPage(page,pageNum);
		if(status==2){
			message("非法访问", "/invest/index.html?status=1");
			return MSG;
		}
		if(status==0){
			status=1;//默认查询招标中
		}
		if(status == Constant.STATUS_complete){//成功案例状态 6，7， 8
			 param.addParam("status", Operator.GTE, 6)
					.addParam("status", Operator.LTE,8);
		}else if(status == Constant.STATUS_REPAYING){//还款中 6,7
			param.addOrFilter("status", 6, 7);
		}else if(status == Constant.STATUS_ALLREPAIED){//还款成功
			param.addParam("status", 8);
		}else if(status == Constant.STATUS_REVIEW){
			param.addParam("status", 3);
		}else if(status == 11){//查询正在招标、还款中、已还款
			param.addOrFilter("status", 1,6,7,8).addOrder(OrderType.DESC,"id");
		}
		else{//招标中 status 1，且account!=accountYes
			param
				.addParam("status", status);
		}
		
		//页面搜索框，过滤条件
		String title = paramString("title");//借款标题
		String startdate = paramString("enddate1");//筹款到期日期
		String enddate = paramString("enddate2");//筹款到期日期
		String isAssignment = paramString("isAssignment");//债权转让
		String account = paramString("account");//借款金额
		String rate = paramString("rate");//年利率
		String timeLimit = paramString("timeLimit");//借款期限
		int borrowType = paramInt("biaoType");//标的类型
		int timeLimitDay = paramInt("timeLimitDay");//天标期限
		String credit = paramString("credit");
		String award = paramString("award");
		//标的类型
		
		if(!StringUtils.isBlank(isAssignment)){
			if(isAssignment.equals("1")){
				param.addParam("isAssignment",1);
			}else{
				param.addParam("isAssignment",0);
			}
		}else{
			param.addParam("isAssignment",0);
		}
		
		if(borrowType!=0){
			if(borrowType == 1){//天标
				param.addParam("isday",1);
				//天标期限
				if(timeLimitDay!=0){
					param.addParam("timeLimitDay", Operator.EQ, timeLimitDay);
				}
			}else{
				param.addParam("type", borrowType);
				//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  start
				newmap.put("biaoType", borrowType);
				//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  end
			}
		}
		//投标金额
	    //v1.8.0.4 TGPROJECT-67 搜索不出数据问题  start    
		if(!account.equals("all") &&!account.equals("accountyes")&&!StringUtils.isBlank(account)){
			String[] accountArgs = account.split("-");
			if(accountArgs.length <= 1 ){
				param.addParam("account", Operator.LTE, NumberUtils.getInt(accountArgs[0]));
			}else if(accountArgs[1].equals(Constant.SEARCH_UP)){
				param.addParam("account", Operator.GTE, NumberUtils.getInt(accountArgs[0]));
			}else if(accountArgs[1].equals(Constant.SEARCH_DOWN)){
				 param.addParam("account", Operator.LT, NumberUtils.getInt(accountArgs[0]));
			}else{
				 param.addParam("account", Operator.GTE, NumberUtils.getInt(accountArgs[0]))
				 	.addParam("account", Operator.LT, NumberUtils.getInt(accountArgs[1]));
			}
		}
		//v1.8.0.4  TGPROJECT-67 搜索不出数据问题  start
		//利率
		if(!rate.equals("all") && !StringUtils.isBlank(rate)){
			String[] rateArgs = rate.split("-");
			if(rateArgs.length <= 1 ){
				param.addParam("apr", Operator.EQ, NumberUtils.getInt(rateArgs[0]));
			}else if(rateArgs[1].equals(Constant.SEARCH_UP)){
				param.addParam("apr", Operator.GTE, NumberUtils.getInt(rateArgs[0]));
			}else if(rateArgs[1].equals(Constant.SEARCH_DOWN)){
				 param.addParam("apr", Operator.LT, NumberUtils.getInt(rateArgs[0]));
			}else{
				 param.addParam("apr", Operator.GTE, NumberUtils.getInt(rateArgs[0]))
				 	.addParam("apr", Operator.LT, NumberUtils.getInt(rateArgs[1]));
			}
		}
		//期限
		if(!timeLimit.equals("all") && !StringUtils.isBlank(timeLimit) && !timeLimit.equals("0") && borrowType !=1 && borrowType != 101){
			String[] timeLimitArgs = timeLimit.split("-");
			if(timeLimitArgs.length <= 1 ){
				param.addParam("timeLimit", Operator.EQ, NumberUtils.getInt(timeLimitArgs[0]));
			}else if(timeLimitArgs[1].equals(Constant.SEARCH_UP)){
				param.addParam("timeLimit", Operator.GTE, NumberUtils.getInt(timeLimitArgs[0]));
			}else if(timeLimitArgs[1].equals(Constant.SEARCH_DOWN)){
				 param.addParam("timeLimit", Operator.LT, NumberUtils.getInt(timeLimitArgs[0]));
			}else{
				 param.addParam("timeLimit", Operator.GTE, NumberUtils.getInt(timeLimitArgs[0]))
				 	.addParam("timeLimit", Operator.LT, NumberUtils.getInt(timeLimitArgs[1]));
			}
			param.addParam("isday",0);
			param.addParam("type",Operator.NOTEQ, 101);
		}
		//信用积分
		if(!credit.equals("all") && !StringUtils.isBlank(credit)){
			String[] creditArgs = credit.split("-");
			if(creditArgs.length <= 1 ){
				param.addParam("user.credit.value", Operator.EQ, NumberUtils.getInt(creditArgs[0]));
			}else if(creditArgs[1].equals(Constant.SEARCH_UP)){
				param.addParam("user.credit.value", Operator.GT, NumberUtils.getInt(creditArgs[0]));
			}else if(creditArgs[1].equals(Constant.SEARCH_DOWN)){
				 param.addParam("user.credit.value", Operator.LT, NumberUtils.getInt(creditArgs[0]));
			}else{
				 param.addParam("user.credit.value", Operator.GT, NumberUtils.getInt(creditArgs[0]))
				 	.addParam("user.credit.value", Operator.LTE, NumberUtils.getInt(creditArgs[1]));
			}
		}
		
		//奖励
		if(!award.equals("all") && !StringUtils.isBlank(award)){
			if(award.equals("yes")){
				param.addParam("award", Operator.GT, 0);
			}else if(award.equals("no")){
				param.addParam("award", Operator.EQ, 0);
			}
		}
		//标题
		if(!title.equals("all") && !StringUtils.isBlank(title)){
			param.addParam("name", Operator.LIKE, title);
		}
		//筹款到期日期
		if(!StringUtils.isBlank(startdate)){
			startdate+= " 00:00:00";
			param.addParam("borrowProperty.endTime", Operator.GTE, DateUtils.getDate2(startdate));
		}
		if(!StringUtils.isBlank(enddate)){
			enddate+= " 23:59:59";
			param.addParam("borrowProperty.endTime", Operator.LTE, DateUtils.getDate2(enddate));
		}
		//排序
		//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 start
		int order = paramInt("order");
		param.addOrder(order);
		//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 end
		
		PageDataList pl = borrowService.getList(param);	
		//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  start
		//newmap.put("naccount", account);
		newmap.put("status", status);
		//v1.8.0.4_u4  TGPROJECT-368  2014-07-16  qinjun  end
		setPageAttribute(pl, param,newmap);
		//招标右侧的栏目
		
		param.addParam("isAssignment", 1);
		setPageAttributeA(pl, param,newmap);
		
		SearchParam paramr = SearchParam.getInstance();
		paramr.addOrFilter("status", 6,7,8);
		paramr.addOrder(OrderType.DESC, "account");
		paramr.addPage(1, 5);
		PageDataList pr = borrowService.getList(paramr);
		request.setAttribute("rList", pr.getList());
		//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 start
		request.setAttribute("orderstr",  getOrderStr());
		//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 end
		List<Borrow> unpubliclist= borrowService.getBorrowByStatus(0);
		request.setAttribute("unpubliclist", unpubliclist);
		//fu 右侧查询信息
		
		List<InvestmentModel> tzlist =borrowService.getInvestmentList();
		request.setAttribute("tzlist", tzlist);
		
		// 总投资金额
		double borrowSum = 0.0;
		borrowSum = borrowService.getBorrowSum();
		request.setAttribute("borrowSum", borrowSum);
		// 总投资产生利息
		double borrowSumInterest = 0.0;
		borrowSumInterest = borrowService.getBorrowSumInterest();
		request.setAttribute("borrowSumInterest", borrowSumInterest);
		//fu
		return SUCCESS;
	}
	
	//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 start
	public String getOrderStr() {
		StringBuffer sb=new StringBuffer("");
		sb.append(getCurOrderStr(1,"金额"));
		sb.append(getCurOrderStr(2,"利率"));
		//sb.append(getCurOrderStr(3,"进度"));
		sb.append(getCurOrderStr(4,"信用"));
		return sb.toString();
	}
	public String getCurOrderStr(int ordertype,String text){
		String account = paramString("account");//借款金额
		String rate = paramString("rate");//年利率
		String timeLimit = paramString("timeLimit");//借款期限
		int borrowType = paramInt("biaoType");//标的类型
		int status = paramInt("status");
		int order = paramInt("order");
		int pageNum = paramInt("pageNum")==0?Page.ROWS:paramInt("pageNum");
		StringBuffer sb=new StringBuffer();
		sb.append("<span><a  class=\'searchbtn'\' href=\"?status="
				+ status + "&timeLimit="
				+ timeLimit + "&biaoType=" + borrowType + "&pageNum="
				+pageNum);
		if(Math.abs(order)==ordertype){
			if(order>0){
				sb.append("&order="+(-ordertype)+"\">");
				sb.append("<font color=\"#FF0000\">"+text+"↑</font>");
			}else{
				sb.append("&order="+(ordertype)+"\">");
				sb.append("<font color=\"#FF0000\">"+text+"↓</font>");
			}
		}else{
			sb.append("&order="+(ordertype)+"\">");
			sb.append(text);
		}
		sb.append("</a> </span>");
		return sb.toString();
	}
	//v1.8.0.4_u2 TGPROJECT-310 lx 2014-05-26 end
	
	@Action(value="detail",
			results={@Result(name="success",type="ftl",location="/invest/detail.html")},
			interceptorRefs={@InterceptorRef(value="mydefault")}
	)
	public String detail() throws Exception {
		long id=paramLong("borrowid");
		Borrow b=borrowService.getBorrow(id);
		if(b == null){
			throw new BussinessException("您查的信息不存在。");
		}
		
		User user1 = getSessionUser();
		if(user1 == null || (b.getUser().getUserId() != user1.getUserId())){
			String[] notView = Global.getValue("not_view_borrow_status").split(",");
			for (String string : notView) {
				if(string.equals(b.getStatus()+"")){
					throw new BussinessException("尚未初审或撤回标只有此标发标人才能查看");
				}
			}
		}
		if(b.getIsAssignment()==1 && (b.getStatus()==59 ||  b.getStatus()==2)){
			throw new BussinessException("已撤回的债权转让标无法查看");
		}
		int repayDay=0;
		if(b.getVerifyTime()!=null){
		 repayDay = DateUtils.getDay(b.getVerifyTime());
		}else{
		 repayDay = 0;
		}
		request.setAttribute("repayDay", repayDay);
		if(b.getStatus()==6 || b.getStatus()==7 || b.getStatus()==8){
			List<BorrowRepayment> repayList =  b.getBorrowRepayments();
			request.setAttribute("repayList", repayList);
		}
		if(b.getIsAssignment() ==1 &&  b.getStatus()!=59){
			BorrowTender tender = borrowService.getAssignMentTenderByBorrowId(id);
			request.setAttribute("oldBorrowId", tender.getBorrow().getId());
		}
		request.setAttribute("borrow", b);
		User user = userService.getUserById(b.getUser().getUserId());
		user.setCredit(user.getCredit());
		if(null!=user.getUserinfo())
		user.getUserinfo().getCity();
		request.setAttribute("user", user);
		CreditRank creditRank = userService.getUserCreditRankByJiFen(user.getCredit().getValue());
		
		//账户详情/ 借款详情  投资详情
		UserAccountSummary summary = accountService.getUserAccountSummary(user.getUserId());
		
		request.setAttribute("creditRank", creditRank);
		request.setAttribute("summary", summary);
		if(b.getIsAssignment() == 1 && b.getStatus() !=8){ //债权转让标原tender
			BorrowTender assignMentTender  = borrowService.getAssignMentTenderByBorrowId(b.getId());
			request.setAttribute("assignMentTender", assignMentTender);
		}
		if(b.getIsAssignment() == 1 && (b.getStatus() ==8 || b.getStatus()==59)){
			BorrowTender assignMentTender  = borrowService.getAssignMentTenderByBorrowId(b.getAssignmentTender());
			request.setAttribute("assignMentTender", assignMentTender);
		}
		
		//计算用户剩余投资金额
		if(getSessionUser()!=null){//用户需要登陆才能查看。
			User sessionUser = userService.getUserById(getSessionUser().getUserId());
			session.put(Constant.SESSION_USER, sessionUser);
			double accountTender = Global.getDouble("tender_account");
			if (accountTender>0) {
				double sumTenderAccount = borrowService.sumTenderWaitAccount(sessionUser.getUserId());
				request.setAttribute("waitTenderAccount", NumberUtils.format2Str(accountTender-sumTenderAccount));
			}
		}
		madeLeaseTender(b);
		List<InvestmentModel> tzlist =borrowService.getInvestmentList();
		request.setAttribute("tzlist", tzlist);
		//v1.8.0.3_u3 TGPROJECT-335  wuing 2014-06-17  start 
		List<BorrowDetail> borrowDetailList = borrowService.getBorrowDetailListByBorrowId(b.getId());  //获取借款信息的borrowDetail信息
		request.setAttribute("borrowDetailList", borrowDetailList);
		//v1.8.0.3_u3 TGPROJECT-335  wujing 2014-06-17  end
		//v1.8.0.3_u3 TGPROJECT-358  qinjun 2014-07-07  start
		repaymentDetail(b);
		//v1.8.0.3_u3 TGPROJECT-358  qinjun 2014-07-07  end
		int page=paramInt("page");
		List<BorrowTender> list = null;
		PageDataList data = null;
		if(b.getIsAssignment()==0){
			SearchParam param=SearchParam.getInstance()
					.addParam("borrow", new Borrow(id))
					.addParam("assignment_borrow_id",0)
					.addPage(page,25);
			 data=borrowService.getTenderList(param);
			 list = data.getList();
		}else{
			SearchParam param=SearchParam.getInstance()
					.addParam("assignment_borrow_id", id)
					.addPage(page,25);
			 data=borrowService.getTenderList(param);
			 list = data.getList();
		}
		request.setAttribute("tenderlist", list);
		return SUCCESS;
	}
	//v1.8.0.3_u3 TGPROJECT-358  qinjun 2014-07-07  start
	private void repaymentDetail(Borrow borrow){
		double account = borrow.getAccount();
		double lilv = borrow.getApr();
		int times = borrow.getTimeLimit();
		int time_limit_day = borrow.getTimeLimitDay();
		String style = borrow.getStyle();
		if(borrow.getIsday() == 1){
			InterestCalculator ic =new EndInterestCalculator(account,lilv/100,time_limit_day);
			//循环每期的利息
			ic.each();
			request.setAttribute("ic", ic);
		}else{
			if(style.equals("0")){
				InterestCalculator ic =new MonthEqualCalculator(account,lilv/100,times);
				ic.each();
			    request.setAttribute("ic", ic);
			}else if(style.equals("3")){
				InterestCalculator ic =new MonthInterestCalculator(account,lilv/100,times);
				ic.each();
			    request.setAttribute("ic", ic);
			}else if(style.equals("2")){
				InterestCalculator ic = new EndInterestCalculator (account,lilv/100,times,InterestCalculator.TYPE_MONTH_END);
				ic.each();
				request.setAttribute("ic", ic);
			}
		}
		request.setAttribute("account", account);
		request.setAttribute("lilv", lilv);
		request.setAttribute("times", times);
		request.setAttribute("time_limit_day", time_limit_day);
	}
	//v1.8.0.3_u3 TGPROJECT-358  qinjun 2014-07-07  end
	private double madeLeaseTender(Borrow borrow){
		Rule rule = ruleService.getRuleByNid("tender_account_rule");
		if (rule !=null&&rule.getStatus()==1) {
			int leaseRate = rule.getValueIntByKey("lease_status");
			if (leaseRate ==1) {
				double rate = rule.getValueDoubleByKey("tender_least_rate");//最低投标比例
				double leaseMoney = rate*borrow.getAccount();//最低投标金额
				double leaseTenderMoney = (borrow.getAccount()-borrow.getAccountYes()-leaseMoney)<leaseMoney?(borrow.getAccount()-borrow.getAccountYes()):leaseMoney; //若剩余标的金额小于最低投标金额的话，最后一个人的投标金额为：最低投标金额+剩余投标金额。否则为最低投标金额
				request.setAttribute("leaseTenderMoney", leaseTenderMoney);
				//计算成立时间
				Date date = borrow.getBorrowProperty().getEndTime();
				date = DateUtils.rollDay(date, 1);
				request.setAttribute("update", DateUtils.dateStr2(date));
			}
		
		}
		return 0;
	}
	
	@Action(value="detailTenderForJson",
			results={@Result(name="success",type="ftl",location="/invest/detail.html")},
			interceptorRefs={@InterceptorRef(value="mydefault")}
	)
	public String detailTenderForJson() throws Exception {
		int page=paramInt("page");
		long id=paramLong("borrowid");
		Borrow b = borrowService.getBorrow(id);
		List<BorrowTender> list = null;
		PageDataList data = null;
		if(b.getIsAssignment()==0){
			SearchParam param=SearchParam.getInstance()
					.addParam("borrow", new Borrow(id))
					.addParam("assignment_borrow_id",0)
					.addPage(page,25);
			 data=borrowService.getTenderList(param);
			 list = data.getList();
			for (BorrowTender bt : list) {
				User user =  new User();
				user.setUsername(StringUtils.hideLastChar(bt.getUser().getUsername(), 3));
				bt.setUser(user);
			}
		}else{
			SearchParam param=SearchParam.getInstance()
					.addParam("assignment_borrow_id", id)
					.addPage(page,25);
			 data=borrowService.getTenderList(param);
			 list = data.getList();
			for (BorrowTender bt : list) {
				User user =  new User();
				user.setUsername(StringUtils.hideLastChar(bt.getUser().getUsername(), 3));
				bt.setUser(user);
			}
		}
		Map<String,Object> map=new HashMap<String,Object>();
		if (list == null||list.size()<1) {
			map.put("msg", null);
		}else{
		      map.put("msg", "success");
		      map.put("data", data);
		}
		printJson(getStringOfJpaMap(map));
		return null;
	}
	
	/**
	 * 校验加息劵是否存在
	 * @return
	 * @throws Exception
	 */
	@Action(value="checkInterestNum")
	public String checkInterestNum() throws Exception {
		User user = getSessionUser();
		String interestNum = paramString("interestNum");
		BorrowInterestRate bir = borrowInterestRateService.findByInterestNum(interestNum);
		Map<String,Object> map = new HashMap<String,Object>();
		if(bir != null){
			if(bir.getStatus() == 2){
				map.put("msg", "加息劵串已过期！");
				map.put("result", false);
			}else if(bir.getStatus() == 3){
				map.put("msg", "加息劵串已使用过！");
				map.put("result", false);
			}else if(bir.getStatus() == 4 || bir.getStatus() == 1){
				map.put("interestRateValue", bir.getValue());
				map.put("msg", "加息劵串可以使用！");
				map.put("result", true);
			}
		}else {
			map.put("msg", "加息劵串无效！");
			map.put("result", false);
		}
		printJson(getStringOfJpaMap(map));
		return null;
	}
	
}

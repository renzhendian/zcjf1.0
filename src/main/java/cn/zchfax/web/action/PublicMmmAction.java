package cn.zchfax.web.action;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.zchfax.api.moneymoremore.LoanJson;
import cn.zchfax.api.moneymoremore.MmmCash;
import cn.zchfax.api.moneymoremore.MmmLoanAuthorize;
import cn.zchfax.api.moneymoremore.MmmLoanTransfer;
import cn.zchfax.api.moneymoremore.MmmModel;
import cn.zchfax.api.moneymoremore.MmmRecharge;
import cn.zchfax.api.moneymoremore.MmmRegister;
import cn.zchfax.api.moneymoremore.MmmResultCode;
import cn.zchfax.api.moneymoremore.SecondaryJson;
import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.disruptor.DisruptorUtils;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.User;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.BorrowParam;
import cn.zchfax.model.APIModel.AccountCashModel;
import cn.zchfax.model.APIModel.RechargeModel;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.ChinapnrPayModelService;
import cn.zchfax.service.UserService;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;

/**
 * 钱多多回调处理类，使用disruptor  前台回调和后台回调都处理业务,处理业务的时候进行判断
 */
@Namespace("/public/mmm")
@ParentPackage("p2p-default") 
public class PublicMmmAction extends BaseAction {
	private static final Logger logger=Logger.getLogger(PublicMmmAction.class);
	
	@Autowired
    private UserService userService;
	@Autowired
	private ChinapnrPayModelService chinapnrPayModelService;
	@Autowired
    private AccountService accountService;
	//v1.8.0.3 TGPROJECT-21 lx 2014-04-10 start
	/**
	 * 取现页面回调
	 * @return
	 */
	@Action(value="cashReturn")
	public String cashReturn(){
		BorrowParam param=new BorrowParam();
		String resultFlag =  System.currentTimeMillis()+"";
		param.setResultFlag(resultFlag);
		String s=getRequestParams();
		logger.debug("取现页面回调："+s);
		request.setAttribute("tenderFlag", resultFlag);
		request.setAttribute("ok_url", "/member/main.html"); //成功返回地址
		request.setAttribute("back_url", "/member/main.html");//失败返回地址
		request.setAttribute("r_msg","取现成功,等待银行处理");
		dealMmmCash(param);
    	return RESULT;
	}
	/**
	 * 取现后台回调
	 * @return
	 */
	@Action(value="cashNotify")
	public String cashNotify(){
		dealMmmCash(new BorrowParam());
		printSuccess();
		return null;
	}
	 private void dealMmmCash(BorrowParam param){
		MmmCash mmmCash = this.cashCall();
		logger.info("------进入回调提现-----" + getRequestParams());
		logger.info("提现回调返回码："+mmmCash.getResultCode());
		boolean verifySignature = mmmCash.successSign();
		if(mmmCash.getResultCode().equals("88") || mmmCash.getResultCode().equals("89")){
			//回调状态码为 88 89 的都进入处理方法
			if(verifySignature){
				logger.info("取现双乾处理成功，进入本地处理…………" + mmmCash.getOrderNo() + "-------");
				AccountCashModel cashModel = new AccountCashModel();// 封装取现共用bean
				cashModel.setCardNo(mmmCash.getCardNo());
				cashModel.setOrderId(mmmCash.getOrderNo());
				cashModel.setOrderAmount(mmmCash.getAmount());
				//v1.8.0.4_u1 TGPROJECT-292 lx start
				//cashModel.setFeeAmt(NumberUtils.getDouble(mmmCash.getFee()));//平台承担的手续费金额
				cashModel.setFeeAmt(NumberUtils.getDouble(mmmCash.getFeeWithdraws()));//用户承担的手续费金额
				//v1.8.0.4_u1 TGPROJECT-292 lx end
				param.setResultCode(mmmCash.getResultCode());
				try {
					DisruptorUtils.doMmmVerifyCashBackTask(param, cashModel);
				} catch (Exception e) {
					logger.error(e);
					logger.error("取现失败：" +  e );
				}
			}
		}else{
			throw new BussinessException(MmmResultCode.getResult(mmmCash.getResultCode(), "cash")
					,"/member/index.html");
		}
	 }
	
	//v1.8.0.3 TGPROJECT-21 lx 2014-04-10 end
	/**
	 * 注册页面回调
	 * @return
	 */
	@Action(value="registerReturn")
	public String registerReturn(){
		BorrowParam param=new BorrowParam();
		String resultFlag =  System.currentTimeMillis()+"";
		param.setResultFlag(resultFlag);
		request.setAttribute("tenderFlag", resultFlag);
		request.setAttribute("ok_url", "/member/main.html"); //成功返回地址
		request.setAttribute("back_url", "/member/main.html");//失败返回地址
		request.setAttribute("r_msg","开户成功");
		dealMmmRegister(param);
    	return RESULT;
	}
	
	/**
	 * 注册后台回调
	 * @return
	 */
	@Action(value="registerNotify")
	public String registerNotify(){
		dealMmmRegister(null);
		printSuccess();
		return null;
	}
	
    private void dealMmmRegister(BorrowParam param){
    	MmmRegister mmmReg = this.registerCall();
    	checkSign(mmmReg,"register");
		User user = userService.getUserByNameEmailPhone(mmmReg.getLoanPlatformAccount());
		user.setApiId(mmmReg.getMoneymoremoreId());
		user.setApiUsercustId(mmmReg.getAccountNumber());
		try {
			DisruptorUtils.apiUserRegister(user,param);//调用异步处理
		} catch (Exception e) {
			logger.error(e);
			logger.error("开户失败：" +  e );
		}
		
    }
    
    //v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
    /**
	 * 授权还款页面回调
	 * @return
	 */
	@Action(value="loanAuthorizeReturn")
	public String loanAuthorizeReturn(){
		BorrowParam param=new BorrowParam();
		String resultFlag =  System.currentTimeMillis()+"";
		param.setResultFlag(resultFlag);
		request.setAttribute("tenderFlag", resultFlag);
		request.setAttribute("ok_url", "/member/main.html"); //成功返回地址
		request.setAttribute("back_url", "/member/main.html");//失败返回地址
		request.setAttribute("r_msg","授权成功");
		dealLoanAuthorize(param);
    	return RESULT;
	}
	
	/**
	 * 授权还款后台回调
	 * @return
	 */
	@Action(value="loanAuthorizeNotify")
	public String loanAuthorizeNotify(){
		dealLoanAuthorize(null);
		printSuccess();
		return null;
	}
	
	private void dealLoanAuthorize(BorrowParam param){
    	MmmLoanAuthorize mmm = this.loanAuthorizeCall();
    	checkSign(mmm,"loanAuthorize");
		User user = userService.getUserByApiId(mmm.getMoneymoremoreId());
		try {
			DisruptorUtils.apiUserActivate(user,param);//调用异步处理
		} catch (Exception e) {
			logger.error(e);
			logger.error("授权失败：" +  e );
		}
		
    }
    //v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	
    /**
	 * 充值前台回调
	 * @return
	 */
	@Action(value="rechargeReturn")
	public String rechargeReturn(){
		BorrowParam param=new BorrowParam();
		String resultFlag =  System.currentTimeMillis()+"";
		param.setResultFlag(resultFlag);
		request.setAttribute("tenderFlag", resultFlag);
		request.setAttribute("ok_url", "/member/main.html"); //成功返回地址
		request.setAttribute("back_url", "/member/main.html");//失败返回地址
		request.setAttribute("r_msg","充值成功");
		dealMmmRecharge(param);
    	return RESULT;
	}
    
	/**
	 * 充值后台回调
	 * @return
	 */
	@Action(value="rechargeNotify")
	public String rechargeNotify(){
		dealMmmRecharge(null);
		printSuccess();
		return null;
	}
	
	private void dealMmmRecharge(BorrowParam param){
		MmmRecharge mmmReg = this.rechargeCall();
    	checkSign(mmmReg,"recharge");
    	AccountLog log=new AccountLog(1L,Constant.RECHARGE,1L,getTimeStr(), getRequestIp());
		String orderNo = mmmReg.getOrderNo(); //订单号。
		logger.info( "订单号：" + mmmReg.getOrderNo() + " 金额：" + mmmReg.getAmount()
				     + " 结果：" + mmmReg.getResultCode() + " 流水号：" + mmmReg.getLoanNo());
		log.setRemark("网上充值,"+Global.getValue("api_name")+"充值,订单号:"  +  orderNo);
		try {
			if(orderNo != null) {
				log.setRemark(getRequestParams());//返回的参数包含 进去
				RechargeModel reModel = new RechargeModel();  //对通用javabean进行参数封装
				reModel.setOrderAmount(mmmReg.getAmount());
				reModel.setOrderId(mmmReg.getOrderNo());
				reModel.setResultMsg(mmmReg.getResultCode());
				reModel.setResult("true");
				reModel.setSerialNo( mmmReg.getLoanNo());
				DisruptorUtils.doRechargeBackTask(reModel,log,param);
			}else {
				logger.info( "**********"+Global.getValue("api_name")+"充值 回调返回订单为空:" + orderNo );
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error("充值失败："+orderNo + "   " +  e );
		}
	}
	//v1.8.0.4  TGPROJECT-27   qj  2014-04-09 start
	/**
	 * 投标页面回调
	 * @return
	 */
	@Action(value="tenderReturn")
	public String tenderReturn(){
		BorrowParam param=new BorrowParam();
		String resultFlag =  System.currentTimeMillis()+"";
		param.setResultFlag(resultFlag);
		request.setAttribute("tenderFlag", resultFlag);
		request.setAttribute("ok_url", "/member/main.html"); //成功返回地址
		request.setAttribute("back_url", "/member/main.html");//失败返回地址
		request.setAttribute("r_msg","投标成功");
//		dealTender(param);
		return RESULT;
	}
	/**
	 * 投标后台回调
	 * @return
	 */
	@Action(value="tenderNotify")
	public String tenderNotify(){
//		BorrowParam param = new BorrowParam();
//		dealTender(param);
		printSuccess();
		return null;
	}
	
	private void dealTender(BorrowParam param){
		MmmLoanTransfer loan = loanCall();
		checkSign(loan,"loan");
		LoanJson loanJson = loan.getLoanList().get(0);
		param.setId(NumberUtils.getLong(loanJson.getBatchNo()));
		param.setMoney(NumberUtils.getDouble2(loanJson.getAmount()));
		param.setTenderNo(loanJson.getOrderNo());
		param.setLoanNo(loanJson.getLoanNo());
		param.setIp(getRequestIp());
		User user = userService.getUserByApiId(loanJson.getLoanOutMoneymoremore());
		logger.info("投标进入 OrderNo:"+loanJson.getOrderNo() + "  LoanNo: " +loanJson.getLoanNo() + "   borrow_id :" + loanJson.getBatchNo());
		try {
			DisruptorUtils.mmmTender(param, user);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			logger.error("投标失败  OrderNo:"+loanJson.getOrderNo() + "  LoanNo: " +loanJson.getLoanNo()+  e );
		}
	}
	//v1.8.0.4  TGPROJECT-27   qj  2014-04-09 stop
	
	//v1.8.0.4  TGPROJECT-57   qj  2014-04-14 start
	@Action(value="loanTransferAuditReturn")
	public String loanTransferAuditReturn(){
		logger.info("------进入回调-----" + getRequestParams());
		return MSG;
	}
	@Action(value="notifyPrintSuccess")
	public String notifyPrintSuccess(){
		logger.info("------进入回调-----" + getRequestParams());
		printSuccess();
		return null;
	}
	//v1.8.0.4  TGPROJECT-57   qj  2014-04-14 stop
	/**
	 * 转账回调打印success
	 * @return
	 */
	@Action(value="loanTransferAuditReturn")
	public String loanNotify(){
		MmmLoanTransfer mmm = loanCall();
		checkSign(mmm,"loan");
		printSuccess();
		return null;
	}
	
	//v1.8.0.4  TGPROJECT-27   qj  2014-04-08 start
	private MmmLoanTransfer loanCall(){
		MmmLoanTransfer mmmLoan = new MmmLoanTransfer();
		mmmLoan.setResultCode(paramString("ResultCode"));
		mmmLoan.setPlatformMoneymoremore(paramString("PlatformMoneymoremore"));
		mmmLoan.setAction(paramString("Action"));
		String LoanJsonListStr = StringUtils.UrlDecoder(paramString("LoanJsonList"));
		mmmLoan.setLoanJsonList(LoanJsonListStr);
		JSONArray loanListjson= JSON.parseArray(LoanJsonListStr);
		List<LoanJson> loanList = new ArrayList<LoanJson>();
		if(loanListjson != null ){
			for (int i = 0; i < loanListjson.size(); i++) {
				JSONObject object = loanListjson.getJSONObject(i);
				LoanJson loan = new LoanJson();
				loan.setLoanOutMoneymoremore(object.getString("LoanOutMoneymoremore"));
				loan.setLoanInMoneymoremore(object.getString("LoanInMoneymoremore"));
				loan.setLoanNo(object.getString("LoanNo"));
				loan.setOrderNo(object.getString("OrderNo"));
				loan.setAmount(object.getString("Amount"));
				loan.setBatchNo(object.getString("BatchNo"));
				loan.setTransferName(object.getString("TransferName"));
				loan.setRemark(object.getString("Remark"));
				JSONArray secondListjson= JSON.parseArray(object.getString("SecondaryJsonList"));
				List<SecondaryJson> secondList = new ArrayList<SecondaryJson>();
				if(secondListjson != null){
					for (int n = 0; n < secondListjson.size(); n++) {
						JSONObject secondObject = loanListjson.getJSONObject(n);
						SecondaryJson secondJson = new SecondaryJson();
						secondJson.setLoanInMoneymoremore(secondObject.getString("LoanInMoneymoremore"));
						secondJson.setAmount(secondObject.getString("Amount"));
						secondJson.setTransferName(secondObject.getString("TransferName"));
						secondJson.setRemark(secondObject.getString("Remark"));
						secondList.add(secondJson);
					}
					loan.setSecondaryList(secondList);
				}
				loanList.add(loan);
			}
		}
		mmmLoan.setLoanList(loanList);
		mmmLoan.setSignInfo(paramString("SignInfo"));
		return mmmLoan;
	}
	//v1.8.0.4  TGPROJECT-27   qj  2014-04-08 stop
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-08 start
	private MmmLoanAuthorize loanAuthorizeCall(){
		MmmLoanAuthorize mmm = new MmmLoanAuthorize();
		mmm.setMoneymoremoreId(paramString("MoneymoremoreId"));
		mmm.setPlatformMoneymoremore(paramString("PlatformMoneymoremore"));
		mmm.setAuthorizeTypeOpen(paramString("AuthorizeTypeOpen"));
		mmm.setAuthorizeTypeClose(paramString("AuthorizeTypeClose"));
		mmm.setAuthorizeType(paramString("AuthorizeType"));
		mmm.setResultCode(paramString("ResultCode"));
		mmm.setSignInfo(paramString("SignInfo"));
		return mmm;
	}
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-08 stop
	//注册返回参数
	private MmmRegister registerCall(){
		MmmRegister mmmReg = new MmmRegister();
		mmmReg.setAccountNumber(paramString("AccountNumber"));
		mmmReg.setMobile(paramString("Mobile"));
		mmmReg.setEmail(paramString("Email"));
		mmmReg.setRealName(paramString("RealName"));
		mmmReg.setIdentificationNo(paramString("IdentificationNo"));
		mmmReg.setMoneymoremoreId(paramString("MoneymoremoreId"));
		mmmReg.setLoanPlatformAccount(paramString("LoanPlatformAccount"));
		mmmReg.setPlatformMoneymoremore(paramString("PlatformMoneymoremore"));
		mmmReg.setResultCode(paramString("ResultCode"));
		mmmReg.setAuthFee(paramString("AuthFee"));
		mmmReg.setAuthState(paramString("AuthState"));
		mmmReg.setSignInfo(paramString("SignInfo"));
		return mmmReg;
	}
	
	//充值返回参数
	private MmmRecharge rechargeCall(){
		MmmRecharge mmmRec = new MmmRecharge();
		mmmRec.setRechargeMoneymoremore(paramString("RechargeMoneymoremore"));
		mmmRec.setPlatformMoneymoremore(paramString("PlatformMoneymoremore"));
		mmmRec.setLoanNo(paramString("LoanNo"));
		mmmRec.setOrderNo(paramString("OrderNo"));
		mmmRec.setAmount(paramString("Amount"));
		mmmRec.setResultCode(paramString("ResultCode"));
		mmmRec.setSignInfo(paramString("SignInfo"));
		return mmmRec;
	}
	
	// 取现返回参数
	private MmmCash cashCall() {
		MmmCash mmmCash = new MmmCash(); 
		mmmCash.setWithdrawMoneymoremore(paramString("WithdrawMoneymoremore"));
		mmmCash.setPlatformMoneymoremore(paramString("PlatformMoneymoremore"));
		mmmCash.setLoanNo(paramString("LoanNo"));
		mmmCash.setOrderNo(paramString("OrderNo"));
		mmmCash.setAmount(paramString("Amount"));
		mmmCash.setFeePercent(paramString("FeePercent"));
		mmmCash.setFee(paramString("Fee"));
		mmmCash.setFreeLimit(paramString("FreeLimit"));
		//v1.8.0.4_u1 TGPROJECT-292 lx start
		mmmCash.setRandomTimeStamp(paramString("RandomTimeStamp"));
		mmmCash.setRemark1(paramString("Remark1"));
		mmmCash.setRemark2(paramString("Remark2"));
		mmmCash.setRemark3(paramString("Remark3"));
		mmmCash.setFeeMax(paramString("FeeMax"));
		mmmCash.setFeeWithdraws(paramString("FeeWithdraws"));
		//v1.8.0.4_u1 TGPROJECT-292 lx end
		mmmCash.setResultCode(paramString("ResultCode"));
		mmmCash.setSignInfo(paramString("SignInfo"));
		return mmmCash;
	}

	private void checkSign(MmmModel mmm,String type){
		logger.info("------进入回调-----" + getRequestParams());
		logger.info("回调返回码："+mmm.getResultCode());
		boolean verifySignature = mmm.successSign();
		if(!mmm.getResultCode().equals("88") || !verifySignature){
			//v1.8.0.4  TGPROJECT-41   qj  2014-04-09 start
			throw new BussinessException(MmmResultCode.getResult(mmm.getResultCode(), type)
					,"/member/index.html");
			//v1.8.0.4  TGPROJECT-41   qj  2014-04-09 stop
		}
	}
	
	
	/**
	 * 回调参数拼接共用方法
	 * @return
	 */
	public String getRequestParams(){
		String params = "";
		try {
			Enumeration  e= (Enumeration)request.getParameterNames();   
			 while(e.hasMoreElements())     {   
			    String parName=(String)e.nextElement();   
			    String value= request.getParameter(parName);
			    params += parName + "=" + value + "&";
			 } 
		} catch (Exception e) {
			logger.error(e);
		}
         return params;		 
	}
	
	/**
	 * 回调打印信息
	 */
	private void printSuccess(){
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().write("SUCCESS");
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("回调打印异常:"+e.getMessage());
		}
		
	}
	
}


package cn.zchfax.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.entity.Record;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;

import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.dao.BorrowTenderDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.Rule;
import cn.zchfax.domain.Site;
import cn.zchfax.domain.SystemConfig;
import cn.zchfax.domain.User;
import cn.zchfax.domain.Warrant;
import cn.zchfax.domain.YwdUser;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.OrderFilter.OrderType;
import cn.zchfax.model.SearchFilter.Operator;
import cn.zchfax.model.account.InvestmentModel;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.ArticleService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.RuleService;
import cn.zchfax.service.SystemService;
import cn.zchfax.service.UserService;
import cn.zchfax.service.WarrantService;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;

@Namespace("/")
@ParentPackage("p2p-default") 
@Result(name="success",type="ftl", location="/index.html") 
public class IndexAction extends BaseAction{

	private static final long serialVersionUID = -2578417106463783748L; 
	private static Logger logger = Logger.getLogger(IndexAction.class);
	@Autowired
	BorrowService borrowService;
	@Autowired
	ArticleService articleService;
	@Autowired
	RuleService ruleService;
	@Autowired
	WarrantService warrantService;
	@Autowired
	AccountService accountService ;
	@Autowired
	UserService userService ;
	@Autowired
	BorrowTenderDao borrowTenderDao;
	@Autowired
	private SystemService systemService;
	@Autowired
	private Dao dao;
	@Action(value="old",results={@Result(name="success",location="/index.html")})
	public String doLogin() throws Exception {
		 Record ftldir = dao.fetch("system", Cnd.where("nid", "=", "con_ftl_dir"));
		if(ftldir.get("value").equals("/WEB-INF/html_zchfax"))
		dao.update("system",
				Chain.make("value","/WEB-INF/html_80zb"),
				Cnd.where("nid","=","con_ftl_dir"));
		else
		dao.update("system",
	            Chain.make("value","/WEB-INF/html_zchfax"),
	            Cnd.where("nid","=","con_ftl_dir"));
		systemService.updateSystemInfo();
		return SUCCESS;
	}
	@Action("index")
	public String index() throws Exception {
		Rule rule = ruleService.getRuleByNid("index_borrow_sort");
		Rule forecastrule = ruleService.getRuleByNid("index_rule");
		Rule assginmentrule = ruleService.getRuleByNid("assginment_rule");
		Map<String,List> map =  getIndexLists(rule);
		request.setAttribute("indexBorrowList", map.get("indexList")); //已经正在招标显示
		Map<String,List> map1 =  getIndexLists(forecastrule);
		request.setAttribute("forecastList", map1.get("indexList")); //预告标
		Map<String,List> map2 =  getIndexLists(assginmentrule);
		request.setAttribute("assginmentlist", map2.get("indexList")); //债权转让
		//商票通
		SearchParam sptParam=SearchParam.getInstance()
				.addPage(0, 4)
				.addParam("type", 103)
				.addOrFilter("status", 1,3,6,7,8)
				.addOrder(OrderType.DESC, "addtime");
		List sptList = borrowService.getList(sptParam).getList();		
		request.setAttribute("sptList", sptList);
		//银票通
		SearchParam tParam=SearchParam.getInstance()
				.addPage(0, 3)
				.addParam("type", 105)
				.addOrFilter("status", 1,3,6,7,8)
				.addOrder(OrderType.DESC, "addtime");
		List yptList = borrowService.getList(tParam).getList();		
		request.setAttribute("yptList", yptList);
		List recommendBorrowList =  getrecommendBorrows();
		//友情链接
		Site linksite=articleService.getSiteByCode("friend_links");
		PageDataList friendLinksList = null;
		if(linksite!=null){			
			SearchParam flFirstParam = SearchParam.getInstance();
			flFirstParam.addPage(0, 6);
			flFirstParam.addParam("site", linksite);
			friendLinksList = articleService.getArticleList(flFirstParam);
			
		}
		if(friendLinksList!=null&&friendLinksList.getList()!=null){
			request.setAttribute("friendLinksList",friendLinksList.getList());
		}
		//媒体资料		
		Site site=articleService.getSiteByCode("mtbd");
		PageDataList mediaList = null;
		PageDataList mediaFirst = null;
		if(site!=null){			
			SearchParam spFirstParam = SearchParam.getInstance();
			spFirstParam.addPage(0, 1);
//			spFirstParam.addParam("site", site);
			spFirstParam.addOrFilter("flag", "t", "h");
			spFirstParam.addOrder(OrderType.DESC, "publish");
			mediaFirst = articleService.getArticleList(spFirstParam);
			//媒体报道首条
			if(mediaFirst!=null&&mediaFirst.getList()!=null&&mediaFirst.getList().size()>=1){
				request.setAttribute("mediaFirst",mediaFirst.getList().get(0));
			}
			SearchParam sp = SearchParam.getInstance();
			sp.addPage(0, 4);
			sp.addParam("site", site);
			sp.addParam("flag",Operator.NOTEQ,"t");
			sp.addParam("flag",Operator.NOTEQ,"h");
			sp.addOrder(OrderType.DESC, "publish");
			mediaList = articleService.getArticleList(sp);
			//媒体报道
			if(mediaList!=null&&mediaList.getList()!=null){
				request.setAttribute("mediaList",mediaList.getList());
			}
		}
		//行业资讯
		Site  inforSiste = articleService.getSiteByCode("information");
		PageDataList infoList = null;
		PageDataList infoFirst = null;
		if(inforSiste!=null){			
			SearchParam infoFirstParam = SearchParam.getInstance();
			infoFirstParam.addPage(0, 1);
			infoFirstParam.addParam("site", inforSiste);
			infoFirstParam.addOrFilter("flag", "t", "h");
			infoFirstParam.addOrder(OrderType.DESC, "publish");
			infoFirst = articleService.getArticleList(infoFirstParam);
			if(infoFirst!=null&&infoFirst.getList()!=null&&infoFirst.getList().size()>=1){
				request.setAttribute("infoFirst",infoFirst.getList().get(0));
			}
			SearchParam infoParam = SearchParam.getInstance();
			infoParam.addPage(0, 8);
			infoParam.addParam("site", inforSiste);
			infoParam.addParam("flag",Operator.NOTEQ,"t");
			infoParam.addParam("flag",Operator.NOTEQ,"h");
			infoParam.addOrder(OrderType.DESC, "publish");
			infoList = articleService.getArticleList(infoParam);
			if(infoList!=null&&infoList.getList()!=null){
				request.setAttribute("infoList",infoList.getList());
			}
		}
		//头条公告
		Site  noticeSiste = articleService.getSiteByCode("wzgg");
		if(noticeSiste !=null){
			PageDataList noticeFirst = null;
			SearchParam noticeFirstParam = SearchParam.getInstance();
			noticeFirstParam.addPage(0, 1);
			noticeFirstParam.addParam("site", noticeSiste);
			noticeFirstParam.addOrFilter("flag", "t", "h");
			noticeFirstParam.addOrder(OrderType.DESC, "publish");
			noticeFirst = articleService.getArticleList(noticeFirstParam);
			if(noticeFirst!=null&&noticeFirst.getList()!=null&&noticeFirst.getList().size()>=1){
				request.setAttribute("noticeFirst",noticeFirst.getList().get(0));
			}
			
			SearchParam  noticeParam = SearchParam.getInstance();
			noticeParam.addParam("site", noticeSiste);	
			noticeParam.addPage(0, 6);
			noticeParam.addOrFilter("flag", "t", "h");
			PageDataList noticeList = articleService.getArticleList(noticeParam);
			//公告
			if(noticeList!=null&&noticeList.getList()!=null){
				request.setAttribute("noticeList",noticeList.getList());
			}
		}
		request.setAttribute("successList", map.get("successList"));
		request.setAttribute("recommendList", recommendBorrowList);
		 //需优化---end
		User user = getSessionUser();
		if(user!=null){
			session.put(Constant.SESSION_USER, user);
		}
		//首页banner 
		SearchParam  picParam = SearchParam.getInstance();
		picParam.addParam("status", 1).addParam("typeId", 1);
		picParam.addOrder(OrderType.ASC, "sort");
		PageDataList plist = articleService.getScrollPicList(picParam);
		request.setAttribute("scrollPic", plist.getList());
		
		// 总投资金额(单位万)
		double borrowSum = borrowService.getBorrowSum();	
		borrowSum = NumberUtils.format2(borrowSum);
		request.setAttribute("borrowSum", borrowSum);
		// 总投资产生利息(单伟万)

		double borrowSumInterest = borrowService.getBorrowSumInterest();
		borrowSumInterest = NumberUtils.format2(borrowSumInterest);
		request.setAttribute("borrowSumInterest", borrowSumInterest);
		
		int tenderSumTimes = 0;
		tenderSumTimes = borrowService.getTenderSumTimes();
		request.setAttribute("tenderSumTimes", tenderSumTimes);
		
		double averageInterest =0;
		averageInterest = borrowService.getAverageInterest();
		request.setAttribute("averageInterest", averageInterest);		
		// 投标动态
		List<InvestmentModel> tzlist =borrowService.getInvestmentList();
		request.setAttribute("tzlist", tzlist);
		request.setAttribute("newTenderList", map.get("newTenderList"));
		return SUCCESS;
	}
	
	private List<Borrow> getrecommendBorrows(){
		//获取推荐标
		List<Borrow> recommendList = new ArrayList<Borrow>();
		SearchParam recommendParam=SearchParam.getInstance()
				.addParam("recommendStatus",1)
				.addOrder(OrderType.DESC, "addtime")
				.addParam("startTenderTime",Operator.LT, new Date())
				.addParam("status", 1)
				.addPage(1,1);
		PageDataList<Borrow> listData = borrowService.getList(recommendParam);
		recommendList.addAll(listData.getList());
		return recommendList;		
	}
	
	@Action(value="safe",
			results={@Result(name="success",type="ftl",location="/safe.html")}
			)
	public String safe() throws Exception {
		SearchParam param=SearchParam.getInstance().addParam("status", 1);
		List safeList = borrowService.getList(param).getList();
		request.setAttribute("safeList", safeList);
		return SUCCESS;
	}
	
	private Map<String,List> getIndexLists(Rule rule){
		Map<String,List> map = new HashMap<String,List>(); 
		List<Borrow> indexBorrowList = new ArrayList<Borrow>();
		int borrow_num = rule.getValueIntByKey("index_borrow_num");
		if(borrow_num == 0){
			borrow_num = 3;
		}
		String[] sorts = rule.getValueStrByKey("sort").split(","); 
		if(sorts!= null){
			int j = 1;
			for (int s = 0; s < sorts.length; s++) {
				SearchParam param = SearchParam.getInstance().addOrder(OrderType.DESC, "addtime").addPage(1);
				List list = new ArrayList();
				String sort = sorts[s];
				String typeRule = rule.getValueStrByKey("not_type");
				if(!typeRule.equals("") && typeRule != null){
					int[] types =  NumberUtils.getInts(typeRule.split(","));
					for (int i = 0; i < types.length; i++) {
						 param.addParam("type", Operator.NOTEQ, types[i]);
					}
				}
				if(sort.equals("a")){//等待初审  
					param.addParam("status", 0);
				}else if(sort.equals("b")){//正在招标
					//v1.8.0.4_u4  TGPROJECT-352  qinjun  2014-07-02  start
					param.addParam("account", Operator.PROPERTY_NOTEQ, "accountYes")
					.addParam("status", 1)
					.addParam("isAssignment", 0)
					.addParam("startTenderTime",Operator.LT, new Date());
					//v1.8.0.4_u4  TGPROJECT-352  qinjun  2014-07-02  end
				}else if(sort.equals("c")){//等待复审
					param.addParam("status", 1).addParam("account", Operator.PROPERTY_EQ, "accountYes");
				}else if(sort.equals("d")){//还款中
					param.addOrFilter("status",6,7);
				}else if(sort.equals("e")){//还款完成
					param.addOrFilter("status",8);  
				}
				//v1.8.0.4_u4  TGPROJECT-352  qinjun  2014-07-02  start
				else if(sort.equals("f")){
					param.addParam("account", Operator.PROPERTY_NOTEQ, "accountYes").addParam("status", 1).addParam("recommendStatus",1);
				}
				//v1.8.0.4_u4  TGPROJECT-352  qinjun  2014-07-02  end
				else if(sort.equals("g")){ //发标预告
					param.addParam("account", Operator.PROPERTY_NOTEQ, "accountYes")
					.addParam("status", 1)
					.addParam("startTenderTime",Operator.GTE, new Date())
					.addOrder(OrderType.DESC, "startTenderTime");
				}
				else if(sort.equals("h")){ //债权转让
					param.addParam("status", 1)
					.addParam("isAssignment", 1)
					.addParam("startTenderTime",Operator.LT, new Date());
				}
				param.addPage(1, 2);//TODO 搞不清楚这块这数据是怎么回事，想取三条就是不行，只好在页面控制了
				list = borrowService.getList(param).getList();//招标
				for (int i = 0; i < list.size(); i++) {
					if(j>borrow_num){break;}
					indexBorrowList.add((Borrow)list.get(i));
				    ++j;
				}
			}
		}
		//获取成功案例数据
		SearchParam sParam=SearchParam.getInstance()
				.addOrFilter("status",6,7,8)
				.addOrder(OrderType.DESC, "addtime")
				.addPage(1);
		List successList = borrowService.getList(sParam).getList();
		map.put("successList", successList);
		map.put("indexList", indexBorrowList);
		SearchParam tParam=SearchParam.getInstance()
				.addParam("status",0)
				.addOrFilter("borrow.status", 1,3,6,7,8)
				.addOrder(OrderType.DESC, "addtime");
		List newTenderList = borrowService.getTenderList(tParam).getList();		
		map.put("newTenderList", newTenderList);
		return map;
	}
	
	/*
	 * 视频播放
	 */
	@Action(value="showVedio",
			results={@Result(name="success",type="ftl",location="/showVedio.html")}
			)
	public String showVedio()  {
		return SUCCESS;
	}
	
	@Action(value="danBaoCompany",
			results={@Result(name="success",type="ftl",location="/danbao/company.html")}
			)
	public String danBaoCompany(){
		long warrantid = paramLong("warrantId");
		Warrant warrant = warrantService.findWarrant(warrantid);
		if (warrant !=null) {
			request.setAttribute("warrant", warrant);
			return "success";
		}else{
			message("获取担保机构异常");
			return MSG;
		}
		
	}

	@Action(value="ywdRegiest")
	public String ywdRegiest(){
		for (int i = 1; i <= 100000; i++) {
			String username = createYwdUsername(i);
			String passwword = (new Random().nextInt(899999)+100000)+"";
			YwdUser user = new YwdUser(username,passwword);
			userService.ywdRegiest(user);
		}
		return MSG;
	}
	
	private String createYwdUsername(int i){
		if(i>0 && i<10){
			return "yw00000"+i;
		}else if(i>=10 && i<100){
			return "yw0000"+i;
		}else if(i>=100 && i<1000){
			return "yw000"+i;
		}else if(i>=1000 && i<10000){
			return "yw00"+i;
		}else if(i>=10000 && i<100000){
			return "yw0"+i;
		}else{
			return "yw"+i;
		}
		
	}
	
	@Action(value="checkValidImgToJSON")
	public String checkValidImgToJSON(){
		boolean isSuccess = true;;
		try {
			checkValidImg();
		} catch (Exception e) {
			isSuccess = false;
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("data", isSuccess+"");
		printJson(JSON.toJSONString(map));
		return null;
	}
	
}

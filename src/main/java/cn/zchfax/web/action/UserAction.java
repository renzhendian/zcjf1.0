package cn.zchfax.web.action;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

import cn.zchfax.api.bestsign.Api;
import cn.zchfax.context.ChinaPnrType;
import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.context.MsgOperateCode;
import cn.zchfax.disruptor.DisruptorUtils;
import cn.zchfax.domain.Area;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.InviteUser;
import cn.zchfax.domain.Rule;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCache;
import cn.zchfax.domain.UserCreditLog;
import cn.zchfax.domain.Userinfo;
import cn.zchfax.domain.Usertrack;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.MsgReq;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.TempIdentifyUser;
import cn.zchfax.model.OrderFilter.OrderType;
import cn.zchfax.model.account.UserAccountSummary;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.MsgService;
import cn.zchfax.service.RuleService;
import cn.zchfax.service.UserCreditService;
import cn.zchfax.service.UserService;
import cn.zchfax.tool.coder.BASE64Decoder;
import cn.zchfax.tool.coder.MD5;
import cn.zchfax.tool.javamail.Mail;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;


@Namespace("/user")
@ParentPackage("p2p-default") 
@Results({    
	  @Result(name="member", type="redirect",location="/member/index.html")
	}) 
public class UserAction extends BaseAction implements ModelDriven<User>  {

	private static final long serialVersionUID = 916623705419638039L;
	private static Logger logger = Logger.getLogger(UserAction.class);
	private String backUrl = "";
	private StringBuffer msg = new StringBuffer();
	private User user = new User();
	@Autowired 
	private UserCreditService userCreditService;
	@Autowired 
	private UserService userService;
	@Autowired 
	private AccountService accountService;
	@Autowired 
	private BorrowService borrowService;
	@Autowired
	private MsgService msgService;
	@Autowired
	private RuleService ruleService;
	@Autowired
	Dao dao;
	@Override
	public User getModel() {
		return user;
	}
	@Action("login")
	public String login(){
		String url = request.getParameter("url");
		logger.info("页面来源路径："+url);
		if(isSession()){
			return MEMBER;
		}
		request.setAttribute("url", url);
		return LOGIN;
	}
	//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 start
	// 邮箱状态为激活，限制登录，重新激活邮箱
		/*@Action("email_update")
		public String email() throws Exception {
			User user = (User)session.get("temp_user");
			user = userService.getUserById(user.getUserId());
			String emailUrl="/user/login.html";
			request.setAttribute("temp_user", user);
				String emailStatus = StringUtils.isNull(user.getEmailStatus());
				String email = StringUtils.isNull(request.getParameter("email"));
				if ( emailStatus.equals("1")|| emailStatus.equals("2")) {
					message("邮箱认证已经审核成功或正在审核中！", "");
					return MSG;
				}
				String errormsg = "";
				if (StringUtils.isBlank(email)) {
					message("email不能为空！", emailUrl);
					return MSG;
				}else if(!StringUtils.isEmail(email)){
					message("email格式不正确！", emailUrl);
					return MSG;
				} else {
					if (user.getEmail() == null || !user.getEmail().equals(email)) {// 用新的邮箱去激活
						if (!userService.checkEmail(email)) {
							message("邮箱已存在，请更改然后再激活", "");
							return MSG;
						}
						user.setEmail(email);
						userService.updateUser(user);
						session.remove("temp_user");
					}
					try {
						sendMail(user);
					} catch (Exception e) {
						logger.error(e.getMessage());
						errormsg = "发送激活邮件失败！";
						message(errormsg, emailUrl);
						return MSG;
					}
				}
				errormsg = "发送激活邮件成功！";
				message(errormsg, emailUrl);
				return MSG;
		}*/
	//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 end
	@Action(value="doLogin",results={@Result(name="change_email", type="ftl",location="/user/email_update.html")}
			,interceptorRefs={@InterceptorRef(value="mylogin")})
	public String doLogin() throws Exception {
		String url =paramString("returnurl");
		String returnUrl="/member/index.html"; 
		loginInfoCheck(); //校验登陆信息
		User u = null;
		u = userService.loginWithPhoneEmailName(user.getUsername(), user.getPassword());
		if (u == null) {
			String errorMsg = "用户不存在或密码错误！";
			logger.info(errorMsg);
			msg.append(errorMsg);
			message(msg.toString(), backUrl);
			return MSG;
		}else{
			//登陆规则验证
			Rule rule = ruleService.getRuleByNid("check_regiest");
			if(rule!=null){
				if(rule.getStatus() ==1 ){
					//是否需要验证邮箱是否激活
					if(rule.getValueIntByKey("check_email") == 1){
						if(u.getEmailStatus() != 1){
							//v1.8.0.4_u1 TGPROJECT-210 lx start
							String errorMsg = "您的邮箱未激活，请先激活再登陆！<input type='button' onclick='sendActiveEmail(\""+user.getEmail()+"\")'  class='btn-action' value='重新发送激活邮件' class='subphone' />";
							//v1.8.0.4_u1 TGPROJECT-210 lx end
							logger.info(errorMsg);
							msg.append(errorMsg);
							//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 start
							/*if(rule.getValueIntByKey("update_email") == 1){
								session.put("temp_user", u);
								request.setAttribute("temp_user", u);
								return "change_email";
							}else{*/
							//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 end
								message(msg.toString(), backUrl);
								return MSG;
								//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 start
								//}
								//v1.8.0.1 TGPROJECT-8 lx 2014-04-01 end
						}
					}
				}
			}
			if (u.getIslock() == 1) {
				String errorMsg = "该账户" + u.getUsername() + "已经被锁定！";
				msg.append(errorMsg);
				message(msg.toString(), backUrl);
				return MSG;
			}else if(!(u.getUserType().getTypeId() == 2 || u.getUserType().getTypeId()==3)){
				message("后台用户不可登陆前台!", backUrl);
				return MSG;
			} else {// 用户正常登陆
				Usertrack track = userService.getLastUserTrack(u.getUserId());// 获取本次信息
				
				if (track != null) {// 获取上次登录信息
					u.setLastip(this.getRequestIp());
					u.setLasttime(new Date());
//					u.setLasttime(track.getLoginTime());
//					u.setLastip(track.getLongIp());
					int drawCashStatus = userService.getUserDrawCashStatus(u);
					logger.info("实名认证状态：" + drawCashStatus);
					if(drawCashStatus > 0){ // 易极付实名认证 状态查询， 若果有就一起更新掉
						u.setRealStatus(1);
					}
					userService.updateUser(u);
				}
				session.put(Constant.SESSION_USER, u);
				TempIdentifyUser tempIdentifyUser = userService.inintTempIdentifyUser(u);
				if(tempIdentifyUser.getEmailStatus()==1 && tempIdentifyUser.getRealStatus()==1 &&tempIdentifyUser.getPhoneStatus()==1 && tempIdentifyUser.getPayPwdStatus()==1 && tempIdentifyUser.getUserBankStatus()==1){
					tempIdentifyUser.setAllStatus(1);
				}
			    session.put(Constant.TEMP_IDENTIFY_USER, tempIdentifyUser);
				
				//bbs in cookie cx 2014-03-24 start
				Cookie cookie=new Cookie("jforumUserInfo", URLEncoder.encode(u.getUsername(),"utf-8"));
				cookie.setMaxAge(-1);
				cookie.setPath("/");
				response.addCookie(cookie);
				// cx 2014-03-24 end
				
				// 插入用户登陆信息
				Usertrack newTrack = new Usertrack();
				newTrack.setUserId(u.getUserId());
				newTrack.setLongIp(this.getRequestIp() + "");
				newTrack.setLoginTime(new Date());
				userService.addUserTrack(newTrack);
				message(msg.toString(), returnUrl, "进入用户中心");
				//v1.8.0.4  TGPROJECT-169 qj 2014-04-22 start
				userCreditService.loginCredit(u);
				//v1.8.0.4  TGPROJECT-169 qj 2014-04-22 stop
				super.systemLogAdd(u, 5, "用户登录成功");
				
				MsgReq req = new MsgReq();
				req.setIp(super.getRequestIp());
				req.setReceiver(u);
				req.setSender(new User(Constant.ADMIN_ID));
				req.setMsgOperate(this.msgService.getMsgOperate(6));
				req.setTime(DateUtils.dateStr4(new Date()));
				DisruptorUtils.sendMsg(req);
				if (StringUtils.isBlank(url)) {
					return MEMBER;
				}else{
					response.sendRedirect(url);
					return null;
				}
			}
		}
	}
	private String loginInfoCheck(){
		backUrl = "/user/login.html";
		if(isSession()){//拦截登陆
			return MEMBER;
		}
		hasCookieValue();//没有保存cookie进行登录校验，有直接读取cookie进行登录  并设置参数。
		loginValidate(); // 执行登陆逻辑前校验是否存在非法数据，如果有直接返回错误页面
		//验证码			
		//checkValidImg();
		return null;
	}
	
	//fu 异步校验用户信息
	@Action(value="checkloginform",results={@Result(name="success", type="ftl",location="/index.html")})
	public String checkloginform(){	
		User u = null;
		String message="";
		u = userService.loginWithPhoneEmailName(user.getUsername(), user.getPassword());
		if (u == null) {
			message = "用户不存在或密码错误！";
		}else if(!(u.getUserType().getTypeId() == 2 || u.getUserType().getTypeId()==3)){
			message = "后台用户不可登陆前台!";
		}else{
			message = "1";
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("msg", message);
		printJson(JSON.toJSONString(map));
		return null;
	}
	
	
	//fu 异步校验用户信息
	@Action(value="active",results={@Result(name="success", type="ftl",location="/registerSuccess.html")})
	public String active() throws Exception {
		User user = null;
		String msg = "";
		String errormsg = "";
		String idstr = request.getParameter("id");
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] idstrBytes = decoder.decodeBuffer(idstr);
		String decodeidstr = new String(idstrBytes);
		String[] idstrArr = decodeidstr.split(",");
		if (idstrArr.length !=4) {
			errormsg = "解析激活码失败，请联系管理员！";
			message(errormsg,"/member/index.html");
			return MSG;
		} else {
			String useridStr = idstrArr[0];
			int user_id = Integer.parseInt(useridStr);
			if( user_id== 0){
				errormsg = "解析激活码失败，请联系管理员！";
				message(errormsg,"/member/index.html");
				return MSG;
			}
			user = userService.getUserById(NumberUtils.getLong(useridStr));
			if (user == null) {
					errormsg = "解析激活码失败，请联系管理员！";
					message(errormsg,"/member/index.html");
					return MSG;
			}
			if(user.getEmailStatus() == 1){
				errormsg = "您的邮箱已激活，请您查阅！！！";
				message(errormsg,"/member/index.html");
				return MSG;
			}
			String userIDAddtime = user.getUserId() + "" + DateUtils.getYYYYMMddHHmmss(user.getAddtime());
			MD5 md5=new MD5();
			userIDAddtime =  md5.getMD5ofStr(userIDAddtime);
			if(!userIDAddtime.equals(StringUtils.isNull(idstrArr[1]))){
				errormsg = "激活失败，请联系管理员！";
				message(errormsg,"/member/index.html");
				return MSG;
			}
			user.setEmailStatus(1);
			user.setStatus(1);
		    User newUser = userService.updateUserLastInfo(user);
			msg = "邮箱认证成功！";
			//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 start  
			if(getSessionUser()!=null){
				TempIdentifyUser tempIdentifyUser=userService.inintTempIdentifyUser(newUser);
				session.put(Constant.TEMP_IDENTIFY_USER, tempIdentifyUser);
			}
			//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 end
			//激活邮箱积分处理
			userCreditService.emailCredit(newUser);
			
			super.systemLogAdd(user, 11, "用户邮箱认证成功");
		}
		if (errormsg.equals("")) {
			message(msg,"/user/login.html","请点击登录");
		} else {
			message(errormsg,"/member/index.html");
		}
		return SUCCESS;
	}
	
	/**
	 * 用户注册第一步
	 * @return
	 * @throws Exception
	 */
	@Action("register")
	public String register() throws Exception{
		HttpSession requestSession = request.getSession(false);
		if(requestSession != null  && null != requestSession.getAttribute("user") ){//删除注册缓存中注册信息
			request.setAttribute("user", requestSession.getAttribute("user"));
			requestSession.removeAttribute("user");//删除缓存
		}
	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 start
		// 取出通过好友邀请链接的inviteUser
		String inviteUsername =  (String)session.get("inviteUsername");
		if (inviteUsername != null || !StringUtils.isBlank(inviteUsername)) {
			request.setAttribute("inviteUsername", inviteUsername);
		}
	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 end
		return REGISTER;
	}
	/**
	 * 用户注册第二步
	 */
	@Action(value="doReg2",results={@Result(name="success", type="ftl",location="/registerStep2.html")})
	public String doReg2() throws Exception {
		HttpSession requestSession = request.getSession(false);
		try {
			if(isSession()){
				return MEMBER;
			}
			requestSession.setAttribute("user", user);//放进缓存，当用户输入错误的时候，还原用户输入的信息，如果保存成功，后面记得删除。
			//验证码 这里加上
			if(!checkValidImg(StringUtils.isNull(request.getParameter("valicode")))){
				message("验证码不正确！","/zhuce.html");
				return MSG;
			}
			String flag = regValidate();
			// 执行登陆逻辑前校验是否存在非法数据，如果有直接返回错误页面
			if (flag.equals("fail")) {
				logger.debug("存在非法数据");
				return flag;
			}else if(flag.equals(MSG)){
				return flag;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info( e.getMessage());
			logger.error(e);
			message(e.getMessage()+",注册失败");
			request.setAttribute("user", requestSession.getAttribute("user"));
			requestSession.removeAttribute("user");//删除缓存
			return MSG;
		}
		return "success";
	}
	/**
	 * 获取手机验证码
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getPhoneCode")
	public String getPhoneCode() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		boolean isSend = false;
		String message = "";
		String codeUniqueId = "";
		int phoneType = Global.getInt("phone_type");
		String phone = StringUtils.isNull(request.getParameter("phone"));
		try {
			if(user.getPhoneStatus()!=1){//如果用户没有绑定手机，这校验此项
				int phoneResult = userService.getPhoneOnly(phone); // 查询手机号码是否存在
				if (phoneResult > 0) {
					throw new BussinessException("手机号码已经存在！");
				}
			}

			if (phone == null || phone.equals("")) {
				throw new BussinessException("手机号码不能为空！");
			} else {
				if (!StringUtils.isMobile(StringUtils.isNull(phone))) {
					throw new BussinessException("手机号码格式不对！");
				}
			}
			// 设置标记没5分钟发标能发一次请求
			checkCanGetMobileCode(1);
			//短信发送限制 防止恶刷 2016-03-21----------------------------------------------------------------------------
		    Date dt = new Date();
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");   
		    String addTime1=sdf.format(dt);  
		    //该号码今天发送次数
			int tCount = dao.count("send_sms_log", Cnd.where("phone", "=", phone).and("addtime",">",addTime1+" 00:00:00").and("count","=","0").and("status","=","1"));
			if(tCount>5){
				map.put("msg", "该号码获取验证码已达今天最高限次，请明日再试，或联系管理员！");
				printJson(JSON.toJSONString(map));
				return null;
			}
			//该ip今天发送次数
			int ipCount = dao.count("send_sms_log", Cnd.where("addip", "=", this.getRequestIp()).and("addtime",">",addTime1+" 00:00:00").and("count","=","0").and("status","=","1"));
			if(ipCount>30){
				map.put("msg", "该IP地址获取验证码已达今天最高限次，请明日再试，或联系管理员！");
				printJson(JSON.toJSONString(map));
				return null;
			}
			//该今天发送验证码总次数
			int regCount = dao.count("send_sms_log", Cnd.where("addtime",">",addTime1+" 00:00:00").and("count","=","0").and("status","=","1"));
			if(regCount>50){
				map.put("msg", "今天注册名额已用完，请明日再试，或联系管理员！");
				printJson(JSON.toJSONString(map));
				return null;
			}
			//短信发送限制 防止恶刷----------------------------------------------------------------------------
			codeUniqueId = madeMobileCode(phoneType, phone, user);
			if (!StringUtils.isBlank(codeUniqueId)) {
				//v1.8.0.4  TGPROJECT-145  qj 2014-4-22 start
				message = "验证码已经发送，请注意查收，"+ Global.getInt("get_phoneCode_time")+"分钟后可以重复获取";
				//v1.8.0.4  TGPROJECT-145  qj 2014-4-22 stop
				isSend = true;
			}
		} catch (Exception e) {
			logger.error(e);
			message = e.getMessage();
			map.put("data", message);
			printJson(JSON.toJSONString(map));
		}
		map.put("data", message);
		map.put("mobile_token", saveToken("mobile_token"));
		map.put("isSend", isSend+"");
		if(!"1".equals(Global.getValue("config_online"))) {
		map.put("code", codeUniqueId);
		}
		session.put(phone, codeUniqueId);  //将验证码放入session中，在手机校验完了以后，删除此session
		printJson(JSON.toJSONString(map));
		return null;
	}
	/**
	 * 用户注册
	 */
	@Action(value="doRegister",results={@Result(name="success", type="ftl",location="/registerSuccess.html")})
	public String doRegister() throws Exception {
		HttpSession requestSession = request.getSession(false);
		String phone = paramString("phone");
		String password = paramString("password");
		String codeUniqueId = "";
		String code = paramString("code");
		String phoneUrl = "/zhuce.html";
		try {
			if(isSession()){
				return MEMBER;
			}
			requestSession.setAttribute("user", user);//放进缓存，当用户输入错误的时候，还原用户输入的信息，如果保存成功，后面记得删除。
			
			//HAOLIP-86  lx  2014-04-24 start
			//从session中获取手机验证码，校验手机号码是否错误
			codeUniqueId = String.valueOf(session.get(phone)); 
			if (StringUtils.isBlank(codeUniqueId) || codeUniqueId.equalsIgnoreCase("null")) {
				throw new BussinessException("手机号码错误，请核对后重获取验证码提交认证请求！", phoneUrl);
			}

			if (!code.equals(codeUniqueId)){
				throw new BussinessException("输入验证码有误！请重新获取验证码进行验证", phoneUrl);
			}
			
			user.setPhoneStatus(1);
			user.setPhone(phone);
			
			//推荐人
			//添加推荐人
			String invite_username = paramString("invite_username"); 
			InviteUser inviteUser = null;
			if(!StringUtils.isBlank(invite_username)){
				User invite_user = userService.getUserByNameEmailPhone(invite_username);
				if(invite_user!=null){
					inviteUser = new InviteUser();
					inviteUser.setInviteUser(invite_user);
					inviteUser.setStatus(0);
					inviteUser.setAddtime(new Date());
					inviteUser.setAddip(getRequestIp());
				}
			}
			
			// 密码MD5加密
			MD5 md5 = new MD5();
			user.setPassword(md5.getMD5ofStr(password));
			//v1.8.0.1_u1 TGPROJECT-226 start
			Rule payPwdRule= ruleService.getRuleByNid("add_paypassword");
			if(payPwdRule!=null){
				if(payPwdRule.getStatus()==1){
					user.setPaypassword(md5.getMD5ofStr(password));
				}
			}
			//v1.8.0.1_u1 TGPROJECT-226 end
			user.setAddtime(new Date());
			user.setAddip(this.getRequestIp());
			//生成用户名
			user.setUsername("zch"+user.getPhone());
			userService.register(user,inviteUser);
			
			//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 start  
//			User tempUser = userService.getUserByNameEmailPhone(phone);
//			if (tempUser != null) {
//				TempIdentifyUser tempIdentifyUser=userService.inintTempIdentifyUser(tempUser);
//				session.put(Constant.TEMP_IDENTIFY_USER, tempIdentifyUser);
//			}
			//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 end
			//手机认证成功，积分处理
//			userCreditService.phoneCredit(tempUser);
//			super.systemLogAdd(user, 12, "用户手机认证成功");
			
			//v1.8.0.4  重要：验证码正确，清掉缓存中的缓存的验证码 start
			session.remove(phone);  //删除内存中存放的手机验证码信息，清楚缓存
			//v1.8.0.4 end
			
			requestSession.setAttribute("session_user", user);
			
			//同步UCenter
			// 开通论坛的时候在开启
			/*
			if("1".equals(Global.getValue("is_open_bbs"))){
				try {
					UCenterHelper.ucenter_register(user.getUsername(), pwd, user.getEmail());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}*/
			//v1.8.0.4_u1 TGPROJECT-210 lx start
		
			//message(msg.toString(), retUrl, retStr);
			request.setAttribute("resent", msg.toString());
			
			//注册成功，发送信息给用户，发送激活邮件不在这里，这里只是发送一些通知行的消息
			MsgReq req1 = new MsgReq();
			req1.setIp(super.getRequestIp());
			req1.setSender(new User(Constant.ADMIN_ID));
			req1.setReceiver(user);
			req1.setMsgOperate(this.msgService.getMsgOperate(24));//注册 通知用户
			req1.setUsername(user.getUsername());			
			DisruptorUtils.sendMsg(req1);
			
			Rule webRegisterRule = ruleService.getRuleByNid("register_web_email"); 
			if(webRegisterRule!= null &&webRegisterRule.getStatus() == 1){
				MsgReq req_web = new MsgReq();
				req_web.setIp(super.getRequestIp());
				req_web.setSender(new User(Constant.ADMIN_ID));
				req_web.setReceiver(user);
				req_web.setMsgOperate(this.msgService.getMsgOperateByCode(MsgOperateCode.WEB_REGISTER));//注册 通知用户
				req_web.setUsername(user.getUsername());			
				DisruptorUtils.sendMsg(req_web);
			}
			User u=userService.getUserByNameEmailPhone(phone);
			if(u!=null) {
			//上上签注册
				Api br =new Api();
				JSONObject res = br.userReg("ssq"+u.getUserId(), null, u.getUsername(), u.getCustomerType().equals(ChinaPnrType.PERSONAL)?"1":"2", null);
			if(res.getString("errno").equals("0")) {
				//TODO 注册成功操作
				logger.info( u.getUserId()+"上上签注册成功");
			}
			logger.info(res);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info( e.getMessage());
			logger.error(e);
			message(e.getMessage()+",注册失败");
			request.setAttribute("user", requestSession.getAttribute("user"));
			requestSession.removeAttribute("user");//删除缓存
			requestSession.removeAttribute("session_user");//删除缓存
			return MSG;
		}
		return "success";
	}
	
	//v1.8.0.3_u3  TGPROJECT-332  qinjun 2014-06-09 start 
//	@Action("updateUserProperty")
//	public String updateUserProperty(){
//		userService.initUserProperty();
//		message("更新成功！");
//		return MSG;
//	}
	//v1.8.0.3_u3  TGPROJECT-332  qinjun 2014-06-09 end 
	
	/**
	 * 判断浏览器中是否有cookie保证用户密码
	 * @return
	 */
	private boolean hasCookieValue(){
		String username="";
		String password="";
		Cookie[]  cookies= request.getCookies();
		if (cookies!=null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if ("cookie_username".equals(StringUtils.isNull(cookie.getName()))) {
					username = cookie.getValue();
				}
				if ("cookie_password".equals(StringUtils.isNull(cookie.getName()))) {
					password = cookie.getValue();
				}
			}
			if(StringUtils.isBlank(username)||StringUtils.isBlank(password)){
				return false;
			}else{
				user.setUsername(username);
				user.setPassword(password);
				return true;
			}
		}
		return false;
	}
	
	private String regValidate() {
		String flag = loginValidate();
		if (flag.equals("fail")) {
			return MSG;
		}
//		User existUser = userService.getUserByName(user.getUsername());
//		if (existUser != null) {
//			String errorMsg = "用户名已经存在！";
//			logger.info(errorMsg);
//			msg.append(errorMsg);
//			message(msg.toString(), backUrl);
//			return MSG;
//		}
		int phoneResult = userService.getPhoneOnly(user.getPhone()); // 查询手机号码是否存在
		if (phoneResult > 0) {
			String errorMsg = "手机号码已经存在！";
			logger.info(errorMsg);
			msg.append(errorMsg);
			message(msg.toString(), backUrl);
			return MSG;
		}
//		else if(!StringUtils.checkUsername(user.getUsername())){
//			String errorMsg = "请输入4-15位由字母和数字组成的用户名";
//			logger.info(errorMsg);
//			msg.append(errorMsg);
//			message(msg.toString(), backUrl);
//			return MSG;
//		}else if(!StringUtils.deniedUsername(user.getUsername())){
//			String errorMsg = "用户名存在非法字符";
//			logger.info(errorMsg);
//			msg.append(errorMsg);
//			message(msg.toString(), backUrl);
//			return MSG;
//		}
//		if (!StringUtils.isEmail(user.getEmail())) {
//			String errorMsg = "邮箱格式不对！";
//			logger.info(errorMsg);
//			msg.append(errorMsg);
//			message(msg.toString(), backUrl);
//			return MSG;
//		}
//		boolean isHasEmail = userService.checkEmail(user.getEmail());
//		if (!isHasEmail) {
//			String errorMsg = "邮箱已经存在！";
//			logger.info(errorMsg);
//			msg.append(errorMsg);
//			message(msg.toString(), backUrl);
//			return MSG;
//		}
		
		String checkPwd=paramString("confirm_password");   //获取确认密码后台做比较
		//校验用户输入两次输入的密码是否正确
		if (!user.getPassword().equals(checkPwd)) {
			message("两次填写的密码不一致，请核对密码后重新提交注册信息！");
			return MSG;
		}
		if(!(user.getPassword().length()>=8 && user.getPassword().length() <=16)){
			message("注册的登陆密码长度必须在8-16位!");
			return MSG;
		}
		if (!(StringUtils.pwdContainStr(user.getPassword())&&StringUtils.pwdContainNum(user.getPassword()))) {
			message("注册的登陆密码不能为纯数字或者纯字母模式，请添加复杂的密码!");
			return MSG;
		}
		
		return "success";
	}
	// 校验表单信息是否非法
	private String loginValidate() {
		// 检查用户名和密码是否为空
		if (StringUtils.isNull(user.getUsername()).equals("")) {
			String errorMsg = "用户名不能为空！";
			logger.info(errorMsg);
			msg.append(errorMsg);
			message(msg.toString(), backUrl);
			return MSG;
		}
		if (StringUtils.isNull(user.getPassword()).equals("")) {
			String errorMsg = "密码不能为空！";
			logger.info(errorMsg);
			msg.append(errorMsg);
			message(msg.toString(), backUrl);
			return MSG;
		}
		return "success";
	}

	@Action("checkUsername")
	public String checkUsername() throws Exception {
		boolean result = userService.checkUsername(user.getUsername());
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		// 直接输入响应的内容
		if (result) {
			out.println("1");
		} else {
			out.println("");
		}
		out.flush();
		out.close();
		return null;
	}

	/**
	 * 用于用户注册时，判断Email是否存在Action   东方贷  手机 和  身份证号 验证  也放在这里边 
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action("checkEmail")
	public String checkEmail() throws Exception {
		boolean result = userService.checkEmail(user.getEmail());
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		// 直接输入响应的内容
		if (result) {
			out.println("1");
		} else {
			out.println("");
		}
		out.flush();
		out.close();
		return null;
	}
	
	@Action("logout")
	public String logout() throws Exception {
		Map session = (Map) ActionContext.getContext().getSession();
		session.put("session_user", null);
		//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 start  
		session.put(Constant.TEMP_IDENTIFY_USER, null);
		//v1.8.0.4 TGPROJECT-61 lx 2014-04-15 end
		// remove cookie cx 2014-03-24
		Cookie cookie=new Cookie("jforumUserInfo", "");
		cookie.setMaxAge(0);
		cookie.setPath("/");
		response.addCookie(cookie);
		
		return LOGIN;
	}
	
	@Action(value="updateuser",results=
			{@Result(name="success",type="ftl",location="/admin/userinfo/user.html")
		})
	public String updateUser() {
		String actionType=StringUtils.isNull(request.getParameter("actionType"));
		User newUser = userService.getUserById(user.getUserId());
		if (newUser != null) {
			newUser.setUptime(this.getTimeStr());
			newUser.setUpip(this.getRequestIp());
		}
		String realname  =user.getRealname();
		String sex = user.getSex();
		String birthday = user.getBirthday();
		int kefu_userid = paramInt("kefu_userid");
		int typeId = paramInt("typeId");
		int islock = paramInt("islock");
		long  invite_userid = paramLong("invite_userid");
		String  inviteMoney = paramString("inviteMoney");
		int status = paramInt("status");
		int province = paramInt("province");
		int city = paramInt("city");
		int area = paramInt("area");
		String cardId = user.getCardId();
		String cardType = paramString("cardType");
		String email = paramString("email");
		String qq = paramString("qq");
		String wangwang = paramString("wangwang");
		String tel = paramString("tel");
		String phone = paramString("phone");
		String address = paramString("address");
		if(kefu_userid!=0){
			UserCache uc = newUser.getUserCache();
			uc.setKefuUserid(kefu_userid);
			newUser.setUserCache(uc);
		}		
		newUser.setRealname(realname);
		newUser.setSex(sex);
		newUser.setBirthday(birthday);
		newUser.setIslock(islock);
		newUser.setInviteMoney(inviteMoney);
		newUser.setStatus(status);
		if(province!=0) {
			Area provinceObj = new Area();
			provinceObj.setId(province);
			newUser.setProvince(provinceObj);
		}
		if(city!=0) {
			Area cityObj = new Area();
			cityObj.setId(city);
			newUser.setCity(cityObj);
		}
		if(area!=0) {
			Area areaObj = new Area();
			areaObj.setId(area);
			newUser.setArea(areaObj);
		}
		
		newUser.setCardId(cardId);
		newUser.setCardType(cardType);
		newUser.setEmail(email);
		newUser.setQq(qq);
		newUser.setWangwang(wangwang);
		newUser.setPhone(phone);
		newUser.setAddress(address);
		newUser.setTel(tel);
		userService.updateUser(newUser);
		//更新内存中的客服列表		
		Global.ALLKEFU = userService.getAllKefu();
		context.setAttribute("allkefu",Global.ALLKEFU);
		
		if(StringUtils.isBlank(actionType)){
			msg.append("更新成功");
			message(msg.toString(), "");
			return SUCCESS;
		}else{
			message("更新成功","/admin/userinfo/user.html");
			User auth_user = getSessionUser();
			super.systemLogAdd(auth_user, 30, "管理员修改用户密码成功");
			return ADMINMSG;
		}
	}
	
	@Action(value="getpwd",results={
			@Result(name="success",type="ftl",location="/getpwd.html"),
			@Result(name="fail",type="ftl",location="/getpwd.html")
	})
	public String getpwd() throws Exception {
		if (isBlank()) {
			request.setAttribute("getpwdtype", "pwd");
			return SUCCESS;
		}
		
		String username = request.getParameter("username");
		String validcode = request.getParameter("validcode");
		String inputEmail =request.getParameter("email");
		logger.info(inputEmail);
		request.setAttribute("getpwdtype", "pwd");
		User u = null;
		if (StringUtils.isNull(validcode).equals("")) {			
			throw new BussinessException("验证码不能为空！","/user/getpwd.html");
		} else if (StringUtils.isNull(username).equals("")) {
			throw new BussinessException("用户名不能为空！","/user/getpwd.html");
		} else if (!this.checkValidImg(validcode)) {
			throw new BussinessException("验证码不正确！","/user/getpwd.html");
		}else{
			u = userService.getUserByNameEmailPhone(username);
			if (u == null) {
				throw new BussinessException("用户未注册！","/user/getpwd.html");				
			}else {
				sendGetPwdMail(u);
			}
		}
		String email = u.getEmail();
		email = (email.substring(0,2)).concat("*").concat(email.substring(email.indexOf('@')));
		this.message("找回密码邮件发送至" + email + "！", "/user/login.html");
		return MSG;
		
	}	
	//v1.0.8.4 TGPROJECT-55 lx 2014-04-14 start
	//v1.8.0.4_u1 TGPROJECT-210 lx start
	@Action(value = "sendActiveEmail")
	public String sendActiveEmail() throws Exception {
		String message = "";
		String email = StringUtils.isNull(request.getParameter("email"));
		User user=userService.getUserByNameEmailPhone(email);
		try {
			if (user == null) {
				throw new BussinessException("非法访问");
			}
			if(user.getEmailStatus()==1){
				throw new BussinessException("邮箱已经认证");
			}
			sendActiveMail(user);
			message = "激活邮件已经重新发送，请及时激活";
		} catch (Exception e) {
			logger.error(e);
			message = e.getMessage();
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("data", message);
		printJson(JSON.toJSONString(map));	
			
		return null;
	}
	//v1.8.0.4_u1 TGPROJECT-210 lx start
	private void sendActiveMail(User user) throws Exception {
		String to = user.getEmail();
		Mail m = Mail.getInstance();
		m.setSubject(Global.getValue("email_from_name"));
		m.setTo(to);
		m.readActiveMailMsg();
		m.replace(user.getUsername(), to, "/user/active.html?id="+m.getdecodeIdStr(user));
		logger.debug("Email_msg:" + m.getBody());
		m.sendMail();
	}
	
	
	@Action(value="show",results={
			@Result(name="success",type="ftl",location="/user/show.html")
	})
	public String show() throws Exception {
		User user = userService.getUserById(paramLong("user_id"));
		Userinfo userinfo = user.getUserinfo();
		
		UserAccountSummary uas = accountService.getUserAccountSummary(user.getUserId());
		SearchParam borrowParam = SearchParam.getInstance();
		borrowParam.addParam("user", user).addOrFilter("status", 3, 6, 7, 8);
		PageDataList borrowList =  borrowService.getList(borrowParam);
		SearchParam investParam = SearchParam.getInstance();
		investParam.addParam("user", user);
		PageDataList investList = borrowService.getTenderList(investParam);
		List attestations = user.getAttestations();
		request.setAttribute("u", user);
		request.setAttribute("info", userinfo);
		request.setAttribute("summary", uas);
		request.setAttribute("borrowList", borrowList.getList());
		request.setAttribute("investList", investList.getList());
		request.setAttribute("attestations", attestations);
		//logger.info(showUser);
		return "success";
	}
	
	private void sendGetPwdMail(User user) throws Exception {
		String to = user.getEmail();
		Mail m = Mail.getInstance();
		m.setTo(to);
		m.readGetpwdMailMsg();
		m.replace(user.getUsername(), to,
				"/user/setpwd.html?id=" + m.getdecodeIdStr(user));
		logger.debug("Email_msg:" + m.getBody());
		m.sendMail();
	}

	@Action(value="setpwd",results={
		@Result(name="success",type="ftl",location="/setpwd.html"),
		@Result(name="fail",type="ftl",location="/msg.html"),
		@Result(name="ok",type="ftl",location="/msg.html")			
	})
	public String setpwd() throws Exception {
		String type = StringUtils.isNull(request.getParameter("type"));
		if (type.equals("")) {// 进入找回密码界面
			String idstr = StringUtils.isNull(request.getParameter("id"));
			BASE64Decoder decoder = new BASE64Decoder();
			byte[] idstrBytes = decoder.decodeBuffer(idstr);
			String decodeidstr = new String(idstrBytes);
			String errormsg = "";
			// [user_id,timestr,email,rancode]
			String[] idstrArr = decodeidstr.split(",");
			if (idstrArr.length < 4) {
				errormsg = "链接无效，请<a href='" + request.getContextPath()
						+ "/getpwd.html'>点此</a>重新获取邮件！";
				this.message(errormsg, "/user/getpwd.html");
				return FAIL;
			}
			long setpwdTime = NumberUtils.getLong(idstrArr[2]);
			if (System.currentTimeMillis() - setpwdTime * 1000 > 1 * 24 * 60
					* 60 * 1000) {
				errormsg = "链接有效时间1天，已经失效，请<a href='"
						+ request.getContextPath()
						+ "/user/getpwd.html'>点此</a>重新获取邮件!";
				this.message(errormsg, "/user/getpwd.html");
				return FAIL;
			}
			User u = userService.getUserById(NumberUtils.getLong(idstrArr[0]));
			if (u == null) {
				errormsg = "用户不存在，请联系客服!";
				this.message(errormsg, "/user/getpwd.html");
				return FAIL;
			}
			request.setAttribute("u", u);
			return SUCCESS;
		} else {// 重新设置密码
			String username = StringUtils.isNull(request
					.getParameter("username"));
			String password = StringUtils.isNull(request
					.getParameter("password"));
			String confirm_password = StringUtils.isNull(request
					.getParameter("confirm_password"));
			String errormsg = "";
			User user = userService.getUserByName(username);
			if (password.equals("") || confirm_password.equals("")) {
				errormsg = "新密码不能为空！";
			} else if (!password.equals(confirm_password)) {
				errormsg = "两次密码不一致！";
			}else if(!(password.length()>=8 && password.length() <=16)){
				errormsg = "注册的登陆密码长度必须在8-16位!";
			}else if (!(StringUtils.pwdContainStr(password)&&StringUtils.pwdContainNum(password))) {
				errormsg = "登陆密码不能为纯数字或者纯字母模式，请添加复杂的密码!";
			}else if (user == null) {
				errormsg = username + "不存在！";
			} else {
				MD5 md5 = new MD5();
				user.setPassword(md5.getMD5ofStr(password));
				//userService.modifyUserPwd(user);
				userService.updateUser(user);
				this.message("重置密码成功！", "/user/login.html", "点此登录");
				return OK;
			}
			request.setAttribute("u", user);
			request.setAttribute("errormsg", errormsg);
			return SUCCESS;
		}
	}
	

	@Action(value="getpaypwd",results={
		@Result(name="success",type="ftl",location="/getpaypwd.html"),
		@Result(name="fail",type="ftl",location="/msg.html"),
		@Result(name="ok",type="ftl",location="/msg.html")			
	})
	public String getpaypwd() throws Exception {
		if (isBlank()) {
			request.setAttribute("getpwdtype", "paypwd");
			return SUCCESS;
		}
		
		String username = request.getParameter("username");
		String validcode = request.getParameter("validcode");
		String inputEmail =request.getParameter("email");
		request.setAttribute("getpwdtype", "paypwd");
		User u = null;
		if (StringUtils.isNull(validcode).equals("")) {
			this.message("验证码不能为空！");
			return MSG;
		} else if (StringUtils.isNull(username).equals("")) {
			this.message("用户名不能为空！");
			return MSG;
		} else if (!this.checkValidImg(validcode)) {
			this.message("验证码不正确！");
			return MSG;
		} else {
			u = userService.getUserByName(username);
			
			if (u == null){
				this.message("用户名未注册！");
				return MSG;
				
			} else {
				sendGetPayPwdMail(u);
			}
		}
		String email = u.getEmail();
		email = (email.substring(0,2)).concat("*").concat(email.substring(email.indexOf('@')));
		this.message("找回支付密码邮件发送至" + email + "！", "/member/index.html");
		
		return MSG;
	}
	
	@Action(value="setpaypwd",results={
			@Result(name="success",type="ftl",location="/setpwd.html"),
			@Result(name="fail",type="ftl",location="/msg.html"),
			@Result(name="ok",type="ftl",location="/msg.html")			
		})
	public String setpaypwd() throws Exception {
		String type = StringUtils.isNull(request.getParameter("type"));
		if (type.equals("")) {// 进入找回密码界面
			String idstr = StringUtils.isNull(request.getParameter("id"));
			BASE64Decoder decoder = new BASE64Decoder();
			byte[] idstrBytes = decoder.decodeBuffer(idstr);
			String decodeidstr = new String(idstrBytes);
			String errormsg = "";
			// [user_id,timestr,email,rancode]
			String[] idstrArr = decodeidstr.split(",");
			if (idstrArr.length < 4) {
				errormsg = "链接无效，请<a href='" + request.getContextPath()
						+ "/getpaypwd.html'>点此</a>重新获取邮件！";
				this.message(errormsg, "/getpaypwd.html");
				return MSG;
			}
			long setpwdTime = NumberUtils.getLong(idstrArr[2]);
			if (System.currentTimeMillis() - setpwdTime * 1000 > 1 * 24 * 60
					* 60 * 1000) {
				errormsg = "链接有效时间1天，已经失效，请<a href='"
						+ request.getContextPath()
						+ "/getpaypwd.html'>点此</a>重新获取邮件!";
				this.message(errormsg, "/getpaypwd.html");
				return MSG;
			}
			User u = userService.getUserById(NumberUtils.getLong(idstrArr[0]));
			if (u == null) {
				errormsg = "用户不存在，请联系客服!";
				this.message(errormsg, "/getpaypwd.html");
				return MSG;
			}
			request.setAttribute("u", u);
			return SUCCESS;
		}else {// 重新设置密码
			String username = StringUtils.isNull(request
					.getParameter("username"));
			String password = StringUtils.isNull(request
					.getParameter("password"));
			String confirm_password = StringUtils.isNull(request
					.getParameter("confirm_password"));
			String errormsg = "";
			User user = userService.getUserByName(username);
			if (password.equals("") || confirm_password.equals("")) {
				errormsg = "新密码不能为空！";
			} else if (!password.equals(confirm_password)) {
				errormsg = "两次密码不一致！";
			}else if(!(password.length()>=8 && password.length() <=16)){
				errormsg = "注册的登陆密码长度必须在8-16位!";
			}else if (!(StringUtils.pwdContainStr(password)&&StringUtils.pwdContainNum(password))) {
				errormsg = "注册的登陆密码不能为纯数字或者纯字母模式，请添加复杂的密码!";
			}else if (user == null) {
				errormsg = username + "不存在！";
			} else {
				MD5 md5 = new MD5();
				user.setPaypassword(md5.getMD5ofStr(password));
				userService.updateUser(user);
				this.message("重置密码成功！", "/user/login.html", "点此登录");
				return MSG;
			}
			request.setAttribute("u", user);
			request.setAttribute("errormsg", errormsg);
			return SUCCESS;
		}
	}
	
	private void sendGetPayPwdMail(User user) throws Exception {
		try {
			String to = user.getEmail();
			Mail m = Mail.getInstance();
			m.setTo(to);
			m.readGetpwdMailMsg();
			m.replace(user.getUsername(), to,
					"/user/setpaypwd.html?id=" + m.getdecodeIdStr(user));
			logger.debug("Email_msg:" + m.getBody());
			m.sendMail();
		} catch (Exception e) {
			this.message("邮箱服务器错误，发送失败", "/user/login.html");
		}
		
	}
	
	
	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 start
	@Action(value="regInvite",results={
			@Result(name="success",type="ftl",location="/register.html")
		})
	public String regInvite() throws Exception {
		//是否启用验证码规则
		String u = paramString("u");
		BASE64Decoder decoder = new BASE64Decoder();
		String user_id = decoder.decodeStr(u);
		long userid = NumberUtils.getLong(user_id);
		User inviteUser = userService.getUserById(userid);
		String inviteUsername="";
		if(inviteUser!=null){
			inviteUsername=inviteUser.getUsername();
		}
		request.setAttribute("inviteUsername", inviteUsername);
		session.put("inviteUsername", inviteUsername);
		return SUCCESS;
	}
	//  v1.8.0.4 TGPROJECT-60 lx 2014-04-15 end
	
	//v1.8.0.4_u4  XINHEHANG-179  qinjun 2014-04-15  start
	@Action("checkPhoneToJson")
	public String checkPhoneToJson(){
		String phone = paramString("phone");
		boolean isSuccess = true;
		String msg = "";
		if(!StringUtils.isBlank(phone)){
			if (!StringUtils.isMobile(StringUtils.isNull(phone))) {
				isSuccess = false;
				msg = "手机号码格式不对！";
			}
			int phoneResult = userService.getPhoneOnly(phone); // 查询手机号码是否存在
			if (phoneResult > 0) {
				isSuccess = false;
				msg = "手机号码已经存在！";
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("data", isSuccess+"");
		map.put("msg", msg);
		printJson(JSON.toJSONString(map));
		return null;
	}
	//v1.8.0.4_u4  XINHEHANG-179  qinjun 2014-04-15  start
}

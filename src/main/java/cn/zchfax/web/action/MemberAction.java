package cn.zchfax.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import cn.zchfax.api.chinapnr.ChinapnrHelper;
import cn.zchfax.api.chinapnr.FssTrans;
import cn.zchfax.api.chinapnr.UsrAcctPay;
import cn.zchfax.api.moneymoremore.MmmHelper;
import cn.zchfax.api.moneymoremore.MmmLoanAuthorize;
import cn.zchfax.api.pay.DeductSign;
import cn.zchfax.api.pay.PayModelHelper;
import cn.zchfax.api.pay.SignmanyBank;
import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.AccountBank;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.Credit;
import cn.zchfax.domain.InterestGenerate;
import cn.zchfax.domain.InviteUser;
import cn.zchfax.domain.Rule;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserAmount;
import cn.zchfax.domain.UserCache;
import cn.zchfax.domain.UserCreditLog;
import cn.zchfax.domain.UserType;
import cn.zchfax.domain.Userinfo;
import cn.zchfax.domain.Usertrack;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchFilter;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.OrderFilter.OrderType;
import cn.zchfax.model.SearchFilter.Operator;
import cn.zchfax.model.account.UserAccountSummary;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.ApiService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.MessageService;
import cn.zchfax.service.RewardExtendService;
import cn.zchfax.service.RuleService;
import cn.zchfax.service.UserCreditService;
import cn.zchfax.service.UserService;
import cn.zchfax.service.UserinfoService;
import cn.zchfax.tool.coder.BASE64Encoder;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.OrderNoUtils;
import cn.zchfax.util.StringUtils;

@Namespace("/member")
@ParentPackage("p2p-default") 

public class MemberAction extends BaseAction{

	private static final long serialVersionUID = -2578417106463783748L;
	private static Logger logger = Logger.getLogger(MemberAction.class);
	@Autowired
	UserService userService;
	@Autowired
	AccountService accountService;
	@Autowired
	UserinfoService userinfoService;
	@Autowired
	MessageService messageService;
	@Autowired
	private RuleService ruleService;
	@Autowired
	ApiService apiService;
	@Autowired
	private UserCreditService userCreditService;
	@Autowired
	private BorrowService borrowService;
	@Autowired
	private RewardExtendService rewardExtendService;
	
	/**
	 * 用户中心模块
	 * @return
	 * @throws Exception
	 */
	@Action("my")
	public String my() throws Exception {
		return SUCCESS;
	}
	 
	@Action(value="index",results={
			@Result(name="success", type="ftl",location="/member/main.html"),
			@Result(name="login", type="redirect",location="/user/login.html")
			})
	public String index() throws Exception {
		try {
			User sessionUser = getSessionUser();
			if (sessionUser == null) {
				return "login";
			} 
			long user_id = sessionUser.getUserId();
			User user  = userService.getUserById(user_id);  //查找用户的信息。
			UserCache cache = user.getUserCache();  //用户扩展信息
			UserAmount amount = user.getUserAmount(); // 信用额度
			Account account = user.getAccount(); // 账户信息表
			Credit  credit  = user.getCredit();  //用户信息表
			UserType userType = user.getUserType(); // 用户类型
			Userinfo userInfo = user.getUserinfo();
			Usertrack track = userService.getLastUserTrack(user_id);//获取用户上次登录的时间
			String ip=this.getRequestIp();//获取用户IP
			String city = this.getAreaByIp();
			UserAccountSummary summary = accountService.getUserAccountSummary(user_id);
			double userSumIncome = userService.getSumUserIncome(user);  //用户总收益，包含生利宝利息
			//未读站内信
			int unRead = messageService.getUnreadMessageCount(user);
			request.setAttribute("user", user);
			request.setAttribute("cache", cache);
			request.setAttribute("amount", amount);
			request.setAttribute("account", account);
			request.setAttribute("credit", credit);
			request.setAttribute("userType", userType);
			request.setAttribute("userinfo", userInfo);
			request.setAttribute("nid", "member");
			request.setAttribute("summary", summary);
			request.setAttribute("unRead", unRead);
			request.setAttribute("track", track);
			request.setAttribute("ip", ip);
			request.setAttribute("city", city);
			request.setAttribute("userSumIncome", userSumIncome);
			double useMoney = summary.getAccountUseMoney();//可用金额
			double NoUseMoney = summary.getAccountNoUseMoney();//冻结金额
			double collectTotal = summary.getCollectTotal();//待收总额
			double accountTotal = summary.getAccountTotal();//账户总额
			double useMoneyPct =0;//可用金额百分比
			double NoUseMoneyPct = 0;//冻结金额百分比
			double collectTotalPct = 0;//待收总额百分比
			if(accountTotal>0){
				useMoneyPct = useMoney*100/accountTotal;
				NoUseMoneyPct = NoUseMoney*100/accountTotal;
				collectTotalPct =collectTotal*100/accountTotal;
			}
			request.setAttribute("useMoneyPct",useMoneyPct);
			request.setAttribute("NoUseMoneyPct",NoUseMoneyPct);
			request.setAttribute("collectTotalPct",collectTotalPct);			
		    if(isOnlineConfig()){//线上
		    	request.setAttribute("load_url", "https://ppm.yiji.com:8443/index.htm");
		    }else{
		    	request.setAttribute("load_url", "http://ppm.yijifu.net:8605/index.htm");
		    }
		    request.setAttribute("webid", Global.getValue("webid"));
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}

		return "success";
	}
	
	@Action(value="vip",results={
			@Result(name="success", type="ftl",location="/member/vip.html"),
			@Result(name="fail", type="ftl",location="/msg.html"),
			@Result(name="payvip", type="ftl",location="/member/huifu/UsrAcctPay.html")
			})
	public String vip() throws Exception {
		User sessionUser=this.getSessionUser();
		User user = userService.getUserById(sessionUser.getUserId());
		String type=request.getParameter("type");
		String vipRemark=StringUtils.isNull(request.getParameter("vip_remark"));
		if(vipRemark.length()>200){
			throw new BussinessException("备注字数最大不能超过200个字符");
		}
		int kefu_userid = paramInt("kefu_userid");
		String kefu_username = "";
		
		//第三方拦截
		apiService.checkApiLoan(user);
		
		UserCache uc=user.getUserCache();
		Account account = user.getAccount();
		if(StringUtils.isNull(type).equals("")){
			List list=userService.getAllKefu();
			double vipFee = Global.getDouble("vip_fee");  //获取vip的费用
			request.setAttribute("vipFee", vipFee);
			request.setAttribute("act", account);
			request.setAttribute("kflist", list);
		}else{
			Rule rule = ruleService.getRuleByNid("vip_check_kefu");
			if (rule != null) {
				if (rule.getStatus() == 1) { //校验此规则是否启用
					int rulerStatus=rule.getValueIntByKey("status");
					if (rulerStatus == 1 ) {//校验客服必须选择
						if(kefu_userid == 0){						
							message("请选择客服!","/member/vip.html");
							return MSG;
						}
					}
				}
			}
			
			if(kefu_userid!=0 && userService.getUserById(kefu_userid)!=null){
				kefu_username  = userService.getUserById(kefu_userid).getUsername();	
			}
			
			double vipfee=NumberUtils.getDouble(Global.getValue("vip_fee"));
			if(account.getUseMoney() <vipfee && account.getTotal()<vipfee ){
				message("可用余额不足，请先充值","/member/account/newrecharge.html");
				return MSG;
			}
			if(uc==null){
				uc=new UserCache();
			}
			if(uc.getVipStatus()==2||uc.getVipStatus()==1){
				message("不允许重复申请！","/member/vip.html");
				return "fail";
			}
			//校验验证码
			checkValidImgWithUrl("/member/vip.html");
			
			if(kefu_userid!=0){
				uc.setKefuUserid(kefu_userid);				
				uc.setKefuUsername(kefu_username);
			}				
			//更新后的用户缓存
			AccountLog accountLog = new AccountLog(sessionUser.getUserId(),Constant.VIP_FEE,Constant.ADMIN_ID,
					this.getTimeStr(),this.getRequestIp());
			boolean isOk=true;
			String checkMsg="";
			try {
				//第三方处理
				String msg = checkVipWithApi(uc, accountLog, user, kefu_userid);
				if(!StringUtils.isBlank(msg)){
					return msg;//汇付跳转页面
				}
			} catch (BussinessException e) {
				isOk=false;
				checkMsg=e.getMessage();
			}catch (Exception e) {
				isOk=false;
				checkMsg="系统出错，联系管理员！";
				logger.error(e.getMessage());
			}
			if(!isOk){
				message(checkMsg,"");
				return MSG;
			}
		}
		if(uc!=null&&uc.getKefuUserid()!=0){
			User kefu = userService.getUserById(uc.getKefuUserid());
			request.setAttribute("kefu", kefu);
			super.systemLogAdd(user, 8, "用户申请VIP");
		}
		Account act = userService.getUserById(user.getUserId()).getAccount();
		request.setAttribute("act", act);
		request.setAttribute("user", user);		
		request.setAttribute("userCache", uc);
		return "success";
	}
	
	private String checkVipWithApi(UserCache uc , AccountLog accountLog,User user,long kefu_userid) throws Exception{
		int apiType = Global.getInt("api_code");//第三方参数
		String vip_remark=request.getParameter("vip_remark");
		if (vip_remark.equals("") || vip_remark ==null) {
			message("请输入备注！","/member/vip.html");
		}
		//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
		switch (apiType) {
		case 1:// 汇付接口
			String money=Global.getValue("vip_fee");
			double vipmoney=NumberUtils.getDouble2(money);
			String ordId=OrderNoUtils.getInstance().getSerialNumber();
			String usrCustId=user.getApiUsercustId();
			UsrAcctPay pnr=new UsrAcctPay(ordId, usrCustId, NumberUtils.format2Str(vipmoney));
			pnr.setMerPriv(user.getUserId()+","+kefu_userid+","+"vip");
			pnr.sign();
			uc.setVipRemark(vip_remark);
			userService.updateUserCache(uc);
			request.setAttribute("pnr", pnr);
			return "payvip";
		case 2:// 易极付接口
			uc.setVipStatus(2);
			uc.setKefuAddtime(new Date());
			uc.setVipRemark(vip_remark);
			userService.applyVip(uc, accountLog);
			return "";
		case 3:// 双乾接口
			uc.setVipStatus(2);
			uc.setKefuAddtime(new Date());
			uc.setVipRemark(vip_remark);
			userService.applyVip(uc, accountLog);
			return "";
		default:
			return "";
		}
		//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	}
	
	/**
	 * 易极付无卡签约
	 * @return
	 */
	@Action(value="deductYjf",results={
			@Result(name="success", type="ftl",location="/member/deductSign.html")})
	public String deductYjf(){
		User user  = this.getSessionUser();
		 DeductSign dedu =  PayModelHelper.deductSign(user.getApiId());
		request.setAttribute("pnr", dedu);
		return "success";
	}
	
	// TGPROJECT-362 无卡代扣签约服务新接口
	@Action(value="doSignmanyBankYjf",results={
			@Result(name="success", type="ftl",location="/member/signmanyBank.html")})
	public String doSignmanyBankYjf(){
		User user = this.getSessionUser();
		SignmanyBank signBank = PayModelHelper.doSiginmanyBank(user.getApiId());
		request.setAttribute("pnr", signBank);
		return "success";
	}
	
	/**
	 * 升级后的实名信息填写
	 * @return
	 */
	@Action(value="apiRealname",results={@Result(name="success", type="ftl",location="/member/identify/activate.html"),
			@Result(name="realname", type="ftl",location="/member/identify/realname.html")})
	public String apiRealname(){
		User user = getSessionUser();
		user = userService.getUserById(user.getUserId());
		//v1.8.0.3_u3 TGPROJECT-330 qj 2014-06-04 start 
		boolean checkPhone = true;
		if(user.getPhoneStatus() == 1){
			checkPhone = false;
		}
		Rule rule = ruleService.getRuleByNid("is_phone_check");
		if(rule!=null && rule.getStatus() == 1){
			if(rule.getValueIntByKey("status")==0){
				checkPhone = false;
			}
		}
		//v1.8.0.3_u3 TGPROJECT-330 qj 2014-06-04 start 
//		if(user.getEmailStatus() != 1){
//			message("请先进行邮箱认证", "/member/identify/email.html", "点击邮箱认证");
//			return MSG;
//		//v1.8.0.3_u3 TGPROJECT-330 qj 2014-06-04 start 
//		}else 
			if(checkPhone) {
		//v1.8.0.3_u3 TGPROJECT-330 qj 2014-06-04 start 
			message("请先绑定手机！", "/member/identify/phone.html","点击绑定手机");
			return MSG;
		}else if(user.getRealStatus() == 0){//提交实名信息
			request.setAttribute("user", user);
			return SUCCESS;
		}else{
			request.setAttribute("user", user);
			return "realname";
		}
	}
	
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
	/**
	 * 第三方授权（投标、还款、扣除管理费授权）
	 * @return
	 */
	@Action(value="loanAuthorize",results={@Result(name="success", type="ftl",location="/member/mmm/loanAuthorize.html")})
	public String loanAuthorize(){
		User user = getSessionUser();
		user = userService.getUserById(user.getUserId());
		if(user.getEmailStatus() != 1){
			message("请先进行邮箱认证", "/member/identify/email.html", "点击邮箱认证");
			return MSG;
		}else if(user.getPhoneStatus() != 1) {
			message("请先绑定手机！", "/member/identify/phone.html","点击绑定手机");
			return MSG;
		}else if(user.getRealStatus() == 0){//提交实名信息
			message("请先实名认证！", "/member/apiRealname.html","点击实名认证");
			return MSG;
		}
		MmmLoanAuthorize mmm = MmmHelper.mmmLoanAuthorize(user);
		request.setAttribute("mmm", mmm);
		return SUCCESS;
	}
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	
	@Action(value="apiLogin",
			results={@Result(name="chinapnrlogin",type="ftl",location="/member/huifu/UserLogin.html"),
				@Result(name="yjfLogin", type="ftl",location="/member/yjfActivate.html")}
			)
	public String apiLogin(){
		User user=getSessionUser();
		user=userService.getUserById(user.getUserId());
		if (StringUtils.isBlank(user.getApiId()) || user.getApiStatus() != 1) {
			message("请先开通"+Global.getString("api_name")+"账户！","/member/index.html");
			return MSG;
		}else{
			Object object = apiService.apiLogin(user);
			if(null != object ){
				return madeApiLoginReturn(object);
			}else{
				logger.info("用户登陆失败");
				throw new BussinessException("系统异常请联系管理员！");
			}
		}
	}
	
	private String madeApiLoginReturn(Object object){
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			request.setAttribute("pnr", object);
			return "chinapnrlogin";
		case 2:// 易极付接口
			request.setAttribute("fys", object);
			return "yjfLogin";
		default:
			message("系统异常！","/member/index.html","返回个人中心");
			return MSG;
		}
	}
	
	@Action(value="invite",
			results={@Result(name="success",type="ftl",location="/member/huifu/UserLogin.html")}
			)
	public String invite() throws Exception {
		User user = this.getSessionUser();
		BASE64Encoder encoder = new BASE64Encoder();
		String s=encoder.encode((user.getUserId()+"").getBytes());
		request.setAttribute("userid", s);
		return "success";
	}
	
	//v1.8.0.4_u1 TGPROJECT-127 lx  start
	@Action(value="usercredit",results={
			@Result(name="success",type="ftl",location="/member/myagent/usercredit.html")
		})
	public String userCredit() throws Exception {
		SearchParam param = SearchParam.getInstance();
		User user=getSessionUser();
		request.setAttribute("myagent_type", "usercredit");//tab判断条件
		if(user!=null){
			param.addParam("user", user);
			param.addPage(paramInt("page"));
			param.addOrder(OrderType.DESC, "id");
			PageDataList<UserCreditLog>  userCreditLogList = userCreditService.findUserCreditLogList(param);
			setPageAttribute(userCreditLogList, param); 
			return SUCCESS;
		}
		message("非法操作");
		return MSG;
	}
	
	@Action(value="friendtender",results={
			@Result(name="success",type="ftl",location="/member/myagent/friendtender.html")
		})
	public String friendTender() throws Exception {
		SearchParam param = SearchParam.getInstance();
		User user=getSessionUser();
		request.setAttribute("myagent_type", "friendtender");//tab判断条件
		if(user!=null){
			
			request.setAttribute("userCreditSummary", userService.getUserCreditSummary(user.getUserId()));//我推荐的用户统计
			
			param.addParam("user", user);
			param.addPage(paramInt("page"));
			param.addParam("userCreditType.nid",Operator.EQ, "invest_success_by_friends");
			param.addOrder(OrderType.DESC, "id");
			PageDataList<UserCreditLog>  userCreditLogList = userCreditService.findUserCreditLogList(param);
			setPageAttribute(userCreditLogList, param); 
			return SUCCESS;
		}
		message("非法操作");
		return MSG;
	}
	
	@Action(value="friendborrow",results={
			@Result(name="success",type="ftl",location="/member/myagent/friendborrow.html")
		})
	public String friendBorrow() throws Exception {
		SearchParam param = SearchParam.getInstance();
		User user=getSessionUser();
		request.setAttribute("myagent_type", "friendborrow");
		if(user!=null){
			List<InviteUser> listinviteUsers = userService.getInvitreUser(user.getUserId());
			if(listinviteUsers.size()>0){
				List<SearchFilter> listFilters = new ArrayList<SearchFilter>();
				for (int i = 0; i < listinviteUsers.size(); i++) {
					listFilters.add(new SearchFilter("user.userId", Operator.EQ, listinviteUsers.get(i).getUser().getUserId()));
				}
				param.addOrFilter(listFilters);
				param.addOrFilter("status", 3,6,7,8)
				.addPage(paramInt("page"))
				.addOrder(OrderType.DESC, "id");
				PageDataList<Borrow> list = borrowService.getFriendBorrowList(param);
				setPageAttribute(list, param); 
			}
			return SUCCESS;
		}
		message("非法操作");
		return MSG;
	}
	//v1.8.0.4_u1 TGPROJECT-127 lx  end
	
	//v1.8.0.4_u2  TGPROJECT-314   qj  2014-05-30 start
	@Action(value="interestGeneratedWealth",results={
			@Result(name="success",type="ftl",location="/member/huifu/InterestGeneratedWealth.html")
		})
	public String interestGeneratedWealth(){
		User user = getSessionUser();
		if(user==null){
			throw new BussinessException("用户不存在！");
		}else{
			if( !(user.getRealStatus()==1 )  ){
				message("您的实名审核未通过！","/member/apiRealname.html","点击进入实名认证");
				return MSG;
			}
			List<AccountBank> banklist = accountService.getBankLists(user.getUserId());    //获取用户可用的银行卡
			if(banklist.size()<1){
				message("请先添加银行卡！","/member/account/bank.html","点击进入绑定银行卡");
				return MSG;
			}
		}
		FssTrans ft = ChinapnrHelper.interestGeneratedWealth(user);
		//添加记录
		InterestGenerate ig = new InterestGenerate(user,ft.getOrdId());
		ig.setStatus(0);
		ig.setAddtime(new Date());
		accountService.addInterestGenerate(ig);
		request.setAttribute("pnr", ft);
		return SUCCESS;
	}
	//v1.8.0.4_u2  TGPROJECT-314   qj  2014-05-30 end

}

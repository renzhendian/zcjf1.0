package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.AreaMmm;

public interface AreaMmmDao extends BaseDao<AreaMmm> {
	List<AreaMmm> getListByPid(String pid);
}

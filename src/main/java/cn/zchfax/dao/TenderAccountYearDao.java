package cn.zchfax.dao;

import cn.zchfax.domain.TenderAccountYear;
import cn.zchfax.domain.User;

/**
 * TGPROJECT-389 wsl 2014-09-12
 * @author wsl
 *
 */
public interface TenderAccountYearDao extends BaseDao<TenderAccountYear>{

	/**
	 * 更新用户年化投资总额信息
	 * @param totalYear
	 * @param monthMoneyYear
	 * @param dayMoneyYear
	 * @param userId
	 */
	public void updateTenderAccountYear(double totalYear, double monthMoneyYear,
			double dayMoneyYear, long userId);

	/**
	 * 获取指定用户用户年化投资总额信息
	 * @param user
	 * @return
	 */
	public TenderAccountYear getTenderAccountYearByUser(User user);
}

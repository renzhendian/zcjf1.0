package cn.zchfax.dao;

import cn.zchfax.domain.BorrowFee;


public interface BorrowFeeDao extends BaseDao<BorrowFee> {
}

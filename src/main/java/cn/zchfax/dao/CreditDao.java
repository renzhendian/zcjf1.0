package cn.zchfax.dao;

import javax.persistence.Query;

import cn.zchfax.domain.Credit;
import cn.zchfax.domain.User;


public interface CreditDao extends BaseDao<Credit>{
	/**
	 * 获取积分对象
	 * @param user
	 * @return
	 */
	public Credit getCreditByUser(User user);
	
	//fu
	public void setCredit(int value,int user_id);
	//fu
}

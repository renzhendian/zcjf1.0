package cn.zchfax.dao;


import cn.zchfax.domain.BorrowIntent;

/**
 * 
 * @author lx TGPROJECT-324 2014-05-29 add
 *
 */
public interface BorrowIntentDao extends BaseDao<BorrowIntent> {

}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.Site;

public interface SiteDao extends BaseDao<Site>{

	public Site getSiteByCode(String code);
	public List getSiteList(); 	
	public List getSubSiteList(int pid);
	
	public List getAllSubSiteList(int pid);
}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.Area;

public interface AreaDao extends BaseDao<Area> {
	public List<Area> getListByPid(int pid);
}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.Article;

public interface ArticleDao extends BaseDao<Article>{	
	
	public List getArticleList(int start,int pernum);
	public int countArticle();
}

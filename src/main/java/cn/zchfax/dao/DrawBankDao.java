package cn.zchfax.dao;

import cn.zchfax.domain.DrawBank;

public interface DrawBankDao extends BaseDao<DrawBank> {
	public DrawBank getDrawBankByBankCode(String bankCode);
}

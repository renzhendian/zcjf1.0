package cn.zchfax.dao;

import cn.zchfax.domain.SendSmsLog;

public interface SendSmsLogDao extends BaseDao<SendSmsLog>{

	/**
	 * 获取短信发送记录
	 * @param phone
	 * @return
	 */
	public SendSmsLog getSendSmsByPhone(String phone);

}

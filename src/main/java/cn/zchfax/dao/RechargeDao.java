package cn.zchfax.dao;

import cn.zchfax.domain.AccountRecharge;

public interface RechargeDao {

	public AccountRecharge getRechargeByTradeno(String orderNo);
	public boolean updateRecharge(int status, String returnText, String trade_no,String flowNo);
}

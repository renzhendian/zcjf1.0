package cn.zchfax.dao;

import cn.zchfax.domain.WebRepayLog;

/**
 * 网站垫付还款dao
 * @author wujing
 *
 */
public interface WebRepayLogDao extends BaseDao<WebRepayLog> {

}

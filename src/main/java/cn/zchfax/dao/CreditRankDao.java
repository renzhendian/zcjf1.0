package cn.zchfax.dao;

import cn.zchfax.domain.CreditRank;


public interface CreditRankDao extends BaseDao<CreditRank>{
	CreditRank getCreditRankByName(String name);   //根据信用等级名称查询
	
	
}

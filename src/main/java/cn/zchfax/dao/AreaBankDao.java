package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.AreaBank;

public interface AreaBankDao extends BaseDao<AreaBank> {
	List<AreaBank> getListByPid(String pid);
}

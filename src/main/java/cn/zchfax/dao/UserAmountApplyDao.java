package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.UserAmountApply;

public interface UserAmountApplyDao extends BaseDao<UserAmountApply> {

	public List<UserAmountApply> getUserAmountApplyList(Long userId);

}

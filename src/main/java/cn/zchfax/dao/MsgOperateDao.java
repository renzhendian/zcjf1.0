package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.MsgOperate;

public interface MsgOperateDao extends BaseDao<MsgOperate>{
	
	public List<MsgOperate> getChilds(int pid);	
}

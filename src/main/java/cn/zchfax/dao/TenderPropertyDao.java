package cn.zchfax.dao;

import cn.zchfax.domain.TenderProperty;

public interface TenderPropertyDao extends BaseDao<TenderProperty> {

}

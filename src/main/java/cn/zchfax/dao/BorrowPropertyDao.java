package cn.zchfax.dao;

import cn.zchfax.domain.BorrowProperty;

public interface BorrowPropertyDao extends BaseDao<BorrowProperty> {

}

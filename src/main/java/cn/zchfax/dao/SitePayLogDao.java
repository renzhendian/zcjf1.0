package cn.zchfax.dao;

import cn.zchfax.domain.SitePayLog;


public interface SitePayLogDao extends BaseDao<SitePayLog> {

	/**
	 * 获取日志的最后一条记录
	 * @return
	 */
	SitePayLog getLastSitePayLog();
	
}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCreditLog;
import cn.zchfax.domain.UserCreditType;

public interface UserCreditLogDao extends BaseDao<UserCreditLog> {
	public List<UserCreditLog> getUserCreditLogByType(UserCreditType type,User user);
}

package cn.zchfax.dao;

import cn.zchfax.domain.AccountLog;


public interface AccountLogDao extends BaseDao<AccountLog> {

	/**
	 * 计算风险准备金总和
	 * @return
	 */
	public double getSumRiskreserveFee();
}

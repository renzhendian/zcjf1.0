package cn.zchfax.dao;

import cn.zchfax.domain.ChinaPnrPayModel;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface ChinapnrPayDao extends BaseDao<ChinaPnrPayModel> {
	public PageDataList<ChinaPnrPayModel> getChinapnrList(SearchParam sp);
	public ChinaPnrPayModel findChinapnrModelByOrd(String ordNo);
}

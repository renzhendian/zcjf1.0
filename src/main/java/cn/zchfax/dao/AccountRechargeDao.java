package cn.zchfax.dao;

import cn.zchfax.domain.AccountRecharge;

public interface AccountRechargeDao extends BaseDao<AccountRecharge> {

	public AccountRecharge getRechargeByTradeno(String orderNo);

	/**
	 * 更新充值单据
	 * @param status
	 * @param returnText
	 * @param trade_no
	 * @return
	 */
	public  boolean updateRecharge(int status, String returnText, String trade_no,String flowNo);

	/**
	 * 线下充值，根据流水号查询
	 * @param serialNo
	 * @return
	 */
	int getCountBySerialNo(String serialNo);
	
}

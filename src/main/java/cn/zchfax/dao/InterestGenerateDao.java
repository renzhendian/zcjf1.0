package cn.zchfax.dao;

import cn.zchfax.domain.InterestGenerate;

/**
 *  生利宝接口 TGPROJECT-314 qj 2014-05-30 add
 *
 */
public interface InterestGenerateDao extends BaseDao<InterestGenerate> {
	
}

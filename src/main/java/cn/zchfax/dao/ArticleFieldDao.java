package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.ArticleField;

public interface ArticleFieldDao extends BaseDao<ArticleField>{	
	
	public List getAritcleFieldListByAid(int aid);
}

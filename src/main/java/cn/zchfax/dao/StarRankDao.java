package cn.zchfax.dao;

import cn.zchfax.domain.StarRank;

public interface StarRankDao extends BaseDao<StarRank> {
	StarRank getStartRankByRank(int rank);

}

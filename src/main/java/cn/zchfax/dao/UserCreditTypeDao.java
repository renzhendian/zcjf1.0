package cn.zchfax.dao;

import cn.zchfax.domain.UserCreditType;

public interface UserCreditTypeDao extends BaseDao<UserCreditType> {

	/**
	 * 根据用户的类型查询积分类型
	 * @param type
	 * @return
	 */
	public UserCreditType getUserCreditType(String type);

	
}

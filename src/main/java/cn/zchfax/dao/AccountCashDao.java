package cn.zchfax.dao;

import cn.zchfax.domain.AccountCash;

public interface AccountCashDao extends BaseDao<AccountCash>{

	 public AccountCash getAccountCashByOrderNo(String orderNo);
}

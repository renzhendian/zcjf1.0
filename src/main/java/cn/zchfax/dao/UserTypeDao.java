package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.UserType;

public interface UserTypeDao extends BaseDao<UserType> {
	
	public List getAllUserType();
	public List getUserTypepurviewsByUserTypeId(long user_type_id);
}

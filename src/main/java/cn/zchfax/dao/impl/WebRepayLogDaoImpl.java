package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.WebRepayLogDao;
import cn.zchfax.domain.WebRepayLog;
/**
 * 垫付还款记录
 * @author wujing
 *
 */
@Repository(value="webRepayLogDao")
public class WebRepayLogDaoImpl extends ObjectDaoImpl<WebRepayLog> implements
		WebRepayLogDao {

}

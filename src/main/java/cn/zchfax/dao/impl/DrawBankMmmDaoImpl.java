package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.DrawBankMmmDao;
import cn.zchfax.domain.DrawBankMmm;

@Repository("drawBankMmmDao")
public class DrawBankMmmDaoImpl extends ObjectDaoImpl<DrawBankMmm> implements DrawBankMmmDao {
	
}

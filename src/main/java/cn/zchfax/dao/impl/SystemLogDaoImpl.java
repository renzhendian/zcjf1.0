package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.SystemLogDao;
import cn.zchfax.domain.SystemLog;

@Repository
public class SystemLogDaoImpl extends ObjectDaoImpl<SystemLog> implements SystemLogDao {

}

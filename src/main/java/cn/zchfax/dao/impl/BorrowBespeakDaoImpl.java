package cn.zchfax.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowBespeakDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowBespeak;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;

@Repository
public class BorrowBespeakDaoImpl extends ObjectDaoImpl<BorrowBespeak> implements BorrowBespeakDao {

	@Override
	public int countBespeakUser(long borrowId) {
		String jpql = " select count(*) from BorrowBespeak where borrow = ?1 ";
		Query query = em.createQuery(jpql).setParameter(1, new Borrow(borrowId));
		int count = NumberUtils.getInt(StringUtils.isNull(query.getSingleResult()));
		return count;
	}

	@Override
	public List<BorrowBespeak> findByBorrow(long id) {
		String sql = "select user_id from borrow_bespeak where borrow_id = ?1";
		Query query = em.createNativeQuery(sql).setParameter(1, id);
		return query.getResultList();
	}

	@Override
	public double getSumBorrowBespesk() {
		String jpql = " select sum(money) as sum  from BorrowBespeak  ";
		Query query = em.createQuery(jpql);
		double sum = NumberUtils.getDouble(StringUtils.isNull(query.getSingleResult()));
		return sum;
	}

	@Override
	public int getBorrowBespeskCount() {
		String jpql = " select count(*) from BorrowBespeak  ";
		Query query = em.createQuery(jpql);
		int count = NumberUtils.getInt(StringUtils.isNull(query.getSingleResult()));
		return count;
	}

	
}

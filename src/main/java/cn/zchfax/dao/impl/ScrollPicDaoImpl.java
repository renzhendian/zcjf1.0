package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.ScrollPicDao;
import cn.zchfax.domain.ScrollPic;
@Repository(value="scrollPicDao")
public class ScrollPicDaoImpl extends ObjectDaoImpl<ScrollPic> implements ScrollPicDao  {
}

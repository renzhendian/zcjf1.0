package cn.zchfax.dao.impl;


import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowFeeDao;
import cn.zchfax.domain.BorrowFee;
@Repository(value="borrowFee")
public class BorrowFeeDaoImpl extends ObjectDaoImpl<BorrowFee> implements BorrowFeeDao {

}

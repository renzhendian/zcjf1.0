package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.PriorRepayLogDao;
import cn.zchfax.domain.PriorRepayLog;

@Repository(value="priorRepayLogDao")
public class PriorRepayLogDaoImpl extends ObjectDaoImpl<PriorRepayLog> implements PriorRepayLogDao {

	
}

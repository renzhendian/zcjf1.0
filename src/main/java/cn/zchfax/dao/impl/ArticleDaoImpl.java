package cn.zchfax.dao.impl;


import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.ArticleDao;
import cn.zchfax.domain.Article;

@Repository(value="articleDao")
public class ArticleDaoImpl extends ObjectDaoImpl<Article>  implements ArticleDao {

	private static Logger logger = Logger.getLogger(ArticleDaoImpl.class);

	@Override
	public List getArticleList(int start,int pernum) {
		String sql = "select a from Article a left join a.site " +
				"order by a.sort asc, a.publish desc,a.id desc,a.addtime desc";
		Query query = em.createQuery(sql);
		query.setFirstResult(start).setMaxResults(pernum);		
		List list = query.getResultList();
		return list;
	}
	@Override
	public int countArticle() {
		String sql = "select a.id from Article a left join a.site";
		Query query  = em.createQuery(sql);
		int total = query.getResultList().size();
		return total;
	}
}

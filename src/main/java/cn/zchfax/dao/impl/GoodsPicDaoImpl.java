package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.GoodsPicDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.Goods;
import cn.zchfax.domain.GoodsPic;

@Repository("goodsPicDao")
public class GoodsPicDaoImpl extends ObjectDaoImpl<GoodsPic> implements GoodsPicDao {
	@Override
	public List<GoodsPic> getGoodsPicByGoodsId(int id){
		String jpql=" from GoodsPic where goods=?1 ";
		List<GoodsPic> list=new ArrayList<GoodsPic>();
		Query q=em.createQuery(jpql).setParameter(1, new Goods(id));
		list=q.getResultList();
		return list;
	}
}

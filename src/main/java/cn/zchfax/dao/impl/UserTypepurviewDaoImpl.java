package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.UserDao;
import cn.zchfax.dao.UserTypeDao;
import cn.zchfax.dao.UserTypepurviewDao;
import cn.zchfax.domain.Purview;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserType;
import cn.zchfax.domain.UserTypepurview;
import cn.zchfax.model.SearchParam;

@Repository
public class UserTypepurviewDaoImpl  extends ObjectDaoImpl<UserTypepurview> implements UserTypepurviewDao {
    Logger logger = Logger.getLogger(UserTypepurviewDaoImpl.class);

    @Autowired
    private UserTypeDao userTypeDao;
    @Autowired
    private UserDao userDao;
    public void addUserTypePurviews(List<Integer> purviewids,long user_type_id) {
    	UserType ut = new UserType();
    	ut.setTypeId(user_type_id);
    	List list = new ArrayList();
    	for(int i=0;i<purviewids.size();i++){
    		int id = (Integer)purviewids.get(i);
    		Purview p =  new Purview();
    		p.setId(id);
    		UserTypepurview utp = new UserTypepurview();    		
    		utp.setPurview(p);
    		utp.setUserType(ut);
    		this.merge(utp);
    	}
    }	
    
	public void delUserTypePurviews(long user_type_id) {		
		List list = userTypeDao.getUserTypepurviewsByUserTypeId(user_type_id);
		this.delete(list);
	}

	@Override
	public List<UserTypepurview> getUserTypepurviewList(long user_id) {
		
		User user = userDao.find(user_id);
		UserType ut = user.getUserType();
		SearchParam param = SearchParam.getInstance().addParam("userType", ut);
		return  this.findByCriteria(param);
	}
    
}

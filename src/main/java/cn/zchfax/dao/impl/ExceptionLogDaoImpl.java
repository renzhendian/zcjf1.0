package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.ExceptionLogDao;
import cn.zchfax.domain.ExceptionLog;

@Repository
public class ExceptionLogDaoImpl extends ObjectDaoImpl<ExceptionLog> implements ExceptionLogDao {

}

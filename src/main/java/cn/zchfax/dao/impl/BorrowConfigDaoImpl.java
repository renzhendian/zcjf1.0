package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowConfigDao;
import cn.zchfax.domain.BorrowConfig;

@Repository("borrowConfigDao")
public class BorrowConfigDaoImpl extends ObjectDaoImpl<BorrowConfig> implements BorrowConfigDao {
	
}

package cn.zchfax.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.AttestationTypeDao;
import cn.zchfax.domain.AttestationType;
import cn.zchfax.model.SearchParam;

@Repository(value="attestationTypeDao")
public class AttestationTypeDaoImpl extends ObjectDaoImpl<AttestationType> implements AttestationTypeDao {

	@Override
	public List<AttestationType> getList(SearchParam param) {
		return this.findByCriteria(param);
	}

	
}

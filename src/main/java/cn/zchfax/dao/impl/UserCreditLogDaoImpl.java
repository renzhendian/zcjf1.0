package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.UserCreditLogDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCreditLog;
import cn.zchfax.domain.UserCreditType;

@Repository
public class UserCreditLogDaoImpl extends ObjectDaoImpl<UserCreditLog> implements UserCreditLogDao {

	@Override
	public List<UserCreditLog> getUserCreditLogByType(UserCreditType type, User user) {
		String jpql=" from UserCreditLog where user=?1 and userCreditType=?2";
		List<UserCreditLog> list=new ArrayList<UserCreditLog>();
		Query q=em.createQuery(jpql).setParameter(1, user).setParameter(2, type);
		list=q.getResultList();
		return list;
	}

}

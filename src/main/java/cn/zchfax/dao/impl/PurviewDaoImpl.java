package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.PurviewDao;
import cn.zchfax.dao.UserDao;
import cn.zchfax.dao.UserTypepurviewDao;
import cn.zchfax.domain.Purview;
import cn.zchfax.domain.UserTypepurview;

@Repository
public class PurviewDaoImpl extends ObjectDaoImpl<Purview> implements PurviewDao {
	private Logger logger=Logger.getLogger(PurviewDaoImpl.class);

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserTypepurviewDao userTypepurviewDao;
	
	@Override
	public List getPurviewByPid(int pid) {
		List list=new ArrayList();
		String sql = "from Purview where pid= ?1";
		try{
			Query query  = em.createQuery(sql);
			query.setParameter(1, pid);
			list = query.getResultList();	
		}catch(Exception e){
			logger.error(e);
		}
		return list;
	}

	@Override
	public List<Purview> getPurviewByUserid(long user_id) {
		List<Purview> purviews =  new ArrayList<Purview>();
		List<UserTypepurview> utpList = userTypepurviewDao.getUserTypepurviewList(user_id);
		for(UserTypepurview utp:utpList){
			purviews.add(utp.getPurview());
		}
		return purviews;
	}

	@Override
	public List getAllPurview() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getAllCheckedPurview(long user_typeid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addPurview(Purview p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Purview getPurview(long id) {
		String sql="from Purview where id=?1";
		Purview p=null;
		try {
			Query query = em.createQuery(sql);
			query.setParameter(1, id);
			p = (Purview)query.getSingleResult();
		} catch (Exception e) {
			logger.error(e);
		}
		return p;
	}

	@Override
	public void delPurview(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isRoleHasPurview(long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void modifyPurview(Purview p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addUserTypePurviews(List<Integer> purviewid, long user_type_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delUserTypePurviews(long user_type_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Purview> getPurviewsByLevel(int level) {
		// TODO Auto-generated method stub
		String sql ="from Purview where level=?1";
		Query query = em.createQuery(sql);
		query.setParameter(1, level);
		return query.getResultList();
	}
	
	
}
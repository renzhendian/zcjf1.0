package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.ViewAutoInvestDao;
import cn.zchfax.domain.ViewAutoInvest;

@Repository
public class ViewAutoInvestDaoImpl extends ObjectDaoImpl<ViewAutoInvest> implements ViewAutoInvestDao {

}

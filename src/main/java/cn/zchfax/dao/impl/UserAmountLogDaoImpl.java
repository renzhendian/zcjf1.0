package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.UserAmountLogDao;
import cn.zchfax.domain.UserAmountLog;

@Repository
public class UserAmountLogDaoImpl extends ObjectDaoImpl<UserAmountLog> implements UserAmountLogDao  {

}

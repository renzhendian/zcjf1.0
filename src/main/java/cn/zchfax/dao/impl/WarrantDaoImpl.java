package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.WarrantDao;
import cn.zchfax.domain.Warrant;

/**
 * @author wujing 
 * @version 创建时间：2013-12-17 下午3:43:24
 * 类说明
 */
@Repository(value="warrantDao")
public class WarrantDaoImpl extends ObjectDaoImpl<Warrant>  implements WarrantDao {

	
}

package cn.zchfax.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.SendSmsLogDao;
import cn.zchfax.domain.SendSmsLog;

@Repository("sendSmsLogDao")
public class SendSmsLogDaoImpl extends ObjectDaoImpl<SendSmsLog> implements SendSmsLogDao{

	@Override
	public SendSmsLog getSendSmsByPhone(String phone) {
		String jpql = "from SendSmsLog where phone = ?1";
		Query query = em.createQuery(jpql).setParameter(1, phone);
		List<SendSmsLog> list = query.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

}

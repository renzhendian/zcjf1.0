package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.StarLogDao;
import cn.zchfax.domain.StarLog;

@Repository(value="starLogDao")
public class StarLogDaoImpl extends ObjectDaoImpl<StarLog> implements StarLogDao {

}

package cn.zchfax.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.eval.ConcatEval;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.UserDao;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.Credit;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCache;
import cn.zchfax.domain.UserType;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.model.DetailUser;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.account.UserCreditSummary;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;

/**
 * 用户表新增、删除、查找Dao类
 * 
 * @author zhuliming
 * @date 2013-5-12-下午2:08:27
 * @version 1.1
 * 
 *          
 * 
 */
@Repository(value = "userDao")
public class UserDaoImpl extends ObjectDaoImpl<User> implements UserDao {

	private static Logger logger = Logger.getLogger(UserDaoImpl.class);

	private DetailUser detailUser;

	/**
	 * 检查是否存在username的用户记录
	 */
	@Override
	public User getUserByUsername(String username) {
		String jpql = " from User where username = ?1 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, username);
		List<User> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		} else if (list.size() == 0) {
			return null;
		} else {
			throw new BussinessException("用户名不是唯一的！");
		}
	}

	@Override
	public User getUserByEmail(String email) {
		String jpql = "from User where email = ?1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, email);
		List<User> list = query.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public User getUserByUsernameAndPassword(String username, String password) {
		String jpql = "from User where username = ?1 and password = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, username);
		query.setParameter(2, password);
		List list = query.getResultList();
		if (list != null && list.size() >= 1) {
			return (User) list.get(0);
		} else {
			return null;
		}
	}
	@Override
	public User getUserByUsernameAndPayPassword(String username, String payPassword) {
		String jpql = "from User where username = ?1 and paypassword = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, username);
		query.setParameter(2, payPassword);
		List list = query.getResultList();
		if (list != null && list.size() >= 1) {
			return (User) list.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * 根据 用户名，email,phone  ps  查询用户
	 * @param username
	 * @param payPassword
	 * @param type
	 * @return
	 */
	@Override
	public User getUserByInputNameAndPassword(String inputName, String payPassword,String type) {
		String jpql = "";
		if("email".equals(type)){//email 登陆
			jpql = "from User where email = ?1 and password = ?2";
		}
		if("userName".equals(type)){//用户名 登陆
			jpql = "from User where username = ?1 and password = ?2";
		}
		if("phone".equals(type)){//手机号 登陆
			jpql = "from User where phone = ?1 and password = ?2";
		}
		
		Query query = em.createQuery(jpql);
		query.setParameter(1, inputName);
		query.setParameter(2, payPassword);
		List list = query.getResultList();
		if (list != null && list.size() >= 1) {
			return (User) list.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * 根据 用户名，email,phone  ps  查询用户
	 * @param username
	 * @param payPassword
	 * @param type
	 * @return
	 */
	@Override
	public User getUserByInputName(String inputName,String type) {
		String jpql = "";
		if("email".equals(type)){//email 登陆
			jpql = "from User where email = ?1 ";
		}
		if("userName".equals(type)){//用户名 登陆
			jpql = "from User where username = ?1 ";
		}
		if("phone".equals(type)){//手机号 登陆
			jpql = "from User where phone = ?1 ";
		}
		
		Query query = em.createQuery(jpql);
		query.setParameter(1, inputName);
		List list = query.getResultList();
		if (list != null && list.size() >= 1) {
			return (User) list.get(0);
		} else {
			return null;
		}
	}


	@Override
	public List<User> getUserList(SearchParam sp) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<User> query = builder.createQuery(User.class);
		Root<User> from = query.from(User.class);
		query.orderBy(builder.desc(from.get("userId")));
		Predicate[] p=sp.bySearchFilter(User.class, builder, query,from);
		TypedQuery<User> tq = em.createQuery(
				query.select(from)
				.where(p)
		);

		return tq.getResultList();
	}

	@Override
	public boolean isRoleHasPurview(long id) {
		return false;
	}

	@Override
	public User getUserByCard(String card) {
		String jpql = "from User where cardId = ?1 and realStatus = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, card);
		query.setParameter(2, 1);
		List list = query.getResultList();
		User user = null;
		if (list.size() >= 1) {
			user = (User) list.get(0);
		}
		return user;
	}

	@Override
	public List getAllUser(int type) {
		String jpql = "from User where typeId = ?1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, type);
		return (List<User>) query.getResultList();
	}

	String queryUserSql = " from user as p1 "
			+ "left join credit as p2 on p1.user_id=p2.user_id "
			+ "left join account as p9 on p1.user_id=p9.user_id "
			+ "left join credit_rank as p3 on p2.value<=p3.point2  and p2.value>=p3.point1 "
			+ "left join user_cache as p4 on p1.user_id=p4.user_id "
			+ "left join area as p5 on p5.id=p1.province "
			+ "left join area as p6 on p6.id=p1.city "
			+ "left join area as p7 on p7.id=p1.area "
			+ "left join user_type p8 on p8.type_id=p1.type_id " + "where 1=1 ";

	String selectSql = "select p1.*,p2.value as credit_jifen,p9.use_money ,p3.pic as credit_pic,p4.vip_status,p4.vip_verify_time,p4.kefu_addtime,"
			+ "p5.name as provincetext,p6.name as citytext,p7.name as areatext,p8.name as typename";

	/**
	 * 获取用户的信息，以及相应的积分
	 */
	public DetailUser getDetailUserByUserid(int userid) {

		User user = this.find(userid);
		Credit credit = user.getCredit();

		Account account = user.getAccount();
		UserCache userCache = user.getUserCache();
		UserType userType = user.getUserType();
		DetailUser detailUser = new DetailUser();
		detailUser.setUser(user);
		detailUser.setCredit_jifen(credit.getValue());
		detailUser.setUse_money(account.getUseMoney());
		detailUser.setVip_status(userCache.getVipStatus());
		detailUser.setVip_verify_time(userCache.getVipVerifyTime().getTime());
		detailUser.setKefu_addtime(userCache.getKefuAddtime().getTime());
		detailUser.setTypename(userType.getName());
		return detailUser;

	}

	public List getAllKefu() {
		String sql = "select a  from User a inner join a.userType b  where b.name like ?1 and  islock=0";
		Query query = em.createQuery(sql);
		query.setParameter(1, "%客服%");
		List list = query.getResultList();
		return list;
	}

	@Override
	public void updateUserApiStatus(String apiId) {
		String jpql = " update User set apiStatus = ?1 where  apiId= ?2 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, 1).setParameter(2, apiId);
		query.executeUpdate();
	}

	@Override
	public void updateRealNameStatus(String apiId, int status) {
		String jpql = " update User set realStatus = ?1 where  apiId= ?2 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, status).setParameter(2, apiId);
		query.executeUpdate();
	}

	@Override
	public int checkPhone(String phone) {
		String jpql = "from User where phone = ?1 and status = 1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, phone);
		List list = query.getResultList();
		return list.size();
	}
	
	@Override
	public User getUserByApiId(String apiId) {
		String jpql = "from User where apiId = ?1 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, apiId);
		List<User> list = query.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	public User findByPaypwd(User user) {
		String jpql = "from User where userId =?1 and paypassword is not null";
		Query query = em.createQuery(jpql);
		query.setParameter(1, user.getUserId());
		List list = query.getResultList();
		if (list != null && list.size() >= 0) {
			return (User) list.get(0);
		} else {
			return null;
		}
	}
	
	//v1.8.0.4_u1 TGPROJECT-127  zxc start
	/**
	 * 积分，查询，我推荐的用户的投资总和，我获得的投资积分总和。
	 * @param userId
	 * @return
	 */
	@Override
	public UserCreditSummary getUserCreditSummary(long userId ){
		String sql = " SELECT * from ( "
				   + " SELECT SUM(bt.account) as  friendTenderMoney,iu.invite_user as user_id  from borrow_tender bt "
				   + " LEFT JOIN borrow b on b.id = bt.borrow_id "
				   + " LEFT JOIN invite_user iu on iu.user_id=bt.user_id "
				   + " WHERE b.`status`>=3 and b.`status` !=99 AND iu.invite_user = :invite_user "
				   + " ) as A  LEFT JOIN ( "
				   + " SELECT SUM(cl.operate_value)  as friendTenderCredit,u. user_id as user_id"
				   + " FROM user_credit_log cl LEFT JOIN "
				   + " `user` u on cl.user_id = u.user_id "
				   +" LEFT JOIN user_credit_type ct on cl.type_id = ct.id "
				   +" WHERE ct.nid = 'invest_success_by_friends' and u.user_id= :user_id "
				   +" ) as B on 1=1 ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("invite_user",userId );
		param.put("user_id", userId);
		List<UserCreditSummary> list = getNamedParameterJdbcTemplate().query(sql, param, getBeanMapper(UserCreditSummary.class));
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}
	//v1.8.0.4_u1 TGPROJECT-127  zxc end

	//v1.8.0.4_u1 TGPROJECT-248【全部】 义乌贷vip卡活动用户  添加 编号义乌贷编号处理 start
	/**
	 * 查询义乌贷vip卡激活的的用户的数量
	 * @return
	 */
	@Override
	public int getYwdVipUsersCount(){
		String jpql = " select count(*) from User where activityStatus = 1 ";
		Query query = em.createQuery(jpql);
		int count = NumberUtils.getInt(StringUtils.isNull(query.getSingleResult()));
		return count + 1;
	}
	//v1.8.0.4_u1 TGPROJECT-248【全部】 义乌贷vip卡活动用户  添加 编号义乌贷编号处理 end
	
}

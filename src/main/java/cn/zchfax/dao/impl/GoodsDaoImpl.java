package cn.zchfax.dao.impl;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.GoodsDao;
import cn.zchfax.domain.Goods;

@Repository("goodsDao")
public class GoodsDaoImpl extends ObjectDaoImpl<Goods> implements GoodsDao {
}

package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.VoteTitleDao;
import cn.zchfax.domain.VoteTitle;
//v1.8.0.4_u1 TGPROJECT-16 zf 2014-5-5 start
@Repository
public class VoteTitleDaoImpl extends ObjectDaoImpl<VoteTitle> implements VoteTitleDao {
}
//v1.8.0.4_u1 TGPROJECT-16 zf 2014-5-5 end
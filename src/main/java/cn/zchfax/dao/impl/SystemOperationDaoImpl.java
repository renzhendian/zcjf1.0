package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.SystemOperationDao;
import cn.zchfax.domain.SystemOperation;

@Repository
public class SystemOperationDaoImpl extends ObjectDaoImpl<SystemOperation> implements SystemOperationDao {

}

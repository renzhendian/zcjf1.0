package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.MsgTypeDao;
import cn.zchfax.domain.MsgType;
@Repository
public class MsgTypeDaoImpl extends ObjectDaoImpl<MsgType> implements MsgTypeDao {

}

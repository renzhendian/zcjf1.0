package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.account.BaseAccountSumModel;
import cn.zchfax.tool.Page;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;

@Repository(value="borrowDao")
public class BorrowDaoImpl extends ObjectDaoImpl<Borrow> implements BorrowDao  {
	
	private static Logger logger = Logger.getLogger(BorrowDaoImpl.class);
	
	@Override
	public List<Borrow> findAll(int start, int end, SearchParam param) {
		return null;
	}

	@Override
	public List<Borrow> unfinshBorrow(long userId) {
		String jpql=" from Borrow where user=?1 and status in (0,1) and type<>110";
		List<Borrow> list=new ArrayList<Borrow>();
		Query q=em.createQuery(jpql).setParameter(1, new User(userId));
		list=q.getResultList();
		return list;
	}

	@Override
	public List<Borrow> list() {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Borrow> query = builder.createQuery(Borrow.class);
		Root<Borrow> from = query.from(Borrow.class);
		TypedQuery<Borrow> typedQuery = getEntityManager().createQuery(
		    query.select(from )
		    .where(
		       new Predicate[]{builder.equal((Expression)from.get("status"), 1),builder.equal((Expression)from.get("accountYes"), (Expression)from.get("account"))}
		    )
		);
		List<Borrow> results = typedQuery.getResultList();
		return results;
	}

	@Override
	public List<Borrow> list(SearchParam param) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Borrow> query = builder.createQuery(Borrow.class);
		Root<Borrow> from = query.from(Borrow.class);
		Predicate[] p=param.bySearchFilter(Borrow.class, builder, query,from);
		TypedQuery<Borrow> typedQuery = em.createQuery(
				query.select(from)
				.where(p)
		);
		List<Borrow> results = typedQuery.getResultList();
		return results;
	}
	
	/**
	 * 原生态sql 更新投标 的 borrow
	 * @param id
	 * @param accountyes
	 * @param tenderTimes
	 * @return
	 */
	int i = 0;
	@Override
	public int updateBorrowAccountyes(long id , double accountyes, int tenderTimes){
		i+=tenderTimes;
	    logger.info("zzzzzzzzzzzzzzzz " + i + " accountyes"+ accountyes +" ");
		String sql = " UPDATE Borrow SET accountYes = accountYes + ?1, tenderTimes = tenderTimes + ?2 " +
                     " WHERE id = ?3  and  accountYes < account ";
		Query query = em.createQuery(sql);
		query.setParameter(1, accountyes);
		query.setParameter(2, tenderTimes);
		query.setParameter(3, id);
	    int a =	query.executeUpdate();
	    return a;
	} 
	
	@Override
	public List<Borrow> getBorrowListOrderByStatus() {
		String jpql=" from Borrow where status in (1,0,6,8,7) order by  ?1,?2,?3,?4,?5 ";
		List<Borrow> list=new ArrayList<Borrow>();
		Query q=em.createQuery(jpql);
		q.setParameter(1, "status =" + 1).setParameter(2, "status =" + 8)
		.setParameter(3, "status =" + 6).setParameter(4, "status =" + 7)
		.setParameter(5, "status =" + 0);
		list=q.getResultList();
		return list;
	}
	
	@Override
	public Borrow getAssignMentBorrowByTenderId(long tender_id){
		String jpql = " from Borrow where assignmentTender = ?1 and  isAssignment = 1 and status in (0,1,3,6,7,8) ";
		Query query = em.createQuery(jpql).setParameter(1, tender_id);
		List<Borrow> list = query.getResultList();
		if(list.size() == 0){
			return null;
		}else{
			return list.get(0);
		}
	}

	@Override
	public List<Borrow> getWaitCancelBorrow(long typeId) {
		//select a from OperateLog a join a.operateFlow b where  a.orderNo = ?1 and b.operateType = ?2  order by a.id desc
		String jpql = "select b from Borrow b join ";
		return null;
	}
	
	@Override
	public double getSuccessBorrowSumAccount(){
		String jpql = " SELECT SUM(account) FROM Borrow WHERE status in (3,6,7,8) ";
		Query query =  em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if(ob != null){
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	
	@Override
	public double getMaxAprBorrow(){
		String jpql = " SELECT MAX(apr) FROM Borrow WHERE status in (3,6,7,8) ";
		Query query =  em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if(ob != null){
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	
	@Override
	public int sumBorrowByDay() {
		String jpql = " SELECT COUNT(b) FROM Borrow b WHERE addtime>?1 and addtime<?2 ";
		Query query =  em.createQuery(jpql);
		 
		query.setParameter(1, DateUtils.valueOf(DateUtils.dateStr2(new Date())));
		query.setParameter(2, new Date());
		Object ob = query.getSingleResult();
		//v1.8.0.4 TGPROJECT-5 zxc 2014-04-01 start
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString()) +1;
		}
		return 1;
		//v1.8.0.4 TGPROJECT-5 zxc 2014-04-01 end
	}

	@Override
	public double sumBorrowAccount(long userId) {
		String jpql = " SELECT SUM(account) FROM Borrow WHERE status =1 and user.userId = ?1 ";
		Query query =  em.createQuery(jpql);
		query.setParameter(1, userId);
		Object ob = query.getSingleResult();
		if(ob != null){
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	//1.8.0.4_u3   TGPROJECT  qinjun 2014-06-25  start

	@Override
	public void updateBorrowStartDate(long borrowId, Date date) {
		String jpql = "UPDATE Borrow SET  startTenderTime = ?1 where id = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, date);
		query.setParameter(2, borrowId);
		int numb = query.executeUpdate();
		logger.info("审核跟新借款开始时间borrowId："+borrowId+"影响行数"+numb);
	}
	
	//1.8.0.4_u3   TGPROJECT  qinjun 2014-06-25  end
	//1.8.0.4_u4 TGPROJECT-345  wujing dytz  start
	public List getTenderUseAccount(double money){
		List list = new ArrayList();
		String sql = "select  t.user_id as userId ,sum(t.account) as money  from borrow_tender t "
				+ "inner join borrow b  on    t.borrow_id=b.id  where (b.status =6 or b.status =7 or b.status =8 or "
				+ "(b.status = 1 and b.type = 110 ) ) and b.type != 101 group by t.user_id ";
		if (money > 0) {
			sql=sql+" having money >="+money;
		}
		Query query = em.createNativeQuery(sql);
		list = query.getResultList();
		return list;
	}

	@Override
	public List getRecallAccount() {
		String sql = "select user_id as userId, sum(repayment_yescapital ) as capital   from borrow_tender  where status =1  group by user_id";
		Query query = em.createNativeQuery(sql);
		List list = query.getResultList(); 
		return list;
	}
	
	
	@Override
	public List getTenderRecall(long tenderUserId, double tenderAccount,
			double recallAccount) {
		String sql ="select  t.user_id as userId ,sum(t.account) as money ,sum(t.repayment_yescapital)  as yesmoney from borrow_tender t inner join borrow b  on    t.borrow_id=b.id  where (b.status =6 or b.status =7 or b.status =8 or (b.status = 1 and b.type = 110 ) ) and b.type != 101 and t.user_id = ?1 group by t.user_id having (money>=?2 and yesmoney>=?3 )";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, tenderUserId);
		query.setParameter(2, tenderAccount);
		query.setParameter(3, recallAccount);
		return query.getResultList();
	}

	//1.8.0.4_u4 TGPROJECT-345  wujing dytz  end
	@Override
	public List borrowByStatus(int borrowStatus) {
		String jpql=" from Borrow where status=?1 and type<>110 ";
		List<Borrow> list=new ArrayList<Borrow>();
		Query q=em.createQuery(jpql).setParameter(1, borrowStatus);
		list=q.getResultList();
		return list;
	}
	//系统当前融资总额
	@Override
	public double getBorrowSum() {
		String jpql = "SELECT SUM(account) FROM  BorrowTender  WHERE borrow.id in (select id from Borrow where((status IN(3,6,7,8) and type!=110) or (type=110))) ";
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	//已还金额
	@Override
	public double getRepayed() {
		String jpql = "select sum(repaymentAccount) from BorrowRepayment br where br.status=1";
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}

	// 系统当前融资产生利息
	@Override
	public double getBorrowSumInterest() {
		String jpql = "SELECT SUM(interest+interestRateSum) FROM  BorrowTender  WHERE borrow.id in (select id from Borrow where((status IN(3,6,7,8) and type!=110) or (type=110))) ";
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	@Override
	public double getSumInterest() {
		String jpql = "SELECT SUM(interest) FROM  BorrowTender  WHERE borrow.id in (select id from Borrow where((status IN(1,3,6,7,8) and type!=110) or (type=110))) ";
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}

	/**
	 * 获取指定月份的借款标总数
	 */
	@Override
	public double[] getBorrowCountList() {
		

		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM ")
		.append(" ( SELECT IFNULL(COUNT(1),0) AS borrowCount1 FROM borrow b WHERE b.time_limit >=1 AND b.time_limit < 3 ) AS a ")
		.append(" LEFT JOIN")
		.append(" ( SELECT IFNULL(COUNT(1),0) AS borrowCount2 FROM borrow b WHERE b.time_limit >=3 AND b.time_limit < 6) AS b ON 1=1 ")
		.append(" LEFT JOIN ")
		.append(" ( SELECT IFNULL(COUNT(1),0) AS borrowCount3 FROM borrow b WHERE b.time_limit >=6 AND b.time_limit < 9 ) AS c ON 1=1 ")
		.append(" LEFT JOIN ")
		.append(" ( SELECT IFNULL(COUNT(1),0) AS borrowCount4 FROM borrow b WHERE b.time_limit >=9 AND b.time_limit <= 12) AS d ON 1=1 ")
		.append(" LEFT JOIN ")
		.append(" (SELECT IFNULL(COUNT(1),0) AS borrowCount FROM borrow b WHERE b.time_limit BETWEEN 1 AND 12 ) AS e ON 1=1; ");
		
		List result = null;
		double[] countArray = {0,0,0,0,0};
		try{
			Query query = em.createNativeQuery(sql.toString());
			result = query.getResultList();
			if (result.size()>0){
				Iterator iterator = result.iterator();
				while( iterator.hasNext() ){
					Object[] row = ( Object[]) iterator.next();			
					double borrowCount1 = Double.parseDouble(row[0].toString());
					double borrowCount2 = Double.parseDouble(row[1].toString());
					double borrowCount3 = Double.parseDouble(row[2].toString());
					double borrowCount4 = Double.parseDouble(row[3].toString());
					double borrowCount = Double.parseDouble(row[4].toString());
					countArray[0] = borrowCount1;
					countArray[1] = borrowCount2;
					countArray[2] = borrowCount3;
					countArray[3] = borrowCount4;
					countArray[4] = borrowCount;
				}
			}
		}catch(Exception e){
			logger.info("数据查询出错！，sql:"+sql);
			logger.error(e);
		}
		return countArray;
	}

	@Override
	public List<Borrow> findBorrowBespeak() {
		String sql = "select id,name from borrow where status=0 and (now() - start_tender_time) <= 4";
		Query query = em.createNativeQuery(sql);
		return query.getResultList();
	}
	
	public double getSumApr(){
		String jpql = "SELECT SUM(apr) FROM  Borrow  WHERE (status IN(1,3,6,7,8) and type!=110) or type=110";
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	
	public int getBorrowCount(){
		String jpql = " SELECT COUNT(b) FROM Borrow b WHERE (b.status IN(1,3,6,7,8) and b.type!=110) or b.type=110 ";
		Query query =  em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 1;
	}
	
	
}

package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.MsgOperateTypeDao;
import cn.zchfax.domain.MsgOperateType;
@Repository
public class MsgOperateTypeDaoImpl extends ObjectDaoImpl<MsgOperateType> implements MsgOperateTypeDao {

}

package cn.zchfax.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.TenderAccountYearDao;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.TenderAccountYear;
import cn.zchfax.domain.User;

/**
 * TGPROJECT-389 wsl 2014-09-12
 * @author wsl
 *
 */
@Repository(value="tenderAccountYearDao")
public class TenderAccountYearDaoImpl extends ObjectDaoImpl<TenderAccountYear> implements TenderAccountYearDao {

	@Override
	public void updateTenderAccountYear(double totalYear, double monthMoneyYear, double dayMoneyYear, long userId){
		String jpql="update TenderAccountYear set totalYear=totalYear+?1,monthMoneyYear=monthMoneyYear+?2," +
				"dayMoneyYear=dayMoneyYear+?3 where user=?4";
		Query q = em.createQuery(jpql).setParameter(1, totalYear).setParameter(2, monthMoneyYear)
				.setParameter(3, dayMoneyYear).setParameter(4, new User(userId));
		q.executeUpdate();
	}

	@Override
	public TenderAccountYear getTenderAccountYearByUser(User user) {
		String jpql = "from TenderAccountYear where user = ?1";
		Query query = em.createQuery(jpql).setParameter(1, user);
		List<TenderAccountYear> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}else{
			return null;
		}
	}

}

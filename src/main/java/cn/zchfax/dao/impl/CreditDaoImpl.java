package cn.zchfax.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.CreditDao;
import cn.zchfax.domain.Credit;
import cn.zchfax.domain.User;

@Repository(value="creditDao")
public class CreditDaoImpl extends ObjectDaoImpl<Credit> implements CreditDao {
	
	/**
	 * 获取积分对象
	 * @param user
	 * @return
	 */
	@Override
	public Credit getCreditByUser(User user){
		String sql = " from Credit  where user = ?1 ";
		Query query = em.createQuery(sql).setParameter(1, user);
		List<Credit> list=query.getResultList();
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	//fu
	public void setCredit(int value,int user_id){
		String  jpql="update Credit set value=?1  where id = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, value).setParameter(2, user_id);
		query.executeUpdate();
		
	}

	//fu
	
	
}

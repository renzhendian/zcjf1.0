package cn.zchfax.dao.impl;


import org.springframework.stereotype.Repository;

import cn.zchfax.dao.TenderRewardDao;
import cn.zchfax.domain.TenderReward;

/**
 *  //1.8.0.4_u3   TGPROJECT-337  qinjun 2014-06-23  start
 * @author wujing
 *
 */
@Repository(value="tenderRewardDao")
public class TenderRewardDaoImpl extends ObjectDaoImpl<TenderReward> implements
		TenderRewardDao {



}

package cn.zchfax.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.AccountCashDao;
import cn.zchfax.domain.AccountCash;
@Repository
public class AccountCashDaoImpl extends ObjectDaoImpl<AccountCash> implements AccountCashDao {

	@Override
	public AccountCash getAccountCashByOrderNo(String orderNo) {
		String jpql = " from AccountCash where orderNo = ?1 ";
		List<AccountCash> list=new ArrayList<AccountCash>();
		Query q=em.createQuery(jpql).setParameter(1, orderNo);
		list=q.getResultList();
		if(list.size() == 0){
			return null;
		}else{
			return list.get(0);
		}
	}

}

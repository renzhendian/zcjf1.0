package cn.zchfax.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowInterestRateDao;
import cn.zchfax.domain.BorrowInterestRate;
import cn.zchfax.domain.User;

@Repository
public class BorrowInterestRateDaoImpl extends ObjectDaoImpl<BorrowInterestRate> implements BorrowInterestRateDao {

	@Override
	public void updateInterestRate(long id, long userId) {
		String sql = "update borrow_interest_rate set user_id = ?1, status=4 where id=?2 and status=1";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, userId).setParameter(2, id);
		query.executeUpdate();
	}

	@Override
	public void borrowerInterestRateFailure() {
		String sql = "update borrow_interest_rate set status=2 where status in(1,4) and now() > DATE_ADD(add_time,INTERVAL 30 DAY)";
		Query query = em.createNativeQuery(sql);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public BorrowInterestRate findByInterestNum(String interestNum, User user) {
		String jpql = "from BorrowInterestRate where user = ?1 and interestNum = ?2";
		Query query = em.createQuery(jpql);
		query.setParameter(1, user);
		query.setParameter(2, interestNum);
		List<BorrowInterestRate> list = query.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public BorrowInterestRate findByInterestNum(String interestNum) {
		String jpql = "from BorrowInterestRate where  interestNum = ?1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, interestNum);
		List<BorrowInterestRate> list = query.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
	

	@Override
	public void update(long id) {
		String sql = "update borrow_interest_rate set status=4 where status=3 and tender_id=?1";
		Query query = em.createNativeQuery(sql).setParameter(1, id);
		query.executeUpdate();
	}

}

package cn.zchfax.dao.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.jam.mutable.MPackage;
import org.springframework.stereotype.Repository;

import cn.zchfax.dao.BorrowTenderDao;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.User;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.account.InvestmentModel;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.StringUtils;

@Repository(value="borrowTenderDao")
public class BorrowTenderDaoImpl extends ObjectDaoImpl<BorrowTender> implements BorrowTenderDao  {

	private static Logger logger = Logger.getLogger(BorrowTenderDaoImpl.class);

	@Override
	public List<BorrowTender> getBorrowTenderList(long borrowid) {
		String jpql = " from BorrowTender where borrow = ?1 and status = 0 ";
		Query  query = em.createQuery(jpql);
		query.setParameter(1, new Borrow(borrowid));
		return query.getResultList();
	}

	@Override
	public List getBorrowTenderListByborrowId(long id, int status) {
		return null;
	}

	@Override
	public List<BorrowTender> getBorrowTenderListByborrowId(long id) {
		String jpql = "select bt from BorrowTender bt join bt.borrow br where br.id = ?1 and bt.status = 0 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, id);
		return query.getResultList();
	}

	@Override
	public List<BorrowTender> getBorrowTenderListByborrow(long id) {
		String jpql = "select bt from BorrowTender bt join bt.borrow br where br.id = ?1 and bt.status in (0,1) ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, id);
		return query.getResultList();
	}

	@Override
	public List getBorrowTenderListByborrowId(long id, long user_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getBorrowTenderListByborrowId(long id, int start, int pernum,
			SearchParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getTenderListByUserId(long user_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getInvestBorrowTenderListByUserid(long user_id, int start,
			int end, SearchParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getInvestBorrowTenderCountByUserid(long user_id,
			SearchParam param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List getSuccessBorrowTenderList(long user_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getSuccessBorrowTenderList(long user_id, int start, int end,
			SearchParam param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double hasTenderTotalPerBorrowByUserid(long bid, long userId) {
		String jpql="select sum(account) from BorrowTender where borrow=?1 and user=?2";
		Query q=em.createQuery(jpql).setParameter(1, new Borrow(bid)).setParameter(2, new User(userId));
		Object ret=q.getSingleResult();
		if(ret==null) return 0;
		return (Double)ret;
	}

	@Override
	public BorrowTender getAssignMentTender(long borrowId){
		String jpql = " from BorrowTender where assignmentId = ?1  ";
		Query query = em.createQuery(jpql).setParameter(1, borrowId);
		List<BorrowTender> list = query.getResultList();
		if(list.size() == 0){
			return null;
		}else{
			return list.get(0);
		}
	}

	@Override
	public double sumCollectionMoney(long userId) {
		String jpql="select sum(waitAccount) from BorrowTender where user=?1";
		Query q=em.createQuery(jpql).setParameter(1, new User(userId));
		Object ret=q.getSingleResult();
		if(ret==null) return 0;
		return (Double)ret;
	}

	@Override
	public int sumTenderByDay() {
		String jpql = " SELECT COUNT(b) FROM BorrowTender b WHERE date(addtime)=curdate() ";
		Query query =  em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 0;
	}

	@Override
	public int sumTenderByTenderNo(String tenderNo) {
		String jpql = "SELECT COUNT(b) FROM BorrowTender b WHERE  subOrdId=?1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, tenderNo);
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 0;
	}

	@Override
	public int sumTender() {
		String jpql = "SELECT count( distinct b.user.id ) FROM BorrowTender b where status in (0,1,3) ";//3为债权转让
		Query query = em.createQuery(jpql);
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 0;
	}
	/**
	 * 
	 * @param time1
	 * @param time2
	 * @return
	 */
	public double getTenderMoneyByTime(String time1,String time2,long userId){
		String sql = "select sum(b.account) from BorrowTender b where user =?1 and status in(0,1,3)";
		if(!StringUtils.isNull(time1).equals("") && !StringUtils.isNull(time2).equals("") ){
			sql = sql+" and addtime>='"+time1+"' and addtime<'"+time2+"'";
		}
		Query query = em.createQuery(sql);		
		query.setParameter(1, new User(userId));
		Object ob = query.getSingleResult();
		if(ob==null) return 0;
		return (Double)ob;


	}

	@Override
	public double countUserCollectionMoney(String time1,String time2,long userId) {
		String sql="select sum(waitAccount) from BorrowTender where user=?1 ";
		if(!StringUtils.isNull(time1).equals("") && !StringUtils.isNull(time2).equals("") ){
			sql = sql+" and addtime>='"+time1+"' and addtime<'"+time2+"'";
		}
		Query q=em.createQuery(sql).setParameter(1, new User(userId));
		Object ret=q.getSingleResult();
		if(ret==null) return 0;
		return (Double)ret;
	}
	@Override
	public int countUserTender(String time1,String time2,long userId) {
		String sql = "SELECT COUNT(b) FROM BorrowTender b where user =?1 and status in (0,1,3) ";//3为债权转让
		if(!StringUtils.isNull(time1).equals("") && !StringUtils.isNull(time2).equals("") ){
			sql = sql+"and addtime>='"+time1+"' and addtime<'"+time2+"'";
		}
		Query query = em.createQuery(sql).setParameter(1, new User(userId));
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 0;
	}

	// v1.8.0.4 TGPROJECT-63 lx 2014-04-16 start
	public double sumInterest(){
		String sql="select sum(repaymentYesinterest) from BorrowTender  ";
		Query q=em.createQuery(sql);
		Object ret=q.getSingleResult();
		if(ret==null) return 0;
		return (Double)ret;
	}
	// v1.8.0.4 TGPROJECT-63 lx 2014-04-16 end

	//TGPROJECT-389 wsl 2014-09-11 start
	@Override
	public double getTenderSumMoney(long userId) {
		String jpql = "select sum(b.account) from BorrowTender b where user =?1 and status in(0,1,3)";
		Query query = em.createQuery(jpql);
		query.setParameter(1, new User(userId));
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}

	@Override
	public int getTenderUseCount(long userId, double money) {
		String sql = " SELECT count(1) from invite_user i where i.invite_user = ?1 and i.is_tender = 1 and i.tender_total >= ?2 ";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, userId);
		query.setParameter(2, money);
		Object ob = query.getSingleResult();
		if (ob !=null) {
			return NumberUtils.getInt(ob.toString());
		}
		return 0;
	}

	@Override
	public double getTenderSumMoneyByBorrowId(long userId,double money,long borrowId) {
		String sql = "SELECT sum(i.tender_total) from invite_user i where i.invite_user = ?1 and i.is_tender = 1 and i.tender_total >= ?2 and i.user_id in(select DISTINCT(bt.user_id) from borrow b join borrow_tender bt where b.id=bt.borrow_id and b.id= ?3) ";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, userId);
		query.setParameter(2, money);
		query.setParameter(3, borrowId);
		Object ob = query.getSingleResult();
		if (ob != null) {
			return NumberUtils.getDouble(ob.toString());
		}
		return 0;
	}
	//TGPROJECT-389 wsl 2014-09-11 end

	@Override
	public Map<String, Double> getUserTenderYieldSum(long userId) {
		String jpql = "select ifnull(sum(repayment_yesinterest),0) as yesInterest ,ifnull(sum(compensation),0) as compen, ifnull(sum(interest_web_site_pay),0) as webinterest ,ifnull(sum(interest_fee),0) as fee from borrow_tender where user_id =?1 and (status =1 or status=3);";
		Query query = em.createNativeQuery(jpql);
		query.setParameter(1, userId);
		List list = query.getResultList();
		Map<String, Double> map = new HashMap<String, Double>();
		  if(list.size()>0){
		    	 Iterator iterator = list.iterator();
		    	 while (iterator.hasNext()) {
		    		 Object[] row = ( Object[]) iterator.next();
		    		 double yesInterest = Double.parseDouble(row[0].toString());
		    		 double compen = Double.parseDouble(row[1].toString());
		    		 double webinterest = Double.parseDouble(row[2].toString());
		    		 double fee = Double.parseDouble(row[3].toString());
		    		 map.put("yesInterest", yesInterest);
		    		 map.put("compen", compen);
		    		 map.put("webinterest", webinterest);
		    		 map.put("fee", fee);
				}
		     }

		
		return map;
	}
	//fu start 投资排行榜  
	@Override
	public List<InvestmentModel> getInvestmentList() {
		String sql = "select u.username,sum(p1.account) as tenderMoney from borrow_tender p1 "
					+"left join borrow b on b.id=p1.borrow_id left join user u on u.user_id=p1.user_id "
					+"where ((b.status in (3,6,7,8) and b.type<> 110) or (b.status in(1,3,6,7,8) and b.type=110)) group by u.username order by tenderMoney desc limit 0,10";
		List<InvestmentModel> list1 = new ArrayList<InvestmentModel>();	
		List list = new ArrayList();	
		Query  query = em.createNativeQuery(sql);
		list= query.getResultList();
		int i=0;
		if(list.size()>0){
	    	 Iterator iterator = list.iterator();
	    	 while (iterator.hasNext()) {
	    		 Object[] row = ( Object[]) iterator.next();
	    		 String username = row[0].toString();
	    		 double money = Double.parseDouble(row[1].toString());
	    		 InvestmentModel im = new InvestmentModel();	
	    		 im.setUsername(StringUtils.hideLastChar(username, 3));
	    		 im.setMoney(money);
	    		 im.setId(i+1);
	    		 list1.add(i, im);
	    		 i++;
			}
	     }	
		return list1;
	}
	
	@Override
	public List<User> getTenderListOver(long borrowid) {
		String jpql = "select distinct user_id from borrow_tender where borrow_id = ?1 and status = 0 and account >= 5000";
		Query  query = em.createNativeQuery(jpql);
		query.setParameter(1, new Borrow(borrowid));
		List list = new ArrayList();
		list= query.getResultList();
		List<User> userList = new ArrayList<User>();
		int i =0;
		if(list.size()>0){
	      Iterator iterator = list.iterator();
	      while (iterator.hasNext()) {
    		 int user_id =  Integer.parseInt(iterator.next().toString());
    		 User u = new User();	
    		 u.setUserId(user_id);;
    		 userList.add(u);
			}
	     }	
		return userList;
	}
	
	
	
	//fu end
}

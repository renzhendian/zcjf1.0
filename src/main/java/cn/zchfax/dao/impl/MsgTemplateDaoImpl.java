package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.MsgTemplateDao;
import cn.zchfax.domain.MsgTemplate;
@Repository("msgTemplateDao")
public class MsgTemplateDaoImpl extends ObjectDaoImpl<MsgTemplate> implements MsgTemplateDao {

}

package cn.zchfax.dao.impl;

import org.springframework.stereotype.Repository;

import cn.zchfax.dao.RuleKeyDao;
import cn.zchfax.domain.RuleKey;
@Repository(value="ruleKeyDao")
public class RuleKeyDaoImpl extends ObjectDaoImpl<RuleKey> implements
		RuleKeyDao {

}

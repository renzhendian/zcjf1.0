package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.GoodsPic;

public interface GoodsPicDao extends BaseDao<GoodsPic> {
	/**
	 * 通过goodId查找goodspic
	 * @param id
	 * @return
	 */
	public List<GoodsPic> getGoodsPicByGoodsId(int id);
}

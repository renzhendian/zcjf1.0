package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.AttestationType;
import cn.zchfax.model.SearchParam;


public interface AttestationTypeDao extends BaseDao<AttestationType>{
	
	public List<AttestationType> getList(SearchParam param);
}

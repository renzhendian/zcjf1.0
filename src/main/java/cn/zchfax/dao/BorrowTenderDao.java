package cn.zchfax.dao;

import java.util.List;
import java.util.Map;

import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.User;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.account.InvestmentModel;

/**
 * @author wujing
 *@date 2013-12-19 下午1:45:16 
 */
public interface BorrowTenderDao extends BaseDao<BorrowTender> {
	/**
	 * 根据borrowid获取BorrowTender列表
	 * @param borrowid
	 * @return
	 */
	public List getBorrowTenderList(long borrowid);
	/**
	 *  根据borrowid获取BorrowTender列表的前num条
	 * @param id
	 * @param num
	 * @return
	 */
	public List getBorrowTenderListByborrowId(long id,int num);

	/**
	 * 获取未还款的tender
	 * @param id
	 * @return
	 */
	public List getBorrowTenderListByborrowId(long id) ;

	/**
	 * 获取还款和未还款的tender
	 * @param id
	 * @return
	 */
	public List<BorrowTender> getBorrowTenderListByborrow(long id);

	public List getBorrowTenderListByborrowId(long id,long user_id) ;

	public List getBorrowTenderListByborrowId(long id,int start,int pernum,SearchParam param);

	public List getTenderListByUserId(long user_id);

	public List getInvestBorrowTenderListByUserid(long user_id,int start,int end,SearchParam param);

	public int getInvestBorrowTenderCountByUserid(long user_id,SearchParam param);

	public List getSuccessBorrowTenderList(long user_id);

	public List getSuccessBorrowTenderList(long user_id, int start, int end,SearchParam param);

	public double hasTenderTotalPerBorrowByUserid(long bid,long userId);
	public BorrowTender getAssignMentTender(long borrowId);
	public double sumCollectionMoney(long userid);      //获取用户的待收本金总和

	/**
	 * 获取投标当天的总投标数
	 * @return
	 */
	public int sumTenderByDay();

	/**
	 * 根据投标订单号查询投标记录
	 * @param tenderNo
	 * @return
	 */
	public int sumTenderByTenderNo(String tenderNo);

	/**
	 * 计算网站总投资人数
	 * @return
	 */
	public int sumTender();

	public double getTenderMoneyByTime(String time1,String time2,long userId);

	public int countUserTender(String time1,String time2,long userId);
	public double countUserCollectionMoney(String time1,String time2,long userId);
	//v1.8.0.4 TGPROJECT-63 lx 2014-04-16 start
	/**
	 * 统计所有收益
	 * @return
	 */
	public double sumInterest();
	//v1.8.0.4 TGPROJECT-63 lx 2014-04-16 end

	//TGPROJECT-389 wsl 2014-09-11 start
	/**
	 * 获取指定投资人的总投资金额
	 * @param userId
	 * @return
	 */
	public double getTenderSumMoney(long userId);

	/**
	 * 查询当前投资人总邀请人投资达到平台规定的人数总和
	 * @param userId
	 * @param money
	 * @return
	 */
	public int getTenderUseCount(long userId, double money);

	/**
	 * 查询指定标当前投资人邀请人的投资总额
	 * @param userId
	 * @param money
	 * @param borrowId
	 * @return
	 */
	public double getTenderSumMoneyByBorrowId(long userId, double money, long borrowId);
	//TGPROJECT-389 wsl 2014-09-11 end
	
	/**
	 * 获取投资人收益总和
	 * @param userId
	 * @return
	 */
	public Map<String, Double> getUserTenderYieldSum(long userId);
	
	
	/**
	 * 投资排行榜
	 * @return
	 */
	public List<InvestmentModel> getInvestmentList();
	
	
	public List<User> getTenderListOver(long borrowid);
}

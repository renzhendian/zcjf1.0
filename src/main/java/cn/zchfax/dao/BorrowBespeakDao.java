package cn.zchfax.dao;


import java.util.List;

import cn.zchfax.domain.BorrowBespeak;

public interface BorrowBespeakDao extends BaseDao<BorrowBespeak> {

	public int countBespeakUser(long borrowId);

	public List<BorrowBespeak> findByBorrow(long id);
	
	public double getSumBorrowBespesk();
	
	public int getBorrowBespeskCount();
	
	
}

package cn.zchfax.dao;

import cn.zchfax.domain.BorrowInterestRate;
import cn.zchfax.domain.User;

public interface BorrowInterestRateDao extends BaseDao<BorrowInterestRate>{

	public void updateInterestRate(long id, long userId);

	public void borrowerInterestRateFailure();

	public BorrowInterestRate findByInterestNum(String interestNum, User user);

	public BorrowInterestRate findByInterestNum(String interestNum);
	
	public void update(long id);

	
}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.BorrowDetail;

/**
 *v1.8.0.3_u3 TGPROJECT-335  qinjun 2014-06-16  
 */
public interface BorrowDetailDao extends BaseDao<BorrowDetail> {
}

package cn.zchfax.dao;

import java.util.List;

import cn.zchfax.domain.GoodsCategory;
/**
 * 
 * @author lx
 *
 */
public interface GoodsCategoryDao extends BaseDao<GoodsCategory> {

	
	public List<GoodsCategory> getListByParentId(int parentId);
	public List<GoodsCategory> getChildList();
}

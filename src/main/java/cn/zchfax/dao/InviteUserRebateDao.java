package cn.zchfax.dao;

import cn.zchfax.domain.InviteUserRebate;

/**
 * 
 * @author lx TGPROJECT-302 add
 *
 */
public interface InviteUserRebateDao extends BaseDao<InviteUserRebate> {
	
}

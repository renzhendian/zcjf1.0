package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.PrizeGoods;
import cn.zchfax.domain.PrizeRule;
import cn.zchfax.domain.PrizeUser;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.prize.PrizeResult;


/**
 *	处理抽奖业务
 *	v1.8.0.4_u4  TGPROJECT-367 qinjun 2014-07-16
 */
public interface PrizeService {
	
	/**
	 * 根据条件查询抽奖用户
	 * @param param
	 * @return
	 */
	public PageDataList<PrizeUser> getAllPrizeUserList(SearchParam param);
	
	/**
	 * 获取所有的抽奖规则
	 * @return
	 */
	public List<PrizeRule> getAllPrizeRule(); 
	
	/**
	 * 根据id获取抽奖规则对象
	 * @param id
	 * @return
	 */
	public PrizeRule getPrizeRuleById(long id);
	
	/**
	 * 根据prizeType获取抽奖规则对象
	 * @param ruleId
	 * @param user
	 * @param money
	 * @return
	 */
	public PrizeRule getPrizeRuleByPrizeType(int prizeType);
	
	/**
	 * 
	 * @param ruleId
	 * @param user
	 * @param money
	 * @return
	 */
	public PrizeResult extractionPrize(long ruleId, User user, double money);
	
	/**
	 * 更新抽奖规则
	 * @param prizeRule
	 */
	public void updatePrizeRule(PrizeRule prizeRule);
	
	/**
	 * 添加抽奖规则
	 * @param prizeRule
	 */
	public void addPrizeRule(PrizeRule prizeRule);
	
	/**
	 * 查询所有的奖品
	 * @param ruleId  规则id
	 * @return
	 */
	public List<PrizeGoods> goodsList(long ruleId);
	
	/**
	 * 根据id获取抽奖产品对象
	 * @param id
	 * @return
	 */
	public PrizeGoods getPrizeGoodsById(long id);
	
	
	/**
	 * 保存奖品规则
	 * @param prizeGoods
	 */
	public void savePrizeGoods(PrizeGoods prizeGoods);
	/**
	 * 更新奖品规则
	 * @param prizeGoods
	 */
	public void updatePrizeGoods(PrizeGoods prizeGoods);
	
}

package cn.zchfax.service;

import cn.zchfax.domain.BorrowInterestRate;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;


public interface BorrowInterestRateService {

	public PageDataList<BorrowInterestRate> list(SearchParam param);

	public void add(BorrowInterestRate bir);

	public BorrowInterestRate find(long id);

	public void updateInterestRate(long id, long userId);

	public void borrowerInterestRateFailure();

	public BorrowInterestRate findByInterestNum(String interestNum, User user);
	
	/**
	 * 根据加息券号查询
	 * @param interestNum
	 * @return
	 */
	public BorrowInterestRate findByInterestNum(String interestNum);
	
	public void update(BorrowInterestRate bir);

}

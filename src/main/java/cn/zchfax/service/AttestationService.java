package cn.zchfax.service;

import cn.zchfax.api.pay.RealNameCertQuery;
import cn.zchfax.domain.Attestation;
import cn.zchfax.domain.Credit;
import cn.zchfax.domain.CreditLog;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface AttestationService {
	
	public PageDataList  getSearchUserCertify(SearchParam p);
	public PageDataList getAttestationBySearchParam(SearchParam p);	
	public void updateCredit(Credit c);	
	public void addCreditLog(CreditLog cl);
	public Attestation getAttestationById(int id);
	public void updateAttestation(Attestation att);
	public Attestation addAttestation(Attestation att);
	public RealNameCertQuery showYjfRealNameMsg(User user);
	
	public int getAttestations(long userid,int typeid);
	//v1.8.0.4 TGPROJECT-58 lx start
	public void deleteAttestation(long userid,int attestationId);
	//v1.8.0.4 TGPROJECT-58 lx start
}

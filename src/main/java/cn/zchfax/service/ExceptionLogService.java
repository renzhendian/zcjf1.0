package cn.zchfax.service;

import cn.zchfax.domain.ExceptionLog;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;


public interface ExceptionLogService {
	
	public ExceptionLog find(long id);
	
	public void save(ExceptionLog item);
	
	public PageDataList<ExceptionLog> page(SearchParam param);


}

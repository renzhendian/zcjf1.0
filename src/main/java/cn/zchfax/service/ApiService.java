package cn.zchfax.service;

import java.util.List;
import java.util.Map;

import javax.jws.WebParam;

import org.springframework.stereotype.Service;

import cn.zchfax.domain.AccountBank;
import cn.zchfax.domain.AccountCash;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.AccountRecharge;
import cn.zchfax.domain.AccountWebDeduct;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowCollection;
import cn.zchfax.domain.BorrowRepayment;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.GlodTransfer;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCache;
import cn.zchfax.model.ApiPayParamModel;
import cn.zchfax.model.BorrowParam;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.APIModel.AccountCashModel;
import cn.zchfax.model.APIModel.WebPayModel;
@Service
public interface ApiService {
	public String hfFreezeBg(String apiUserCustId ,double amount);  //汇付冻结
	public void hfUnfreezeBg(User user,String trxId,long id,double money,String type,List<Object> taskList);
	
	/**
	 * 更具第三方获取查询param
	 */
	public SearchParam getdrawBankParam();
	/**
	 * vip处理
	 */
	public void verifyVipSuccess(UserCache cache , double vipFee,List<Object> taskList);
	/**
	 * 绑卡回调处理
	 */
	public AccountBank addAccountBank(Object object);
	/**
	 * 用户提交实名处理
	 * @param user
	 */
	public void applyRealname(User user);
	/**
	 * 发标，产生订单号并保存。
	 */
	public void addBorrowCreateTradeNo(Borrow model, List<Object> taskList);
	
	/**
	 * 放款，放款接口
	 */
	public void FullSuccessLoanMoney(String apiNo, Borrow model,BorrowTender tender, User borrowUser,
			User tenderUser, List<Object> taskList,String apiMethodType);
	 /**
	  * 放款，分发奖励
	  */
	public void FullSuccessAward(Borrow model, BorrowTender tender, User borrowUser,
			User tenderUser, List<Object> taskList, double awardValue);
	
	/**
	 * 管理费处理方法：
	 * 由于汇付没有直接从用户划款值商户的接口，所以在处理管理费时模拟一笔
	 * 还款操作，付款人为借款者，收款方为平台.扣费的方式为评分到每一个投资者的
	 * 投标订单里面去扣除
	 * 确保这笔资金是从借款人账户里面扣除的
	 */
	public void doBorrowFee(List<Object> taskList,
			List<BorrowTender> tenderList, double borrowfee, Borrow borrow,
			String menageFee);
	/**
	 * 放款，收取手续费
	 */
    public void FullSuccessDeductFee(List<Object> taskList,
			List<BorrowTender> tenderList, double borrowfee, Borrow borrow,
			String menageFee);
	/**
	 * 放款，风险备用金
	 */
	public void FullSuccessRiskFee(List<Object> taskList,
			List<BorrowTender> tenderList, double riskFee, Borrow borrow,
			String menageFee);
	/**
	 * 还款给网站，还款给网站接口
	 */
	public void repayToWebSiteLoanMoney(BorrowRepayment repay,double money,Borrow borrow ,List<Object> taskList );
	/**
	 * 还款给网站，最后一步，封装对象<易极付等接口用>
	 */
	public void repayToWebSiteLoanMoneyEnd(ApiPayParamModel apiModel,Borrow model,BorrowRepayment repay,User borrowUser,List<Object> taskList,String apiMethodType);
	/**
	 * 网站垫付，还款给网站接口
	 */
	public void webSitePayLoanMoney(Borrow borrow,List<Object> taskList,User tenderUser, double money);
	/**
	 * 网站垫付，还款<本金、利息、逾期罚息>最后一步，划款过程。
	 */
	public void webSitePayLoanMoneyEnd(Borrow borrow,ApiPayParamModel apm, BorrowRepayment br, List<Object> taskList,String apiMethodType);
	/***
	 * 好利贷网站，特殊垫付。
	 */
	public void haoLiDaidWebSitePayLoanMoney(Borrow borrow,List<Object> taskList,User tender_user, double dealMoney);
	/**
	 * 利贷最后划款操作、易极付接口专用
	 * @param taskList
	 * @param borrow
	 * @param br
	 * @param tendersStr
	 * @param totalToPay
	 * @param apiMethodType
	 */
	public void haoLiDaidWebSitePayLoanMoneyEnd(List<Object> taskList, Borrow borrow,
			BorrowRepayment br, ApiPayParamModel apm,
			String apiMethodType);
	/**
	 * 网站垫付，判断资金是否足够
	 */
	public void webSitePayMoneyEnough(List<Object> taskList, double webSitePayMoney);
	
	/**
	 * 投标冻结接口
	 * @param borrowTender
	 */
	public void addTenderFreezeMoney(BorrowTender borrowTender,BorrowParam param,String apiMethodType,List<Object> taskList);
	
	
	/**
	 * 投流转标，直接放款方法
	 */
	public void flowBorrowLoan(BorrowTender borrowTender,User tenderUser,Borrow borrow ,User borrowUser,List<Object> taskList);
	/**
	 * 网站后台充值
	 */
	public void webRecharge(AccountRecharge ar, List<Object> taskList);
	//v1.8.0.4_u4  TGPROJECT-365  qinjun 2014-07-14 start
	/**
	 * 网站划款
	 */
	public void webTransfer( List<Object> taskList,User toUser,double money);
	//v1.8.0.4_u4  TGPROJECT-365  qinjun 2014-07-14 end
	/**
	 * 网站后台账户间转账
	 */
	public void glodTransfer( GlodTransfer gt, List<Object> taskList);
	/**
	 * 网站后台扣款
	 */
	public void webDeduct(AccountWebDeduct awd , List<Object> taskList);
	/**
	 * 撤标过程
	 */
	public void faillBorrow(List<Object> taskList, Borrow model,String apiMethodType);
	/**
	 * 还款，放款接口
	 */
	public void repayLoanMoney(String apiId, double money,List<Object> taskList,Borrow borrow,BorrowCollection c,String apiMethodType);
	/**
	 * 还款，放款接口;
	 */
	public void payManageFee(String apiId, double money,List<Object> taskList,Borrow borrow);
	/**
	 * 还款，扣除利息管理费(易极付)；扣除利息管理费与归还利息（汇付）
	 */
	public void repayBorrowFee(Borrow model,BorrowRepayment repay ,User tenderUser,double borrowFee,double interest,List<Object> taskList,BorrowCollection c,String apiMethodType);
	/**
	 * 还款，放款接口
	 */
	public void repayLoanMoneyEnd(List<Object> taskList,
			List<Object> borrowFeeTaskList, Borrow model,
			BorrowRepayment repay, User borrowUser,ApiPayParamModel apm,String apiMethodType);
	/***
	 * 流转标还款,放款接口
	 */
	public void flowRepayLoanMoney(String apiId,ApiPayParamModel apiModel,double money ,String apiMethodType,Borrow borrow,List<Object> taskList);
	/***
	 * 流转标还款,利息管理费
	 */
	public void flowRepayBorrowFee(Borrow model, BorrowRepayment repayment,
			List<Object> interestFeeList, User tenderUser, double borrow_fee,double repayments ,BorrowTender tender, List<Object> taskList);
	/***
	 * 流转标还款,最后调用,易极付使用 划账。
	 */
	public void flowRepayLoanMoneyEnd(List<Object> taskList,List<Object> interestFeeList,Borrow model
			,BorrowRepayment repayment,ApiPayParamModel apiModel ,User borrowUser,String apiMethodType );
	
	 /**
	  * 放款，解冻资金接口
	  */
	public void FullSuccessUnFreezeMoney(User tenderUser, String trxId, long borrowId,
			double money, List<Object> taskList,
			String apiMethodType);
	/**
	 * 处理取现走后台方式的方法
	 * @param cash
	 * @param taskList
	 */
	public void doCash(AccountCash cash , List<Object> taskList,AccountLog log,double fee,double servFee,String cardNo);
	
	/**
	 * 取现复审，第三方处理
	 * @param cash
	 * @param taskList
	 * @return
	 */
	public double  verifyCash(AccountCash cash,List<Object> taskList);
	
	/**
	 * 是否需要注册第三方
	 * @param user
	 * @return
	 */
	public Object doRegisterApi(User user);
	/**
	 * 用户第三方登陆
	 * @param user
	 * @return
	 */
	public Object apiLogin(User user);
	
	/**
	 * 第三方校验
	 */
	public void checkApiCashMessage(User user,String payPassword);
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 start
	/**
	 * 查询账户资金
	 * @param user
	 * @return
	 */
	public Map<String, String> findApiAccount(User user) throws Exception;
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 stop
	
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
	/**
	 * 第三方发标、投标、自动投标拦截
	 */
	public void checkApiLoan(User user);
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	/**
	 * 第三方判断是否能满标复审
	 * @param user
	 */
	public void isCanVerifyFullSuccess(long borrowId);
	/**
	 * 积分兑换现金触发接口
	 * @param user
	 * @param money
	 * @param taskList
	 */
	public void creditForMoney(User user,double money ,List<Object> taskList);
	
	//1.8.0.4_u3   TGPROJECT-337  qinjun 2014-06-23  start
	/**
	 * 处理关于平台划账给用户的方法
	 * @param payModel
	 * @param money
	 * @param taskList
	 */
	public void doWebPayMoney(WebPayModel payModel,List<Object> taskList);
	//1.8.0.4_u3   TGPROJECT-337  qinjun 2014-06-23  end
	//帝业投资自动扣款功能   start
	
	/**
	 * 双乾用户扣款给平台
	 * @param user
	 * @param money
	 */
	public void doMmmUserPayWeb(User user,double money,List<Object> taskList);
	
	//帝业投资自动扣款功能   end
	
	/**
	 * 借款人划款给用户
	 * @param taskList
	 * @param borrowfee
	 * @param borrow
	 * @param toUser
	 */
	public void	borrowertTransferToUser(List<Object> taskList,double borrowfee,Borrow borrow,User toUser);
	
	
	/**
	 * 汇付提现
	 * @param cashModel
	 * @param cash
	 * @param log
	 */
	public void doHfCashService(AccountCashModel cashModel, AccountCash cash,AccountLog log);
	
	public void doBankReturn(AccountCashModel cashModel);
}

package cn.zchfax.service;

import cn.zchfax.domain.SystemLog;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;


public interface SystemLogService {
	
	public SystemLog find(long id);
	
	public void save(SystemLog item);
	
	public PageDataList<SystemLog> page(SearchParam param);


}

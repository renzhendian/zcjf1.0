package cn.zchfax.service;

import cn.zchfax.domain.BorrowRepayment;
import cn.zchfax.domain.PriorRepayLog;
import cn.zchfax.domain.Rule;
import cn.zchfax.model.MsgReq;
import cn.zchfax.model.borrow.BorrowModel;


/**
 * AutoService
 * @author fuxingxing
 * 
 */
public interface AutoService {
	public void fullSuccess(BorrowModel wrapModel);
	public void fail(BorrowModel wrapModel);
	public void repay(BorrowRepayment repay);
	public void priorRepay(BorrowRepayment repay,PriorRepayLog ppLog);
	public void repayToWebSite(BorrowRepayment repay);
	public void flowRepay(BorrowRepayment repay);
	public void msgReq(MsgReq req);
	
	/**
	 * 计算逾期天数及逾期罚息
	 */
	public void lateDaysAndInterest();
	
	//v1.8.0.3_u3 TGPROJECT-334  2014-06-11  qinjun start 
	/**
	 * 汇丰贷提前还款
	 * @param repay
	 * @param ppLog
	 * @exception Exception
	 */
	public void hfPriorRepay(BorrowRepayment repay,PriorRepayLog ppLog) throws Exception;
	//v1.8.0.3_u3 TGPROJECT-334  2014-06-11  qinjun end
	
	//TGPROJECT-372 老账房项目提前还款  2014-07-21 wujing start
	/**
	 * 老账房项目提前还款方法：
	 * 需求说明：提前还款需多支付一个月利息（即：提前还款当前的下一期的利息）
	 * @param repay
	 * @param ppLog
	 */
	public void LZFPriorRepay(BorrowRepayment repay, PriorRepayLog ppLog)throws Exception;
	//TGPROJECT-372 老账房项目提前还款  2014-07-21 wujing end
}

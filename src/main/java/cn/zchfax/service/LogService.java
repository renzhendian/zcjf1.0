package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.SitePayLog;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

/**
 * 日志记录接口  site_pay_log  等操作接口
 * @author zxc
 *
 */
public interface LogService {
	
	public PageDataList getSitePayList(SearchParam param);
	/**
	 * 查询，所有记录
	 * @param param
	 * @return
	 */
	public List<SitePayLog> getAllSitePayList(SearchParam param);
}

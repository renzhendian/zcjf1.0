package cn.zchfax.service;

import cn.zchfax.domain.TenderProperty;

/**
 * @author wujing
 *@date 2013-12-19 上午10:56:55 
 */
public interface TenderPropertyService {
	
	public void addTenderProperty(TenderProperty tenderProperty);

}

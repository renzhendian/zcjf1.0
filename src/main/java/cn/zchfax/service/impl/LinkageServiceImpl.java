package cn.zchfax.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.dao.LinkageDao;
import cn.zchfax.domain.Linkage;
import cn.zchfax.service.LinkageService;
@Service(value="linkageService")
@Transactional
public class LinkageServiceImpl implements LinkageService {

	@Autowired
	LinkageDao linkageDao;
	@Override
	public Linkage getLinkageById(int id) {
		return linkageDao.getLinkageById(id);
	}

}

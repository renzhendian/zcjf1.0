package cn.zchfax.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.dao.TenderPropertyDao;
import cn.zchfax.domain.TenderProperty;
import cn.zchfax.service.TenderPropertyService;
/**
 * @author wujing 
 * @version 创建时间：2013-12-19 上午11:05:52
 * 类说明
 */
@Service(value="tenderPropertyService")
@Transactional
public class TenderPropertyServiceImpl extends BaseServiceImpl implements
		TenderPropertyService {

	@Autowired
	private TenderPropertyDao tenderPropertyDao;
	
	@Override
	public void addTenderProperty(TenderProperty tenderProperty) {
		tenderPropertyDao.save(tenderProperty);

	}

	
}

package cn.zchfax.service.impl;

import org.springframework.stereotype.Service;

import cn.zchfax.dao.UserLogDao;
import cn.zchfax.service.UserLogService;
@Service
public class UserLogServiceImpl implements UserLogService {

	private UserLogDao userLogDao;
	
	public UserLogDao getUserLogDao() {
		return userLogDao;
	}

	public void setUserLogDao(UserLogDao userLogDao) {
		this.userLogDao = userLogDao;
	}

}

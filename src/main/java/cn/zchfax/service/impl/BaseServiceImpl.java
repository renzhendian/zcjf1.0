package cn.zchfax.service.impl;

/**
 * 基础工具类
 */
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import cn.zchfax.api.chinapnr.ChinapnrHelper;
import cn.zchfax.api.chinapnr.ChinapnrModel;
import cn.zchfax.api.chinapnr.QueryFssAccts;
import cn.zchfax.api.moneymoremore.MmmHelper;
import cn.zchfax.api.moneymoremore.MmmLoanOrderQuery;
import cn.zchfax.api.moneymoremore.MmmLoanTransfer;
import cn.zchfax.api.moneymoremore.MmmResultCode;
import cn.zchfax.api.moneymoremore.MmmToLoanTransferAudit;
import cn.zchfax.api.pay.ApplyMobileBinding;
import cn.zchfax.api.pay.ApplyWithDraw;
import cn.zchfax.api.pay.BankCodeBindingAddInfo;
import cn.zchfax.api.pay.BankCodeBindingRemove;
import cn.zchfax.api.pay.BankNoQuery;
import cn.zchfax.api.pay.DeductDepositApply;
import cn.zchfax.api.pay.DepositQuery;
import cn.zchfax.api.pay.PayModelHelper;
import cn.zchfax.api.pay.RealNameCertQuery;
import cn.zchfax.api.pay.RealNameCertSave;
import cn.zchfax.api.pay.SmsCaptcha;
import cn.zchfax.api.pay.SmsConfirmVerifyCode;
import cn.zchfax.api.pay.TradeClosePoolTogether;
import cn.zchfax.api.pay.TradeCreatePoolReverse;
import cn.zchfax.api.pay.TradeCreatePoolTogether;
import cn.zchfax.api.pay.TradePayPoolReverse;
import cn.zchfax.api.pay.TradePayPoolTogether;
import cn.zchfax.api.pay.TradePayerApplyPoolTogether;
import cn.zchfax.api.pay.TradePayerQuitPoolTogether;
import cn.zchfax.api.pay.TradePoolReceiveBorrow;
import cn.zchfax.api.pay.TradeTransfer;
import cn.zchfax.api.pay.UserAccountQuery;
import cn.zchfax.api.pay.VerifyFacade;
import cn.zchfax.api.pay.WithdrawQquery;
import cn.zchfax.api.pay.YjfRegister;
import cn.zchfax.api.pay.YzzNewWithraw;
import cn.zchfax.context.Global;
import cn.zchfax.context.MmmType;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.ChinaPnrPayModel;
import cn.zchfax.domain.DrawBank;
import cn.zchfax.domain.MmmPay;
import cn.zchfax.domain.PriorRepayLog;
import cn.zchfax.domain.SitePayLog;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserAmount;
import cn.zchfax.domain.UserAmountLog;
import cn.zchfax.domain.YjfPay;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.exception.ManageBussinessException;
import cn.zchfax.model.BorrowParam;
import cn.zchfax.tool.javamail.Mail;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.OrderNoUtils;
import cn.zchfax.util.StringUtils;

public class BaseServiceImpl {
	
	private Logger logger=Logger.getLogger(BaseServiceImpl.class);
	private final String tips = Global.getValue("api_name")+"提醒：";
	/**
	 * 封装资金记录
	 * @param log
	 * @param act
	 * @param operateValue
	 */
	public void fillAccountLog(AccountLog log,Account act,double operateValue){
		fillAccountLog(log,act,operateValue,0,"");
	}

	/**
	 * 是否开通线上环境配置。
	 * @return
	 */
	public boolean isOnlineConfig(){
		return "1".equals(Global.getValue("config_online"));
	}
	
	/**
	 * 是否开通第三方接口
	 * @return
	 */
	public boolean isOpenApi(){
		return "1".equals(Global.getValue("open_yjf"));
	}
	
	/**
	 * 激活发送邮件
	 * @param user
	 * @throws Exception
	 */
	public void sendMail(User user) throws Exception {
		String to = user.getEmail();
		Mail m = Mail.getInstance();
		m.setTo(to);
		m.readActiveMailMsg();
		m.replace(user.getUsername(), to, "/user/active.html?id="
				+ m.getdecodeIdStr(user));
		logger.debug("Email_msg:" + m.getBody());
		m.sendMail();
	}
	
	/**
	 * 封装资金记录
	 * @param log
	 * @param act
	 * @param operateValue
	 */
	public void fillAccountLog(AccountLog log,Account act,double operateValue,String remark){
		fillAccountLog(log,act,operateValue,0,remark);
	}
	
	public void fillAccountLog(AccountLog log,Account act,double operateValue,long borrow_id,String remark){
		fillAccountLog(log,act,log.getUser(),log.getToUser(),operateValue,borrow_id,remark);
	}
	public void fillAccountLog(AccountLog log,Account act,User user,User toUser,double operateValue,long borrow_id,String remark){
		fillAccountLog(log,log.getType(),act,user,toUser,operateValue,borrow_id,remark);
	}
	public void fillAccountLog(AccountLog log,String type,Account act,User user,User toUser,double operateValue,long borrow_id,String remark){
		log.setType(type);
		log.setUser(user);
		log.setToUser(toUser);
		log.setMoney(operateValue);
		log.setTotal(act.getTotal());
		log.setUseMoney(act.getUseMoney());
		log.setNoUseMoney(act.getNoUseMoney());
		log.setCollection(act.getCollection());
		log.setRepay(act.getRepay());
		log.setRemark(remark);
		log.setAddtime(new Date());
	}
	
	public void fillPriorRepayLog(PriorRepayLog log ,User user,String type,double account , double money){
		log.setUser(user);
		log.setType(type);
		log.setAccount(account);
		log.setMoney(money);
		log.setAddtime(new Date());
	}
	
	public void fillSitePayLog(SitePayLog SitePayLog,double accountPay, double accountTotal, Borrow borrow, BorrowTender borrowTender,
			double moneyPay,double moneyTotal, String type, User user){
		
		SitePayLog.setAccountPay(accountPay);//实际垫付金额
		SitePayLog.setAccountTotal(accountTotal);
		SitePayLog.setAddtime(new Date());
		SitePayLog.setBorrow(borrow);
		SitePayLog.setBorrowTender(borrowTender);
		SitePayLog.setMoneyPay(moneyPay);
		SitePayLog.setMoneyTotal(moneyTotal);
		SitePayLog.setType(type);
		SitePayLog.setUser(user);
	}
	
	public UserAmountLog fillUserAmountLog(UserAmount ua,String type,double operateValue,String remark,String ip){
		UserAmountLog amountLog=new UserAmountLog();
		amountLog.setUser(ua.getUser());
		amountLog.setType(type);
		amountLog.setAccount(Math.abs(operateValue));
		amountLog.setAccountTotal(ua.getCredit());
		amountLog.setAccountUse(ua.getCreditUse()-operateValue);
		amountLog.setAccountNouse(ua.getCreditNouse()+ operateValue);
		amountLog.setRemark(remark);
		amountLog.setAddtime(new Date());
		amountLog.setAddip(ip);
		return amountLog;
	}
	
	protected String getLogRemark(Borrow b){
		String s="<a href='"+Global.getString("weburl")+
				"/invest/detail.html?borrowid="+b.getId()+"' target=_blank>"+
				b.getName()+"</a>";
		return s;
	}
	
	public String getRequestParams(HttpServletRequest request ){
		String params = "";
		try {
			Enumeration  e= (Enumeration)request.getParameterNames();   
			 while(e.hasMoreElements())     {   
			    String parName=(String)e.nextElement();   
			    String value= request.getParameter(parName);
			    params += parName + "=" + value + "&";
			 } 
		} catch (Exception e) {
			logger.error(e);
		}
         return params;		 
	}
	
	public void sendActiveMail(User user){
		try {
			String to = user.getEmail();
			Mail m = Mail.getInstance();
			m.setSubject(Global.getValue("email_from_name"));
			m.setTo(to);
			m.readActiveMailMsg();
			m.replace(user.getUsername(), to, "/user/active.html?id="+m.getdecodeIdStr(user));
			logger.debug("Email_msg:" + m.getBody());
			m.sendMail();
		} catch (Exception e) {
			logger.error(e);
			throw new BussinessException("注册发送邮件失败！");
		}
	}
	
	
	/**---------------------------- 所有易极付接口处理方法如下 ---------------------------*/
	
	
	/**
	 * 易极付用户注册
	 * @param user
	 * @return
	 */
	public synchronized YjfRegister userRegister(User user){
		YjfRegister  pm = null;
		if(isOpenApi()){
			pm = PayModelHelper.userRegister(user);
			logger.info("returnMsg: " +  pm.getResultMessage() +  "  returnCode" + pm.getResultCode());
			//v1.8.0.3_u2 TGPROJECT-293 2014-05-29   qinjun start
			if(!pm.getResultCode().contains("EXECUTE_SUCCESS")){
				if(!pm.getResultCode().contains("REGISTER_SUCCESS_UNKOW_REAL")){
					//易极付更新接口，用户在其他易极付网站实名过，此状态为用户开户成功、但未实名认证
					throw new BussinessException(tips +"身份证信息，" + pm.getResultMessage());
				}
			}
			//v1.8.0.3_u2 TGPROJECT-293 2014-05-29   qinjun  end
		}
		return pm;
	}
	
	 /**实名认证
		 * realNameCert.query 实名认证
		 *  "message":"添加成功","resultCode":"EXECUTE_SUCCESS","success":true
		 */
		public  RealNameCertSave realNameCertSave(User user){
			RealNameCertSave rcs = null;
			if(isOpenApi()){
					rcs = PayModelHelper.realNameCertSave(user);
				if(!"EXECUTE_SUCCESS".equals( rcs.getResultCode() )){
					throw new BussinessException(tips + rcs.getMessage()+rcs.getResultCode());
				}
			}
			return rcs;
		}
		
		/**
		 * 用户易极付 实名认证查询
		 * @return
		 */
	    public	RealNameCertQuery realNameCertQuery(String apiId){
	    	if(isOpenApi()){
	    		RealNameCertQuery rnc = PayModelHelper.realNameCertQuery(apiId);
	    		return rnc;
	    	}
	    	return null;
	    }
		
		/**
		 * 申请绑定手机
		 * @param apiId
		 * @param mobile
		 * @return
		 */
		public ApplyMobileBinding applyMobileBinding(String apiId, String mobile){
			if(isOpenApi()){
				ApplyMobileBinding amb = PayModelHelper.applyMobileBinding(apiId, mobile);
				if(!amb.getResultCode().contains("EXECUTE_SUCCESS")){
					throw new BussinessException("获取验证码失败");
				}
				return amb;
			}
			return null;
		}
		
		/**
		 * 更新绑定手机
		 * @param apiId
		 * @param mobile
		 * @return
		 */
		public ApplyMobileBinding  updateMobileBinding(String apiId, String mobile){
			if(isOpenApi()){
				ApplyMobileBinding amb = PayModelHelper.updateMobileBinding(apiId, mobile);
				if(!amb.getResultCode().contains("EXECUTE_SUCCESS")){
					throw new BussinessException("获取验证码失败");
				}
				return amb;
			}
			return null;
		}
		
		/**
		 * 短信发送验证码服务
		 * @param userId
		 * @param username
		 * @return
		 */
		public SmsCaptcha smsCaptcha(String userId, String username){
			if(isOpenApi()){
				SmsCaptcha sc = PayModelHelper.smsCaptcha(userId, username);
				if(StringUtils.isBlank(sc.getCheckCodeUniqueId())){
					throw new BussinessException("获取验证码失败");
				}
				return sc;
			}
			return null;
		}
		
		/**
		 * 短信发送验证码服务
		 * @param userId
		 * @param username
		 * @return
		 */
		public SmsConfirmVerifyCode smsConfirmVerifyCode(String CodeId, String code){
			if(isOpenApi()){
				if(StringUtils.isBlank(code) || StringUtils.isBlank(CodeId)){
					throw new BussinessException("验证码通道错误！");
				}
				
				SmsConfirmVerifyCode scf = PayModelHelper.smsConfirmVerifyCode(CodeId, code);
				if(!scf.getResultCode().contains("EXECUTE_SUCCESS")){
					throw new BussinessException("验证码验证失败");
				}
				return scf;
			}
			return null;
		}
		
		/**
		 * 转账功能
		 * @param sellerUserId
		 * @param payerUserId
		 * @param money
		 */
		public TradeTransfer tradeTransfer(String sellerUserId,String payerUserId, String money,String tradeName){
			if(isOpenApi()){
				TradeTransfer tt =	PayModelHelper.tradeTransfer(sellerUserId, payerUserId, money,tradeName);
				return tt;
			}else{return null;}
		}
	
/*↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓普通标【发标-投标-放款-还款】功能处理↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓*/
	/**
	 *创建交易号
	 * @param borrow
	 * @param user
	 */
	public TradeCreatePoolTogether tradeCreatePoolTogether(String money, String apiId){
		if(isOpenApi()){
			TradeCreatePoolTogether ttp =PayModelHelper.tradeCreatePoolTogether(money, apiId);
			return ttp;
		}else{
			return  null;
		}
	}
	
	/**
	 * 投标冻结资金
	 * @param apiId
	 * @param money
	 * @param tradeNo
	 * @return
	 */
	public TradePayerApplyPoolTogether  tradePayerApplyPoolTogether(String apiId, String money, String tradeNo){
		if(isOpenApi()){
			TradePayerApplyPoolTogether tpa = PayModelHelper.tradePayerApplyPoolTogether(apiId, money, tradeNo);
			return tpa;
		}else{
			return null;
		}
	}
	
	/**
	 * 放款操作
	 * @param tradeNo
	 * @param borrowTenderList
	 * @param to_apiId
	 * @return
	 */
	public TradePayPoolTogether tradePayPoolTogether(String tradeNo){ //放款
		if(isOpenApi()){
			TradePayPoolTogether tpt=PayModelHelper.tradePayPoolTogether(tradeNo);
			return tpt;
		}else{
			return null;
		}
	}
	
	/**
	 *取消投标
	 * @param tradeNo
	 * @param subTradeNo
	 * @param payApiId
	 * @return
	 */
	public  TradePayerQuitPoolTogether tradePayerQuitPoolTogether(String tradeNo, String subTradeNo,String payApiId){
    	if(isOpenApi()){
    		TradePayerQuitPoolTogether tqt = PayModelHelper.tradePayerQuitPoolTogether(tradeNo, subTradeNo, payApiId);
    	    return tqt;
    	}else{
    		return null;
    	}
	}
	
	/**
	 * 非流转标 关闭交易
	 * @param tradeNo
	 * @return
	 */
	public  TradeClosePoolTogether tradeClosePoolTogether(String tradeNo){
		if(isOpenApi()){
			TradeClosePoolTogether tt = PayModelHelper.tradeClosePoolTogether(tradeNo);
			return tt;
		}else{
			return null;
		}
	}
/*↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓还款流程开始↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓*/
	/**
	 * 创建还款交易号
	 * @param payerUserId
	 * @param tradeAmount
	 * @return
	 */
   public TradeCreatePoolReverse  tradeCreatePoolReverse(String payerUserId, String tradeAmount){
	   if(isOpenApi()){
		   TradeCreatePoolReverse tcp = PayModelHelper.tradeCreatePoolReverse(payerUserId, tradeAmount);  
		   return tcp;
		}else{
			return null;
		}
   }
   
   /**
    * 还款
    * @param tradeNo
    * @param payerUserId
    * @param tendersArray
    * @return
    */
   public  TradePayPoolReverse tradePayPoolReverse(String tradeNo,String payerUserId, String tender,String money ){
	   if(isOpenApi()){
			TradePayPoolReverse tpr =  PayModelHelper.tradePayPoolReverse(tradeNo, payerUserId, tender, money);
			return tpr;
		}else{
			return null;
		}
   }
   
   /**
	 * 流转标
	 * 集资创建交易接口和普通标创建一样只是参数有变化  那就用同一个吧   大家写的时候要注意
	 * 
	 */
	public  TradeCreatePoolTogether tradeCreatePool(String apiId,String money){
		if(isOpenApi()){
			TradeCreatePoolTogether  tpt = PayModelHelper.tradeCreatePool(apiId, money);
			return tpt;
		}else{
			return null;
		}
	}
	
	/**
	 * 流标款付款接口  和 给力标一样  只是  传值 不一样  这里直接用  给力标 付款
	 */
	public TradePoolReceiveBorrow tradePoolReceiveBorrow(YjfPay yjfPay){
		if(isOpenApi()){
			TradePoolReceiveBorrow tt = PayModelHelper.tradePoolReceiveBorrow(yjfPay);
			return tt;
		}else{
			return null;
		}
	}
	
	/**
	 * 查询银行联行号
	 * @param districtName
	 * @param bankId
	 * @return
	 */
   public  BankNoQuery 	bankNoQuery(String districtName,String bankId){
	   if(isOpenApi()){
		   BankNoQuery bnq = PayModelHelper.bankNoQuery(districtName, bankId);
		   return bnq;
	   }
	   return null;
   }
   
   /**
    * 校验银行卡的有效性
    */
   public VerifyFacade verifyFacade(String accountNo  , DrawBank db, User user){
	   VerifyFacade vf = null;
	   if(isOnlineConfig()){
		   String notCheckBankCodes = Global.getString("not_check_bankcode");
		   if( !notCheckBankCodes.contains(db.getBankCode()) ){
			   vf = PayModelHelper.verifyFacade(accountNo, db, user);
			    if(!"VS".equals(vf.getVerifyStatus())){//校验银行卡的有效性失败
			    	throw new BussinessException("绑卡校验信息失败："+ vf.getResultMessage());
			    }  
		   }
	   }
	   return vf;
   }
	
	/**
	 * 调用yjf绑定银行卡接口
	 */
	public BankCodeBindingAddInfo bankCodeBindingAddInfo(String cardNo,String apiId,String name, String cardType, String bankType){
		if(isOpenApi()){
			return PayModelHelper.bankCodeBindingAddInfo(cardNo, apiId, name, cardType, bankType);
		}
		return null;
	}
	/**
	 * 解绑银行卡
	 * @param userId
	 * @param bankCardNo
	 * @return
	 */
	public BankCodeBindingRemove bankCodeBindingRemove(String userId,String bankCardNo){
		if(isOpenApi()){
			BankCodeBindingRemove bb =PayModelHelper.bankCodeBindingRemove(userId, bankCardNo);
			if(!bb.getResultCode().contains("EXECUTE_SUCCESS")){
				throw new BussinessException("解绑银行卡失败");
			}
			return bb;
		}
		return null;
	}

	//v1.8.0.4_u4 TGPROJECT-360  qinjun  2014-07-11  start
	/**
	 * 提现申请
	 */
	public YzzNewWithraw applyWithDraw(String apiId,String userName,String channelApi, String account,
			String money, String province, String city, String bankCnapsNo,String bankCode,
			String cardType,String publicTag,String drawType,String cashNum){
		if(isOpenApi()){
			YzzNewWithraw ad = PayModelHelper.yzzNewWithraw(apiId,userName, channelApi,account,money,
					province, city, bankCnapsNo,bankCode,cardType,publicTag,drawType,cashNum);
			return ad;
		}
		return null;
	}
	//v1.8.0.4_u4 TGPROJECT-360  qinjun  2014-07-11  end
	
	/**
	 * 调用yjf账户查询接口
	 */
	public UserAccountQuery userAccountQuery(User user){
		if(isOpenApi()){
			UserAccountQuery uaq = PayModelHelper.userAccountQuery(user);
			return uaq;
		}
		return null;
	}
	/**
	 * 调用yjf账户查询接口
	 */
	public DepositQuery userRechargeQuery(String currPage, String apiId){
		if(isOpenApi()){
			DepositQuery uaq = PayModelHelper.depositQuery(currPage,apiId);
			return uaq;
		}
		return null;
	}
	
	/**
	 * 调用yjf账户查询接口
	 */
	public WithdrawQquery userCashQuery(String currPage, String apiId){
		if(isOpenApi()){
			WithdrawQquery wq = PayModelHelper.withdrawQquery(currPage,apiId);
			return wq;
		}
		return null;
	}
	
	/**
	 * 无卡代扣接口
	 * @return
	 */
	public DeductDepositApply deductDepositApply(String certType, String certNo, 
    		String userId ,String amount,String tradeBizProductCode,String bankAccountNo,
    		String bankAccountName,String bankCode,String provName,String cityName,String order){
		String bizIdentity="SPECIAL_MERCHANT";  //请求身份表示
		String bizNo="032";   //业务编号
		String deductType="sync";//代扣类型
		DeductDepositApply dedu  = PayModelHelper.deductDepositApply(deductType, certType, certNo, userId, amount, bizIdentity, bizNo, tradeBizProductCode,bankAccountNo,bankAccountName,bankCode,provName,cityName,order);
		return dedu;
	}
	public void deleteBorrow(Borrow borrow, AccountLog log, BorrowParam param) {
		
	}
	
	/** 新无卡代扣充值接口      */
	
	/*public YzzNewDeduct doNewYzzNewDecut(){
		
	}*/
	/**---------------------------- 易极付接口结束 ---------------------------*/
	
	
	/**---------------------------- 所有汇付接口处理方法如下 ---------------------------*/
	
	/**
	 * 用户资金冻结接口
	 * freeze :冻结金额
	 * user_id :用户id
	 * usrcustid:用户号
	 * 返回一个冻结的订单号,在解冻时必须要的
	 * */
	public synchronized String freezeForChinapnr(ChinaPnrPayModel cppModel){
		String returnTrxid="";
			//String ordId=StringUtils.generatePnrTradeNO(user_id,"FR");
			//汇付2.0特殊规则，订单号为纯数字
			String ordId=OrderNoUtils.getInstance().getSerialNumber();
			ChinapnrModel retMod=ChinapnrHelper.usrFreezeBg(cppModel.getUsrCustId(), 
					NumberUtils.format2Str(cppModel.getOrdamt()),ordId,DateUtils.newdateStr2(new Date()));
			if(retMod==null||!retMod.success()){
				logger.info("汇付处理失败，失败原因："+retMod.getRespDesc());
				throw new BussinessException("汇付扣除冻结款失败！失败原因:"+retMod.getRespDesc());
			}else{
				returnTrxid = retMod.getTrxId();
				logger.info("返回的returnordid："+returnTrxid);
				
			}
		return returnTrxid;
	}
	
	public synchronized ChinapnrModel autoTender(User user,String[][] args,long borrowId,double transmoney){
		ChinapnrModel retMod = ChinapnrHelper.autoTender(user, args, borrowId,  transmoney);
		if(retMod==null||!retMod.success()){
			logger.info("汇付处理失败，失败原因："+retMod.getRespDesc());
			throw new BussinessException("汇付扣除冻结款失败！失败原因:"+retMod.getRespDesc());
		}
		return retMod;
	}
	
	
	/**
	  * 汇付2.0资金解冻接口
	  * trxId:资金冻结时候的交易流水号
	  * 在汇付2.0解冻资金的依据是冻结时产生的流水号，
	  * 不根据用户所传入的资金来控制
	  * */
	public synchronized void newUsrUnFree(long userid,String trxId){
		if(isOpenApi()){
			//String ordId=StringUtils.generatePnrTradeNO(user_id,"UF");
			String ordId=OrderNoUtils.getInstance().getSerialNumber();
			String date=DateUtils.newdateStr2(new Date());
			ChinapnrModel retMod=ChinapnrHelper.newUsrUnFreeze(userid+"", date, ordId, trxId);
			if(retMod==null||!retMod.success()){
				throw new BussinessException("钱管家解除冻结款失败！失败原因:"+retMod.getRespDesc());
			}
		}
	}
	
	/**
	 * 自动扣款(满标放款接口)
	 * outCustId:出账账户,inUustId:入账账户,transAmt:交易金额,fee:费率,userid:用户id
	 * subordId:投标时所产生的订单编号，subOrddate:投标时所产生的订单时间
	 * args：在满标复审时要上交给平台的管理费，格式是二维数组方式
	 * isdefault:是否默认0
	 * */
	public synchronized void LoansChianpnr(ChinaPnrPayModel cpp){
		if(isOpenApi()){
			String isdefault="N";
			ChinapnrModel china=ChinapnrHelper.loans(cpp.getUsrCustId(), cpp.getPayusrCustId(),NumberUtils.format2Str(cpp.getOrdamt()),cpp.getFee(), cpp.getOrdId(), 
					cpp.getOrddate(), cpp.getSubordId(), cpp.getSuborddate(),isdefault,cpp.getManageFee());
			if (china==null||!china.success()) {
				throw new BussinessException("钱管家扣除投标放款失败，失败原因"+china.getRespDesc());
			}
		}
	}
	/**
	 * 汇付2.0自动扣款(还款)
	 * outCustId:出账账户,inCustId:入账账户,transAmt:交易金额,fee:费率
	 * userid:用户id,args:分账给金账户的分账明细
	 * */
	public synchronized void RepaymenChinapnr(ChinaPnrPayModel cpp){
		if(isOpenApi()){
			if (null==cpp.getOrdId()||null==cpp.getOrddate()) {
				cpp.setOrdId(OrderNoUtils.getInstance().getSerialNumber());
				cpp.setOrddate(DateUtils.newdateStr2(new Date()));
			}
			ChinapnrModel china=ChinapnrHelper.repayment(cpp.getUsrCustId(), cpp.getPayusrCustId(), NumberUtils.format2Str(cpp.getOrdamt()), cpp.getFee(),
					cpp.getOrdId(), cpp.getOrddate(), cpp.getSubordId(), cpp.getSuborddate(),cpp.getManageFee());
			if (china==null||!china.success()) {
				throw new BussinessException("钱管家扣除还款失败，失败原因"+china.getRespDesc());
			}
		}
	}
	/**
	 * 商户划账接口：参数说明
	 * inCustId:收款方的用户号,
	 * inAcctId:子账户（目前处于待定状态，由于这个子账户在我们平台上还无法从汇付方获取到,目前取消此参数）
	 * transAmt:交易金额,
	 * outAcctId:子账户（在系统划账时，此账户就为系统的子账户，应该可以默认了）
	 **/
	public synchronized void transferChinapnr(ChinaPnrPayModel cpm){
		if(isOpenApi()){
			String ordId=OrderNoUtils.getInstance().getSerialNumber(); 
			cpm.setOrdId(ordId);
			ChinapnrModel ch=ChinapnrHelper.transfer(NumberUtils.format2Str(cpm.getOrdamt()), cpm.getPayusrCustId(),ordId,cpm.getPayuserId(),cpm.getUsrCustId());
			if (ch==null||!ch.success()) {
				throw new BussinessException("给用户划账失败，失败原因！"+ch.getRespDesc());
			}
			
		}
	}
	
	/**
	 * 后台审核用户取现专用接口
	 * @param ordId:申请提现时的订单号
	 * @param usrCustId：申请提现时的用户号
	 * @param transAmt：提现金额
	 * @param userid：用户id
	 */
	public synchronized boolean cashAuditChianpnr(String ordId,String usrCustId,String transAmt,long usrid,String flag){
		boolean result=false;
		if(isOpenApi()){
			ChinapnrModel chinapnr=ChinapnrHelper.cashAudit(ordId, usrCustId, transAmt, usrid,flag);
			logger.info("复审结果"+chinapnr.getRespCode()+":"+chinapnr.getRespDesc());
			if (chinapnr==null || !chinapnr.success()) {
				logger.info("取现复审失败，失败原因"+chinapnr.getRespDesc() + "orderId:" + ordId);
				new BussinessException("用户取现复核失败，失败原因！"+chinapnr.getRespDesc());
				result=false;
			}else{
				result=true;
			}
		}
		return result;
	}
	
	/**
	 * 填充汇付对象，类似放款方法，资金由投资人到借款人
	 * @param cmdid  汇付交易对象
	 * @param sysType  系统交易对象
	 * @param model    
	 * @param tender   
	 * @param borrowUser
	 * @param tenderUser
	 * @manageFee 不收利息管理费直接 传 "0.00"
	 * @return
	 */
	public ChinaPnrPayModel fillTenderChnrModel(String cmdid, String sysType,double ordamt,Borrow model,
			BorrowTender tender, User borrowUser, User tenderUser,String manageFee){
		if(isOpenApi()){
			ChinaPnrPayModel cppModelLoans = new ChinaPnrPayModel(cmdid,NumberUtils.format2(ordamt)
					,tenderUser.getUserId(),Long.toString(model.getUser().getUserId()),"0",null,DateUtils.dateStr4(new Date()),sysType);
			cppModelLoans.setUsrCustId(tenderUser.getApiUsercustId());
			cppModelLoans.setPayusrCustId(borrowUser.getApiUsercustId());
			cppModelLoans.setOrdId(OrderNoUtils.getInstance().getSerialNumber());
			cppModelLoans.setOrddate(DateUtils.newdateStr2(new Date()));
			cppModelLoans.setBorrowId(String.valueOf(model.getId()));
			cppModelLoans.setSubordId(tender.getSubOrdId());
			cppModelLoans.setSuborddate(tender.getSubOrdDate());
			cppModelLoans.setTenderId(tender.getId()+"");
			cppModelLoans.setFee(manageFee);
			return cppModelLoans;
		}
		return null;
	}
	
	/**
	 * 填充汇付对象，类似还款，资金由借款人到投资人，包括奖励
	 * @param cmdid //交易编码
	 * @param sysType// 系统交易类型
	 * @param ordamt
	 * @param model
	 * @param tender
	 * @param borrowUser
	 * @param tenderUser
	 * @manageFee 不收利息管理费直接 传 "0.00"
	 * @return
	 */
	public ChinaPnrPayModel fillBorrowChnrModel(String cmdid, String sysType,double ordamt,Borrow model, 
			BorrowTender tender,User borrowUser, User tenderUser,String manageFee){
		if(isOpenApi()){
			ChinaPnrPayModel cppModelLoans = new ChinaPnrPayModel(cmdid,NumberUtils.format2(ordamt)
					,borrowUser.getUserId(),Long.toString(tenderUser.getUserId()),"0",null,DateUtils.dateStr4(new Date()),sysType);
			cppModelLoans.setUsrCustId(borrowUser.getApiUsercustId());
			cppModelLoans.setPayusrCustId(tenderUser.getApiUsercustId());
			cppModelLoans.setOrdId(OrderNoUtils.getInstance().getSerialNumber());
			cppModelLoans.setOrddate(DateUtils.newdateStr2(new Date()));
			cppModelLoans.setBorrowId(String.valueOf(model.getId()));
			cppModelLoans.setSubordId(tender.getSubOrdId());
			cppModelLoans.setSuborddate(tender.getSubOrdDate());
			cppModelLoans.setTenderId(tender.getId()+"");
			cppModelLoans.setFee(manageFee);
			return cppModelLoans;
		}
		return null;
	}
	public synchronized void queryAcctchi(){
		ChinapnrModel ch=ChinapnrHelper.queryAcctschin();
	}
	public synchronized void reconcliaChina(String beginDate,String endDate,String pageNum,String pageSize,String queryTransType){
		ChinapnrModel cha=ChinapnrHelper.reconciliationchin(beginDate, endDate, pageNum, pageSize, queryTransType);
	}
	
	public synchronized void cashReconchina(String beginDate,String endDate,String pageNum,String pageSize){
		ChinapnrModel chan=ChinapnrHelper.cashReconciliationchin(beginDate, endDate, pageNum, pageSize);
	}
	
	public synchronized void saveReconchina(String beginDate,String endDate,String pageNum,String pageSize){
		ChinapnrModel chain=ChinapnrHelper.saveReconchin(beginDate, endDate, pageNum, pageSize);
	}
	
	public synchronized void trfReconchina(String beginDate,String endDate,String pageNum,String pageSize) {
		ChinapnrModel chianp=ChinapnrHelper.trfReconchina(beginDate, endDate, pageNum, pageSize);
	}
	
	/**
	 * 用户提现银行卡解绑
	 * @param usrCusId
	 * @param cardId
	 * @return
	 */
	public ChinaPnrPayModel chinaCardRemove(String usrCusId,String cardId) {
		ChinapnrModel chianm=ChinapnrHelper.chinaCardRemove(usrCusId, cardId);
		if (!chianm.getRespCode().equals("000")) {
			throw new BussinessException("默认取现卡不允许删除");
		}
		return null;
	}
	
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 start
	/**
	 * 汇付2.0中查询用户余额
	 * usrCustId:用户的汇付用户号
	 * */
	public synchronized Map<String,String> queryBalanceBgchianpnr(String usrCustId){
		Map<String , String> map=new HashMap<String, String>();
		logger.info("进入汇付判断查询usrCustId:"+usrCustId);
		ChinapnrModel cpm=ChinapnrHelper.queryBalanceBg(usrCustId);
		if (cpm==null||!cpm.success()) {
			new BussinessException("查询用户余额失败");
		}
		map.put("apiUseMoney", cpm.getAvlBal());
		map.put("apiNoUseMoney", cpm.getFrzBal());
		map.put("acctBal", cpm.getAcctBal());
		return map;
	}
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 stop
	/**
	 * 汇付2.0中查询订单状态
	 * ordId:要查询的订单编号。
	 * querytransType：交易类型：LOANS，交易放款；REPAYMENT，还款交易
	 * */
	public synchronized String queryTransChinapnr(String ordId ,String querytransType){
	//	String ordDate=DateUtils.chinaPnrDate();
		ChinapnrModel china=ChinapnrHelper.queryTransStat(ordId, "20140106", querytransType);
		return china.getRespDesc();
	}
	
	public synchronized Map<String, Object> queryFssAccts(String usrCustId){
		Map<String, Object> map = new HashMap<String, Object>();
		QueryFssAccts qs = ChinapnrHelper.fssAcctsQuery(usrCustId);
		if (!qs.success()) {
			map.put("errorMsg", "生利宝查询接口异常");
			map.put("TotalAsset", 0);
			map.put("TotalProfit", 0);
		}else{
			map.put("TotalAsset", qs.getTotalAsset());
			map.put("TotalProfit", qs.getTotalProfit());
		}
		return map;
	}
	
	
	
	
	/**---------------------------- 汇付接口结束 ---------------------------*/
	
	
	
	/**---------------------------- 所有双乾接口处理方法如下 ---------------------------*/
	
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 start
	/**
	 * 钱多多查询余额
	 * @param cash
	 * @param feePercent
	 * @return
	 * @throws Exception 
	 */
	public synchronized Map<String,String> mmmQalanceQuery(User user) throws Exception{
		Map<String,String> map = new HashMap<String, String>();
		String resp =  MmmHelper.balanceQuery(user);
		String[] resps = resp.split("\\|");
		map.put("apiUseMoney", resps[0]);
		map.put("apiTotal", resps[1]);
		map.put("apiNoUseMoney", resps[2]);
		return map;
	}
		//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 stop
	
	/**
	 * 自动投标
	 * @return
	 */
	public synchronized String mmmAutoTender(){
		return "";
	}
	
	/**
	 * 第三方查询是否审核投标（满标复审时）
	 * @return
	 */
	public synchronized void mmmQueryTenderSuccess(String borrowId){
		List<MmmLoanOrderQuery> queryList = MmmHelper.loanOrderQueryByBorrowId(borrowId);
		for (MmmLoanOrderQuery mmmLoanOrderQuery : queryList) {
			if(mmmLoanOrderQuery.getTransferAction().equals("1")){//投标进入判断
				if(!mmmLoanOrderQuery.getTransferState().equals("1")||//该笔是否已经转帐
				!(mmmLoanOrderQuery.getActState().equals("1")||mmmLoanOrderQuery.getActState().equals("3"))//该笔账单是否已已经通过或者自动通过
						){
					throw new ManageBussinessException("投标记录中有未审核或者错误的投标，请您先处理","/admin/borrow/schedule.html");
				}
			}
			
		}
	}
	
	//v1.8.0.4  TGPROJECT-34   qj  2014-04-04 start
	/**
	 * 扣费(管理费、利息管理费等给平台)
	 * @return
	 */
	public synchronized String mmmPayUser(MmmPay mmmPay){
		mmmPay.setOrderNo(OrderNoUtils.getInstance().getSerialNumber());
		List<MmmLoanTransfer> list = MmmHelper.deductLoan(mmmPay);
		for (MmmLoanTransfer mmmLoanTransfer : list) {
			if(!mmmLoanTransfer.getResultCode().equals("88")){
				throw new BussinessException("扣款失败："+MmmResultCode.getResult(mmmLoanTransfer.getResultCode(), "loan"));
			}
		}
		if(mmmPay.getOperateType().equals(MmmType.ADDTENDER)){//投标
			MmmLoanTransfer mmm = list.get(0);
			String loanNo = mmm.getLoanList().get(0).getLoanNo();
			return loanNo;
		}else{
			return "";
		}
	}
	
	public synchronized String mmmRepay(MmmPay mmmPay){
		mmmPay.setOrderNo(OrderNoUtils.getInstance().getSerialNumber());
		List<MmmLoanTransfer> list = MmmHelper.mmmRepayInterest(mmmPay);
		for (MmmLoanTransfer mmmLoanTransfer : list) {
			if(!mmmLoanTransfer.getResultCode().equals("88")){
				throw new BussinessException("还款失败："+MmmResultCode.getResult(mmmLoanTransfer.getResultCode(), "loan"));
			}
		}
		return "";
	}
	
	public synchronized String mmmVerifyBorrow(MmmPay mmmPay){
		MmmToLoanTransferAudit mmm = MmmHelper.toLoanTransferAudit(mmmPay.getOrderNo(),mmmPay.getNeedAudit());
		if(!mmm.getResultCode().equals("88")){
			throw new BussinessException("复审失败："+MmmResultCode.getResult(mmm.getResultCode(), "verifyFullBorrow"));
		}
		return null;
	}
	
	//v1.8.0.4  TGPROJECT-34   qj  2014-04-04 stop
	/**---------------------------- 双乾接口结束 ---------------------------*/
	
}

package cn.zchfax.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.dao.TenderAccountYearDao;
import cn.zchfax.domain.TenderAccountYear;
import cn.zchfax.model.SearchParam;
import cn.zchfax.service.TenderAccountYearService;

/**
 * TGPROJECT-389 wsl 2014-09-12
 * @author wsl
 *
 */
@Repository("tenderAccountYearService")
@Transactional
public class TenderAccountYearServiceImpl implements TenderAccountYearService {

	@Autowired
	private TenderAccountYearDao tenderAccountYearDao;
	
	@Override
	public List<TenderAccountYear> findTenderAccountYearList(SearchParam param) {
		List<TenderAccountYear> tenderAccountYearList = tenderAccountYearDao.findByCriteria(param);
		return tenderAccountYearList;
	}

	@Override
	public TenderAccountYear findTenderAccountYearById(long id) {
		return tenderAccountYearDao.find(id);
	}

	@Override
	public void updateTenderAccountYear(double accountYear, double i,
			double accountYear2, long userId) {
		tenderAccountYearDao.updateTenderAccountYear(accountYear, i, accountYear2, userId);
		
	}

}

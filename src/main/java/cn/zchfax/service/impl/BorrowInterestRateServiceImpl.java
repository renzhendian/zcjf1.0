package cn.zchfax.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.dao.BorrowInterestRateDao;
import cn.zchfax.domain.BorrowInterestRate;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.service.BorrowInterestRateService;

@Service("borrowInterestRateService")
@Transactional
public class BorrowInterestRateServiceImpl implements BorrowInterestRateService {

	@Autowired
	private BorrowInterestRateDao borrowInterestRateDao;

	@Override
	public PageDataList<BorrowInterestRate> list(SearchParam param) {
		return borrowInterestRateDao.findPageList(param);
	}

	@Override
	public void add(BorrowInterestRate bir) {
		borrowInterestRateDao.save(bir);
	}

	@Override
	public BorrowInterestRate find(long id) {
		return borrowInterestRateDao.find(id);
	}

	@Override
	public void updateInterestRate(long id, long userId) {
		borrowInterestRateDao.updateInterestRate(id, userId);
	}

	@Override
	public void borrowerInterestRateFailure() {
		borrowInterestRateDao.borrowerInterestRateFailure();
	}

	@Override
	public BorrowInterestRate findByInterestNum(String interestNum, User user) {
		return borrowInterestRateDao.findByInterestNum(interestNum, user);
	}

	@Override
	public BorrowInterestRate findByInterestNum(String interestNum) {
		return borrowInterestRateDao.findByInterestNum(interestNum);
	}
	
	@Override
	public void update(BorrowInterestRate bir) {
		borrowInterestRateDao.update(bir);
	}

	
}

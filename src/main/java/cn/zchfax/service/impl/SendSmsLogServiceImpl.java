package cn.zchfax.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.dao.SendSmsLogDao;
import cn.zchfax.domain.SendSmsLog;
import cn.zchfax.service.SendSmsLogService;

@Service(value="sendSmsLogService")
@Transactional
public class SendSmsLogServiceImpl implements SendSmsLogService {

	@Autowired
	private SendSmsLogDao sendSmsLogDao;
	
	@Override
	public SendSmsLog addSendSms(SendSmsLog sendSmsLog) {
		return sendSmsLogDao.save(sendSmsLog);
	}
	
	@Override
	public void updateSendSms(SendSmsLog sendSmsLog) {
		sendSmsLogDao.update(sendSmsLog);
	}

	@Override
	public SendSmsLog getSendSmsByPhone(String phone) {
		return sendSmsLogDao.getSendSmsByPhone(phone);
	}
	
}

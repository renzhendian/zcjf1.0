package cn.zchfax.service.impl;
/**
 * 此service   没有事物 只是api处理工具类
 * @author zxc
 *
 */
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.api.chinapnr.CardCashOut;
import cn.zchfax.api.chinapnr.ChinapnrHelper;
import cn.zchfax.api.chinapnr.ChinapnrLogin;
import cn.zchfax.api.chinapnr.ChinapnrModel;
import cn.zchfax.api.chinapnr.ChinapnrRegister;
import cn.zchfax.api.moneymoremore.MmmBalanceQuery;
import cn.zchfax.api.moneymoremore.MmmHelper;
import cn.zchfax.api.moneymoremore.MmmRegister;
import cn.zchfax.api.pay.ForwardYJFSit;
import cn.zchfax.api.pay.PayModelHelper;
import cn.zchfax.api.pay.RealNameCertQuery;
import cn.zchfax.api.pay.UserAccountQuery;
import cn.zchfax.api.pay.YjfRegister;
import cn.zchfax.context.ChinaPnrType;
import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.context.MmmType;
import cn.zchfax.context.YjfType;
import cn.zchfax.dao.AccountBankDao;
import cn.zchfax.dao.AccountCashDao;
import cn.zchfax.dao.AccountDao;
import cn.zchfax.dao.AccountLogDao;
import cn.zchfax.dao.BorrowDao;
import cn.zchfax.dao.BorrowTenderDao;
import cn.zchfax.dao.DrawBankDao;
import cn.zchfax.dao.UserDao;
import cn.zchfax.dao.YjfDao;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.AccountBank;
import cn.zchfax.domain.AccountCash;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.AccountRecharge;
import cn.zchfax.domain.AccountWebDeduct;
import cn.zchfax.domain.Borrow;
import cn.zchfax.domain.BorrowCollection;
import cn.zchfax.domain.BorrowRepayment;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.ChinaPnrPayModel;
import cn.zchfax.domain.DrawBank;
import cn.zchfax.domain.GlodTransfer;
import cn.zchfax.domain.MmmPay;
import cn.zchfax.domain.User;
import cn.zchfax.domain.UserCache;
import cn.zchfax.domain.YjfPay;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.exception.ManageBussinessException;
import cn.zchfax.model.ApiPayParamModel;
import cn.zchfax.model.BorrowParam;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.APIModel.AccountCashModel;
import cn.zchfax.model.APIModel.WebPayModel;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.ApiService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.tool.coder.MD5;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.OrderNoUtils;
import cn.zchfax.util.StringUtils;

@Service(value="apiService")
@Transactional
public class ApiServiceImpl extends BaseServiceImpl implements ApiService {
	
	@Autowired
	private YjfDao yjfDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private AccountBankDao accountBankDao;
	@Autowired
	private DrawBankDao drawBankDao;
	@Autowired
	private AccountService accountService;
	@Autowired
	private AccountDao accountDao;
	@Autowired
	private BorrowService borrowService;
	@Autowired
	private BorrowDao borrowDao;
	@Autowired
	private BorrowTenderDao borrowTenderDao;
	@Autowired
	private AccountLogDao accountLogDao;
	
	@Autowired
	private AccountCashDao accountCashDao;
	
	Logger logger = Logger.getLogger(ApiServiceImpl.class);
	
	//获取第三方接口的类型
	private int getApiType(){
		return Global.getInt("api_code");//接口类型 1：汇付，2：易极付，3:双乾...
	}
	
	/**
	 * 由于此冻结方法比较特殊，在进行投标冻结时，必须要将冻结流水号保存在投标记录中，所以对于冻结，进行单独处理
	 */
	@Override
	public String hfFreezeBg(String apiUserCustId, double amount) {
		//处理汇付冻结接口，如果冻结失败，直接提示投标失败,不进行异步调用处理
				ChinaPnrPayModel cpp=new ChinaPnrPayModel();   //封装用户冻结信息
				cpp.setUsrCustId(apiUserCustId);
				cpp.setOrdamt(amount);
				String trxId=freezeForChinapnr(cpp);  
		return trxId;
	}
	
	/**
	 * 汇付解冻方法封装
	 */
	@Override
	public void hfUnfreezeBg(User user, String trxId, long id,double money,String type,List<Object> taskList) {
		ChinaPnrPayModel cppModel = new ChinaPnrPayModel(Constant.UsrUnFreeze,money
				,user.getUserId(),null,"0",null,DateUtils.newdateStr2(new Date()),type);
		cppModel.setUsrCustId(user.getApiUsercustId());
		cppModel.setTrxId(trxId);
		cppModel.setBorrowId(id+"");
		taskList.add(cppModel);
	}
	
	@Override
	public SearchParam getdrawBankParam(){
		SearchParam drawBankParam = new SearchParam();
		switch (getApiType()) {
			case 1://汇付接口
				break;
			case 2://易极付接口
				if(!"ALL".equals(Constant.DRAW_CARDTYPE)){//借记卡 对私  可用
					drawBankParam.addParam("cardType", Constant.DRAW_CARDTYPE)
					.addParam("state", "y");
				}
				if(!"ALL".equals(Constant.DRAW_CHANNEL_NO)){
					drawBankParam.addParam("channelNo", Constant.DRAW_CHANNEL_NO);
				}
				break;
			case 3://双乾支付接口
				drawBankParam.addParam("state", "1");
			default:
				break;
		}
		return drawBankParam;
	}
	@Override
	public void verifyVipSuccess(UserCache cache , double vipFee,List<Object> taskList){
		switch (getApiType()) {
			case 1://汇付接口
				cache.setVipStatus(1);
				break;
			case 2://易极付接口
				YjfPay yjfPayVip = new YjfPay(null, Global.getValue("trade_name_vip"), vipFee+"", 
						YjfType.VERIFYVIPSUCCESS, null, null, YjfType.TRADETRANSFER, 
					    null, Global.getValue("yjf_partnerId"), 
					    null, cache.getUser().getApiId());
				taskList.add(yjfPayVip);
				break;
			case 3://双乾接口
				MmmPay mmmPay = new MmmPay("2","1","1",cache.getUser().getUserId(),cache.getUser().getApiId(),1,Global.getValue("plat_form_mmm")
						,vipFee,MmmType.MMM_PAY_USER,"vip_fee",MmmType.VERIFYVIPSUCCESS,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	@Override
	public AccountBank addAccountBank(Object object){
		AccountBank ab = null;
		switch (getApiType()) {
			case 1://汇付接口
				CardCashOut cardCashOut = (CardCashOut) object;
				ab = ckeckBankHf(cardCashOut,ab);
				break;
			case 2://易极付接口
				ab = (AccountBank) object;
				ab.setStatus(1);
				break;
			case 3://易极付接口
				ab = (AccountBank) object;
				ab.setStatus(1);
				break;
			default:
				break;
		}
		logger.info("绑卡addAccountBank"+"方法执行结束"+ab.getStatus());
		return ab;
	}
	
	@Override
	public void applyRealname(User user){
		switch (getApiType()) {
			case 1://汇付接口,不触发接口
				break;
			case 2://易极付接口
				RealNameCertQuery rnc = realNameCertQuery(user.getApiId());
				logger.info("用户"+user.getUserId()+"实名认证，易极付状态："+rnc.getStatus());
				if("AUDIT_PASSED".equals(rnc.getStatus())||"CHECK_PASSED".equals(rnc.getStatus())){ //如果用户已经在易极付实名认证通过
					user.setRealStatus(1);
				}else if("NEW_APP".equals(rnc.getStatus())){     //用户已经提交，易极付正在审核中，此时系统处理是等待审核中
					user.setRealStatus(2);
				}else{
					realNameCertSave(user); //先实名认证
				}
				break;
			default:
				break;
		}
		
	}
	
	/**
	 * 发标，产生订单号并保存。
	 */
	@Override
	public void addBorrowCreateTradeNo(Borrow model,List<Object> taskList){
		switch (getApiType()) {
			case 1://汇付接口,发标不触发接口操作
				break;
			case 2://易极付接口
				//发标     易极付创建交易号
				if(model.getType()==Constant.TYPE_FLOW){//流标特殊处 n理
					YjfPay yjfPay =  new YjfPay(model.getId()+"", null, model.getAccount()+"", YjfType.ADDBORROW, null,
							null, YjfType.TRADECREATEPOOL, null, null, null, model.getUser().getApiId() + "");			
					taskList.add(yjfPay);
				}else{ 			
					YjfPay yjfPay =  new YjfPay(model.getId()+"", null, model.getAccount()+"", YjfType.ADDBORROW, null,
							null, YjfType.TRADECREATEPOOLTOGETHER, null, null, null, model.getUser().getApiId() + "");
					taskList.add(yjfPay);
				}
				break;
			default:
				break;
		}
	}
	
	/**
	 * 投标,冻结资金接口
	 */
	@Override
	public void addTenderFreezeMoney(BorrowTender borrowTender,BorrowParam param,String apiMethodType,List<Object> taskList){
		 boolean isDo = apiMethodType.contains(""+getApiType());//调用是否执行。
		switch (getApiType()) {
			case 1:   // 汇付
				if(!isDo)return;
				String trxId = "";
				if(param.isAuto() == true){//自动投标
					String[][] args=new String[][]{{borrowTender.getBorrow().getUser().getApiUsercustId(),NumberUtils.format2Str(borrowTender.getAccount()),NumberUtils.format2Str(1.00)}};     //拼装投标信息的json格式的二维数组
					ChinapnrModel cModel = autoTender(borrowTender.getUser(), args, borrowTender.getBorrow().getId(),  NumberUtils.format2(borrowTender.getAccount()));
					//自动投标直接set
					borrowTender.setSubOrdId(cModel.getOrdId());
					borrowTender.setSubOrdDate(cModel.getOrdDate());
					trxId = hfFreezeBg(borrowTender.getUser().getApiUsercustId(), NumberUtils.format2(borrowTender.getAccount()));
				}else{
					trxId = hfFreezeBg(borrowTender.getUser().getApiUsercustId(), NumberUtils.format2(borrowTender.getAccount()));
					//汇付投标特殊处理：将投标回调回来封装到param中的投标订单号与投标时间封装到tender中去
					borrowTender.setSubOrdId(param.getTenderNo());
					borrowTender.setSubOrdDate(param.getTenderDate());
				}
				borrowTender.setTrxId(trxId);
				break;
			case 2:
				if(!isDo)return;
				String tradeNo  = "";
				if("1".equals(Global.getValue("open_yjf"))){//不开通易极付功能    不校验交易号
					YjfPay yp = yjfDao.getBorrowTradeNo(borrowTender.getBorrow().getUser().getApiId() + "", borrowTender.getBorrow().getId() + "" , 1);//获取易极付交易号 --》每个标都有一个交易号
					if(yp == null){
						throw new BussinessException("标： " + borrowTender.getBorrow().getId() + "  查询交易号失败,请联系客户");
					}
					tradeNo = yp.getTradeno();
					YjfPay yjfPay = new YjfPay(borrowTender.getBorrow().getId()+"", null, NumberUtils.format2(borrowTender.getAccount())+"", 
		                    YjfType.ADDTENDER, null, null,YjfType.TRADEPAYERAPPLYPOOLTOGETHER , 
		                    null, null, tradeNo, borrowTender.getUser().getApiId());
					taskList.add(yjfPay);
				}
				break;
			case 3:
				if(!isDo)return;
				MmmPay mmmPay = new MmmPay("2","1","",borrowTender.getUser().getUserId(),borrowTender.getUser().getApiId(),borrowTender.getBorrow().getUser().getUserId(),borrowTender.getBorrow().getUser().getApiId()
						, borrowTender.getAccount(),MmmType.MMM_PAY_USER,borrowTender.getBorrow().getId()+"",MmmType.ADDTENDER,new Date());
				mmmPay.setTenderId(borrowTender.getId()+"");
				mmmPay.setFullAmount(borrowTender.getBorrow().getAccount());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
		
	}
	
	/**
	 * 放款，放款接口
	 */
	 @Override
	public void FullSuccessLoanMoney(String apiNo,Borrow model,BorrowTender tender,User borrowUser, 
			User tenderUser,List<Object> taskList,String apiMethodType){
		boolean isDo = apiMethodType.contains(""+getApiType());//调用是否执行。
	    model = borrowDao.find(model.getId());
		switch (getApiType()) {
			case 1://处理汇付接口
				//添加汇付放款任务
				// 流转标投标  直接划款，开始处理汇付任务1、解冻投标资金，2、划款到借款人账户；划款操作
				if(!isDo)return;
				ChinaPnrPayModel cppModelLoans = fillTenderChnrModel(ChinaPnrType.LOANS, ChinaPnrType.AUTOVERIFYFULLSUCCESS,tender.getAccount()
						,model, tender, borrowUser, tenderUser,"0.00");
				taskList.add(cppModelLoans); 
				break;
			case 2://处理易极付接口
				//添加易极付放款任务
				if(!isDo){return;}
				YjfPay yp=yjfDao.getBorrowTradeNo(borrowUser.getApiId()+"", model.getId()+"", 1);//查询交易号
				 YjfPay yjPay = new YjfPay(model.getId()+"", null, NumberUtils.format2(model.getAccount())+"",
						YjfType.AUTOVERIFYFULLSUCCESS, null, 
						   null, YjfType.TRADEPAYPOOLTOGETHER, null, null, yp.getTradeno(), null);  //这里只需要   tradeNo
				 taskList.add(yjPay);
				 logger.info("添加易极付任务成功： borrowId:" + model.getId());
				break;
			case 3://处理双乾接口
				//v1.8.0.4_u5  TGPROJECT-400  qinjun  2014-08-18  start
				//添加双乾放款任务
				if(!isDo){return;}
				List<BorrowTender> list = model.getBorrowTenders();
				for (BorrowTender t : list) {
					User t_user = t.getUser();
					MmmPay mmmPay = new MmmPay("", "", "1", t_user.getUserId(), t_user.getApiId()
							,borrowUser.getUserId(), borrowUser.getApiId(),t.getAccount(), MmmType.MMM_VERIFY_BORROW, 
							model.getId()+"", MmmType.AUTOVERIFYFULLSUCCESS, new Date());
					mmmPay.setOrderNo(t.getTrxId());
					mmmPay.setFullAmount(model.getAccount());
					
					taskList.add(mmmPay);
				}
				//v1.8.0.4_u5  TGPROJECT-400  qinjun  2014-08-18  end
				break;
			default:
				break;
		}
	}
	 
	 private String getApiTenderNoStr(Borrow model){
		StringBuffer sb = new StringBuffer();
		List<BorrowTender> list = model.getBorrowTenders();
		for (int i = 0 ;i<list.size() ; i++) {
			BorrowTender borrowTender = list.get(i);
			sb.append(borrowTender.getTrxId());
			if(i<list.size()-1){
				sb.append(",");
			}
		}
		return sb.toString();
	}
	 
	 /**
	  * 放款，解冻资金接口
	  */
	 @Override
	public void FullSuccessUnFreezeMoney(User tenderUser, String trxId,long borrowId, double money
			 , List<Object> taskList,String apiMethodType ){
		boolean isDo = apiMethodType.contains(""+getApiType());
		switch (getApiType()) {
		case 1://汇付接口
			if(!isDo)return;
			hfUnfreezeBg(tenderUser, trxId, borrowId, money, ChinaPnrType.AUTOVERIFYFULLSUCCESS, taskList);
			break;
		case 2://易极付接口
			
			break;

		default:
			break;
		}
	}
	 /**
	  * 放款，分发奖励
	  * User borrowUser:借款用户
	  *  User tenderUser：投标用户
	  */
	 @Override
	 public void FullSuccessAward(Borrow model,BorrowTender tender,User borrowUser, User tenderUser,List<Object> taskList,double awardValue){
		switch (getApiType()) {
			case 1://汇付
				//奖励汇付才处理
				logger.info("用户投流转标"+model.getName()+"奖励"+awardValue+",用户");
				ChinaPnrPayModel cppm = fillBorrowChnrModel(ChinaPnrType.REPAYMENT, ChinaPnrType.AUTOREWARD, awardValue
						, model, tender, borrowUser, tenderUser,"0.00");
				taskList.add(cppm);
				break;
			case 2://易极付
				 YjfPay yjfPayawArdValue = new YjfPay(model.getId()+"", Global.getValue("trade_name_award"), NumberUtils.format2(awardValue)+"", 
		                 YjfType.AUTOVERIFYFULLSUCCESS, null,
		                  null, YjfType.TRADETRANSFER, null, tenderUser.getApiId(), 
		                  null, borrowUser.getApiId());
				taskList.add(yjfPayawArdValue);
				break;
			case 3://双乾
				MmmPay mmmPay = new MmmPay("2","2","1",borrowUser.getUserId(),borrowUser.getApiId(),tenderUser.getUserId(),
						tenderUser.getApiId(),awardValue,MmmType.MMM_PAY_USER,model.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
		
	 }
	 
	/**
	 * 放款，收取手续费
	 */
	 @Override
	public void	FullSuccessDeductFee(List<Object> taskList,List<BorrowTender> tenderList,double borrowfee,Borrow borrow,String menageFee){
		borrow = borrowDao.find(borrow.getId());
		 switch (getApiType()) {
			case 1://汇付
				doBorrowFee(taskList, tenderList, borrowfee, borrow, menageFee);
				break;
			case 2://易极付
				//转账给  参与账户  当做风险基金池
				YjfPay feepayChargeFee = new YjfPay(borrow.getId()+"", Global.getValue("trade_name_manage_fee"), NumberUtils.format2(borrowfee)+"", YjfType.AUTOVERIFYFULLSUCCESS,
						                   null, null, YjfType.TRADETRANSFER, null, Global.getValue("yjf_partnerId"), 
						                   null, borrow.getUser().getApiId());
				taskList.add(feepayChargeFee);
				break;
			//v1.8.0.4  TGPROJECT-34   qj  2014-04-10 start
			case 3://双乾
				MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),1,Global.getValue("plat_form_mmm")
						,borrowfee,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
				taskList.add(mmmPay);
				break;
			//v1.8.0.4  TGPROJECT-34   qj  2014-04-10 start
			default:
				break;
		}
	}
	/**
	 * 放款，风险备用金
	 */
	 @Override
	public void	FullSuccessRiskFee(List<Object> taskList,List<BorrowTender> tenderList,double riskFee,Borrow borrow,String menageFee){
		switch (getApiType()) {
		case 1://汇付
			doBorrowFee(taskList, tenderList, riskFee, borrow, menageFee);
			break;
		case 2:// 易极付
			//转账给  参与账户  当做风险基金池
			YjfPay feepayChargeFee = new YjfPay(borrow.getId()+"", Global.getValue("trade_name_guarantee"), NumberUtils.format2(riskFee)+"", YjfType.AUTOVERIFYFULLSUCCESS,
					                   null, null, YjfType.TRADETRANSFER, null, Global.getValue("guarantee_money_accoount"), 
					                   null, borrow.getUser().getApiId());
			taskList.add(feepayChargeFee);
			break;
		case 3://双乾
			MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),1,Global.getValue("plat_form_mmm")
					,riskFee,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
			taskList.add(mmmPay);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 还款，还<本金等>接口
	 */
	@Override
	public void repayLoanMoney(String apiId,double money,List<Object> taskList,Borrow borrow,BorrowCollection c,String apiMethodType){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付接口
				if(!isDo)return;
				User tenderUser = c.getBorrowTender().getUser();
				User borrowUser = borrow.getUser();
				ChinaPnrPayModel cpp = fillBorrowChnrModel(ChinaPnrType.REPAYMENT, ChinaPnrType.AUTOREPAY, money
						, borrow, c.getBorrowTender(), borrowUser, tenderUser,"0.00");
				taskList.add(cpp);
				break;
			case 2://易极付接口
				//平台获得一般逾期的利息
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2Str(money), 
			    		YjfType.AUTOREPAY, null, null, YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apiId, null, borrow.getUser().getApiId());
			    taskList.add(yjfPay);
				break;
			case 3://双乾
				if(!isDo)return;
				MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),c.getBorrowTender().getUser().getUserId(),
						c.getBorrowTender().getUser().getApiId(),money,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	/**
	 * 还款，逾期罚息接口
	 */
	@Override
	public void payManageFee(String apiId, double money,List<Object> taskList,Borrow borrow){
		switch (getApiType()) {
			case 1://汇付接口
				List<BorrowTender> tenderList = borrowTenderDao.getBorrowTenderList(borrow.getId());
				doBorrowFee(taskList, tenderList, money, borrow, "");
				break;
				
			case 2://易极付接口
				//平台获得一般逾期的利息
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2Str(money), 
			    		YjfType.AUTOREPAY, null, null, YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apiId, null, borrow.getUser().getApiId());
			    taskList.add(yjfPay);
				break;
			case 3://双乾
				MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),1,Global.getValue("plat_form_mmm")
						,money,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.AUTOREPAY,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	
	/**
	 * 还款，易极付利息管理费，汇付，还利息与扣除利息管理费。
	 */
	@Override
	public void repayBorrowFee(Borrow model,BorrowRepayment repay ,User tenderUser,double borrowFee,double interest
			,List<Object> taskList,BorrowCollection c,String apiMethodType){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		User borrowUser = model.getUser();
		switch (getApiType()) {
			case 1://汇付接口
				if(!isDo)return;
				String borrowFeeStr = borrowFee != 0? borrowFee+"":"0.00";
				ChinaPnrPayModel cpp = fillBorrowChnrModel(ChinaPnrType.REPAYMENT,  ChinaPnrType.AUTOREPAY, interest - borrowFee
						, model, c.getBorrowTender(), borrowUser, tenderUser, borrowFeeStr);
				taskList.add(cpp);
				break;
			case 2://易极付接口
				if(!isDo)return;
				//利息管理费 易极付转账给 平台账户。
				if(borrowFee != 0){
					YjfPay  yjfManage = new YjfPay(model.getId()+"", Global.getValue("trade_name_interest_fee"), NumberUtils.format2(borrowFee) +"", YjfType.DOREPAY, 
							null, repay.getPeriod()+"", YjfType.TRADETRANSFER, null, 
							Global.getValue("yjf_partnerId"), null, tenderUser.getApiId()+"");
					taskList.add(yjfManage); // 扣除管理费，要放到还款的后面
				}
				break;
			case 3://双乾
				if(!isDo)return;
				MmmPay mmmPay = null;
				if(borrowFee>0){
					mmmPay = new MmmPay("2","2","1",borrowUser.getUserId(),borrowUser.getApiId(),c.getBorrowTender().getUser().getUserId(),
							c.getBorrowTender().getUser().getApiId(),interest,MmmType.MMM_REPAY_INTEREST,model.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
					mmmPay.setSecondaryAmount(borrowFee);
					mmmPay.setSecondaryMmmId(Global.getValue("plat_form_mmm"));
				}else{
					mmmPay = new MmmPay("2","2","1",borrowUser.getUserId(),borrowUser.getApiId(),c.getBorrowTender().getUser().getUserId(),
							c.getBorrowTender().getUser().getApiId(),interest,MmmType.MMM_PAY_USER,model.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
				}
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	
	/**
	 * 还款，还款最后划款接口，易极付接口用
	 */
	@Override
	public void repayLoanMoneyEnd(List<Object> taskList,List<Object> borrowFeeTaskList
			,Borrow model,BorrowRepayment repay,User borrowUser, ApiPayParamModel apm,String apiMethodType){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付接口
				
				break;
				
			case 2://易极付接口
				//平台获得一般逾期的利息
				//添加还款易极付任务
				if(!isDo)return;
			    YjfPay  yjfPay = new YjfPay(model.getId()+"", null, NumberUtils.format2(apm.getTotalMoney())+"", 
			    		YjfType.AUTOREPAY, null, repay.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apm.getTenderStr().substring(0, apm.getTenderStr().length()-1), null, borrowUser.getApiId());
			    
			    taskList.add(yjfPay);
			    taskList.addAll(borrowFeeTaskList);//管理费，扣除添加任务表
				break;
	
			default:
				break;
		}
	}
	
	/***
	 * 流转标还款,还款
	 */
	@Override
	public void flowRepayLoanMoney(String apiId,ApiPayParamModel apiModel,double money ,String apiMethodType,Borrow borrow,List<Object> taskList){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付接口
				break;
			case 2://易极付接口
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2Str(money), 
			    		YjfType.FLOWREPAY, null, null, YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apiId, null, borrow.getUser().getApiId());
			    taskList.add(yjfPay);
				break;
	
			default:
				break;
		}
	}
	/***
	 * 流转标还款,利息管理费
	 */
	@Override
	public void flowRepayBorrowFee(Borrow model, BorrowRepayment repayment,
			List<Object> interestFeeList, User tenderUser, double borrow_fee,double repayments,BorrowTender tender , List<Object> taskList){
		switch (getApiType()) {
			case 1://汇付接口
				String borrowFeeStr = borrow_fee != 0? borrow_fee+"":"0.00";
				ChinaPnrPayModel chinaModel = fillBorrowChnrModel(ChinaPnrType.REPAYMENT, ChinaPnrType.FLOW_REPAY,
						repayments-borrow_fee, model,tender, model.getUser(), tenderUser, borrowFeeStr);
				taskList.add(chinaModel);
				break;
			case 2://易极付接口
				//利息管理费 易极付转账给 平台账户。
				if(borrow_fee != 0){
					YjfPay  yjfManage = new YjfPay(model.getId()+"", Global.getValue("trade_name_interest_fee"), NumberUtils.format2(borrow_fee) +"", YjfType.FLOWREPAY, 
							null, repayment.getPeriod()+"", YjfType.TRADETRANSFER, null, 
							Global.getValue("yjf_partnerId"), null, tenderUser.getApiId()+"");
					interestFeeList.add(yjfManage);
				}
				break;
			case 3://双乾
				MmmPay mmmPay = new MmmPay("2","2","1",model.getUser().getUserId(),model.getUser().getApiId(),tender.getUser().getUserId(),
						tender.getUser().getApiId(),repayments,MmmType.MMM_REPAY_INTEREST,model.getId()+"",MmmType.FLOW_REPAY,new Date());
				mmmPay.setSecondaryAmount(borrow_fee);
				mmmPay.setSecondaryMmmId(Global.getValue("plat_form_mmm"));
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	
	/***
	 * 流转标还款,最后调用,易极付使用 划账。
	 */
	@Override
	public void flowRepayLoanMoneyEnd(List<Object> taskList,List<Object> interestFeeList,Borrow model
			,BorrowRepayment repayment,ApiPayParamModel apiModel ,User borrowUser,String apiMethodType ){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付接口
				
				break;
			case 2://易极付接口
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(model.getId()+"", null, NumberUtils.format2(apiModel.getTotalMoney())+"", 
			    		YjfType.FLOWREPAY, null, repayment.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apiModel.getTenderStr(), null, borrowUser.getApiId());
			    taskList.add(yjfPay);
			    taskList.addAll(interestFeeList);
				break;
	
			default:
				break;
		}
	}

	/**
	 * 还款给网站，还款给网站接口
	 */
	@Override
	public void repayToWebSiteLoanMoney(BorrowRepayment repay,double money,Borrow borrow ,List<Object> taskList ){
		borrow = borrowDao.find(borrow.getId());
		switch (getApiType()) {
			case 1://汇付
				List<BorrowTender> tenderList = borrow.getBorrowTenders();
				doBorrowFee(taskList, tenderList, money, borrow, "");
				break;
			case 2://易极付
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2(money)+"", 
			    		YjfType.AUTOREPAY, null, repay.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
			    		null, Global.getValue("guarantee_money_accoount"), null, borrow.getUser().getApiId());
			    taskList.add(yjfPay);
				break;
			//v1.8.0.4_u1  TGPROJECT-228  qj 2014-05-05  start	
			case 3:
				MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),1,Global.getValue("plat_form_mmm")
						, money,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.WEBSITEREPAY,new Date());
				taskList.add(mmmPay);
			//v1.8.0.4_u1  TGPROJECT-228  qj 2014-05-05  end	
			default:
				break;
		}
	}
	
	
	/**
	 * 还款给网站，最后一步，封装对象<易极付等接口用>
	 */
	@Override
	public void repayToWebSiteLoanMoneyEnd(ApiPayParamModel apiModel,Borrow model,BorrowRepayment repay,User borrowUser,List<Object> taskList,String apiMethodType){
		
		boolean isDo = apiMethodType.contains(getApiType()+"");
			switch (getApiType()) {
			case 1://汇付
				if(!isDo)return;
				break;
			case 2://易极付
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(model.getId()+"", null, NumberUtils.format2(apiModel.getTotalMoney())+"", 
			    		YjfType.AUTOREPAY, null, repay.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
			    		null, apiModel.getTenderStr(), null, borrowUser.getApiId());
			    taskList.add(yjfPay);
			    
				break;
			default:
				break;
		}
	}
	/**
	 * 汇付绑卡操作
	 * @param cardCashOut
	 * @param ab
	 */
	private AccountBank ckeckBankHf(CardCashOut cardCashOut,AccountBank ab){
		logger.info("进入绑卡业务处理方法");
		long userId = NumberUtils.getLong(cardCashOut.getMerPriv());
	    User user =	userDao.find(userId);
	    if(user == null){
	    	throw new BussinessException("用户绑定银行卡失败 ，user :" + user + " userId:"+userId);
	    }
	    String accountId = cardCashOut.getOpenAcctId();
	    String bankId = cardCashOut.getOpenBankId();
	    //同一个账号不能绑定两次
	    ab =  accountBankDao.getAccountBankByCardNo(accountId, user);
	    DrawBank db = drawBankDao.getDrawBankByBankCode(bankId);
	    if(db== null){
	    	throw new BussinessException("bankId:"+bankId + " accountId" + accountId + "  系统找不到指定的参数， userId:"+userId);
	    }
	    if(ab==null){//添加新的银行卡
	    	ab = new AccountBank();
	    	ab.setUser(user);
	    	ab.setAccount(accountId);
	    	ab.setBank(db);
	    	ab.setAddip("");
	    	ab.setAddtime(new Date());
	    	ab.setStatus(1);
	    }else{
	    	throw new BussinessException("no:" + accountId + " 已经绑定过，此次绑定失败 ");
	    }
	    return ab;
	}
	/**
	 * 网站垫付，还款<本金、利息、逾期罚息>
	 */
	@Override
	public void webSitePayLoanMoney(Borrow borrow, List<Object> taskList,User tenderUser,double money ){
		switch (getApiType()) {
			case 1://汇付
				ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, money, 1,
						String.valueOf(tenderUser.getUserId()), "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
				cppModel.setPayusrCustId(tenderUser.getApiUsercustId());
				cppModel.setUsrCustId("");
				taskList.add(cppModel);//从主账号
				break;
			case 2://易极付
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2Str(money), 
						YjfType.WEBSITEPAYFORLATEBORROW, null, null, YjfType.TRADEPAYPOOLREVERSE, 
						null, tenderUser.getApiId(), null, Global.getValue("guarantee_money_accoount")); //从备用金转账给投资人
				taskList.add(yjfPay);
				break;
			//v1.8.0.4_u1  TGPROJECT-228  qj 2014-05-05  start	
			case 3:
				//v1.8.0.4_u4 TGPROJECT-364 qinjun 2014-07-01 start 
				MmmPay mmmPay = new MmmPay("2","2","1",1,Global.getValue("web_pay_account"),tenderUser.getUserId(),tenderUser.getApiId()
						, money,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.WEBSITE,new Date());
				//v1.8.0.4_u4 TGPROJECT-364 qinjun 2014-07-01 start 
				taskList.add(mmmPay);
				break;
			//v1.8.0.4_u1  TGPROJECT-228  qj 2014-05-05  end	
			default:
				break;
		}
	}
	
	//帝业投资自动扣款功能   start
	@Override
	public void doMmmUserPayWeb(User user, double money,List<Object> taskList) {
		MmmPay mmmPay = new MmmPay("2","2","1",user.getUserId(),user.getApiId(),1,Global.getValue("plat_form_mmm")
				, money,MmmType.MMM_PAY_USER,"",MmmType.WEBSITEREPAY,new Date());
		taskList.add(mmmPay);
	}

	//帝业投资自动扣款功能   start
	/**
	 * 网站垫付，还款<本金、利息、逾期罚息>最后一步，易极付等接口划款过程。
	 */
	@Override
	public void webSitePayLoanMoneyEnd(Borrow borrow,ApiPayParamModel apm,BorrowRepayment br,List<Object> taskList,String apiMethodType){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付
				break;
			case 2://易极付
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2(apm.getTotalMoney())+"", 
						YjfType.WEBSITEPAYFORLATEBORROW, null, br.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
						null, apm.getTenderStr().substring(0, apm.getTenderStr().length()-1), null, Global.getValue("guarantee_money_accoount")); //从备用金转账给投资人
				taskList.add(yjfPay);
				break;
			default:
				break;
		}
	}
	/***
	 * 好利贷网站，特殊垫付,易极付接口。
	 */
	@Override
	public void haoLiDaidWebSitePayLoanMoney(Borrow borrow,List<Object> taskList,User tender_user, double dealMoney){
		switch (getApiType()) {
			case 1://汇付任务
				break;
			case 2://易极付接口
				
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2Str(dealMoney), 
						YjfType.WEBSITEPAYFORLATEBORROW, null, null, YjfType.TRADEPAYPOOLREVERSE, 
						null, tender_user.getApiId(), null, Global.getValue("guarantee_money_accoount") ); //备用金账户，转账给投资人。
				taskList.add(yjfPay);
				break;
			default:
				break;
		}
	}
	/**
	 * 网站垫付，判断资金是否足够
	 */
	@Override
	public void webSitePayMoneyEnough(List<Object>  taskList, double webSitePayMoney){
		switch (getApiType()) {
			case 1://汇付
				break;
			case 2://易极付
				isWebSitePayEnough(webSitePayMoney, taskList);
				break;
			default:
				break;
		}
	}
	
	private void isWebSitePayEnough( double webSitePayMoney, List<Object> taskList){
		//查询用户的易极付可用资金
		for (Object ob : taskList) {
			YjfPay yjfPay = (YjfPay)ob;
			webSitePayMoney += NumberUtils.getDouble(yjfPay.getMoney());
		}
		User user = new User();
		user.setApiId(Global.getValue("guarantee_money_accoount"));
		UserAccountQuery ua = userAccountQuery(user);
		double siteUseMoney = 0d;
		try {
			siteUseMoney = NumberUtils.getDouble(ua.getBalance()) - NumberUtils.getDouble(ua.getFreezeAmount()); 
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		if( siteUseMoney < webSitePayMoney&&isOpenApi()){
			throw new ManageBussinessException("账户金额不足，垫付失败");
		}
	}
	/**
	 * 利贷最后划款操作、易极付接口专用
	 * @param taskList
	 * @param borrow
	 * @param br
	 * @param tendersStr
	 * @param totalToPay
	 * @param apiMethodType
	 */
	@Override
	public void haoLiDaidWebSitePayLoanMoneyEnd(List<Object> taskList,Borrow borrow, 
			BorrowRepayment br,ApiPayParamModel apm, String apiMethodType ){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付任务
				break;
			case 2://易极付接口
				if(!isDo)return;
				YjfPay  yjfPay = new YjfPay(borrow.getId()+"", null, NumberUtils.format2(apm.getTotalMoney())+"", 
						YjfType.WEBSITEPAYFORLATEBORROW, null, br.getPeriod()+"", YjfType.TRADEPAYPOOLREVERSE, 
						null, apm.getTenderStr().substring(0, apm.getTenderStr().length()-1), null, Global.getValue("guarantee_money_accoount") ); //备用金账户，转账给投资人。
				taskList.add(yjfPay);
				
				break;
			default:
				break;
		}
	}
	
	
	
	/**
	 * 管理费处理方法：
	 * 由于汇付没有直接从用户划款值商户的接口，所以在处理管理费时模拟一笔
	 * 还款操作，付款人为借款者，收款方为平台.扣费的方式为评分到每一个投资者的
	 * 投标订单里面去扣除
	 * 确保这笔资金是从借款人账户里面扣除的
	 */
	@Override
	public void doBorrowFee(List<Object> taskList,
			List<BorrowTender> tenderList, double borrowfee, Borrow borrow,String menageFee) {
		double total = 0;
		for (int i = 0; i < tenderList.size(); i++) {
			BorrowTender borrowTender = tenderList.get(i);
			double borrowAccount=borrow.getAccountYes();          //获取标的总额
			double feeMoney = 0;
			if(i == tenderList.size() -1 ){
				feeMoney= borrowfee - total;        //获取最后一个人
			}else{
				feeMoney = NumberUtils.format2(borrowTender.getAccount()/borrowAccount*borrowfee);        //获取每个人
				total += feeMoney;
			}
			feeMoney = NumberUtils.format2(feeMoney);
			if(feeMoney>0){
				ChinaPnrPayModel cppFee = new ChinaPnrPayModel(ChinaPnrType.REPAYMENT, feeMoney, borrow.getUser().getUserId(), 
						ChinaPnrType.ADMININ, "0", null, DateUtils.dateStr4(new Date()), ChinaPnrType.AUTOVERIFYFULLSUCCESS);
				cppFee.setOrdId(OrderNoUtils.getInstance().getSerialNumber());
				cppFee.setOrddate(DateUtils.newdateStr2(new Date()));
				cppFee.setBorrowId(borrow.getId()+"");
				cppFee.setUsrCustId(borrow.getUser().getApiUsercustId());
				cppFee.setSuborddate(borrowTender.getSubOrdDate());
				cppFee.setSubordId(borrowTender.getSubOrdId());
				cppFee.setFee("0.00");
				cppFee.setManageFee(menageFee);
				cppFee.setPayusrCustId("1");
				taskList.add(cppFee);
			}
		}
		
	}


	/**
	 * 流转标，投标放款。
	 */
	@Override
	public void flowBorrowLoan(BorrowTender borrowTender, User tenderUser,
			Borrow borrow, User borrowUser,List<Object> taskList) {
		switch (getApiType()) {
		case 1:
			ChinaPnrPayModel  cppModel= fillTenderChnrModel(ChinaPnrType.LOANS, ChinaPnrType.AUTOFLOWLOANS, borrowTender.getAccount()
					, borrow, borrowTender, borrowUser, tenderUser,"0.00");
			taskList.add(cppModel);
			break;
		case 2:
			YjfPay yp=yjfDao.getBorrowTradeNo(borrowUser.getApiId()+"", borrow.getId()+"", 1);//查询交易号
			String tenderUserStr = tenderUser.getApiId() +  "=" + NumberUtils.format2( borrowTender.getAccount()); //所有投资人列表
			YjfPay yjfPayFlow = new YjfPay(borrow.getId()+"", null, borrowTender.getAccount()+"", YjfType.ADDTENDER, 
					                   null, null, YjfType.TRADEPOOLRECEIVEBORROW, null, borrow.getUser().getApiId()+"", yp.getTradeno(), tenderUserStr);
			taskList.add(yjfPayFlow);
			break;
			//v1.8.0.4  TGPROJECT-34   qj  2014-04-10 start
		case 3://双乾
			MmmPay mmmPay = new MmmPay("2","1","1",tenderUser.getUserId(),tenderUser.getApiId(),borrowUser.getUserId(),borrowUser.getApiId()
					, borrowTender.getAccount(),MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.ADDTENDER,new Date());
			taskList.add(mmmPay);
			break;
		//v1.8.0.4  TGPROJECT-34   qj  2014-04-10 start
		default:
			break;
		}
	}
	/**
	 * 网站后台充值
	 */
	@Override
	public void webRecharge(AccountRecharge ar , List<Object> taskList) {
		switch (getApiType()) {
			case 1:
				ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, ar.getMoney(), 1,
						String.valueOf(ar.getUser().getUserId()), "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
				cppModel.setPayusrCustId(ar.getUser().getApiUsercustId());
				cppModel.setUsrCustId("");
				taskList.add(cppModel);
				break;
			case 2:
				YjfPay yjfPay = new YjfPay(null, Global.getValue("trade_offline_recharge"), 
		                   	ar.getMoney()+"", YjfType.VERIFYRECHARGE, null, null, YjfType.TRADETRANSFER, 
							null, ar.getUser().getApiId(), null, Global.getValue("yjf_partnerId"));
				taskList.add(yjfPay);
				break;
			case 3:
				MmmPay mmmPay = new MmmPay("2","1","1",1,Global.getValue("plat_form_mmm"),ar.getUser().getUserId(),ar.getUser().getApiId()
						, ar.getMoney(),MmmType.MMM_PAY_USER,"后台充值",MmmType.WEB_RECHARGE,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	
	//v1.8.0.4_u4  TGPROJECT-365  qinjun 2014-07-14 start
	/**
	 * 网站划款
	 */
	@Override
	public void webTransfer( List<Object> taskList,User toUser,double money) {
		switch (getApiType()) {
			case 1:
				ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, money, 1,
						String.valueOf(toUser.getUserId()+""), "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
				cppModel.setPayusrCustId(toUser.getApiUsercustId());
				cppModel.setUsrCustId("");
				taskList.add(cppModel);
				break;
			case 2:
				YjfPay yjfPay = new YjfPay(null, Global.getValue("trade_offline_recharge"), 
							money+"", YjfType.WEB_TRANSFER, null, null, YjfType.TRADETRANSFER, 
							null, toUser.getApiId(), null, Global.getValue("yjf_partnerId"));
				taskList.add(yjfPay);
				break;
			case 3:
				MmmPay mmmPay = new MmmPay("2","1","1",1,Global.getValue("plat_form_mmm"),toUser.getUserId(),toUser.getApiId()
						, money,MmmType.MMM_PAY_USER,"发放奖励",MmmType.REWARD_PAY,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	//v1.8.0.4_u4  TGPROJECT-365  qinjun 2014-07-14 end
	
	@Override
	public void glodTransfer(GlodTransfer gt, List<Object> taskList) {
		switch (getApiType()) {
			case 1:
				ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, gt.getMoney(), 1,
						gt.getAccount(), "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
				cppModel.setPayuserId(gt.getPayAccount());
				cppModel.setUsrCustId(gt.getAccount());
				cppModel.setPayusrCustId(Global.getValue("chinapnr_merid"));
				taskList.add(cppModel);
				break;
			case 2:
				break;
			default:
				break;
		}
		
	}
	
	@Override
	public void webDeduct(AccountWebDeduct awd , List<Object> taskList) {
		switch (getApiType()) {
			case 1:
				break;
			case 2:
				YjfPay yjfPay = new YjfPay(null, Global.getValue("trade_name_web_deduct"), 
						awd.getMoney()+"", YjfType.CASHBACK, null, null, YjfType.TRADETRANSFER, 
							null,  Global.getValue(awd.getDedcutAccount()), null,awd.getUser().getApiId());
				taskList.add(yjfPay);
				break;
			default:
				break;
		}
		
	}
	
	/**
	 * 撤标过程
	 */
	@Override
	public void faillBorrow(List<Object> taskList,Borrow model,String apiMethodType){
		boolean isDo = apiMethodType.contains(getApiType()+"");
		switch (getApiType()) {
			case 1://汇付接口
				if (!isDo) return;
				List<BorrowTender> listTender = model.getBorrowTenders();
				for (BorrowTender borrowTender : listTender) {
					hfUnfreezeBg(borrowTender.getUser(), borrowTender.getTrxId(), model.getId(), borrowTender.getAccount(), ChinaPnrType.FAILL_BORROW, taskList);
				}
				
				break;
			case 2://易极付接口
				if (!isDo) return;
				List<YjfPay> tenders =  yjfDao.getTendersPayed( model.getId()+"");
				for (YjfPay yp : tenders) {
					YjfPay yjfPayDeal = new YjfPay(model.getId()+"", null, NumberUtils.format2Str(Double.parseDouble(yp.getMoney())), 
							YjfType.FAIL, null, null, YjfType.TRADEPAYERQUITPOOLTOGETHER, 
							yp.getSubtradeno(), null, yp.getTradeno(), yp.getUserid());
					taskList.add(yjfPayDeal);
				}
				break;
			case 3://双乾接口
				if (!isDo) return;
				MmmPay mmmPay = new MmmPay();
				mmmPay.setOrderNo(getApiTenderNoStr(model));
				mmmPay.setNeedAudit("2");
				mmmPay.setAddtime(new Date());
				mmmPay.setOperateType(MmmType.AUTOCANCEL);
				mmmPay.setType(MmmType.MMM_VERIFY_BORROW);
				mmmPay.setBorrowId(model.getId()+"");
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}

	@Override
	public void doCash(AccountCash cash, List<Object> taskList,AccountLog log,double fee,double servFee,String cardNo) {
		String cashIsVerify = Global.getValue("cash_is_verify"); //获取取现是否需要校验
		switch (getApiType()) {
		case 1://汇付接口
			//汇付处理手续费方法
			AccountBank accountBank = accountBankDao.getAccountBankByCardNo(cardNo, cash.getUser());
			cash.setAccountBank(accountBank);
			double manageFee = 0d;//用户取现支付的全部费用
			if(Global.getInt("is_pay_cash") == 0){//汇付取现手续费，网站不垫付
				manageFee +=fee;
			}
			manageFee+=servFee;//当网站不收取网站服务费的时候，传进来的是0
			
			double money = cash.getTotal()+manageFee;//v1.8.0.4 周学成  加上网站服务费
			
			cash.setFee(manageFee);//v1.8.0.4 周学成  加上网站服务费
			
			cash.setCredited(cash.getTotal());
			if (cashIsVerify.equals("1")) {
				//不需要审核，直接扣除
				log.setRemark("提现扣款");
				log.setType(Constant.CASH_SUCCESS);
				if(cash.getUser().getAccount().getUseMoney() < money ){
					money = cash.getTotal();
				}else{
					cash.setTotal(money);
				}
				accountDao.updateAccount(-money, -money, 0, cash.getUser().getUserId());
				//设置状态为取现成功
				cash.setStatus(1);
			}else{
				//需要审核，冻结资金
				log.setRemark("提现冻结");
				log.setMoney(money);
				cash.setCredited(cash.getTotal());
				if(cash.getUser().getAccount().getUseMoney() < money ){
					money = cash.getTotal();
				}else{
					cash.setTotal(money);
				}
				accountDao.updateAccount(0, -money, money, cash.getUser().getUserId());
				//设置状态为取现待审核
				cash.setStatus(0);
			}
			break;
		case 2://易极付接口
			
			break;
		case 3://双乾接口
			//v1.8.0.4_u1 TGPROJECT-292 lx start
			/**
			 * 双乾提现接口升级，回调参数中包括用户承担的手续费
			 */
			//double totalFee =cash.getTotal()*Global.getDouble("mmm_cash_fee_rate");//双乾取现总的费用（包括用户和商户），fee是商户的费用
			//if(totalFee>0&&totalFee<=1){totalFee = 1;}
			//double cashFee= NumberUtils.getDoubleFormat2Up(totalFee) - fee;//用户取现费用
			double cashFee=fee;
			cash.setFee(cashFee);
			//v1.8.0.4_u4 TGPROJECT-363  qinjun  2014-07-14 start
			//取现手续费扣费方式
			double totalmoney  = 0;
			int cashFeeType = Global.getInt("cash_fee_type");
			if(cashFeeType == 1){//内扣
				totalmoney = cash.getTotal();
			}else{
				totalmoney = cash.getTotal() + cashFee;
			}
			//v1.8.0.4_u4 TGPROJECT-363  qinjun  2014-07-14 end
			cash.setCredited(totalmoney);
			log.setMoney(totalmoney);
			log.setRemark("提现扣款,提现金额："+cash.getTotal()+", 手续费 ："+cashFee);
			log.setType(Constant.CASH_SUCCESS);
			accountDao.updateAccount(-totalmoney, -totalmoney, 0, cash.getUser().getUserId());
			//v1.8.0.4_u1 TGPROJECT-292 lx end
			//设置状态为取现成功
			cash.setStatus(1);
			break;
		default:
			break;
		}
	}
	public void doHfCashService(AccountCashModel cashModel, AccountCash cash,
			AccountLog log) {
		logger.info("取现回调进入业务方法----");
		cash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
		String cashIsVerify = Global.getValue("cash_is_verify"); //获取取现是否需要校验
		
		AccountBank accountBank = accountBankDao.getAccountBankByCardNo(cashModel.getCardNo(), cash.getUser());
		cash.setAccountBank(accountBank);
		double manageFee = 0d;//用户取现支付的全部费用
		if(Global.getInt("is_pay_cash") == 0){//汇付取现手续费，网站不垫付
			manageFee +=cashModel.getFeeAmt();
		}
		manageFee+=cashModel.getServFee();//当网站不收取网站服务费的时候，传进来的是0
		
		double money = cash.getTotal()+manageFee;//v1.8.0.4 周学成  加上网站服务费
		
		cash.setFee(manageFee);//v1.8.0.4 周学成  加上网站服务费
		
		cash.setCredited(cash.getTotal());
		
		//根据状态，判断操作订单
		if ("999".equals(cashModel.getReturnCode())) {  //处理中
			if (cash.getStatus()==4) {  //资金冻结
				doCashByChinapnrByfour(cashIsVerify,money,cash,log);	//业务处理方法
			}
		} else if("000".equals(cashModel.getReturnCode())){  //成功
			if (cash.getStatus()==1) {	//已经处理成功
				return;
			}
			if (cash.getStatus() ==8) {	//冻结状态，扣除冻结
				logger.info("订单："+cashModel.getOrderId()+"，提现成功！进入本地处理！");
				log.setRemark("提现处理成功，手续费"+manageFee+"元。");
				log.setType(Constant.CASH_SUCCESS);
				if(cash.getUser().getAccount().getUseMoney() < money ){
					money = cash.getTotal();
				}else{
					cash.setTotal(money);
				}
				accountDao.updateAccount(-money, 0, -money, cash.getUser().getUserId());
				//设置状态为取现成功
				cash.setStatus(1);
				updateCash(cash, log);
				return ;
			}
			if (cash.getStatus() ==4) {  //处理中状态到处理成功
				log.setRemark("提现处理成功，手续费"+manageFee+"元。");
				log.setType(Constant.CASH_SUCCESS);
				if(cash.getUser().getAccount().getUseMoney() < money ){
					money = cash.getTotal();
				}else{
					cash.setTotal(money);
				}
				accountDao.updateAccount(-money, -money, 0, cash.getUser().getUserId());
				//设置状态为取现成功
				cash.setStatus(1);
				updateCash(cash, log);
			}
			
		}
	}
	
	//汇付提现回调返回码999，状态是4，业务处理方法
	private void doCashByChinapnrByfour(String cashIsVerify,double money,AccountCash cash,AccountLog log){
		if (cashIsVerify.equals("1")) {
			//不需要审核，直接扣除
			log.setRemark("提现处理中");
			log.setType(Constant.CASH_SUCCESS);
			if(cash.getUser().getAccount().getUseMoney() < money ){
				money = cash.getTotal();
			}else{
				cash.setTotal(money);;
			}
			accountDao.updateAccount(-money, -money, 0, cash.getUser().getUserId());
			//设置状态为处理中
			cash.setStatus(8);
			
			updateCash(cash, log);
		}else{
			//需要审核，冻结资金
			log.setRemark("提现冻结");
			log.setType(Constant.CASH_FROST);
			log.setMoney(money);
			cash.setCredited(cash.getTotal());
			if(cash.getUser().getAccount().getUseMoney() < money ){
				money = cash.getTotal();
			}else{
				cash.setTotal(money);
			}
			accountDao.updateAccount(0, -money, money, cash.getUser().getUserId());
			//设置状态为取现待审核
			cash.setStatus(0);
			updateCash(cash, log);
		}
	}
	
	
	private void updateCash(AccountCash cash ,AccountLog log){
		accountCashDao.update(cash);
		Account act = accountDao.getAcountByUser(cash.getUser());
		User user =userDao.find(cash.getUser().getUserId()); 
		log.setMoney(cash.getTotal());
		log.setTotal(act.getTotal());
		log.setUseMoney(act.getUseMoney());
		log.setNoUseMoney(act.getNoUseMoney());
		log.setCollection(act.getCollection());
		log.setRepay(act.getRepay());
		log.setUser(user);
		log.setAddtime(new Date());
		log.setToUser(new User(Constant.ADMIN_ID));
		accountLogDao.save(log);
	}
	
	//此处只处理第三方业务，对于本地
	@Override
	public double  verifyCash(AccountCash cash, List<Object> taskList) {
		boolean reflag = false;
		double account = 0;
		switch (getApiType()) {
		case 1: //汇付处理取现复审
			 reflag=cashAuditChianpnr(cash.getOrderNo(), cash.getUser().getApiUsercustId(),NumberUtils.format2Str(cash.getCredited()), cash.getUser().getUserId(), "S");   //触发汇付任务
			 account =  cash.getTotal();
			 //account =  cash.getTotal()+cash.getFee();			
			 break;
		case 2://易极付取现复审处理
			accountService.dealAccountCash(cash, cash.getTotal(), taskList);//处理提现并添加任务。
			reflag = borrowService.doApiTask(taskList);   //执行任务接口
			logger.info("取现订单号：" + ((YjfPay)taskList.get(0)).getOrderno());
			cash.setOrderNo(((YjfPay)taskList.get(0)).getOrderno());
			account =  cash.getTotal();
			if(reflag){ cash.setDealStatus(4);}//取现申请成功，易及付
			break;
		default:
			break;
		}
		if (reflag) {
			cash.setStatus(1);			
		}else{
			cash.setStatus(2);
		}
		return account;
		
	}
	/**
	 * 是否需要注册第三方
	 * @param user
	 * @return
	 */
	@Override
	public Object doRegisterApi(User user) {
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			ChinapnrRegister reg=ChinapnrHelper.userRegister(user);
			return reg;
		case 2:// 易极付接口
			//v1.8.0.3_u2 TGPROJECT-293 2014-05-29   qinjun start
			if (StringUtils.isEmpty(user.getCardOff())) {
				throw new BussinessException("身份证到期时间不能为空,请填写！");
			} else {
				if (!(StringUtils.checkDateString(user.getCardOff()) || "0"
						.equals(user.getCardOff()))) {
					throw new BussinessException("身份证到期时间格式错误,请填写！");
				}
			}
			YjfRegister us = userRegister(user);
			if (us == null) {
			} else {
				if(us.getResultCode().contains("REGISTER_SUCCESS_UNKOW_REAL")){
					user.setRealStatus(4);
				}else{
					user.setRealStatus(1);
				}
				user.setApiId(us.getUserId());
				user.setApiStatus(1);
			}
			return user;
			//v1.8.0.3_u2 TGPROJECT-293 2014-05-29   qinjun end
		case 3:// 环迅接口
			MmmRegister mmmReg = MmmHelper.mmmRegister(user);
			return mmmReg;
		default:
			break;
		}
		return null;
	}
	@Override
	public Object apiLogin(User user){
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			ChinapnrLogin usrLogin = ChinapnrHelper.chinapnrUserLogin(user);
			return usrLogin;
		case 2:// 易极付接口
			ForwardYJFSit fys = PayModelHelper.forwardYJFSit(user.getApiId());
			fys.Createsign();
			return fys;
		default:
			break;
		}
		return null;
	}
	
	@Override
	public void checkApiCashMessage(User user,String payPassword){
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			break;
		case 2:// 易极付接口
			//易极付校验支付密码
			MD5 md5 = new MD5(); 
			if( StringUtils.isBlank(payPassword) || !md5.getMD5ofStr(payPassword).equals(user.getPaypassword())){
				throw new BussinessException("支付密码不正确！","/member/account/newcash.html");
			}
			break;
		default:
			break;
		}
	}
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 start
	@Override
	public Map<String, String> findApiAccount(User user) throws Exception{
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		Map<String, String> map = new HashMap<String,String>();
		switch (apiType) {
		case 1:// 汇付接口
			map = queryBalanceBgchianpnr(user.getApiUsercustId());
			break;
		case 2:// 易极付接口
			break;
		case 3:// 双乾接口
			map = mmmQalanceQuery(user);
			break;
		default:
			break;
		}
		return map;
	}
	//v1.8.0.4  TGPROJECT-25   qj  2014-04-03 stop
	
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
	@Override
	public void checkApiLoan(User user) {
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			break;
		case 2:// 易极付接口
			break;
		case 3:// 双乾接口
			if(user.getApiLoanAuthorize() == 0){
				throw new BussinessException("请先授权再进行该操作！","/member/loanAuthorize.html","点击进行授权");
			}
			break;
		default:
			break;
		}
	}
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 start
	@Override
	public void isCanVerifyFullSuccess(long borrowId) {
		int apiType = NumberUtils.getInt(Global.getValue("api_code"));
		switch (apiType) {
		case 1:// 汇付接口
			break;
		case 2:// 易极付接口
			borrowService.isCanVerifyFullSuccess(borrowId);
			break;
		case 3:// 双乾接口
			//mmmQueryTenderSuccess(borrowId+"");
			break;
		default:
			break;
		}
	}
	//v1.8.0.4  TGPROJECT-42   qj  2014-04-09 stop
	
	@Override
	public void creditForMoney(User user,double money ,List<Object> taskList) {
		switch (getApiType()) {
			case 1:
				ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, money, 1,
						user.getUserId()+"", "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
				cppModel.setPayusrCustId(user.getApiUsercustId());
				cppModel.setUsrCustId("");
				taskList.add(cppModel);
				break;
			case 2:
				YjfPay yjfPay = new YjfPay(null, Global.getValue("trade_offline_recharge"), 
						money+"", YjfType.VERIFYRECHARGE, null, null, YjfType.TRADETRANSFER, 
							null, user.getApiId(), null, Global.getValue("yjf_partnerId"));
				taskList.add(yjfPay);
				break;
			case 3:
				MmmPay mmmPay = new MmmPay("2","1","1",1,Global.getValue("plat_form_mmm"),user.getUserId(),user.getApiId()
						, money,MmmType.MMM_PAY_USER,"积分兑换现金",MmmType.GOODS_EXPENSE_MONEY,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
		
	}
	//1.8.0.4_u3   TGPROJECT-337  qinjun 2014-06-23  start

	@Override
	public void doWebPayMoney(WebPayModel payModel,List<Object> taskList) {
		switch (getApiType()) {
		case 1:
			ChinaPnrPayModel cppModel = new ChinaPnrPayModel(ChinaPnrType.TRANSFER, payModel.getMoney(), 1,
					String.valueOf(payModel.getPayUser().getUserId()), "0", null, DateUtils.getNowTimeStr(), ChinaPnrType.WEBPAY);
			cppModel.setPayusrCustId(payModel.getPayUser().getApiUsercustId());
			cppModel.setUsrCustId("");
			taskList.add(cppModel);
			break;
		case 2:
			YjfPay yjfPay = new YjfPay(null, Global.getValue("trade_offline_recharge"), 
					payModel.getMoney()+"", YjfType.VERIFYRECHARGE, null, null, YjfType.TRADETRANSFER, 
						null, payModel.getPayUser().getApiId(), null, Global.getValue("yjf_partnerId"));
			taskList.add(yjfPay);
			break;
		case 3:
			MmmPay mmmPay = new MmmPay("2","1","1",1,Global.getValue("plat_form_mmm"),payModel.getPayUser().getUserId(),payModel.getPayUser().getApiId()
					, payModel.getMoney(),MmmType.MMM_PAY_USER,"发放奖励",MmmType.REWARD_PAY,new Date());
			taskList.add(mmmPay);
			break;
		default:
			break;
	}
	}
	//1.8.0.4_u3   	TGPROJECT-337  qinjun 2014-06-23  end
	
	//v1.8.0.4_u4  TGPROJECT-346  qinjun 2014-07-01 start
	/**
	 * 放款，收取手续费
	 */
	 @Override
	public void	borrowertTransferToUser(List<Object> taskList,double borrowfee,Borrow borrow,User toUser){
		borrow = borrowDao.find(borrow.getId());
		 switch (getApiType()) {
			case 1://汇付
				break;
			case 2://易极付
				break;
			case 3://双乾
				MmmPay mmmPay = new MmmPay("2","2","1",borrow.getUser().getUserId(),borrow.getUser().getApiId(),toUser.getUserId(),
						toUser.getApiId(),borrowfee,MmmType.MMM_PAY_USER,borrow.getId()+"",MmmType.AUTOVERIFYFULLSUCCESS,new Date());
				taskList.add(mmmPay);
				break;
			default:
				break;
		}
	}
	//v1.8.0.4_u4  TGPROJECT-346  qinjun 2014-07-01 end
	 
	 
	 @Override
		public void doBankReturn(AccountCashModel cashModel) {
			logger.info("取现异步回调进入业务方法----");
			AccountCash cash = new AccountCash();
			cash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
			
			if(cash==null){
				logger.info("订单不存在，请查询数据库");
				return;
			}
			AccountLog log = new AccountLog();
			Account act = accountDao.getAcountByUser(cash.getUser());
			User user =userDao.find(cash.getUser().getUserId());
			
			cash.setLoanNo(cashModel.getLoanNo());
			
			double manageFee = 0d;//用户取现支付的全部费用
			if(Global.getInt("is_pay_cash") == 0){//汇付取现手续费，网站不垫付
				manageFee +=cashModel.getFeeAmt();
			}
			manageFee+=cashModel.getServFee();//当网站不收取网站服务费的时候，传进来的是0
			
			double money = cash.getTotal()+manageFee;//加上网站服务费
			
			cash.setFee(manageFee);//加上网站服务费
			
			cash.setCredited(cash.getTotal());
			
			if ("000".equals(cashModel.getReturnCode())) {	//取现处理成功
				if (cash.getStatus()==1) {	//已经处理成功
					return;
				}
				if (cash.getStatus() ==8) {	//冻结状态，扣除冻结
					logger.info("订单："+cashModel.getOrderId()+"，提现成功！进入本地处理！");
					log.setRemark("提现处理成功，手续费"+manageFee+"元。");
					log.setType(Constant.CASH_SUCCESS);
					if(cash.getUser().getAccount().getUseMoney() < money ){
						money = cash.getTotal();
					}else{
						cash.setTotal(money);
					}
					accountDao.updateAccount(-money, 0, -money, cash.getUser().getUserId());
					//设置状态为取现成功
					cash.setStatus(1);
					accountCashDao.update(cash);
					
					log.setMoney(money);
					log.setTotal(act.getTotal());
					log.setUseMoney(act.getUseMoney());
					log.setNoUseMoney(act.getNoUseMoney());
					log.setCollection(act.getCollection());
					log.setRepay(act.getRepay());
					log.setUser(user);
					log.setAddtime(new Date());
					log.setToUser(new User(Constant.ADMIN_ID));
					accountLogDao.save(log);
					return ;
				}
				logger.info("提现状态错误！orderNo:" + cash.getOrderNo() +"status:" + cash.getStatus());
			}else if ("400".equals(cashModel.getReturnCode())) {		//取现处理失败
				if (cash.getStatus()==1) {	//已经处理成功的扣除
					logger.info("订单："+cashModel.getOrderId()+"，提现银行处理失败！进入本地处理！");
					log.setRemark("提现处理失败，资金退回，手续费"+manageFee+"元。");
					log.setType(Constant.CASH_FAIL);
					accountDao.updateAccount(money, money, 0, cash.getUser().getUserId());	//提现资金退回
					//修改取现记录状态
					cash.setStatus(2);
					accountCashDao.update(cash);
					
					log.setMoney(money);
					log.setTotal(act.getTotal());
					log.setUseMoney(act.getUseMoney());
					log.setNoUseMoney(act.getNoUseMoney());
					log.setCollection(act.getCollection());
					log.setRepay(act.getRepay());
					log.setUser(user);
					log.setToUser(new User(Constant.ADMIN_ID));
					accountLogDao.save(log);
					return ;
				}else if (cash.getStatus()==8) {	//冻结的提现解冻处理
					logger.info("订单："+cashModel.getOrderId()+"，提现银行处理失败！进入本地处理！");
					log.setRemark("提现处理失败，资金退回，手续费"+manageFee+"元。");
					log.setType(Constant.CASH_FAIL);
					accountDao.updateAccount(0, money, -money, cash.getUser().getUserId());	//资金解冻
					cash.setStatus(2);
					accountCashDao.update(cash);
					
					log.setMoney(money);
					log.setTotal(act.getTotal());
					log.setUseMoney(act.getUseMoney());
					log.setCollection(act.getCollection());
					log.setRepay(act.getRepay());
					log.setUser(user);
					log.setToUser(new User(Constant.ADMIN_ID));
					accountLogDao.save(log);
					return ;
				}
				logger.info("提现状态错误！orderNo:" + cash.getOrderNo() +"status:" + cash.getStatus());
			}
		}
	
}

package cn.zchfax.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.dao.AccountDao;
import cn.zchfax.dao.AccountLogDao;
import cn.zchfax.dao.AccountRechargeDao;
import cn.zchfax.dao.InviteUserDao;
import cn.zchfax.dao.InviteUserRebateDao;
import cn.zchfax.dao.RebateProportionDao;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.AccountRecharge;
import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.ChinaPnrPayModel;
import cn.zchfax.domain.InviteUser;
import cn.zchfax.domain.InviteUserRebate;
import cn.zchfax.domain.RebateProportion;
import cn.zchfax.domain.User;
import cn.zchfax.domain.YjfPay;
import cn.zchfax.exception.ManageBussinessException;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.service.ApiService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.InviteUserRebateService;
import cn.zchfax.service.MsgService;
import cn.zchfax.util.OrderNoUtils;
import cn.zchfax.util.StringUtils;
/**
 * 
 * @author lx TGPROJECT-302 add
 *
 */
@Service(value="inviteUserRebateService")
@Transactional
public class InviteUserRebateServiceImpl extends BaseServiceImpl implements InviteUserRebateService {

	private Logger logger=Logger.getLogger(InviteUserRebateServiceImpl.class);
	@Autowired
	InviteUserRebateDao inviteUserRebateDao;
	@Autowired
	private ApiService apiService;
	@Autowired
    private BorrowService borrowService;
	@Autowired
	private AccountDao accountDao;
	@Autowired
	private AccountLogDao accountLogDao;
	@Autowired
	private InviteUserDao inviteUserDao;
	@Autowired
	private MsgService msgService;
	@Autowired
	AccountRechargeDao accountRechargeDao;
	@Autowired
	RebateProportionDao rebateProportionDao; 
	
	@Override
	public PageDataList<InviteUserRebate> getInviteUserRebateListBySearchParam(SearchParam param){
		return inviteUserRebateDao.findPageList(param);
	}
	@Override
	public List<InviteUserRebate> getExportInviteUserRebateList(SearchParam param){
		return inviteUserRebateDao.findAllPageList(param).getList();
	}
	@Override
	public InviteUserRebate getInviteUserRebateById(long id){
		return inviteUserRebateDao.find(id);
	}
	@Override
	public void cancelInviteRebate(InviteUserRebate iur){
		inviteUserRebateDao.update(iur);
	}
	@Override
	public void verify(InviteUserRebate iur,AccountLog log,AccountRecharge ar) throws Exception{
		List<Object> taskList = new ArrayList<Object>();
		int apiType = Global.getInt("api_code");
		User rechargeUser = ar.getUser();
		if(rechargeUser.getApiStatus()==0 || StringUtils.isBlank(rechargeUser.getApiId())){
			throw new ManageBussinessException("该用户没有开通或激活"+Global.getValue("api_name")+"账户！！", "/admin/inviteuserrebate/inviterebatelist.html");
		}
		apiService.webRecharge(ar,taskList);//第三方接口
		if(borrowService.doApiTask(taskList)){//调用转账功能
			//修改账户金额
			Account act=accountDao.getAcountByUser(rechargeUser);
			accountDao.updateAccount(ar.getMoney(), ar.getMoney(), 0,rechargeUser.getUserId() );
			//修改订单状态
			ar.setStatus(1);
			String tradeNo = "";
			switch (apiType) {
			case 1://汇付
				ChinaPnrPayModel cpm = (ChinaPnrPayModel)taskList.get(0);
				tradeNo = cpm.getOrdId();
				break;
			case 2://易极付
				YjfPay yjf = (YjfPay)taskList.get(0);
				tradeNo = yjf.getOrderno();
				break;

			default:
				tradeNo = OrderNoUtils.getInstance().getSerialNumber();
				break;
			}
			ar.setTradeNo(tradeNo);
			ar.setRemark("获取推荐提成成功!");
			accountRechargeDao.save(ar);
			
			//插入资金记录表
			log.setUser(ar.getUser());
			User toUser = new User();
			log.setToUser(toUser);
			toUser.setUserId(Constant.ADMIN_ID);
			log.setMoney(ar.getMoney());
			log.setTotal(act.getTotal());
			log.setUseMoney(act.getUseMoney());
			log.setNoUseMoney(act.getNoUseMoney());
			log.setCollection(act.getCollection());
			log.setRepay(act.getRepay());
			accountLogDao.save(log);
			
			//更新
			inviteUserRebateDao.update(iur);
			//充值成功，发消息通知
			/*MsgReq req = new MsgReq();
			req.setReceiver(ar.getUser());
			req.setSender(new User(Constant.ADMIN_ID));
			req.setMsgOperate(this.msgService.getMsgOperate(8));
			req.setTime(DateUtils.dateStr4(new Date()));
			req.setMoney(""+ar.getMoney());
			DisruptorUtils.sendMsg(req);*/
		}else{
			throw new ManageBussinessException(Global.getString("api_name")+"处理出错！！！");
		}
	}
	/**
	 * 计算推荐人的回扣比例和金额
	 */
	@Override
	public Map<String, Double> calculateRebate(InviteUser iu,BorrowTender bt){
		List<InviteUser> inviteUserList=inviteUserDao.getInvitreUser(iu.getInviteUser().getUserId());
		Date d=bt.getBorrow().getVerifyTime();
		double rebateProportion=0.0;
		double rebateAmount=0.0;
		double calculateRebate=0.0;
		for (InviteUser iUser:inviteUserList) {
			calculateRebate+=inviteUserDao.getInvitreUser(iUser.getUser(), bt.getBorrow());
		}
		List<RebateProportion> rpList=rebateProportionDao.getRebateProportionAllList();
		for (RebateProportion rp:rpList) {
			if(rp.getBeginAccount()<calculateRebate && rp.getEndAccount()>=calculateRebate){
				rebateProportion=rp.getRebate();
				break;
			}
		}
		Map<String, Double> map=new HashMap<String, Double>();
		map.put("rebateProportion", rebateProportion);
		return map;
	}
	
	/**
	 * 计算推荐人的回扣比例和金额
	 */
	@Override
	public double calculateRebate(InviteUserRebate iur){
		List<InviteUser> inviteUserList=inviteUserDao.getInvitreUser(iur.getInviteUser().getUserId());
		double rebateProportion=0.0;
		double calculateRebate=0.0;
		for (InviteUser iUser:inviteUserList) {
			calculateRebate+=inviteUserDao.getInvitreUser(iUser.getUser(), iur.getBorrowTender().getBorrow());
		}
		List<RebateProportion> rpList=rebateProportionDao.getRebateProportionAllList();
		for (RebateProportion rp:rpList) {
			if(rp.getBeginAccount()<calculateRebate && rp.getEndAccount()>calculateRebate){
				rebateProportion=rp.getRebate();
				break;
			}
		}
		return rebateProportion;
	}
	
	@Override
	public void update(InviteUserRebate iur){
		inviteUserRebateDao.update(iur);
	}
}

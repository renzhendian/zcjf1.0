package cn.zchfax.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.zchfax.api.chinapnr.CashOut;
import cn.zchfax.api.chinapnr.FssTrans;
import cn.zchfax.api.pay.BankCodeBindingAddInfo;
import cn.zchfax.api.pay.BankNoQuery;
import cn.zchfax.api.pay.DeductDepositApply;
import cn.zchfax.api.pay.DeductSign;
import cn.zchfax.api.pay.DepositQuery;
import cn.zchfax.api.pay.PayModelHelper;
import cn.zchfax.api.pay.SignmanyBank;
import cn.zchfax.api.pay.UserAccountQuery;
import cn.zchfax.api.pay.WithdrawQquery;
import cn.zchfax.context.Constant;
import cn.zchfax.context.Global;
import cn.zchfax.context.YjfType;
import cn.zchfax.dao.AccountBankDao;
import cn.zchfax.dao.AccountCashDao;
import cn.zchfax.dao.AccountDao;
import cn.zchfax.dao.AccountLogDao;
import cn.zchfax.dao.AccountRechargeDao;
import cn.zchfax.dao.AccountWebDeductDao;
import cn.zchfax.dao.AreaBankDao;
import cn.zchfax.dao.AreaDao;
import cn.zchfax.dao.BorrowTenderDao;
import cn.zchfax.dao.DrawBankDao;
import cn.zchfax.dao.DrawBankMmmDao;
import cn.zchfax.dao.GlodTransferDao;
import cn.zchfax.dao.InterestGenerateDao;
import cn.zchfax.dao.UserDao;
import cn.zchfax.dao.WebGlodLogDao;
import cn.zchfax.disruptor.DisruptorUtils;
import cn.zchfax.domain.Account;
import cn.zchfax.domain.AccountBank;
import cn.zchfax.domain.AccountCash;
import cn.zchfax.domain.AccountLog;
import cn.zchfax.domain.AccountRecharge;
import cn.zchfax.domain.AccountWebDeduct;
import cn.zchfax.domain.AreaBank;
import cn.zchfax.domain.ChinaPnrPayModel;
import cn.zchfax.domain.DrawBank;
import cn.zchfax.domain.DrawBankMmm;
import cn.zchfax.domain.GlodTransfer;
import cn.zchfax.domain.InterestGenerate;
import cn.zchfax.domain.Rule;
import cn.zchfax.domain.User;
import cn.zchfax.domain.WebGlodLog;
import cn.zchfax.domain.YjfPay;
import cn.zchfax.exception.BussinessException;
import cn.zchfax.exception.ManageBussinessException;
import cn.zchfax.model.BorrowParam;
import cn.zchfax.model.MsgReq;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.APIModel.AccountCashModel;
import cn.zchfax.model.APIModel.RechargeModel;
import cn.zchfax.model.account.BaseAccountSumModel;
import cn.zchfax.model.account.BorrowSummary;
import cn.zchfax.model.account.CollectSummary;
import cn.zchfax.model.account.InvestSummary;
import cn.zchfax.model.account.RepaySummary;
import cn.zchfax.model.account.UserAccountSummary;
import cn.zchfax.model.account.WebAccountSumModel;
import cn.zchfax.service.AccountService;
import cn.zchfax.service.ApiService;
import cn.zchfax.service.BorrowService;
import cn.zchfax.service.MsgService;
import cn.zchfax.service.RewardExtendService;
import cn.zchfax.service.RuleService;
import cn.zchfax.util.DateUtils;
import cn.zchfax.util.NumberUtils;
import cn.zchfax.util.OrderNoUtils;
import cn.zchfax.util.StringUtils;

@Service(value="accountService")
@Transactional
public class AccountServiceImpl extends BaseServiceImpl implements AccountService {

	private Logger logger=Logger.getLogger(AccountServiceImpl.class);
	@Autowired
	AccountRechargeDao accountRechargeDao;
	@Autowired
	AccountCashDao accountCashDao;
	@Autowired
	private AccountDao accountDao;
	@Autowired
	private AccountLogDao accountLogDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private AccountBankDao accountBankDao;
	@Autowired
	private DrawBankDao drawBankDao;
	@Autowired
	private AreaDao areaDao;
	@Autowired
	private MsgService msgService;
	@Autowired 
	private BorrowTenderDao borrowTenderDao;
	@Autowired
	private AccountWebDeductDao accountWebDeductDao;
	@Autowired
	private ApiService apiService;
	@Autowired
	private GlodTransferDao glodTransferDao;
	@Autowired
	private AreaBankDao areaBankDao;
	@Autowired
	private WebGlodLogDao webGlodLogDao; 
	@Autowired
	private DrawBankMmmDao drawBankMmmDao;
	@Autowired
	private InterestGenerateDao interestGenerateDao;
	//v1.8.0.4_u4  TGPROJECT-349  qinjun  2014-07-02  start 
	@Autowired
	private RuleService ruleService;
	//v1.8.0.4_u4  TGPROJECT-349  qinjun  2014-07-02  start 
	//v1.8.0.4_u4  TGPROJECT-355 qinjun 2014-07-04 start
	@Autowired
	private RewardExtendService rewardExtendService;
	//v1.8.0.4_u4  TGPROJECT-355 qinjun 2014-07-04 end

	@Override
	public void addRecharge(AccountRecharge r) {
		accountRechargeDao.save(r);
	}
	@Autowired
	private BorrowService borrowService;
	//v1.8.0.4_u2 TGPROJECT-308 lx 2014-05-22 start
	public AccountBank getAccountBankByCardNo(String cardNo, User user){
		return accountBankDao.getAccountBankByCardNo(cardNo, user);
	}
	//v1.8.0.4_u2 TGPROJECT-308 lx 2014-05-22 end
	/**
	 * 获取用户逇账户详情
	 */
	public UserAccountSummary getUserAccountSummary(long user_id) {
		UserAccountSummary  uas=new UserAccountSummary();

		//账户金额信息
		User user =  userDao.find(user_id);
		Account act=user.getAccount();
		uas.setAccountTotal(act.getTotal());
		uas.setAccountUseMoney(act.getUseMoney());
		uas.setAccountNoUseMoney(act.getNoUseMoney());
		uas.setCollectTotal(act.getCollection());//待收总和
		uas.setRepayTotal(act.getRepay());//待还

		//还款情况
		RepaySummary repay=accountDao.getRepaySummary(user_id);
		uas.setRepayInterest(repay.getRepayInterest());//待还利息总和
		uas.setNewestRepayDate(repay.getRepayTime());//最近待还时间
		uas.setNewestRepayMoney(repay.getRepayAccount());//最近还款的本金
		uas.setRecentlyRepaymentTotal(repay.getRepayTotal());//最近待还本息
		uas.setLateTotal(repay.getLateTopay()); //逾期未还款总金额
		uas.setLateRepayed(repay.getLateRepayed());//迟还款总金额
		uas.setYesRepayTotal(repay.getHasRepayed());//已经还款金额

		//借款情况
		BorrowSummary borrow=accountDao.getBorrowSummary(user_id);
		uas.setBorrowTotal(borrow.getBorrowTotal());//借款本金
		uas.setBorrowInterest(borrow.getBorrowInterest());
		uas.setBorrowTimes(borrow.getBorrowTimes());//成功借款多少次

		//投资情况(成功投资情况)
		InvestSummary i=accountDao.getInvestSummary(user_id);
		uas.setInvestTotal(i.getInvestTotal());//投资总和
		uas.setInvestInterest(i.getInvestInterest());//成功投资产生的利息
		uas.setInvestTimes(i.getInvestTimes());//成功投资次数

		//待收情况
		CollectSummary collect=accountDao.getCollectSummary(user_id);
		uas.setNewestCollectMoney(collect.getCollectMoney());//最近待收本息
		uas.setNewestCollectInterest(collect.getCollectInterest());//最近待收利息
		uas.setNewestCollectDate(collect.getCollectTime());
		//待收总额前边已经算过，这里不再添加
		uas.setCollectInterestTotal(collect.getCollectInterestTotal());//待收利息总和
		//净资产
		uas.setAccountOwnMoney(NumberUtils.format2(act.getTotal()-act.getRepay()));
		//充值
		uas.setRechargeTotal(accountDao.getChargeTotal(user_id));
		//提现
		uas.setCashTotal(accountDao.getCacheTotal(user_id));

		int[] countArray = accountDao.getCountRusultBySql(user_id);
		uas.setTenderingCount(countArray[0]);//正在投标的笔数
		uas.setWaitCollectCount(countArray[1]);//等待回款
		uas.setHasCollectCount(countArray[2]);//已经回款
		uas.setWaitRepayCount(countArray[3]);//等待还款
		uas.setHadRepayCount(countArray[4]);//已还款的笔数

		double[] yesIncoming = accountDao.getUserIncoming(user_id);
		uas.setHasCollectInterestTotal(yesIncoming[0]);//已经收利息的总和
		uas.setHasCollectTotal(yesIncoming[1]);//已收本金总和

		//投标奖励总和
		double awardTotal = accountDao.getSumAwardDeductByUser(user_id);
		uas.setAwardTotal(awardTotal);

		//满标待审投资情况
		double[] tenderArray = accountDao.getWaitFullSummary(user_id);//等待复审或者满标投标个数
		//v1.8.0.4_u3  TGPROJECT-339  qinjun 2014-06-24  start
		uas.setWaitFullBorrowTenderCount((int)tenderArray[0]);//等待复审或者满标投标总额
		//v1.8.0.4_u3  TGPROJECT-339  qinjun 2014-06-24  end
		uas.setWaitFullBorrowTenderTotal(tenderArray[1]);

		return uas;
	}
	//v1.8.0.4_u1 TGPROJECT-289 lx start
	@Override
	public WebAccountSumModel getWebAccountSumModel(){
		return accountDao.getWebAccountSumModel();
	}
	//v1.8.0.4_u1 TGPROJECT-289 lx end
	@Override
	public void doRechargeTask(RechargeModel re,AccountLog log){
		if ("true".equals(re.getResult())){ //验证通过并且处理成功
			try {
				newRecharge(re,log, log.getRemark());
			} catch (Exception e) {
				logger.error(e);
			}
		}else {
			failRecharge(re, log, log.getRemark());
			throw new BussinessException("充值失败！"); 
		}
	}

	@Override
	public void cashCallBack(AccountCashModel cashModel,BorrowParam param){

		logger.info("取现回调进入业务方法----");
		AccountCash dbCash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
		if(dbCash==null){
			logger.info("订单不存在，请查询数据库");
			return;
		}
		/*if( dbCash.getStatus() !=4){
			logger.info("业务处理状态不对，orderNo:" + dbCash.getOrderNo() +"status:" + dbCash.getStatus());  
			return;
		}*/
		AccountLog log = new AccountLog();
		AccountCash cash = new AccountCash();
		cash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
		int apiType = Global.getInt("api_code");
		logger.info("接口类型"+apiType);
		cash.setLoanNo(cashModel.getLoanNo());
		if(apiType ==1){	//汇付
			apiService.doHfCashService(cashModel, cash, log);
		}else{
			logger.info("系统接口参数错误----------------");
			throw new BussinessException("取现失败！");
		}
		
	}

	// v1.8.0.3 TGPROJECT-21 lx 2014-04-09 start
	@Override
	public void mmmCashCallBack(AccountCashModel cashModel, BorrowParam param) {
		logger.info("取现回调进入业务方法----");
		AccountCash dbCash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
		if( dbCash == null || (dbCash.getStatus() !=4 && dbCash.getStatus()!=1)){
			logger.info("业务处理状态不对，orderNo:" + dbCash.getOrderNo() +"status:" + dbCash.getStatus());  
			return;
		}
		AccountLog log = new AccountLog();
		AccountCash cash = checkApiBack(cashModel,log,param);
		accountCashDao.update(cash);
		Account act = accountDao.getAcountByUser(cash.getUser());
		log.setMoney(cash.getCredited());
		log.setTotal(act.getTotal());
		log.setUseMoney(act.getUseMoney());
		log.setNoUseMoney(act.getNoUseMoney());
		log.setCollection(act.getCollection());
		log.setRepay(act.getRepay());
		accountLogDao.save(log);
	}
	// v1.8.0.3 TGPROJECT-21 lx 2014-04-09 end
	private AccountCash checkApiBack(AccountCashModel cashModel,AccountLog log,BorrowParam param){
		int apiCode = Global.getInt("api_code");
		AccountCash cash = new AccountCash();
		logger.info("接口类型"+apiCode);
		switch (apiCode) {
		case 1: //汇付处理
			cash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
			//拦截
			if (cash.getStatus() ==4) {   //如果订单状态不为4，则已经处理过，不再进行订单处理。4为提交状态，
				List<Object> taskList = new ArrayList<Object>();
				User user =userDao.find(cash.getUser().getUserId());  //重新获取user
				log.setUser(user);
				log.setType(Constant.CASH_FROST);
				log.setToUser(new User(Constant.ADMIN_ID));
				log.setAddip(param.getIp());
				log.setAddtime(new Date());
				//执行取现操作
				apiService.doCash(cash, taskList, log,cashModel.getFeeAmt(),cashModel.getServFee(),cashModel.getCardNo());   
			}else{
				logger.info("提现拦截，取现状态："+cash.getStatus());
				//v1.8.0.4_u2 TGPROJECT-305 lx start
				//throw new BussinessException("取现回调处理状态异常status："+cash.getStatus());
				logger.info("提现回调状态异常status："+cash.getStatus());
				//v1.8.0.4_u2 TGPROJECT-305 lx end
			}
			logger.info("取现完成"+cash.getId());
			break;
		case 2://易极付处理
			String orderNo = cashModel.getOrderId();

			logger.info("disruptor 处理提现进入：orderNo："+ orderNo);
			cash = accountCashDao.getAccountCashByOrderNo(orderNo);
			if(cash == null || cash.getStatus() != 4){
				logger.info("提现拦截，取现状态："+cash.getStatus());
				throw new BussinessException("取现回调处理状态异常status："+cash.getStatus());
			}
			double money = cash.getTotal();
			log.setUser(cash.getUser());
			log.setToUser(new User(1));
			log.setType(Constant.CASH_SUCCESS);
			if(cashModel.isResult()){
				logger.info("提现成功回调，扣除冻结款。");
				logger.info(Global.getValue("api_name")+"提现申请处理成功,order: " + orderNo + " money:" + cashModel.getOrderAmount());
				accountDao.updateAccount(-money, 0, -money, cash.getUser().getUserId());
				log.setRemark("提现成功，扣除冻结金额：" + cash.getTotal()+" 元");
				log.setType(Constant.CASH_SUCCESS);
				log.setAddtime(new Date());
				cash.setStatus(1);
				//v1.8.0.4_u3  TGPROJECT-342   qinjun  2014-06-24  start
				cash.setFee(cashModel.getFeeAmt());
				//v1.8.0.4_u3  TGPROJECT-342   qinjun  2014-06-24  end
				cash.setDealStatus(1);//易及付取现成功
			}else{
				logger.info("提现失败回调过来，解冻冻结款。");
				logger.info(Global.getValue("api_name")+"提现申请处理失败,order: " + orderNo + " money:" + cashModel.getOrderAmount());
				//解冻
				accountDao.updateAccount(0, money, -money, cash.getUser().getUserId());
				log.setRemark("提现失败，返回冻结资金"+cash.getTotal()+" 元");
				log.setType(Constant.CASH_FAIL);
				log.setAddtime(new Date());
				cash.setStatus(2);
				cash.setDealStatus(2);//易及付取现失败
			}
			break;
			// v1.8.0.3 TGPROJECT-21 lx 2014-04-09 start
		case 3:
			cash=accountCashDao.getAccountCashByOrderNo(cashModel.getOrderId());   //获取取现记录,回调中处理根据订单查询
			User user =userDao.find(cash.getUser().getUserId());  //重新获取user
			log.setUser(user);
			log.setToUser(new User(Constant.ADMIN_ID));
			//拦截
			if (param.getResultCode().equals("88")) {
				if (cash.getStatus() ==4) {   //如果订单状态不为4，则已经处理过，不再进行订单处理。4为提交状态，
					List<Object> taskList = new ArrayList<Object>();

					log.setType(Constant.CASH_FROST);
					log.setAddip(param.getIp());
					log.setAddtime(new Date());
					//执行取现操作,记住  cashModel.getFeeAmt()---->平台承担的手续费金额
					apiService.doCash(cash, taskList, log,cashModel.getFeeAmt(),cashModel.getServFee(),cashModel.getCardNo());   
					//v1.8.0.4_u4  TGPROJECT-349  qinjun  2014-07-02  start 
					//如果取现费用为0，若规则启用，则是启用免费取现额度
					Rule rule = ruleService.getRuleByNid("free_cash_rule");
					if(rule != null && rule.getStatus() == 1){
						if(cashModel.getFeeAmt() == 0){
							Account act = accountDao.getAcountByUser(cash.getUser());
							act.setFreeCashMoney(act.getFreeCashMoney() - cash.getTotal());
							accountDao.save(act);
						}
					}
					//v1.8.0.4_u4  TGPROJECT-349  qinjun  2014-07-02  end
				}else{
					logger.info("提现拦截，取现状态："+cash.getStatus());
					//v1.8.0.4_u2 TGPROJECT-305 lx start
					logger.info("提现回调状态异常status："+cash.getStatus());
					if(cash.getStatus() == 1){
						throw new BussinessException("取现成功！");
					}else{
						throw new BussinessException("取现回调处理状态异常status："+cash.getStatus());
					}

					//v1.8.0.4_u2 TGPROJECT-305 lx end
				}
				logger.info("取现完成"+cash.getId());
			}else if (param.getResultCode().equals("89")){
				logger.info("提现失败回调过来，归还提现款。");
				if(cash.getStatus() == 1){//提现申请成功，处理银行转账失败的情况。
					logger.info(Global.getValue("api_name")+"提现申请处理失败,order: " + cashModel.getOrderId() + " money:" + cashModel.getOrderAmount());
					double totalmoney = cash.getTotal()+cash.getFee();
					//解冻
					accountDao.updateAccount(totalmoney, totalmoney, 0, cash.getUser().getUserId());
					log.setRemark("提现失败，返回资金"+totalmoney+" 元");
					log.setType(Constant.CASH_FAIL);
					log.setAddtime(new Date());
					cash.setStatus(2);
				}else{
					throw new BussinessException("提现失败回调过来,取现状态异常:"+cash.getStatus());
				}
			}
			break;
			// v1.8.0.3 TGPROJECT-21 lx 2014-04-09 end
		default:
			break;
		}
		return cash;
	}

	/**
	 * 充值后台调用
	 * @throws Exception 
	 */
	@Override
	public void newRecharge(RechargeModel rem,AccountLog log,String params) throws Exception {
		logger.info("进入充值"+"订单号："+rem.getOrderId());
		AccountRecharge existRecharge=accountRechargeDao.getRechargeByTradeno(rem.getOrderId());
		if(existRecharge==null){
			logger.info("订单号不存在："+ rem.getOrderId());
			throw new BussinessException("订单号不存在, orderNo:" + rem.getOrderId());
		}
		if(existRecharge.getMoney() != Double.parseDouble(rem.getOrderAmount())){ //校验金额是否相等
			logger.info("金额不相等"+Global.getValue("api_name")+"回调金额：" + rem.getOrderAmount() + " 订单：" + rem.getOrderId()
					+ "---金额不符,充值被拒绝");
			throw new BussinessException(Global.getValue("api_name")+"回调金额：" + rem.getOrderAmount() + " 订单：" + rem.getOrderId()
					+ "---金额不符,充值被拒绝");
		}
		existRecharge.setReturnTxt(params);
		long userId = existRecharge.getUser().getUserId();
		if(existRecharge.getStatus()==0||existRecharge.getStatus()==2){
			logger.debug("异步回调处理订单： " + existRecharge.getTradeNo());
			//修改账户金额
			Account act=accountDao.getAcountByUser(existRecharge.getUser());
			double fee = rechargeFee(rem);
			int rechargeWeb = Global.getInt("recharge_web");  //获取平台是否自己垫付充值手续费
			double rechargeMoney = 0;
			if (rechargeWeb ==1 || rechargeWeb==0 ) { //平台垫付充值手续费,不收取任何手续费
				rechargeMoney=existRecharge.getMoney();
			}else{//从用户自己账户扣款
				rechargeMoney = NumberUtils.format2(existRecharge.getMoney() - fee);
			}
			accountDao.updateAccount(rechargeMoney, rechargeMoney, 0,userId );

			//修改订单状态
			existRecharge.setStatus(1);//设置状态
			existRecharge.setReturnTxt(params);//返回参数
			existRecharge.setSerialNo(rem.getSerialNo());//返回流水号
			existRecharge.setFee(fee);
			if(rechargeWeb==0){//不扣手续费
				existRecharge.setRemark(existRecharge.getRemark());
			}else if(rechargeWeb==1){//平台垫付充值手续费，备注要说明
				existRecharge.setRemark(existRecharge.getRemark()+",网站垫付手续费：" + NumberUtils.format2(fee));
			}else{
				existRecharge.setRemark(existRecharge.getRemark()+",手续费：" + NumberUtils.format2(fee));
			}
			accountRechargeDao.update(existRecharge);

			//插入资金记录表
			log.setUser(existRecharge.getUser());
			logger.info("充值回调用户Id是"+userId);
			User toUser = new User();
			toUser.setUserId(Constant.ADMIN_ID);
			log.setToUser(toUser);
			log.setMoney(rechargeMoney);
			log.setTotal(act.getTotal());
			log.setUseMoney(act.getUseMoney());
			log.setNoUseMoney(act.getNoUseMoney());
			log.setCollection(act.getCollection());
			log.setRepay(act.getRepay());
			String remark  = "";
			if(rechargeWeb == 0){
				remark = "网银充值"+rechargeMoney +"元" ;
			}else if(rechargeWeb == 1){
				remark = "网银充值"+rechargeMoney +"元" + ",网站垫付手续费：" + fee +"元";
			}else{
				remark = (fee==0)? ("网银充值"+rechargeMoney +"元"):("网银充值"+rechargeMoney +"元 ,扣除手续费：" + fee +"元");
			}
			log.setRemark(remark);
			accountLogDao.save(log);
			//v1.8.0.4_u4  TGPROJECT-355 qinjun 2014-07-04 start
			//第一次充值成功送红包
			rewardExtendService.rechargeExtendRedPacket(existRecharge.getUser(),rechargeMoney);
			//v1.8.0.4_u4  TGPROJECT-355 qinjun 2014-07-04 end
			//充值成功，发消息通知
			MsgReq req = new MsgReq();
			req.setReceiver(existRecharge.getUser());
			req.setSender(new User(Constant.ADMIN_ID));
			req.setMsgOperate(this.msgService.getMsgOperate(8));
			req.setTime(DateUtils.dateStr4(new Date()));
			req.setMoney(""+rechargeMoney);
			DisruptorUtils.sendMsg(req);
		}else{
			logger.info("订单状态不对：orderNo--" + existRecharge.getTradeNo() + " status:" + existRecharge.getStatus());
		}
	}
	/**
	 * 充值失败
	 * @param re
	 * @param log
	 */
	@Override
	public void failRecharge(RechargeModel rem,AccountLog log , String params) {
		AccountRecharge existRecharge=accountRechargeDao.getRechargeByTradeno(rem.getOrderId());
		if(existRecharge==null){
			return ;
		}
		existRecharge.setReturnTxt(params);
		existRecharge.setTradeNo(rem.getSerialNo());
		if(existRecharge.getStatus()==0){
			accountRechargeDao.updateRecharge(2,rem.getResultMsg(), rem.getOrderId(),rem.getSerialNo());
		}
	}

	@Override
	public PageDataList<Account> getAccontList(SearchParam param) {
		return accountDao.findPageList(param);
	}

	@Override
	public List<Account> getSumAccontList(SearchParam param) {
		PageDataList<Account> pageList =  accountDao.findAllPageList(param);
		return pageList.getList();
	}

	@Override
	public List<AccountLog> getSumAccontLogList(SearchParam param) {
		PageDataList<AccountLog> pageList =  accountLogDao.findAllPageList(param);
		return pageList.getList();
	}
	@Override
	public PageDataList<AccountLog> getAccontLogList(SearchParam param) {
		return accountLogDao.findPageList(param);
	}
	@Override
	public List<AccountCash> getSumCashList(SearchParam param) {
		return accountCashDao.findAllPageList(param).getList();
	}
	/*@Override
	public List<InvestSummary>  getInvestSummaryList(int type){
		return accountDao.getInvestSummaryList(type);
	}*/
	@Override
	public PageDataList<AccountCash> getCashList(SearchParam param) {
		return accountCashDao.findPageList(param);
	}
	@Override
	public List<AccountRecharge> getSumRechargeList(SearchParam param) {
		PageDataList<AccountRecharge> pageList = accountRechargeDao.findAllPageList(param);
		return pageList.getList();
	}
	@Override
	public PageDataList<AccountRecharge> getRechargeList(SearchParam param) {
		return accountRechargeDao.findPageList(param);
	}
	@Override
	public PageDataList<AccountWebDeduct> getWebDeductList(SearchParam param) {
		return accountWebDeductDao.findPageList(param);
	}
	@Override
	public Account getAccountByUser(User u) {
		return accountDao.findByPropertyForUnique("user", u);
	}
	@Override
	public List<AccountBank> getBankListByUser(User u) {
		return accountBankDao.findByProperty("user", u);
	}
	@Override
	public void addAccountBank(Object object) {
		AccountBank ab = apiService.addAccountBank(object);
		accountBankDao.save(ab);		
	}
	@Override
	public PageDataList<AccountBank> getAccountBankList(SearchParam param) {
		return accountBankDao.findPageList(param);
	}
	@Override
	public AccountBank getAccountBankById(int id) {
		return accountBankDao.find(id);
	}
	@Override
	public AccountWebDeduct getAccountWebDeductById(int id) {
		return accountWebDeductDao.find(id);
	}
	@Override
	public void addOrUpdateAccountBank(AccountBank ab) {
		accountBankDao.merge(ab);
	}
	@Override
	public void delAccountBankById(int id) {
		accountBankDao.delete(id);
	}
	@Override
	public AccountBank getAccontBankByAccount(String account) {
		return accountBankDao.findByPropertyForUnique("account", account);
	}

	@Override
	public boolean  yjfNewCash(AccountCash accountCash,BorrowParam param) {		
		List<Object> taskList = new ArrayList<Object>();
		User user =userDao.find(accountCash.getUser().getUserId());  //重新获取user
		double maxCash=NumberUtils.getDouble(Global.getValue("most_cash"));
		double money =  accountCash.getTotal(); //提现金
		if(money>maxCash){
			throw new BussinessException("提现金额不能大于"+maxCash+", 请您联系客服");
		}
		AccountLog log = new AccountLog(user.getUserId(),Constant.CASH_FROST,Constant.ADMIN_ID,
				DateUtils.getNowTimeStr(),param.getIp());
		//执行取现操作
		String cashIsVerify = Global.getValue("cash_is_verify"); //获取取现是否需要校验
		if (cashIsVerify.equals("1")){  //取现不需要审核触发易极付 
			dealAccountCash(accountCash, accountCash.getTotal(), taskList);//易极付取现查询银行卡是否正确
			if(borrowService.doApiTask(taskList)){//处理提现业务
				logger.info("提现订单号："+ ((YjfPay)taskList.get(0)).getOrderno());
				accountCash.setOrderNo(((YjfPay)taskList.get(0)).getOrderno());//将订单号存入
				accountCash.setStatus(4);//将状态设置为临时状态，待易极付回调处理
				accountCash.setDealStatus(4);//申请取现成功。
			}else{
				throw new BussinessException("提现失败");
			}
		}else{
			accountCash.setStatus(0);//取现需要审核将状态设为0
		}
		log.setRemark("提现冻结");
		accountDao.updateAccount(0, -accountCash.getTotal(), accountCash.getTotal(), accountCash.getUser().getUserId());
		//保存提现申请
		accountCashDao.update(accountCash);
		Account account = accountDao.getAcountByUser(new User(accountCash.getUser().getUserId()));
		//保存资金记录
		log.setMoney(money);
		log.setTotal(account.getTotal());
		log.setUseMoney(account.getUseMoney());
		log.setNoUseMoney(account.getNoUseMoney());
		log.setCollection(account.getCollection());
		log.setRepay(account.getRepay());
		accountLogDao.save(log);
		return true;
	}

	@Override
	public void glodCashSuccess(CashOut cash,WebGlodLog log){
		logger.info("金账户取现需要复审   开始111------");
		int sum = webGlodLogDao.sumWebLogByOrdid(cash.getOrdId());
		if(sum>0){
			logger.info("已经处理过的提现："+ cash.getOrdId());
			return;
		}
		String cashIsVerify = Global.getValue("cash_is_verify"); //获取取现是否需要校验
		boolean reflag = true ;
		if (!cashIsVerify.equals("1")) {//需要审核
			logger.info("金账户取现需要复审   开始222------");
			reflag=cashAuditChianpnr(cash.getOrdId(), Global.getString("chinapnr_merid"),cash.getTransAmt(), 1, "S");   //触发汇付任务	
		}
		if(reflag){
			logger.info("金账户取现复审 成功插入记录....");
			log.setStatus(1);
			webGlodLogDao.save(log);
		}
	}
	//v1.8.0.3 TGPROJECT-64    qj     2014-04-16 start
	@Override
	public void webGlodLogBack(WebGlodLog log){
		List list = webGlodLogDao.findByProperty("ordId", log.getOrdId());
		if(list == null || list.size() == 0){
			webGlodLogDao.save(log);
		}else{
			logger.info("已经添加了金账户记录");
		}
	}
	//v1.8.0.3 TGPROJECT-64    qj     2014-04-16 stop

	@Override
	public AccountCash getAccountCash(long id) {
		return accountCashDao.find(id);
	}

	/**
	 * 处理提现的业务
	 * @param cash
	 */
	@Override
	public void dealAccountCash(AccountCash cash,double money, List<Object> taskList){
		User drawUser = userDao.find(cash.getUser().getUserId());
		AccountBank accountBank = accountBankDao.find(cash.getAccountBank().getId());   //获取取现所选择的银行卡
		DrawBank  drawBank = accountBank.getBank();  //获取银行卡的详情信息
		AreaBank province = accountBank.getProvince();//省
		AreaBank city = accountBank.getCity(); //市区
		if(province==null || city==null){
			throw new ManageBussinessException("开户银行地址不详细!","");
		}
		BankNoQuery bnq = bankNoQuery(city.getName(), drawBank.getBankCode());

		if(bnq==null || StringUtils.isBlank( bnq.getBankLasalle()) ){
			throw new BussinessException(" 根据您提供的银行卡所在市区，我们查询不到任何交易银行，" +
					" 请您核对绑定银行卡时选择的省市区 ");
		}
		//统计 成功取现的个数			
		int cashNum = accountDao.getSuccessAccountCash(drawUser.getUserId());


		String userId = drawUser.getApiId() + "," + drawUser.getRealname() + "," + drawBank.getPayChannelApi()
				+ "," + accountBank.getAccount() + ","+ province.getName() +"," + city.getName()
				+","+ bnq.getBankLasalle()+","+drawBank.getBankCode()+","+drawBank.getCardType()
				+","+drawBank.getPublicTag()+","+cash.getDrawType()+"," + cashNum;
		YjfPay yjfPayDraw = new YjfPay(null, null, money+"", YjfType.VERIFYCASH, 
				null, null, YjfType.APPLYWITHDRAW, null,null, null, userId);
		taskList.add(yjfPayDraw);
	}

	/**
	 * 提现审核   通过， 拒绝
	 */
	@Override
	public boolean verifyCash (AccountCash cash, AccountLog log) {
		boolean result = false;
		List<Object> taskList = new ArrayList<Object>();// 任务
		AccountCash ac = accountCashDao.find(cash.getId());
		User drawUser = ac.getUser();
		double money =  ac.getTotal();//提现金额+手续费，易极付，手续费默认为0
		if(cash.getStatus()==1){//审核通过
			money = apiService.verifyCash(cash, taskList);
			if(cash.getStatus() ==2){ // 第三方处理失败
				//解冻
				cash.setVerifyRemark(Global.getValue("api_name")+"处理失败，提现审核失败，返回冻结资金");
				accountDao.updateAccount(0, money, -money, cash.getUser().getUserId());
				log.setRemark(Global.getValue("api_name")+"处理失败，提现审核失败，返回冻结资金"+cash.getTotal()+" 元");
				log.setType(Constant.CASH_FAIL);

				Account act = accountDao.getAcountByUser(drawUser);
				log.setAddtime(new Date());
				log.setMoney(money);
				log.setTotal(act.getTotal());
				log.setUseMoney(act.getUseMoney());
				log.setNoUseMoney(act.getNoUseMoney());
				log.setCollection(act.getCollection());
				log.setRepay(act.getRepay());
				accountLogDao.save(log);

			}else{
				//易极付处理取现结果在回调用处理：
				int apiCode = Global.getInt("api_code");
				if (apiCode ==1) {//汇付处理体现的方法。
					cash.setVerifyRemark(Global.getValue("api_name")+"处理成功，取现审核成功！");
					accountDao.updateAccount(-money, 0, -money, cash.getUser().getUserId());
					log.setRemark(Global.getValue("api_name")+"处理成功，提现审核成功，扣Ruse除金额"+cash.getCredited()+"元和手续费"+cash.getFee()+ "元");
					log.setType(Constant.CASH_SUCCESS);

					Account act = accountDao.getAcountByUser(drawUser);
					log.setAddtime(new Date());
					log.setMoney(money);
					log.setTotal(act.getTotal());
					log.setUseMoney(act.getUseMoney());
					log.setNoUseMoney(act.getNoUseMoney());
					log.setCollection(act.getCollection());
					log.setRepay(act.getRepay());
					accountLogDao.save(log);
				}else{
					logger.info("易极付取现复审操作不做资金处理处理，只能在回调中做业务处理");
					cash.setStatus(4);//等待回调处理
				}
				result= true;//到此易极付和 汇付都成功。
			}
		}else{//审核拒绝
			//解冻
			accountDao.updateAccount(0, money, -money, cash.getUser().getUserId());
			Account act = accountDao.getAcountByUser(drawUser);
			log.setRemark("提现审核失败，返回冻结资金"+money+" 元");
			log.setType(Constant.CASH_FAIL);
			log.setAddtime(new Date());
			log.setMoney(money);
			log.setTotal(act.getTotal());
			log.setUseMoney(act.getUseMoney());
			log.setNoUseMoney(act.getNoUseMoney());
			log.setCollection(act.getCollection());
			log.setRepay(act.getRepay());
			accountLogDao.save(log);
		}
		accountCashDao.update(cash);
		return result;
	}

	@Override
	public List getDrawBankBySearchParam(SearchParam param) {
		return drawBankDao.findByCriteria(param);		
	}

	/**
	 * 双乾支查询银行卡
	 * @param param
	 * @return
	 */
	@Override
	public List getDrawBankMmmBySearchParam(SearchParam param) {
		return drawBankMmmDao.findByCriteria(param);		
	}

	@Override
	public DrawBank getDrawBankById(int id) {
		return drawBankDao.find(id);
	}

	@Override
	public DrawBankMmm getDrawBankMmmById(int id) {
		return drawBankMmmDao.find(id);
	}

	@Override
	public void delAccountCashById(int id) {
		accountCashDao.delete(id);
	}
	@Override
	public void updateAccountCash(AccountCash ac) {
		accountCashDao.merge(ac);
	}
	@Override
	public void cancelCash(Account act,AccountCash cash, AccountLog log) {

		cash.setStatus(2);
		updateAccountCash(cash);

		//double money = cash.getTotal()+cash.getFee();
		double money = cash.getTotal();
		accountDao.updateAccount(0, money, -money, cash.getUser().getUserId());
		log.setMoney(money);
		log.setTotal(act.getTotal());
		log.setUseMoney(act.getUseMoney());
		log.setNoUseMoney(act.getNoUseMoney());
		log.setCollection(act.getCollection());
		log.setRepay(act.getRepay());
		accountLogDao.save(log);		
	}

	/**
	 * 调用yjf绑定银行卡接口
	 */
	@Override
	public boolean bindingBankNo(String cardNo,User user, DrawBank drawBank, long city ){

		if(isOpenApi()){//正式环境下面，要开启
			AreaBank areaBank = areaBankDao.find(city);
			BankNoQuery bnq = bankNoQuery(areaBank.getName(), drawBank.getBankCode());
			if(StringUtils.isBlank( bnq.getBankLasalle()) ){
				throw new BussinessException(" 根据您提供的银行卡所在市区，我们查询不到任何交易银行，" +
						" 请您核对绑定银行卡时选择的省市区 ");
			}
			//校验银行卡信息
			verifyFacade(cardNo, drawBank, user);	
			BankCodeBindingAddInfo bba =  PayModelHelper.bankCodeBindingAddInfo(cardNo, user.getApiId(), user.getRealname(),
					drawBank.getCardType(), drawBank.getBankCode());
			if(!bba.getResultCode().contains("EXECUTE_SUCCESS")){
				throw new BussinessException("绑定银行卡失败：" + bba.getResultCode() + "/" + bba.getMessage() );
			}else{
				return true;
			}
		}
		return true;

	}

	/**
	 * 解除绑定银行卡
	 * @param user
	 * @param accountBankId
	 */
	@Override
	public void accountBankRemove(User user, String accountBankId){
		AccountBank ab = accountBankDao.find(Integer.parseInt(accountBankId));
		//v1.8.0.4_u2 TGPROJECT-311 lx 20140-05-23 start
		//如果是双乾的接口，解绑就不需要调整第三方
		if(Global.getInt("api_code")==2){
			bankCodeBindingRemove(user.getApiId()+"", ab.getAccount());
		}
		//v1.8.0.4_u2 TGPROJECT-311 lx 20140-05-23 end
		if(Global.getInt("api_code")==1){
			chinaCardRemove(user.getApiUsercustId(), ab.getAccount());
		}
		ab.setStatus(0);
		accountBankDao.update(ab);
	}
	/**
	 * 查询yjf账户信息
	 * @param user
	 */
	@Override
	public UserAccountQuery getYjfAccount(User user) {
		return userAccountQuery(user);
	}
	/**
	 * 查询yjf充值信息
	 */
	@Override
	public DepositQuery getYjfRechargeByUser(String currPage, String apiId) {
		return userRechargeQuery(currPage,apiId);
	}
	/**
	 * 查询yjf提现信息
	 */
	@Override
	public WithdrawQquery getYjfCashByUser(String currPage, String apiId) {
		return userCashQuery(currPage,apiId);
	}

	@Override
	public AccountRecharge getAccountRechargeById(int id){
		return accountRechargeDao.find(id);
	}

	@Override
	public void webRecharge(AccountLog log,AccountRecharge ar) throws  Exception {
		List<Object> taskList = new ArrayList<Object>();
		int apiType = Global.getInt("api_code");
		User rechargeUser = ar.getUser();
		if(rechargeUser.getApiStatus()==0 || StringUtils.isBlank(rechargeUser.getApiId())){
			throw new ManageBussinessException("该用户没有开通或激活"+Global.getValue("api_name")+"账户！！", "/admin/account/rechargelist.html");
		}
		apiService.webRecharge(ar,taskList);//第三方接口
		if(borrowService.doApiTask(taskList)){//调用转账功能
			//修改账户金额
			Account act=accountDao.getAcountByUser(rechargeUser);
			accountDao.updateAccount(ar.getMoney(), ar.getMoney(), 0,rechargeUser.getUserId() );
			//修改订单状态
			ar.setStatus(1);
			String tradeNo = "";
			switch (apiType) {
			case 1://汇付
				ChinaPnrPayModel cpm = (ChinaPnrPayModel)taskList.get(0);
				tradeNo = cpm.getOrdId();
				break;
			case 2://易极付
				YjfPay yjf = (YjfPay)taskList.get(0);
				tradeNo = yjf.getOrderno();
				break;

			default:
				tradeNo = OrderNoUtils.getInstance().getSerialNumber();
				break;
			}

			ar.setTradeNo(tradeNo);
			ar.setRemark("网站代理充值成功!");
			accountRechargeDao.save(ar);

			//插入资金记录表
			log.setUser(ar.getUser());
			User toUser = new User();
			log.setToUser(toUser);
			toUser.setUserId(Constant.ADMIN_ID);
			log.setMoney(ar.getMoney());
			log.setTotal(act.getTotal());
			log.setUseMoney(act.getUseMoney());
			log.setNoUseMoney(act.getNoUseMoney());
			log.setCollection(act.getCollection());
			log.setRepay(act.getRepay());
			accountLogDao.save(log);
			//充值成功，发消息通知
			MsgReq req = new MsgReq();
			req.setReceiver(ar.getUser());
			req.setSender(new User(Constant.ADMIN_ID));
			req.setMsgOperate(this.msgService.getMsgOperate(8));
			req.setTime(DateUtils.dateStr4(new Date()));
			req.setMoney(""+ar.getMoney());
			DisruptorUtils.sendMsg(req);
		}else{
			throw new ManageBussinessException(Global.getString("api_name")+"处理出错！！！");
		}
	}

	@Override
	public void glodTransfer(GlodTransfer gt){
		List<Object> taskList = new ArrayList<Object>();
		apiService.glodTransfer(gt,taskList);//第三方接口
		if(borrowService.doApiTask(taskList)){
			glodTransferDao.save(gt);
		}else{
			ChinaPnrPayModel cpm = (ChinaPnrPayModel)taskList.get(0);
			throw new ManageBussinessException("易极付处理出错!"+cpm.getErrorMsg());
		}
	}
	@Override
	public PageDataList getAllGlodTransfer(SearchParam param){
		return glodTransferDao.findPageList(param);
	}

	@Override
	public void webDeduct(AccountLog log,AccountWebDeduct awd){
		User rechargeUser = awd.getUser();
		if(rechargeUser.getApiStatus()==0 || StringUtils.isBlank(rechargeUser.getApiId())){
			throw new ManageBussinessException("该用户没有开通或激活"+Global.getValue("api_name")+"账户！！");
		}
		accountWebDeductDao.save(awd);
	}
	@Override
	public void verifyWebDeduct(AccountLog log,AccountWebDeduct awd){
		List<Object> taskList = new ArrayList<Object>();
		int apiType = Global.getInt("api_code");
		User deductUser = awd.getUser();
		Account act=accountDao.getAcountByUser(deductUser);
		if(deductUser.getApiStatus()==0 || StringUtils.isBlank(deductUser.getApiId())){
			throw new ManageBussinessException("该用户没有开通或激活"+Global.getValue("api_name")+"账户！！");
		}
		if(act.getUseMoney() < awd.getMoney()){
			throw new ManageBussinessException("用户资金不足，扣款失败！");
		}
		if(awd.getStatus() == 0 || awd.getStatus() == 2){
			accountWebDeductDao.update(awd);
			return;
		}
		apiService.webDeduct(awd,taskList);//第三方接口
		if(borrowService.doApiTask(taskList)){//调用转账功能
			//修改账户金额

			accountDao.updateAccount(-awd.getMoney(), -awd.getMoney(), 0,deductUser.getUserId() );
			//修改订单状态
			awd.setStatus(1);
			String tradeNo = "";
			switch (apiType) {
			case 1://汇付
				break;
			case 2://易极付
				YjfPay yjf = (YjfPay)taskList.get(0);
				tradeNo = yjf.getOrderno();
				break;

			default:
				tradeNo = OrderNoUtils.getInstance().getSerialNumber();
				break;
			}
			//插入订单号
			awd.setTradeNo(tradeNo);
			accountWebDeductDao.update(awd);

			//插入资金记录表
			log.setMoney(awd.getMoney());
			log.setTotal(act.getTotal());
			log.setUseMoney(act.getUseMoney());
			log.setNoUseMoney(act.getNoUseMoney());
			log.setCollection(act.getCollection());
			log.setRepay(act.getRepay());
			log.setRemark("网站扣款，金额："+awd.getMoney()+"元。");
			accountLogDao.save(log);
		}else{
			throw new ManageBussinessException("易极付处理出错！！！");
		}
	}

	@Override
	public double getUserNetMoney(User user) {
		double accountMoney = 0;
		double collectionMoney=borrowTenderDao.sumCollectionMoney(user.getUserId());       //获取用户所有的待收本金总和
		accountMoney=user.getAccount().getUseMoney()+collectionMoney-user.getAccount().getRepay();
		return accountMoney;
	}

	@Override
	public double getAllPropertyWaitRepayMoney(long user_id) {
		return accountDao.getAllPropertyBorrowRepayAccount(user_id);
	}

	@Override
	public List<AccountBank> getBankByUserId(long userid, int isbind) {
		return accountBankDao.getBankByUserId(userid, isbind);
	}

	@Override
	public AccountBank getBankByNoAndApiId(String no, String yid) {
		return accountBankDao.getBankByNo(no, yid);
	}

	@Override
	public void updateIsbind(AccountBank bank) {
		accountBankDao.update(bank);
	}

	@Override
	public boolean  deductRecharge(AccountRecharge recharge,AccountLog log) {
		User user = recharge.getUser();
		AccountBank accountBank  = accountBankDao.find(recharge.getAccountBank().getId()); //获取绑定银行卡信息
		DrawBank bank  = accountBank.getBank();   //获取银行卡的详情信息
		//默认的证件类型为身份证,tradeBizProductCode此业务
		DeductDepositApply dedu = deductDepositApply("ID", user.getCardId(),user.getApiId() , recharge.getMoney()+"", "", 
				accountBank.getAccount(), user.getRealname(), bank.getBankCode(),
				accountBank.getProvince().getName(), accountBank.getCity().getName(), recharge.getTradeNo());
		accountRechargeDao.save(recharge);
		boolean result = Boolean.parseBoolean(dedu.getSuccess().toString());
		if (result) {
			String orderNo = dedu.getOrderNo(); //订单号。
			logger.info( "订单号：" + dedu.getOrderNo() + " 金额：" + dedu.getPayAmount() 
					+ " 结果：" + dedu.getSuccess().toString() + " 流水号：" + dedu.getDepositId() );
			log.setRemark("网上充值,"+Global.getValue("api_name")+"充值,订单号:"  +  orderNo);
			try {
				if(orderNo != null) {
					RechargeModel reModel = new RechargeModel();  //对通用javabean进行参数封装
					reModel.setOrderAmount(dedu.getPayAmount());
					reModel.setOrderId(dedu.getOrderNo());
					reModel.setResultMsg(dedu.getResultMessage());
					reModel.setResult(dedu.getSuccess().toString());
					reModel.setSerialNo(dedu.getDepositId());
					DisruptorUtils.doRechargeBackTask(reModel, log,null);
				}else {
					logger.info( "**********"+Global.getValue("api_name")+"充值 回调返回订单为空:" + orderNo );
				}
			}catch(Exception e){

			}
			return true;
		}else{
			return false;
		}


	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public List<AccountBank> getBankLists(long userId){
		return  accountBankDao.getAllBankList(userId);
	}

	/**
	 * 
	 * @param serialNo
	 * @return
	 */
	@Override
	public boolean checkRechargeOffLine(String serialNo){
		return  accountRechargeDao.getCountBySerialNo(serialNo)==0;
	}

	/**
	 * 充值手续费工具方法
	 * @return
	 */
	private double rechargeFee(RechargeModel re){
		int apiType = Global.getInt("api_code");
		switch (apiType) {
		case 1: //汇付可以直接从返回参数汇总取出返回的手续费金额
			return re.getFeeAmt();
		case 2: //易极付通过计算取得
			String feeRateStr = Global.getValue("recharge_fee");
			double feeRate = 0;
			try {
				if(!StringUtils.isBlank(feeRateStr)){
					feeRate = Double.parseDouble(feeRateStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
			double fee = (Double.parseDouble(re.getOrderAmount()))*feeRate;
			return fee;
		default:
			return 0;
		}
	}

	@Override
	public void seveCash(AccountCash cash) {
		accountCashDao.save(cash);

	}

	@Override
	public void saveWebGlodLog(WebGlodLog log){
		webGlodLogDao.save(log);
	}

	@Override
	public int sumWebLogByOrdid(String ordid){
		return webGlodLogDao.sumWebLogByOrdid(ordid);
	}

	@Override
	public PageDataList<WebGlodLog> getWebGlodLogList(SearchParam param) {
		return webGlodLogDao.findPageList(param);
	}

	/**
	 * 根据查询条件，查询出所有的记录
	 * @param param
	 * @return
	 */
	@Override
	public List<WebGlodLog> getAllGlodLogList(SearchParam param) {
		return webGlodLogDao.findByCriteria(param);
	}
	// v1.8.0.3 TGPROJECT-11 lx 2014-04-03 start
	/**
	 * 查询用户投资情况
	 * @param tenderParam
	 * @param userParam
	 * @return
	 */
	@SuppressWarnings({"unchecked", "rawtypes" })
	@Override
	public PageDataList<BaseAccountSumModel> getUserInvestDetail(SearchParam tenderParam, SearchParam userParam){
		List<BaseAccountSumModel> list = accountDao.getBaseAccountSum(tenderParam, userParam);
		return new PageDataList(userParam.getPage(),list);
	}
	// v1.8.0.3 TGPROJECT-11 lx 2014-04-03 end

	@Override
	public void madeDeductSign(DeductSign dedu) {
		AccountBank accountBank = accountBankDao.getBankByNo(dedu.getCardNo(), dedu.getUserId());;
		if (dedu.getIsSuccess() =="true") {
			accountBank.setStatus(1);//签约成功
		}else{
			accountBank.setStatus(2); //签约失败
		}
		logger.info("用户:"+dedu.getUserId()+",银行卡签约结果："+dedu.getMessage());
		accountBankDao.update(accountBank);
	}
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 start
	@Override
	public void madeInterestGenerateCall(FssTrans fssTrans) {
		InterestGenerate ig = fillInterestGenerate(fssTrans);
		if(ig != null){
			double money = ig.getAmount();
			User user = ig.getUser();
			if(ig.getType().equals("in")){
				accountDao.updateAccount(-money, -money, 0,0, 0,user.getUserId());  // 修改金额
				Account act=accountDao.getAcountByUser(user);
				AccountLog log=new AccountLog(user.getUserId(),Constant.INTEREST_GENERATE_IN,user.getUserId());
				fillAccountLog(log, Constant.INTEREST_GENERATE_IN, act, user, user, money, 0, 
						"转入【生利宝】资金:"+money);
				accountLogDao.save(log);
			}else if(ig.getType().equals("out")){
				accountDao.updateAccount(money, money, 0,0, 0,user.getUserId());  // 修改金额
				Account act=accountDao.getAcountByUser(user);
				AccountLog log=new AccountLog(user.getUserId(),Constant.INTEREST_GENERATE_OUT,user.getUserId());
				fillAccountLog(log, Constant.INTEREST_GENERATE_OUT, act, user, user, money, 0, 
						"【生利宝】转出资金:"+money);
				accountLogDao.save(log);
			}
			interestGenerateDao.save(ig);
		}else{
			throw new BussinessException("操作异常！");
		}
	}
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 end
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 start
	private InterestGenerate fillInterestGenerate(FssTrans fssTrans){
		InterestGenerate ig = null;
		String ordId = fssTrans.getOrdId();
		List<InterestGenerate> igList = interestGenerateDao.findByProperty("ordId", ordId);
		if(igList != null&&igList.size()==1){
			ig = igList.get(0);
			if(fssTrans.getUsrCustId().equals(ig.getUser().getApiUsercustId())&&ig.getStatus()==0){
				if(fssTrans.getTransType().equals("I")){//转入生利宝
					ig.setType("in");
				}else if(fssTrans.getTransType().equals("O")){//转出生利宝
					ig.setType("out");
				}else{
					throw new BussinessException("状态异常！");
				}
				ig.setAmount(NumberUtils.getDouble2(fssTrans.getTransAmt()));
				ig.setStatus(1);
				ig.setCalltime(new Date());
			}else if(fssTrans.getUsrCustId().equals(ig.getUser().getApiUsercustId())&&ig.getStatus()==1){
				throw new BussinessException("操作成功！");
			}else{
				throw new BussinessException("操作异常！");
			}
		}else{
			throw new BussinessException("操作异常！");
		}
		return ig;
	}
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 end
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 start
	@Override
	public void addInterestGenerate(InterestGenerate ig) {
		interestGenerateDao.save(ig);
	}
	// v1.8.0.4_u2 TGPROJECT-314 qj 2014-05-30 end
	//v1.8.0.4_u4  TGPROJECT-357 qinjun  2014-07-07  start
	@Override
	public double getHuifuServFee(AccountCash accountCash) {
		double fee = 0;
		User user = accountCash.getUser();
		double cashMoney = accountCash.getTotal();
		double rechargeTotal = accountDao.getChargeTotal(user.getUserId());
		double tenderTotal = accountDao.getInvestSummary(user.getUserId()).getInvestTotal();
		Rule cashRule = ruleService.getRuleByNid("huifu_cash_servfee");
		if(cashRule!= null && cashRule.getStatus() == 1){
			int yddStatus = cashRule.getValueIntByKey("ydd");
			if(yddStatus == 1){
				double ruleCashMoney = cashRule.getValueDoubleByKey("cash_money");
				double ruleCashFee = cashRule.getValueDoubleByKey("cash_fee");
				double ruleCashTenderRate = cashRule.getValueDoubleByKey("cash_tender_rate");
				double ruleCashRate = cashRule.getValueDoubleByKey("cash_rate");
				//v1.8.0.4_u4  TGPROJECT-369  qinjun  2014-07-16  start
				double cashLowestMoney = cashRule.getValueDoubleByKey("cash_lowest_money");
				logger.info("(用户:"+user.getUsername()+",提现金额："+cashMoney+",充值总金额:"+rechargeTotal+",投标总金额:"+tenderTotal
						+"),(rule参数配置：取现金额判断："+ruleCashMoney+",取现金额判断费："+ruleCashFee+"，比较投资金额大用户的费率："+ruleCashTenderRate+",小的投资金额费率："+ruleCashRate
						+"小于"+cashLowestMoney+"提现费率"+ruleCashFee);
				if(cashMoney<cashLowestMoney){//小于500，收费标准=提现金额*0.25%
					fee = ruleCashRate * ruleCashRate;
					logger.info("小于"+cashLowestMoney+"提现费:"+fee);
				}else{
					//v1.8.0.4_u4  TGPROJECT-369  qinjun  2014-07-16  start
					//一、累计投资金额≥充值金额 1）提现金额≤3万元，费用=3元； 2）提现金额>3万元，费用=提现金额*0.01%
					if(tenderTotal>=rechargeTotal){
						if(cashMoney<=ruleCashMoney){
							fee = ruleCashFee;
							logger.info("累计投资金额≥充值金额,提现金额≤3万元，费用=3元,提现费："+fee);
						}else{
							fee = cashMoney * ruleCashTenderRate;
							logger.info("累计投资金额≥充值金额,提现金额>3万元，费用=提现金额*0.01%,提现费："+fee);
						}
					}else{
						//累计投资金额<充值金额1）提现金额≤累计投资金额a. 提现金额≤3万元，费用=3元；b. 提现金额>3万元，费用=提现金额*0.01%；
						//2）提现金额>累计投资金额 a.提现金额≤3万元，费用=3元+（提现金额-累计投资金额）*0.25%； b.提现金额>3万元，费用=提现金额*0.01%+（提现金额-累计投资金额）*0.25%
						if(cashMoney<=tenderTotal){
							if(cashMoney<=ruleCashMoney){
								fee = ruleCashFee;
								logger.info("累计投资金额<充值金额,提现金额≤累计投资金额a. 提现金额≤3万元，费用=3元,提现费："+fee);
							}else{
								fee = cashMoney * ruleCashTenderRate;
								logger.info("累计投资金额<充值金额,提现金额≤累计投资金额b. 提现金额>3万元，费用=提现金额*0.01%；提现费："+fee);
							}
						}else{
							if(cashMoney<=ruleCashMoney){
								fee = ruleCashFee + (cashMoney-tenderTotal) * ruleCashRate;
								logger.info("累计投资金额<充值金额,提现金额>累计投资金额 a.提现金额≤3万元，费用=3元+（提现金额-累计投资金额）*0.25%,提现费："+fee);
							}else{
								fee = cashMoney * ruleCashTenderRate + (cashMoney-tenderTotal)*ruleCashRate;
								logger.info("累计投资金额<充值金额,提现金额>累计投资金额 b.提现金额>3万元，费用=提现金额*0.01%+（提现金额-累计投资金额）*0.25%,提现费："+fee);
							}
						}
					}
				}
			}
		}else{
			fee = cashMoney*Global.getDouble("serv_fee_rate");
		}
		return fee;
	}
	//v1.8.0.4_u4  TGPROJECT-357 qinjun  2014-07-07  end


	//TGPROJECT-362   无卡签约   start
	@Override
	public void doSignmanyBank(SignmanyBank signBank) {
		String cardNo = signBank.getCardNo();   //获取银行卡
		String userId = signBank.getUserId();
		AccountBank accountBank = accountBankDao.getBankByNo(cardNo, userId);
		if (signBank.getIsSuccess().equals("true")) { //判断是否签约成功
			accountBank.setStatus(1);//签约成功
		}else{
			accountBank.setStatus(2); //签约失败
			logger.info("签约失败");
		}
		logger.info("用户:"+signBank.getUserId()+",银行卡签约结果："+signBank.getMessage());
		accountBankDao.update(accountBank);
	}

	//TGPROJECT-362   无卡签约   end

}

package cn.zchfax.service;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.zchfax.domain.Article;
import cn.zchfax.domain.ScrollPic;
import cn.zchfax.domain.Site;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;
import cn.zchfax.model.tree.SiteTree;

@Service
public interface ArticleService {
	public Site getSiteByCode(String code);
	public Site getSiteById(int id);
	public List getSubSiteList(int pid);
	public SiteTree getSiteTree();
	public void modifySite(Site site);
	public void addSite(Site site);
	public void delSite(int siteId);
	public PageDataList getArticleList(int page);
	public Article getArticle(int id);
	public void addArticle(Article a,String files);
	public void delArticle(int id);
	public void modifyArticle(Article a,String url);
	public List getArticleFileds(int id);
	public Article getArticleByEname(String ename);
	public PageDataList getArticleList(Site site,int page,int row);
	public boolean checkSiteCode(String code);
	public List<Site> getSubSitePageDataList(SearchParam param);
	public PageDataList getArticleList(SearchParam param);
	public PageDataList<ScrollPic>  getScrollPicList(SearchParam param);
	public void delScrollPic(long id);
	public void modifyScrollPic(ScrollPic sp);
	public ScrollPic getScrollPicListById(long id);
	public void addScrollPic(ScrollPic sp);
}

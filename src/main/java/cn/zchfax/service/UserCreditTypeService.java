package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.UserCreditType;

public interface UserCreditTypeService {

	public List<UserCreditType> findAll();
	public UserCreditType find(int id);
	public void update(UserCreditType uct);
}

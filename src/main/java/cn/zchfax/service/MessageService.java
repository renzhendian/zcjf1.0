package cn.zchfax.service;

import cn.zchfax.domain.Message;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface MessageService {
	
	public Message addMessage(Message msg);
	public PageDataList getMessageBySearchParam(SearchParam  param);

	/**
	 * 未读站内信条数
	 */
	public int getUnreadMessageCount(User user);
	public Message getMessageById(int id);
	public void modifyMessge(Message message);
	//public void deleteReceiveMessage(int[] ids);
	public void deleteMessage(Integer[] ids);
	public void setReadMessage(Integer[] ids);
	public void setUnreadMessage(Integer[] ids);
}




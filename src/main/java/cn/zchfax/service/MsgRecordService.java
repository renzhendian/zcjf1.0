package cn.zchfax.service;

import cn.zchfax.model.MsgReq;

/*****************
* @ClassName: MsgRecordService
* @Description: 消息发送service
* 
* @date 2013-7-11 下午2:25:23
*
*****************/ 
public interface MsgRecordService {
	
	public String send(MsgReq req);
	
}

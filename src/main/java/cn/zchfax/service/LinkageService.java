package cn.zchfax.service;

import org.springframework.stereotype.Service;

import cn.zchfax.domain.Linkage;
@Service
public interface LinkageService {
	
	public Linkage getLinkageById(int id);

}

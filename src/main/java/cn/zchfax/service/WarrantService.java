package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.Warrant;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface WarrantService {
	/**
	 * 查询担保机构
	 * @param id
	 * @return
	 */
	public Warrant findWarrant(long id);
	
	/**
	 * 修改担保机构
	 * @param warrant
	 */
	public void updateWarrant(Warrant warrant);
	
	public PageDataList<Warrant>  findListWarrant(SearchParam param);
	
	public void addWarrant(Warrant warrant);
	
	public List<Warrant> WarrantList();
}

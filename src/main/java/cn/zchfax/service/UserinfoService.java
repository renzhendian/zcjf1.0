package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.Area;
import cn.zchfax.domain.AreaBank;
import cn.zchfax.domain.AreaMmm;
import cn.zchfax.domain.Credit;
import cn.zchfax.domain.CreditRank;
import cn.zchfax.domain.StarRank;
import cn.zchfax.domain.User;
import cn.zchfax.domain.Userinfo;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface UserinfoService {

	public Userinfo getUserinfoByUserId(long user_id);
	public void updateUserinfo(Userinfo userinfo,long user_id);
	public void updateBuilding(Userinfo userinfo,long user_id);
	public void updateCompany(Userinfo userinfo,long user_id);
	public void updateFirm(Userinfo userinfo,long user_id);
	public void updateFinance(Userinfo userinfo,long user_id);	
	public void updateContact(Userinfo userinfo,long user_id);
	public void updateMate(Userinfo userinfo,long user_id);
	public void updateEducation(Userinfo userinfo,long user_id);
	public void updateAll(Userinfo userinfo);
	public List<Area> getAreaListByPid(int pid);
	public Area getAreaById(int id);
	public PageDataList<Userinfo> getUserinfoList(SearchParam p);
	public void updateUserCredit(Credit credit); //跟新用户积分信息
	public Credit findUserCredit(User user);   //获取用户积分信息
	
	public CreditRank getCreditRankByName(String name);   //根据信用等级名称获取信用等级对象
	public StarRank getStarRankByRank(int rank);
	List<AreaBank> getAreaBankListByPid(long pid);
	List<AreaMmm> getMmmBankListByPid(long pid);
	
	
}
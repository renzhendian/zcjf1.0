package cn.zchfax.service;

import cn.zchfax.domain.SendSmsLog;

public interface SendSmsLogService {

	/**
	 * 添加短信发送记录
	 * @param sendSmsLog
	 */
	public SendSmsLog addSendSms(SendSmsLog sendSmsLog);
	
	/**
	 * 更新发送短信条数记录
	 */
	public void updateSendSms(SendSmsLog sendSmsLog);
	
	/**
	 * 获取短信发送记录
	 * @param phone
	 * @return
	 */
	public SendSmsLog getSendSmsByPhone(String phone);

}

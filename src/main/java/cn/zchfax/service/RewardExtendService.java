package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.BorrowTender;
import cn.zchfax.domain.RewardExtend;
import cn.zchfax.domain.User;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

/**
 * //1.8.0.4_u4 TGPROJECT-345  wujing dytz 
 * 奖励业务service
 * @author wujing
 *
 */
public interface RewardExtendService {
	
	/**
	 * 添加奖励记录
	 * @param rewardExtend
	 */
	public void addReward(RewardExtend rewardExtend);
	
	/**
	 * 查询奖励
	 * @param id
	 * @return
	 */
	public RewardExtend getRewardByid(long id);
	
	

	
	//v1.8.0.4_u4  TGPROJECT-356  qinjun 2014-07-04  start
	public void doRewardAsReadyMoney();
	//v1.8.0.4_u4  TGPROJECT-356  qinjun 2014-07-04  start
	/**
	 * 处理注册并且实名认证通过之后，投资金额达到1000以后直接发放奖励：
	 * 备注：投资人和推荐人都发放50块钱奖励
	 */
	public void doRegisterAndIdentReward();
	
	/**
	 * 获取带分页的记录
	 * @param param
	 * @return
	 */
	public PageDataList<RewardExtend> getPageLIstReward(SearchParam param);
	
	/**
	 * 注册实名认证后送红包
	 * @param user
	 */
	public void doRegisterIdentRedPacket(User user);
	
	/**
	 * 处理红包变现金功能
	 * @param rewardList
	 */
	public void doRedExtend(String[] ids);
	
	/**
	 * 根据红包id，计算所选择的红包总额
	 * @param ids
	 * @return
	 */
	public double getSumRewardById(String[] ids);
	
	/**
	 * 用户充值送红包
	 * @param user
	 * @param money 充值金额
	 */
	public void rechargeExtendRedPacket(User user,double money);
	/**
	 * 用户投标送红包
	 * @param user
	 */
	public void tenderExtendRedPacket(User user,double money, List taskList) ;
	

}

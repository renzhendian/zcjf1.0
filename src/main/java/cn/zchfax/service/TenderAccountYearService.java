package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.TenderAccountYear;
import cn.zchfax.model.SearchParam;

/**
 * TGPROJECT-389 wsl 2014-09-12
 * @author wsl
 *
 */
public interface TenderAccountYearService {
	
	/**
	 * 获取所有用户年化投资总额信息
	 * @param param
	 * @return
	 */
	public List<TenderAccountYear> findTenderAccountYearList(SearchParam param);
	
	/**
	 * 根据id查询用户年化投资总额信息
	 * @return
	 */
	public TenderAccountYear findTenderAccountYearById(long id);

	public void updateTenderAccountYear(double accountYear, double i,
			double accountYear2, long userId);

}

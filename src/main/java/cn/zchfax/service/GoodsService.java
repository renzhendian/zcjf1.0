package cn.zchfax.service;

import java.util.List;

import cn.zchfax.domain.Goods;
import cn.zchfax.model.PageDataList;
import cn.zchfax.model.SearchParam;

public interface GoodsService {
	public List<Goods> findAll();
	public Goods find(int id);
	public void update(Goods goods);
	public PageDataList<Goods> findPageList(SearchParam param);
}

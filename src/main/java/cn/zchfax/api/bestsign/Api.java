package cn.zchfax.api.bestsign;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
public class Api{ 
	
    private static String developerId = "1824993985878295074";
    private static String pem = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALXJwDItuiOt8GlJC1mmGobqQ/4nXwSQVbMXUyeGMabwV3l9ANUfJ4zXZz5OTJkjhaT7jpcI+2DHbNsTJogl1+fOrjZaFCvo8hIUWQ5Bmis9Eqm0LQnnM/PL8ZfUa0IMz2Mwan/ncNq9Cgl5qFlh82pvgZOXGYSdn1nBvdsBN2SrAgMBAAECgYAnVH37nT8PZxuDe+UzwScuF1MckqIRF7Ui6SH5hCQL7SXPyAa9FV92LkhNB5206CWK+03vSWcxmhPoFe2ZOS7Lv3JyIcJDduNQ8DFEhRa9afsm7i73VQMXb5MlQMo7EeiCXfeCUKPgIcZqNV9wSQrKDaGaNs2G51OQJebwEnRUAQJBAO4zLChUDG+nVokSZaaUZkBrIDj2QYRvAY3lME37pttVeecwOru6JELJCoYwAtgnC9qi/4ZelHdpfF//TnczMqsCQQDDX2fJfpkI1QSaWR2qub/YqS5/K305SiWh74BsiVrhGoEecJebShStFibpCm0ZInQPKUrPNrpCFCXT3hv+/JYBAkEA7TJd/Ya8n47GOFbujHPoYquKec6XLXaQ2Kjqci1vWaiDbLl1My1III0sArXajz5Kqpx30WlrdIHBjaKhk0eTYwJAK1VV59M0jkjxJGrvsZZRfvTHY116AOL5RdmhWnF8vsDqMp42gjMEMv/e0YGGgTSrGVHHTf6NgWqGmV+CRiQwAQJBAOxpPSUu4V54zb54D9keiI/N1Gcdknq0Y0PQZwStJsgX5I3PXk9XFCGrdUSyGKYGSVExsiwEDXcnkBGSH3L9doo=";
    private static String host = "https://openapi.bestsign.info/openapi/v3";
	
	/**
	 * 用户注册
	 * @param account
	 * @param mobile
	 * @param name
	 * @param userType
	 * @param mail
	 * @return
	 * @throws Exception
	 */
    public JSONObject userReg(final String account, final String mobile, final String name, final String userType, final String mail) throws Exception {
        final String path = "/user/reg";
        Map<String, Object> data = new HashMap<String, Object>();
		data.put("account", account);
		data.put("mobile", mobile);
		data.put("name", name);
		data.put("userType", userType);
		data.put("mail", mail);
        return doPost(path, data);
    }
    /**
     * 设置个人用户证件信息
     * @param account 用户帐号	 Y	 	为哪个用户设置证件信息就填该用户的帐号
     * @param identity 证件号	 Y	30
     * @param name 姓名	 Y	50	需要和证件上登记的一致
     * @return
     * @throws Exception
     */
    public JSONObject userSetPersonalCredential(final String account, final String identity, final String name) throws Exception {
    	final String path = "/user/setPersonalCredential";
    	Map<String, Object> data = new HashMap<String, Object>();
		data.put("account", account);
		data.put("identity", identity);
		data.put("name", name);
    	return doPost(path, data);
    }
    /**
     * 申请数字证书
     * @param account 用户帐号	 Y	 	为哪个用户申请证书就填该用户的帐号
     * @return
     * @throws Exception
     */
    public JSONObject userApplyCert(final String account) throws Exception {
    	final String path = "/user/applyCert";
    	Map<String, Object> data = new HashMap<String, Object>();
    	data.put("account", account);
    	return doPost(path, data);
    }
    /**
     * 创建用户签名/印章图片
     * @param account 用户帐号	 Y	创建哪个用户的签名/印章图片就填该用户的帐号
     * @return
     * @throws Exception
     */
    public JSONObject signatureImageUserCreate(final String account) throws Exception {
    	final String path = "/signatureImage/user/create";
    	Map<String, Object> data = new HashMap<String, Object>();
    	data.put("account", account);
    	return doPost(path, data);
    }
    /**
     * 上传合同文件
     * @param account 用户帐号	Y	 	必须要指定一个用户帐号作为操作者
     * @param fdata 文件数据，base64编码	Y	例如：
													FileInputStream file = new FileInputStream("d: \\test\\接口系统.pdf");
													byte[] bdata = IOUtils.toByteArray(file); 
													String fdata =Base64.encodeBase64String(bdata);
     * @param fmd5  文件md5值	Y	 	例如：
											FileInputStream file = new FileInputStream("d: \\test\\接口系统.pdf");
											byte[] bdata = IOUtils.toByteArray(file); 
											String fmd5 = DigestUtils.md5Hex(bdata);
     * @param ftype 文件类型	Y	10	如PDF, docx, png等
     * @param fname 文件名	Y	50	 
     * @param fpages 文件页数	Y	3
     * @return
     * @throws Exception
     */
    public JSONObject storageUpload(final String account,String fdata,String fmd5,String ftype,String fname,String fpages) throws Exception {
    	final String path = "/storage/upload";
    	Map<String, Object> data = new HashMap<String, Object>();
    	data.put("account", account);
    	data.put("fdata", fdata);
    	data.put("fmd5", fmd5);
    	data.put("ftype", ftype);
    	data.put("fname", fname);
    	data.put("fpages", fpages);
    	System.out.println("data:"+data);
    	return doPost(path, data);
    }
    /**
     * 创建合同
     * @param account 用户帐号	Y	 	必须要指定一个用户帐号作为合同的创建者（建议统一为开发者的account，以便后期查询）
     * @param fid 文件编号	Y
     * @param expireTime 合同过期时间	Y		合同必须在指定的过期时间之前完成签署，一旦过期将无法签署。如“1490672595”,秒级的unix时间戳；默认过期天数是7天，即当前时间戳加上604800
     * @param title 合同标题	Y	50	 
     * @return
     * @throws Exception
     */
    public JSONObject contractCreate(String account,String fid,String expireTime,String title) throws Exception {
    	final String path = "/contract/create";
    	Map<String, Object> data = new HashMap<String, Object>();
    	data.put("account", account);
    	data.put("fid", fid);
    	data.put("expireTime", expireTime);
    	data.put("title", title);
    	System.out.println("data:"+data);
    	return doPost(path, data);
    }
    /**
     * 添加合同签署者
     * @param contractId 合同编号	Y	创建合同
     * @param signer 要添加的签署者account	Y
     * @return
     * @throws Exception
     */
    public JSONObject contractAddSigner(String contractId,String signer) throws Exception {
    	final String path = "/contract/addSigner";
    	Map<String, Object> data = new HashMap<String, Object>();
    	data.put("contractId", contractId);
    	data.put("signer", signer);
    	System.out.println("data:"+data);
    	return doPost(path, data);
    }
    
    public static JSONObject parseExecutorResult(String executorResult) {
        if (StringUtils.isBlank(executorResult)) {
            return null;
        }
        return JSON.parseObject(executorResult);
    }
	private String getSignData(final String... args) {
        StringBuilder builder = new StringBuilder();
        int len = args.length;
        for (int i = 0; i < args.length; i++) {
            builder.append(Utils.convertToUtf8(args[i]));
            if (i < len - 1) {
                builder.append("\n");
            }
        }

        return builder.toString();
    }
	private String getPostUrlByRsa(Map<String, Object> data, String path) throws Exception {

        String randomStr = Utils.rand(1000, 9999) + "";
        String unix = Long.toString(System.currentTimeMillis());
        String rtick = unix + randomStr;

        String jsonData = JSON.toJSONString(data);
        String dataMd5 = EncodeUtils.md5(jsonData.getBytes("UTF-8"));
        String sign = String.format("developerId=%srtick=%ssignType=rsa/openapi/v3%s/%s", developerId, rtick, path, dataMd5); // 生成签名字符串
        System.out.println(sign);

        String signDataString = this.getSignData(sign);
        String signData = Base64.encodeBase64String(EncodeUtils.rsaSign(signDataString.getBytes("UTF-8"), pem));

        signData = URLEncoder.encode(signData, "UTF-8");
        path = path + "/?developerId=" + developerId + "&rtick=" + rtick + "&sign=" + signData + "&signType=rsa";
        System.out.println(path);
        return path;
    }
	private JSONObject doPost(final String path, Map<String, Object> data) throws Exception, IOException {
		String url = host + getPostUrlByRsa(data, path); // rsa的话 pem为私钥
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String dataString  = JSONObject.toJSONString(data);
        Map<String, Object> res  = HttpSender.getResponseString("POST", url, dataString, headers);
        String resString = (String) res.get("responseData");
        return parseExecutorResult(resString);
	}

}

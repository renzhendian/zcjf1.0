package cn.zchfax.api.chinapnr;

/**
 * 汇付债权装让接口
 * @author wujing
 *
 */
public class AutoCreditAssign extends ChinapnrModel {
	
	private String sellCustId;    //转让人
	
	private String creditAmt;     //转让金额
	
	private String bidDetails;     //转让明细
	
	private String buyCustId;     // 承接人
	
	private String creditDealAmt;    // 承接金额

	
	public String getSellCustId() {
		return sellCustId;
	}

	public void setSellCustId(String sellCustId) {
		this.sellCustId = sellCustId;
	}

	public String getCreditAmt() {
		return creditAmt;
	}

	public void setCreditAmt(String creditAmt) {
		this.creditAmt = creditAmt;
	}

	public String getBidDetails() {
		return bidDetails;
	}

	public void setBidDetails(String bidDetails) {
		this.bidDetails = bidDetails;
	}

	public String getBuyCustId() {
		return buyCustId;
	}

	public void setBuyCustId(String buyCustId) {
		this.buyCustId = buyCustId;
	}

	public String getCreditDealAmt() {
		return creditDealAmt;
	}

	public void setCreditDealAmt(String creditDealAmt) {
		this.creditDealAmt = creditDealAmt;
	}

	
	
	
	
}

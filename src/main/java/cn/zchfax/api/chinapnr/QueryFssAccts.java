package cn.zchfax.api.chinapnr;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 汇付升利宝查询接口
 * @author wujing
 *
 */
public class QueryFssAccts extends ChinapnrModel {
	
	private String reqExt;  // 扩展参数
	

	private String[] paramNames=new String[] {
			"version","cmdId","merCustId","usrCustId","reqExt","chkValue"};
	
	public QueryFssAccts(String userCustId){
		super();
		this.setCmdId("QueryFssAccts");
		this.setReqExt("");
		this.setUsrCustId(userCustId);
	}
	
	public QueryFssAccts(){
		
	}
	
	public StringBuffer getMerData() throws UnsupportedEncodingException{
		StringBuffer MerData = super.getMerData();
		MerData .append(getUsrCustId())
					 .append(getReqExt());
		return MerData;
	}
	
	@Override
	public ChinapnrModel response(String res) throws IOException {
		super.response(res);
		QueryFssAccts fs = new QueryFssAccts();
		 try {
				JSONObject json= JSON.parseObject(res);
				this.setTotalAsset(json.getString("TotalAsset"));
				this.setTotalProfit(json.getString("TotalProfit"));
		 }catch  (Exception e){
			 e.printStackTrace();
		 }
		 return null;
	}

	public String getReqExt() {
		return reqExt;
	}

	public void setReqExt(String reqExt) {
		this.reqExt = reqExt;
	}

	public String[] getParamNames() {
		return paramNames;
	}

	public void setParamNames(String[] paramNames) {
		this.paramNames = paramNames;
	}
	
	

}

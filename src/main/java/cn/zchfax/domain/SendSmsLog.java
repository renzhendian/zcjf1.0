package cn.zchfax.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="send_sms_log")
public class SendSmsLog implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1739877912218936494L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	private String phone;
	
	@Lob
	private String content;//短信内容

	private int status;//注册状态，1为已注册，2为发送成功，-1为发送失败
	
	private int count;//同一个手机发送次数
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date addtime;

	private String addip;

	public SendSmsLog() {
	}
	
	public SendSmsLog(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public String getAddip() {
		return addip;
	}

	public void setAddip(String addip) {
		this.addip = addip;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}

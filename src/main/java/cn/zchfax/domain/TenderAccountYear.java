package cn.zchfax.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * TGPROJECT-389 wsl 2014-09-12
 * 用户年化投资总额表
 * @author wsl
 *
 */
@Entity
@Table(name="tender_account_year")
public class TenderAccountYear implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6913215287330703534L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", updatable=false)
	private User user;
	
	@Column(name="total_year")
	private double totalYear;
	
	@Column(name="month_money_year")
	private double monthMoneyYear;
	
	@Column(name="day_money_year")
	private double dayMoneyYear;

	public TenderAccountYear() {
	}
	
	public TenderAccountYear(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getTotalYear() {
		return totalYear;
	}

	public void setTotalYear(double totalYear) {
		this.totalYear = this.monthMoneyYear + this.dayMoneyYear;
	}

	public double getMonthMoneyYear() {
		return monthMoneyYear;
	}

	public void setMonthMoneyYear(double monthMoneyYear) {
		this.monthMoneyYear = monthMoneyYear;
	}

	public double getDayMoneyYear() {
		return dayMoneyYear;
	}

	public void setDayMoneyYear(double dayMoneyYear) {
		this.dayMoneyYear = dayMoneyYear;
	}
	
}

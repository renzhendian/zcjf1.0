package cn.zchfax.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

@Entity
@Table(name = "borrow_interest_rate")
public class BorrowInterestRate implements Serializable {

	private static final long serialVersionUID = 135046113335181088L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tender_id")
	private BorrowTender tender;
	
	@Column(name="interest_num")
	private String interestNum;
	
	private int status;
	
	private int type;
	
	private double value;
	
	private double money;
	
	@Column(name="add_time")
	private Date addTime;
	
	@Column(name="add_ip")
	private String addIp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BorrowTender getTender() {
		return tender;
	}

	public void setTender(BorrowTender tender) {
		this.tender = tender;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getAddIp() {
		return addIp;
	}

	public void setAddIp(String addIp) {
		this.addIp = addIp;
	}

	public String getInterestNum() {
		return interestNum;
	}

	public void setInterestNum(String interestNum) {
		this.interestNum = interestNum;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getRateStauts(){
		switch(this.status){
		case 1:
			return "未使用";
		case 2:
			return "已过期";
		case 3:
			return "已使用";
		case 4:
			return "已分配";
		default:
			return "";
		}	
	}
}

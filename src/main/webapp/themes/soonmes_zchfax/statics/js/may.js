/**
 * Created by Administrator on 2017/9/7.
 */
$(function(){
    $(".header").mouseenter(function(){
        $(this).addClass("hover");
    }).mouseleave(function(){
        $(this).removeClass("hover");
    });
    $("#show").mouseenter(function(){
        $("#show-wx").addClass("show-fadein");
    }).mouseleave(function(){
        $("#show-wx").removeClass("show-fadein");
    });

    $(".login_switch a").each(function(j){
        $(this).on("mouseenter",function(){
            $(this).addClass("cur").siblings().removeClass("cur");
            $(".tabs .tabs-list").eq(j).fadeIn(500).siblings().hide()
        })
    });
    var tt=true;
    $("#chose").on("click",function(){
        if(tt){
            $(this).children("i").removeClass("focus");
            tt=false;
        }
        else {
            $(this).children("i").addClass("focus");
            tt=!tt;
        }
    });
    // 个人中心
    $(".user-main").mouseenter(function(){
        $(this).addClass("user-hover");
    }).mouseleave(function(){
        $(this).removeClass("user-hover");
    });
    $(".menu li").mouseenter(function(){
        $(this).addClass("cur");
        $(this).children("a").children("i").css("margin","6px 10px 6px 0px");
        $(this).children(".nav").stop().fadeIn(200);
        $(this).siblings().children(".nav").stop().fadeOut(200);
    }).mouseleave(function(){
        $(this).removeClass("cur");
        $(this).children(".nav").stop().fadeOut(200);
        $(this).children("a").children("i").css("margin","6px 0px");
    });
    $(".nav").mouseenter(function(){
        $(this).parents("li").css("background-color","#3e3e3e");
    }).mouseleave(function(){
        $(this).parents("li").css("background-color","");
    });
    // 导航特效
    $(window).scroll(function () {
        var scrolls = $(this).scrollTop();
        if (scrolls > 0) {
            $(".header_vip .header-bottom").addClass("head-fixed");
            $(".user-main").addClass("index-user-main");
        }
        else {
            $(".header-bottom").removeClass("head-fixed");
            $(".user-main").removeClass("index-user-main");
        }
        if(scrolls<220){
            $(".aside").css({'position':'static'});
        }else if(scrolls>1450){
            $(".aside").css({top : ($('.section').height()-$(".aside").height()+30)});
            $(".aside").css({'position':'absolute'});
        }else{
            $(".aside").css({'position':'absolute'});
            $(".aside").css({top : (scrolls-210)});
        }
    });
    // 产品详情
    $(".newlv").hover(function () {
        $(this).parent().parent().parent().children(".hintbox0").fadeIn(100);
    }, function () {
        $(this).parent().parent().parent().children(".hintbox0").fadeOut(100);
    });
    //活期余额选择
    $(".choose span").click(function () {
        $(this).addClass("cur").siblings().removeClass('cur');
        $(this).children("input").attr("checked", "checked");
    });
    // 选项卡
    tapsswitch($(".tap_t li"))
    function tapsswitch(a){
        a.click(function(){
            $(this).addClass("cur").siblings().removeClass("cur");
            var n=$(this).index();
            $(this).parent().siblings(".tap_i").children("li").eq(n).addClass("cur").siblings().removeClass("cur");
        })
    }

    $(".sidebar .switch").click(function(){if($(this).hasClass("off")){$(this).removeClass("off");$(".sidebar").removeClass("close");}else{$(this).addClass("off");$(".sidebar").addClass("close");}}),GoTop();function GoTop(){$(".down").hide();$(".down").click(function(){$("body, html").animate({scrollTop:0},500)});$(window).scroll(function(){$(document).scrollTop()>=100?$(".down").show(200):$(".down").hide(200);$(document).scrollTop()>=100?$(".switch").css("top","290px"):$(".switch").css("top","240px");})}
});
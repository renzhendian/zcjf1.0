$(function() {
    /*账户中心菜单折叠、展开*/
    $('.account-menu h1').click(function() {

        if($(this).next('ul').is(':hidden')) {
            $('.account-menu h1').removeClass('unfolder');
            $('.account-menu ul:visible').stop(true,false).toggle('blind', 240, 'easeOutExpo');
            $(this).addClass('unfolder').next('ul').stop(true,false).toggle('blind', 240, 'easeOutExpo');
        }
        
    });


    

    /*未读信息通知的数字大于99时，全部设为99以保证数字的宽度不超过外围容器*/
    !parseInt($('.index-top .unread span').text()) ? $('.index-top .unread').remove() : '';
    parseInt($('.index-top .unread span').text()) > 99 ? $('.index-top .unread span').text(99) : '';


   

    

    /* 账户中心通用：地区选择-三级联动下拉列表*/
    $('#province').change(function(){
        $.ajax({
            url     : '/member/identify/showarea.html',
            dataType: 'json',
            data    : 'pid=' + $('#province').val(),
            success : function(json){
                $('#city option, #area option').remove();
                $('#city, #area').append('<option value="-1">请选择</option>');
                $(json).each(function(i){
                    $('#city').append('<option value="' + json[i].id + '">' + json[i].name + '</option>');
                });
            }
        });
    });

    $('#city').change(function(){
        $.ajax({
            url     : '/member/identify/showarea.html',
            dataType: 'json',
            data    : 'pid=' + $('#city').val(),
            success : function(json){
                $('#area option').remove();
                $('#area').append('<option value="-1">请选择</option>');
                $(json).each(function(i){
                    $('#area').append('<option value="' + json[i].id + '">'+ json[i].name + '</option>');
                });
            }
        });
    });
});
